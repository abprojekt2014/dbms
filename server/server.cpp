#include "server.h"

using boost::asio::ip::tcp;
using std::string;

Server::Server(boost::asio::io_service& io_service) : _socket(io_service){
    tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 1666));
    acceptor.accept(_socket);
}

Server::~Server(){}

std::string& Server::get_data(){
    return adat;
}

bool Server::read(){
    try{
        size_t header;
        boost::system::error_code error;

        boost::asio::read(_socket, boost::asio::buffer(&header, sizeof(header)), error);
        if (error)
            throw boost::system::system_error(error);
        //std::cout << "Reading " << header << " bytes" << std::endl;

        size_t rc = boost::asio::read(_socket, boost::asio::buffer(_data), boost::asio::transfer_exactly(header), error);
        if (error)
            throw boost::system::system_error(error);

        adat = deserialize<std::string>(_data);

        return true;
    }
    catch(std::exception& e){
        _error = e.what();
    }
    return false;
}

bool Server::set_write_return_value(bool _rv){
    write_return_value = _rv;

    return true;
}

bool Server::write(AdatSor& msg){
    try{
        std::string serialized = serialize(msg);

        const size_t header = serialized.size();
        //std::cout << "Sending " << header << " bytes" << std::endl;

        size_t rc = boost::asio::write(
                _socket,
                boost::asio::buffer(&header, sizeof(header))
                );
        rc = boost::asio::write(
                _socket,
                boost::asio::buffer(serialized)
                );
        //std::cout << "Sent " << rc << " bytes" << std::endl;

        return true;
    }
    catch(std::exception& e){
        _error = e.what();
    }
    return false;
}

std::string Server::error()
{
    return _error;
}
