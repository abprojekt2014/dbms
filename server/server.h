#ifndef SERVER_H
#define SERVER_H

#if defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#include <windows.h>
#endif // _WIN32

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "../parser/adatsor.h"

class Server{
private:
    enum { _max_length = 1024 };
    char _data[_max_length];

    std::string adat;
    std::string _error;
    boost::asio::ip::tcp::socket _socket;

public:
    bool write_return_value;

    Server(boost::asio::io_service& io_service);
    ~Server();

    bool read();
    bool set_write_return_value(bool _rv);
    bool write(AdatSor& adat);
    std::string error();

    std::string& get_data();
};

#endif
