#include "tools_widget.h"
#include <QFileDialog>
#include "vector"
using namespace std;

tools_widget::tools_widget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	hide_buttons();
	refresh_list();
}

tools_widget::~tools_widget()
{

}

void tools_widget::show_buttons()
{
	ui.pushButton_close_query->setVisible(true);
	ui.pushButton_execute_query->setVisible(true);
	ui.pushButton_save_query->setVisible(true);
}

void tools_widget::hide_buttons()
{
	ui.pushButton_close_query->hide();
	ui.pushButton_execute_query->hide();
	ui.pushButton_save_query->hide();
}

QString tools_widget::getSaveFileName()
{
	QString file_name = QFileDialog::getSaveFileName(this, "Save data...", "Query", "Query (*.qr)");
	return file_name;
}

QString tools_widget::getOpenFileName()
{
	QString file_name = QFileDialog::getOpenFileName(this, "Open file...", "Query", "Query (*.qr)");
	return file_name;
}

void tools_widget::refresh_list()
{
	// Set the number of columns in the tree
	ui.widget_database_list->clear();
    ui.widget_database_list->setColumnCount(1);    
}

void tools_widget::add_to_list(QString table, vector<QString> columns)
{
	//databases
    QTreeWidgetItem* item_table_1 = addTreeRoot(table+"(Table)");

		QTreeWidgetItem* item_columns = addTreeChild(item_table_1, "Columns");

			for (long index=0;  index<(long)columns.size(); ++index)
			{
				try {
					QTreeWidgetItem* item_column = addTreeChild(item_columns, columns.at(index));
				}
				catch (exception& e) {
					//index hiba
				}
			}
}

QTreeWidgetItem* tools_widget::addTreeRoot(QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
	QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui.widget_database_list);

    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);

	return treeItem;
}

QTreeWidgetItem* tools_widget::addTreeChild(QTreeWidgetItem *parent, QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem();

    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);

    // QTreeWidgetItem::addChild(QTreeWidgetItem * child)
    parent->addChild(treeItem);

	return treeItem;
}
