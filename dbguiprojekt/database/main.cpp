#include "database_gui.h"
#include "query_widget.h"
#include "structures.h"
#include <iostream>
#include <QtGui/QApplication>

#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/write.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/array.hpp>

using namespace std;

int main(int argc, char *argv[])
{

	QApplication a(argc, argv);
	database_gui d;
	d.show();

	return a.exec();

}