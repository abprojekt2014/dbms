#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <boost/variant.hpp>

const int max_column_length = 64;

struct _NETWORK_PACKET
{
	char error_code;		//1 byte
	long remaining_packets;
	char col_number;		//1 byte
	long row_number;		//4 byte
	long row_size;			//4 byte
	long size;				//4 byte
	char data[];			// ebben vannak az adatok
};

struct _NETWORK_COLUMNS
{
	char type;
	char binary_size;			//egy oszlop hany byteot foglal el valojaban
	char name[max_column_length];
};


#endif //STRUCTURES_H