#include "database_gui.h"
#include <iostream>
#include <QtGui/QMessageBox>
#include <QtGui/QLayout>
#include <string>
#include <vector>

using namespace std;

database_gui::database_gui(QWidget *parent, Qt::WFlags flags): QMainWindow(parent, flags)
{
	ui.setupUi(this);

	_query_widget = new query_widget(this);

	_tools_widget = new tools_widget(this);

	centralWidget()->setLayout(new QHBoxLayout());
	centralWidget()->layout()->addWidget(_tools_widget);
	centralWidget()->layout()->addWidget(_query_widget);
	_query_widget->hide();

	connect(_tools_widget->ui.pushButton_refreshlist, SIGNAL(clicked()), _tools_widget->ui.widget_database_list, SLOT(clear()));
	connect(_tools_widget->ui.pushButton_refreshlist, SIGNAL(clicked()), this, SLOT(refresh_clicked()));
	
	connect(_tools_widget->ui.pushButton_new_query, SIGNAL(clicked()), this, SLOT(new_query_clicked()));
	connect(_tools_widget->ui.pushButton_open_query, SIGNAL(clicked()), this, SLOT(open_query_clicked()));
	connect(_tools_widget->ui.pushButton_close_query, SIGNAL(clicked()), this, SLOT(close_query_clicked()));
	
	connect(_tools_widget->ui.pushButton_save_query, SIGNAL(clicked()), this, SLOT(save_query_clicked()));

	connect(_tools_widget->ui.pushButton_execute_query, SIGNAL(clicked()), this, SLOT(execute_query_clicked()));
	connect(_query_widget->ui.tab_query_1, SIGNAL(text_changed()), this, SLOT(ellenoriz()));
}

void database_gui::refresh_clicked()
{
	_tools_widget->refresh_list();

	boost::asio::io_service io_service;
	client* _client = new client(io_service);
	if(!_client->connect())
	{
		error_message(QString::fromStdString(_client->get_error()));
		_tools_widget->ui.label_connection->setText("");
	}
	else
	{

	//get the list of tables with their columns
	try {
		vector<QString> tables = get_qstring_vector(_client->getTablesName());
		for (long index=0;  index<(long)tables.size(); ++index)
		{
			try {
				vector<QString> col = get_qstring_vector(_client->getColumnsName(tables.at(index).toStdString()));
				_tools_widget->add_to_list(tables.at(index), col);
			}
			catch (exception& e) {
				error_message("Index exceeds vector dimensions.");
			}
		}
	}
	catch (exception& e) {
		error_message("Index exceeds vector dimensions.");
	}
	}

	delete _client;
}

void database_gui::new_query_clicked()
{
	if(!_query_widget->isVisible())
	{
		_query_widget->show();
		_tools_widget->show_buttons();
	}
	_query_widget->new_query();
	opened.push_back(1);
}

void database_gui::open_query_clicked()
{
	QString file_name = _tools_widget->getOpenFileName();
	if(!_query_widget->isVisible())
	{
		_query_widget->show();
		_tools_widget->show_buttons();
	}
	_query_widget->read_from_file(file_name);
	opened.push_back(1);
}

void database_gui::close_query_clicked()
{
	int i = _query_widget->current_tab_index();
	QMessageBox msgBox;
	msgBox.setInformativeText("Do you want to save your changes?");
	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	int ret = msgBox.exec();
	
	switch (ret) {
		case QMessageBox::Save:
			save_query_clicked();
			break;
		case QMessageBox::Discard:
			_query_widget->close_query();
			break;
		case QMessageBox::Cancel:
			break;
		default:
			break;
	}

	opened.erase(opened.begin()+i);
	if(opened.size() == 0)
	{
		_query_widget->hide();
		_tools_widget->hide_buttons();
	}
}

void database_gui::save_query_clicked()
{
	int i = _query_widget->current_tab_index();
	QString file_name = _tools_widget->getSaveFileName();
	if(file_name != "")
	{
		_query_widget->write_to_file(file_name);
		_query_widget->set_current_tab_name(file_name);
	}
}

void database_gui::execute_query_clicked()
{
	boost::asio::io_service io_service;
	client* _client = new client(io_service);
	if(!_client->connect())
	{
		error_message(QString::fromStdString(_client->get_error()));
		_tools_widget->ui.label_connection->setText("");
	}
	else
	{
		if(!_client->write((_query_widget->get_query_text()).toUtf8().constData()))
		{
			error_message(QString::fromStdString(_client->get_error()));
			return;
		}
		if(!_client->read())
		{
			error_message(QString::fromStdString(_client->get_error()));
			return;
		}
		std::vector<std::string> st = _client->get_row();
		_query_widget->write_header_result(st, _client->has_error());
		while(!_client->finish_read())
		{
			if(!_client->read())
			{
				error_message(QString::fromStdString(_client->get_error()));
				return;
			}
			std::vector<std::string> st = _client->get_row();
			_query_widget->write_header_result(st, _client->has_error());
		}
	}

	delete _client;
}

void database_gui::error_message(QString message)
{
	QMessageBox messageBox;
	messageBox.critical(0,"Error", message);
	messageBox.setFixedSize(500,200);
}

std::vector<QString> database_gui::get_qstring_vector(std::vector<std::string> s)
{
	vector<QString> vect;
	for(int c = 0; c < s.size(); c++)
	{
		QString qstr = QString::fromStdString(s.at(c));
		vect.push_back(qstr);
	}
	return vect;
}

database_gui::~database_gui()
{
	//delete _query_widget;
	//delete _tools_widget;
	//delete _client;
}
