#ifndef DATABASE_GUI_H
#define DATABASE_GUI_H

#include <QtGui/QMainWindow>
#include "ui_database_gui.h"
#include <QtGui/QMessageBox>

#include "query_widget.h"
#include "tools_widget.h"
#include "../../projekt/src/testclient/client.h"
#include "structures.h"

class database_gui : public QMainWindow
{
private:
	Q_OBJECT

	Ui::database_gui ui;

	query_widget *_query_widget;
	tools_widget *_tools_widget;

	vector<int> opened;
	QMessageBox close_or_save;

	void check_network();

public:
	database_gui(QWidget *parent = 0, Qt::WFlags flags = 0);
	~database_gui();

private slots:

	void refresh_clicked();
	void new_query_clicked();
	void open_query_clicked();
	void close_query_clicked();

	void save_query_clicked();
	void execute_query_clicked();

	void error_message(QString message);

	std::vector<QString> get_qstring_vector(std::vector<std::string> s);

};

#endif // DATABASE_GUI_H
