#include "query_widget.h"
#include <QtGui/QLayout>
#include <qstring.h>
#include <QtGui/qtableview.h>
#include <QtGui/qlabel.h>
#include <QtGui/qstandarditemmodel.h>
#include <iostream>
#include <fstream>
#include <string>
#include "structures.h"
#include "database_gui.h"
using namespace std;

query_widget::query_widget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	ui.tabWidget_query->clear();
	ui.tabWidget_result->clear();

	QWidget* pg = new QWidget();
	QTableView* tv = new QTableView();	
	pg->setLayout(new QHBoxLayout());
	pg->layout()->addWidget(tv);
	ui.tabWidget_result->addTab(pg,"result");

	QWidget* pg2 = new QWidget();	
	QLabel* l = new QLabel();
	pg2->setLayout(new QHBoxLayout());
	pg2->layout()->addWidget(l);
	ui.tabWidget_result->addTab(pg2,"message");
}

query_widget::~query_widget()
{

}

QString query_widget::get_query_text()
{
	QTextEdit* tt = qobject_cast<QTextEdit*>(ui.tabWidget_query->currentWidget()->layout()->itemAt(0)->widget());
	return tt->toPlainText();
}

void query_widget::write_header_result(std::vector<std::string> dat, bool error)
{
	//message
	ui.tabWidget_result->setCurrentIndex(1);
	QLabel* l = qobject_cast<QLabel*>(ui.tabWidget_result->currentWidget()->layout()->itemAt(0)->widget());
	if (error)
	{
		std::string s = "";
		for(int c = 0; c < dat.size(); c++)
			s += dat.at(c) += " ";
		l->setText(QString::fromStdString(s));
	}
	else
	{
		l->setText("Succesful query execution.");

		//result
		ui.tabWidget_result->setCurrentIndex(0);
		QTableView* tv = qobject_cast<QTableView*>(ui.tabWidget_result->currentWidget()->layout()->itemAt(0)->widget());

		QStandardItemModel *model = new QStandardItemModel(0,dat.size(),this); //Rows and Columns
		for(int c = 0; c < dat.size(); c++)
			model->setHorizontalHeaderItem(c, new QStandardItem(QString::fromStdString(dat.at(c))));
		tv->setModel(model);
	}
}

void query_widget::write_row_result(std::vector<std::string> dat, bool error)
{
	//message
	ui.tabWidget_result->setCurrentIndex(1);
	QLabel* l = qobject_cast<QLabel*>(ui.tabWidget_result->currentWidget()->layout()->itemAt(0)->widget());
	if (error)
	{
		std::string s = "";
		for(int c = 0; c < dat.size(); c++)
			s += dat.at(c) += " ";
		l->setText(QString::fromStdString(s));
	}
	else
	{
		//result
		ui.tabWidget_result->setCurrentIndex(0);
		QTableView* tv = qobject_cast<QTableView*>(ui.tabWidget_result->currentWidget()->layout()->itemAt(0)->widget());
		QStandardItemModel *model = qobject_cast<QStandardItemModel*>(tv->model());
		int r = model->rowCount();
		model->setRowCount(r+1);

		for(int c = 0; c < dat.size(); c++)
		{
			QStandardItem *firstRow = new QStandardItem(QString::fromStdString(dat.at(c)));
			firstRow->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
			model->setItem(r,c,firstRow);
		}
		tv->setModel(model);
	}
}

void query_widget::write_to_file(const QString& file_name)
{
	QString s = get_query_text();

	fstream f(file_name.toAscii().data(), ios_base::out);
	
	f << ".qr" << endl;
	f << s.toStdString() << endl;
	f.close();
}

void query_widget::read_from_file(const QString& file_name)
{
	fstream f(file_name.toAscii().data(), ios_base::in);

	string type;
	f >> type;
	string line;
	getline(f, line);

	if(type == ".qr")
	{
		string q = "";
		//string line;
		while (getline(f, line))
		{
			q = q + line + "\n";
		}
		q = q.substr(0, q.size()-1);
		QString qstr = QString::fromStdString(q);

		//new tab
		QWidget* pg = new QWidget();
		QTextEdit * t = new QTextEdit();
		t->setText(qstr);
	
		pg->setLayout(new QHBoxLayout());
		pg->layout()->addWidget(t);

		ui.tabWidget_query->addTab(pg,file_name+"*");
		ui.tabWidget_query->setCurrentWidget(pg);
	}

}

void query_widget::new_query()
{
	//new tab
	QWidget* pg = new QWidget();
	QTextEdit * t = new QTextEdit();
	connect(t, SIGNAL(textChanged()), this, SLOT(on_text_changed()));
	
	pg->setLayout(new QHBoxLayout());
	pg->layout()->addWidget(t);

	ui.tabWidget_query->addTab(pg,"new_query*");
	ui.tabWidget_query->setCurrentWidget(pg);
}

void query_widget::close_query()
{
	ui.tabWidget_query->removeTab(current_tab_index());
}

void query_widget::set_current_tab_name(QString name)
{
	ui.tabWidget_query->setTabText(current_tab_index(), name);
}

int query_widget::current_tab_index()
{
	return ui.tabWidget_query->currentIndex();
}
