#ifndef QUERY_WIDGET_H
#define QUERY_WIDGET_H

#include <QtGui/QWidget>
#include "ui_query_widget.h"
#include "table_result.h"

class query_widget : public QWidget
{
	Q_OBJECT

public:
	Ui::query_widget ui;

	query_widget(QWidget *parent = 0);
	~query_widget();

	void write_to_file(const QString& file_name);
	void read_from_file(const QString& file_name);

	void new_query();
	void close_query();

	int current_tab_index();
	void set_current_tab_name(QString name);

	QString get_query_text();
	void write_header_result(std::vector<std::string> dat, bool error);
	void write_row_result(std::vector<std::string> dat, bool error);
};

#endif // QUERY_WIDGET_H