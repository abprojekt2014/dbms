cmake_minimum_required(VERSION 2.6.2)

project(indexmanager)

find_package(Boost)

include_directories(${Boost_INCLUDE_DIRS})

set(SRCS
    index_manager.cpp
)

set(HEADERS
    index.h
    index_manager.h
    iterator.h
)

add_library(
    indexmanager 
    ${SRCS}
    ${HEADERS}
    ${Boost_FUNCTIONAL_LIBRARY})

target_link_libraries(
    indexmanager
    hash_index
)

