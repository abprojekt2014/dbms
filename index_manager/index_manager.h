#ifndef _INDEX_MANAGER_H_
#define _INDEX_MANAGER_H_

#include <string>
#include <list>
#include <map>
#include <utility>
#include "index.h"

enum indextype
{
    idxBPlusTree = 0,
    idxHashTable,
};

class index_t;

class indexmanager
{
public:
    struct index_info
    {
        std::string table_name;
        std::string column_name;
        columntype  coltype;
        int         column_size;
        indextype   type;
        index_t*    idx;
    };

    virtual ~indexmanager();

    // getting the instance
    static indexmanager& getinstance();

    // creates a new index file
    index_t* createindex(indextype idxtype, const std::string table_name, const std::string column_name, columntype type, int size_of_column);

    // returns a pointer to the index if that exists
    index_t* openindex(const std::string name);

    // deletes an index file
    bool deleteindex(const std::string name);

    std::list<std::string> getindexnames();

protected:
    // constructor
    indexmanager();
    table_t* system_indices;

    //                   index name, other stuff
    std::map<std::string, index_info*> idx_infos;

    index_info* get_index_info(std::string name);
};

#endif // INDEX_MANAGER_H
