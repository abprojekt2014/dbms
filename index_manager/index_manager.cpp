#include <stdexcept>
#include <memory>
#include "index_manager.h"
#include "../hash_index/hash_index.h"
#include "../record_manager/table_manager.h"
#include "../record_manager/serializer.hpp"

using namespace std;

indexmanager::indexmanager()
{
    system_indices = tablemanager::getinstance().opentable("system_indices");

    if (system_indices == nullptr)
    {
        catalog c;

        c.addcolumn("table_name", ctString, 64);
        c.addcolumn("column_name", ctString, 64);
        c.addcolumn("column_type", ctInteger, 4);
        c.addcolumn("column_length", ctInteger, 4);
        c.addcolumn("index_type", ctInteger, 4);

        // we have to create a new table with the name "system_indices"
        system_indices = tablemanager::getinstance().createtable("system_indices", c);
    }

    serializer_t* s = new serializer_t(system_indices->getcatalog().rowlength());
    BYTE* binrow = new BYTE[system_indices->getcatalog().rowlength()];

    iterator_t* it = system_indices->get_iterator();
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        string tname;
        string cname;
        string idxname;
        long coltype;
        long clen;
        long itype;
        index_info* info;

        if (!system_indices->read_row(row, binrow))
        {
            assert(false);
            continue;
        }

        tname = s->deserialize_str(binrow, 0, 64);
        cname = s->deserialize_str(binrow, 64, 64);
        coltype = (long)s->deserialize_int(binrow, 128, 4);
        clen = (long)s->deserialize_int(binrow, 132, 4);
        itype = (long)s->deserialize_int(binrow, 136, 4);

        info = new index_info;
        info->coltype = (columntype)coltype;
        info->column_name = cname;
        info->column_size = clen;
        info->table_name = tname;
        info->type = (indextype)itype;
        info->idx = nullptr;

        index_t* idx;
        switch(itype)
        {
        case idxBPlusTree:
            assert(false);
            delete info;
            return;
        case idxHashTable:
            idx = new hash_index();
            break;
        default:
            assert(false);
            break;
        }

        if (!idx->open(tname, cname))
        {
            assert(false);
            goto finally;
        }

        info->idx = idx;
        idxname = idx->getname();

        // insert the newly opened index into the info map
        idx_infos.insert(pair<string, index_info*>(idxname, info));
    }

finally:
    delete it;
    delete s;
    delete[] binrow;
}

indexmanager::~indexmanager()
{
    if (nullptr != system_indices)
    {
        system_indices->close();
        system_indices = nullptr;
    }

    // freeing ocupied memory
    for (pair<string, index_info*> e: idx_infos)
    {
        if (nullptr != e.second)
        {
            if (nullptr != e.second->idx)
            {
                delete e.second->idx;       // freeing the index
                e.second = nullptr;
            }
            delete e.second;                // freeing the allocated index_info
            e.second = nullptr;
        }
    }

    idx_infos.clear();
}

// getting the instance
indexmanager& indexmanager::getinstance()
{
    static indexmanager instance;

    return instance;
}

// creates a new index file
index_t* indexmanager::createindex(indextype idxtype, const std::string table_name, const std::string column_name, columntype coltype, int size_of_column)
{
    index_t* idx = nullptr;

    switch (idxtype)
    {
    case idxBPlusTree:
        assert(false);  // not implemented
        break;
    case idxHashTable:
        idx = new hash_index();
        break;
    default:
        return nullptr;
    }

    if (!idx->create(table_name, column_name, coltype, size_of_column))
    {
        return nullptr;
    }

    index_info* info = new index_info;
    info->coltype = coltype;
    info->column_name = column_name;
    info->column_size = size_of_column;
    info->table_name = table_name;
    info->type = idxtype;
    info->idx = idx;

    string idx_name = idx->getname();

    // inserting the index into the table
    BYTE* binrow = new BYTE[system_indices->getcatalog().rowlength()];
    unique_ptr<serializer_t> s(new serializer_t(system_indices->getcatalog().rowlength()));
    if (nullptr == s)
    {
        delete[] binrow;
        return nullptr;
    }

    // inserting the current index into the map
    idx_infos.insert(pair<string, index_info*>(idx_name, info));

    // building the new row
    s->serialize_str(binrow, 0, table_name, 64);
    s->serialize_str(binrow, 64, column_name, 64);
    s->serialize_int(binrow, 128, coltype, 4);
    s->serialize_int(binrow, 132, size_of_column, 4);
    s->serialize_int(binrow, 136, idxtype, 4);

    // writing the new row into the table
    system_indices->insert_row(binrow);

    delete[] binrow;

    return idx;
}

// opens an index file - it should identify dynamically the type of the index file
index_t* indexmanager::openindex(const std::string name)
{
    index_info* info = get_index_info(name);
    if (nullptr == info)
    {
        return nullptr;
    }

    return info->idx;
}


// deletes an index file
bool indexmanager::deleteindex(const std::string name)
{
    index_info* info = get_index_info(name);
    if (nullptr == info)
    {
        return false;
    }

    unique_ptr<iterator_t> it(system_indices->get_iterator());
    unique_ptr<serializer_t> s(new serializer_t(system_indices->getcatalog().rowlength()));
    BYTE* binrow = new BYTE[system_indices->getcatalog().rowlength()];

    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        string tname;
        string cname;

        assert(system_indices->read_row(row, binrow));

        tname = s->deserialize_str(binrow, 0, 64);
        cname = s->deserialize_str(binrow, 64, 64);

        if (info->table_name.compare(tname) == 0 &&
            info->column_name.compare(cname) == 0)
        {
            // this is the index what we are looking for
            system_indices->delete_row(row);
            break;
        }
    }

    if (nullptr != info->idx)
    {
        info->idx->close();             // closing the file
        delete info->idx;               // freeing the index class
    }

    info->idx = nullptr;
    remove(name.c_str());               // deleting the index file
    idx_infos.erase(name);
    //delete info;                      // freeing the info

    delete[] binrow;

    return true;
}

indexmanager::index_info* indexmanager::get_index_info(std::string name)
{
    index_info* info;

    try
    {
        info = idx_infos.at(name);
    }
    catch (const std::out_of_range)
    {
        return nullptr;
    }

    return info;
}

std::list<std::string> indexmanager::getindexnames()
{
    std::list<std::string> lista;

    for (std::pair<std::string, index_info*> p: idx_infos)
    {
        lista.push_back(p.first);
    }

    return lista;
}
