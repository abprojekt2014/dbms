#ifndef _INDEX_H_
#define _INDEX_H_

#include "../record_manager/table_iterator.h"
#include "../include/dbtypes.h"
#include "../record_manager/rm_types.h"
#include "../record_manager/table_types.h"
#include "iterator.h"

//
// The interface for an index
//
// Notes:
//  * the name of an index should be shorter or equal with 64
//
class index_t
{
protected:
    friend class indexmanager;

public:
    virtual ~index_t() {};

    static std::string generatename(const std::string table_name, const std::string column_name) { return std::string("idx_") + table_name + "_" + column_name; }

    // returns the name of the file, not the path, only after the file is created/opened
    virtual std::string getname() = 0;

    // creates a new index file
    virtual bool create(std::string tablename, std::string columnname, columntype type, int column_size) = 0;
    // opens an index file
    virtual bool open(std::string tablename, std::string columnname) = 0;
    // closes the file
    virtual bool close() = 0;

    // returns the name of the table
    virtual std::string gettablename() = 0;
    // returns the name of the column
    virtual std::string getcolumnname() = 0;
    // sets the column's width
    virtual void setcolumnlength(int length) = 0;
    // returns the column's length
    virtual int getcolumnlength() = 0;

    // iterates all the elements
    virtual iterator_t* getiterator() = 0;

    /// !!! The minimum or maximum is a serialized datatype, the length of the column is already known
    /// (because it must be set (if a new index is created) or if the index file was opened it is stored in the headers)
    // iterates trough the interval where the elements are greater or equal
    // than the minimum and lesser or equal than the maximum
    virtual iterator_t* getiterator(void* minimum, void* maximum) = 0;
    // returns an iterator which iterates the elements what are less than the maximum element
    virtual iterator_t* getiterator_less(void* maximum) = 0;
    // returns an iterator which iterates the elements what are less or equal than the maximum element
    virtual iterator_t* getiterator_less_or_equal(void* maximum) = 0;
    // returns an iterator which iterates the elements what are greater than the minimum element
    virtual iterator_t* getiterator_greater(void* minimum) = 0;
    // returns an iterator which iterates the elements what are greater or equal than the minimum element
    virtual iterator_t* getiterator_greater_or_equal(void* minimum) = 0;
    // returns an iterator which iterates trough the rows which are equal with the given value
    virtual iterator_t* getiterator_equal(void* value) = 0;

    // returns the lowest value (leftmost in the b-tree)
    virtual recordid_t getmin() = 0;
    // returns the highest value (rightmost in the b-tree)
    virtual recordid_t getmax() = 0;

    // inserts a row into the index file
    virtual bool insertrow(recordid_t row, void* value) = 0;
    // deletes a row from an index file
    virtual bool deleterow(recordid_t row) = 0;
    // updating the value   -   it's equal with a delete and insert operation
    virtual bool update(recordid_t row, void* new_value) = 0;
};

#endif // _INDEX_H_
