#ifndef _ITERATOR_H_
#define _ITERATOR_H_

#include "../record_manager/rm_types.h"

class iterator_t
{
public:
    virtual ~iterator_t() {};
    virtual recordid_t first() = 0;
    virtual recordid_t next() = 0;
    virtual bool end() = 0;
    virtual bool read_value(void* value) = 0;      //if this iterator is from an index this function must return true
};

#endif //_ITERATOR_H_
