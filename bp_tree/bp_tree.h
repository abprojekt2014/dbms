#ifndef _BP_TREE_H_
#define _BP_TREE_H_

#include <assert.h>
#include <boost/pool/object_pool.hpp>
#include "iterator_interface.h"
#include "../include/dbtypes.h"
#include <string>
#include "../recordmanager/ansi/blockfile_ansi.h"

#define BP_TREE_INDEX_FILE_BP_ORDER 4
#define BP_TREE_INDEX_FILE_N_RECORD 3

#define BP_FILE_MAGIC ((unsigned int)('ITPB'))
#define BP_ROOT_BLOCK_ID        1
#define BP_FREE_NODE_BLOCK_ID   2

template<
//kulcs tipusa
class TKEY,
//ertek tipusa
class TVALUE,
//egy adott belso csomopontobol kiindulo alcsomopontok szama
unsigned int BP_ORDER= 4,
//a levelcsomopontokabn talalhato kulcs<->ertek parok szama
unsigned int N_RECORD= BP_ORDER-1
>class BPlusTree{
    static_assert(3<= BP_ORDER,"BP_ORDER min 3");//minimum 3 gyerek csomopont szukseges a ketteoszthatosag erdekeben
    static_assert(2<= N_RECORD,"N_RECORD min 2");//minimum 1 gyerek szukseges a ketteoszthatosag erdekeben
protected:
    static const unsigned int NODE_TRESHOLD= (BP_ORDER+1)/2;
    static const unsigned int RECORD_TRESHOLD= (N_RECORD+1)/2;

    //csomopont tipusok
    enum NODE_TYPE{
        NODE_TYPE_INNER,
        NODE_TYPE_LEAF
    };

#pragma pack(push,1)
    struct BP_TREE_FILE_HEADER{
        unsigned int magic;
        unsigned short tree_order;
        unsigned short record_order;
        unsigned int depth;
        unsigned int n_elem;
        blockid_t root_block_id;
        blockid_t first_free_block_id;
    };
#pragma pack(pop)

#pragma pack(push,4)
    struct BP_TREE_FILE_FREE_BLOCK_ENTRY{
        blockid_t next;
    };

    struct BP_TREE_FILE_NODE_HEADER{
        NODE_TYPE node_type;
        unsigned int n_key;
        blockid_t parent_block_id;
        bool minimal_sized;
    };

    struct BP_TREE_FILE_INNER_NODE{
        BP_TREE_FILE_NODE_HEADER header;
        TKEY keys[BP_ORDER];
        blockid_t children_block_id[BP_ORDER+1];
    };

    struct BP_TREE_FILE_LEAF_NODE{
        BP_TREE_FILE_NODE_HEADER header;
        TKEY keys[N_RECORD];
        TVALUE values[N_RECORD];
        blockid_t prev_leaf_node_block_id;
        blockid_t next_leaf_node_block_id;
    };
#pragma pack(pop)

    //blokkok
    block_t header_block;
    ///block_t root_block;
    block_t first_free_block;

    //fejlec
    BP_TREE_FILE_HEADER *tree_file_header;

    inline
    bool
    verify_header(void){
        return ( (BP_FILE_MAGIC== tree_file_header->magic) &&
            (BP_ORDER== tree_file_header->tree_order) &&
            (N_RECORD== tree_file_header->record_order) );
    }

    struct BP_NODE;

    //a kozbeeso csomopontok, adatot nem tartalmaznak
    struct INNER_NODE: BP_NODE{
        TKEY keys[BP_ORDER];           //kulcsok
        BP_NODE *children[BP_ORDER+1]; //az utod csomopontok: lehetnek belso csomopontok vagy levelek
        bool children_loaded[BP_ORDER+1];
        BP_TREE_FILE_INNER_NODE *file_header;
        //konstruktor
        INNER_NODE(void):
            BP_NODE(NODE_TYPE_INNER),
            file_header((BP_TREE_FILE_INNER_NODE *)this->file_block.buffer)
            {
                for(unsigned int i= 0;i!= BP_ORDER+1;++i){
                    children_loaded[i]= false;
                }
            };

        //
        //inc_n_key
        //
        inline
        void
        inc_n_key(){
            assert(BP_ORDER> this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(NODE_TRESHOLD< ++this->n_key){//noveljuk a rekordok szamat
                this->minimal_sized= false;//minimalis elemszam flag beallitasa
            }
            assert(BP_ORDER>= this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //dec_n_key
        //
        inline
        void
        dec_n_key(){
            assert(BP_ORDER>= this->n_key);
            assert(0!= this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(NODE_TRESHOLD>= --this->n_key){//noveljuk a rekordok szamat
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }
            assert(BP_ORDER> this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //set_split_n_elem
        //
        inline
        void
        set_split_n_elem(bool new_node){
/**
 * elemek szamat allitja be annyira, amellyi lesz egy ketteosztas utan
 */
            assert(BP_ORDER>= this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(!new_node){
                this->n_key= NODE_TRESHOLD-1;//csokkentjuk az elemek szamat az eredeti csomopontban
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }else{
                this->n_key= BP_ORDER-NODE_TRESHOLD;//ketteosztjuk a kulcsok szamat
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }
            assert(BP_ORDER>= this->n_key);
            assert((NODE_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //get_insert_find_position
        //
        unsigned int
        get_insert_find_position(
            const TKEY &key
        )const{
/**
 * \return a pozicio, ahova be kell szurni az erteket
 */
            unsigned key_i= 0;

            //ellenorzes
            assert(BP_ORDER> this->n_key);

            //linearis kereses
            while((key_i< this->n_key) && (key>= this->keys[key_i])){
                ++key_i;
            }
            return key_i;
        }

        //
        // get_key_position
        //
        bool
        get_key_position(
            const TKEY &key,
            unsigned int *index= NULL
        )const{
/**
 * egy adott kulcs poziciojat teriti vissza
 * \return true, ha az adott kulcs letezik a csomopontban
 */
            unsigned int i_search;

            //ellenorzes
            assert(BP_ORDER> this->n_key);

            //az elem megkeresese a rekordban
            i_search= this->get_insert_find_position(key);
            if( (BP_ORDER> i_search) && //belso csomopontokban kulcs+1 utod van
                (key== this->keys[i_search]) ){
                //az elemet megtalalta
                if(NULL!= index){
                    *index= i_search;
                }
                return true;
            }
            //adott kulcsu elem nem letezik a faban
            return false;
        }

        //
        // get_child
        //
        BP_NODE *
        get_child(
            const unsigned int index
        )const{
            assert(BP_ORDER>= index);
            assert(this->n_key>= index);

            return this->children[index];
        }

        inline
        BP_NODE *
        get_first_child(void){
            return get_child(0);
        }

        inline
        BP_NODE *
        get_last_child(void){
            return get_child(this->n_key);
        }

        //
        // remove_element
        //
        void
        remove_element(
            const unsigned int index
        ){
/**
 * egy adott indexu elemet kivesz, az utana levoket balra tolja
 * \param index az elem indexet adja meg (nem a kulcset)
 */
            unsigned int remove_i;

            assert(BP_ORDER> index);
            assert(this->n_key> index);

            for(remove_i= index+1; remove_i!= this->n_key; ++remove_i){
                this->keys[remove_i-1]= this->keys[remove_i];
                this->children[remove_i-1]= this->children[remove_i];
            }

            dec_n_key();
        }

        inline
        const
        TKEY &
        get_key(
            const unsigned int index
        )const{
            assert(BP_ORDER> index);

            return this->keys[index];
        }

        inline
        void
        set_key(
            const TKEY &key,
            const unsigned int index
        ){
            assert(N_RECORD> index);

            this->keys[index]= key;
        }

        inline
        void
        get_child(
            const unsigned int index,
            BP_NODE *child
        )const{
            assert(BP_ORDER>= index);
            assert(NULL!= child);

            *child= this->children[index];
        }

        const
        block_t &
        update_file_block(void){
            file_header->header.minimal_sized= this->minimal_sized;
            file_header->header.node_type= this->node_type;
            file_header->header.n_key= this->n_key;
            if(NULL!= this->parent){
                file_header->header.parent_block_id= this->parent->file_block.blockid;
            }
            memcpy(file_header->keys,this->keys,sizeof(TKEY)*this->n_key);
            for(unsigned int i= 0;i!= this->n_key;++i){
                file_header->children_block_id[i]= children[i]->file_block.blockid;
            }
            file_header->children_block_id[this->n_key]= children[this->n_key]->file_block.blockid;

            return this->file_block;
        }
    };

    //csomopont interfesz
    struct BP_NODE{
        NODE_TYPE node_type;
        unsigned int n_key;         //aktualisan hasznalt kulcsok szama
        INNER_NODE *parent;
        bool minimal_sized;
        block_t file_block;
    protected:
        //konstruktor
        BP_NODE(NODE_TYPE n_type):
            node_type(n_type),
            n_key(0),
            parent(NULL),
            minimal_sized(true){};
        //====================
        //interfesz fuggvenyek
        //====================
        virtual void inc_n_key()= 0;
        virtual void dec_n_key()= 0;
        virtual void set_split_n_elem(const bool new_node)= 0;
        virtual unsigned int get_insert_find_position(const TKEY &key)const= 0;
        virtual bool get_key_position(
            const TKEY &key,
            unsigned int *index= NULL
        )const= 0;
        virtual void remove_element(const unsigned int index)= 0;
        virtual const TKEY &get_key(const unsigned int index)const= 0;
        //get_first_key
        inline const TKEY &get_first_key(void)const{
            return get_key(0);
        }
        //get_last_key
        inline const TKEY &get_last_key(void)const{
            return get_key(n_key-1);
        }
        virtual void set_key(const TKEY &key, const unsigned int index)= 0;
        virtual const block_t &update_file_block(void)= 0;
    };

    //level csomopontok, az adatokat tartalmazzak
    struct LEAF_NODE: BP_NODE{
        TKEY keys[N_RECORD];        //kulcsok
        TVALUE values[N_RECORD];    //ertekek
        LEAF_NODE *prev_leaf_node;  //mutato a jobbra (novekvo sorrendben) a kovetkezo levelre
        LEAF_NODE *next_leaf_node;  //mutato a jobbra (novekvo sorrendben) a kovetkezo levelre
        BP_TREE_FILE_LEAF_NODE *file_header;
        //konstruktor
        LEAF_NODE(void):
            BP_NODE(NODE_TYPE_LEAF),
            prev_leaf_node(NULL),
            next_leaf_node(NULL),
            file_header((BP_TREE_FILE_LEAF_NODE *)this->file_block.buffer)
            {};

        //
        //inc_n_key
        //
        inline
        void
        inc_n_key(){
/**
 * elemek szamat modosito fuggveny
 */
            assert(N_RECORD>= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(RECORD_TRESHOLD< ++this->n_key){//noveljuk a rekordok szamat
                this->minimal_sized= false;//minimalis elemszam flag beallitasa
            }
            assert(N_RECORD>= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //dec_n_key
        //
        inline
        void
        dec_n_key(){
            assert(N_RECORD>= this->n_key);
            assert(0!= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(RECORD_TRESHOLD>= --this->n_key){//noveljuk a rekordok szamat
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }
            assert(N_RECORD>= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //set_split_n_elem
        //
        inline
        void
        set_split_n_elem(bool new_node){
/**
 * elemek szamat allitja be annyira, amellyi lesz egy ketteosztas utan
 */
            assert(N_RECORD>= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
            if(!new_node){
                this->n_key= RECORD_TRESHOLD;//csokkentjuk az elemek szamat az eredeti csomopontban
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }else{
                this->n_key= N_RECORD-RECORD_TRESHOLD;//ketteosztjuk a kulcsok szamat
                this->minimal_sized= true;//minimalis elemszam flag beallitasa
            }
            assert(N_RECORD>= this->n_key);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);
        }

        //
        //get_insert_find_position
        //
        unsigned int
        get_insert_find_position(
            const TKEY &key
        )const{
/**
 * \return a pozicio, ahova be kell szurni az erteket
 */
            unsigned key_i= 0;

            //ellenorzes
            assert(N_RECORD>= this->n_key);

            //linearis kereses
            while((key_i <this->n_key) && (key> keys[key_i])){
                ++key_i;
            }
            return key_i;
        }

        //
        // get_key_position
        //
        bool
        get_key_position(
            const TKEY &key,
            unsigned int *index= NULL
        )const{
/**
 * egy adott kulcsu elemet keres meg a rekordban
 * \return true, ha az adott kulcsu elem letezik a rekordban
 */
            unsigned int i_search;
            //az elem megkeresese a rekordban
            i_search= this->get_insert_find_position(key);
            if(key== this->keys[i_search]){
                //az elemet megtalalta
                if(NULL!= index){
                    *index= i_search;
                }
                return true;
            }
            //adott kulcsu elem nem letezik a faban
            return false;
        }

        //
        // find_key
        //
        bool
        find_key(
            const TKEY &key,
            TVALUE *value= NULL,
            unsigned int *index= NULL
        )const{
/**
 * egy adott kulcsu elemet keres meg a rekordban
 * \return true, ha az adott kulcsu elem letezik a rekordban
 */
            unsigned int i_search;

            if(this->get_key_position(key,&i_search)){
                if(NULL!= value){
                    *value= this->values[i_search];
                }
                if(NULL!= index){
                    *index= i_search;
                }

                return true;
            }

            return false;
        }

        //
        // insert_nonfull
        //
        bool
        insert_nonfull(
            const TKEY &key,
            const TVALUE &value,
            const unsigned int insert_i
        ){
/**
 * beszur egy elemet egy olyan rekordba, ahol biztosan van hely
 * \return true, ha beszuras tortent, false ha csak felulirodott egy regi elem
 */
            unsigned int sort_i= 0;

            //ellenorzes
            assert(N_RECORD> this->n_key);
            assert(this->n_key+1>= insert_i);//vegere is lehet szurni
            assert(N_RECORD>= insert_i);
            assert((RECORD_TRESHOLD>= this->n_key)== this->minimal_sized);

            if( (N_RECORD> insert_i) &&
                (0!= this->n_key) &&
                (this->keys[insert_i]== key) ){
                //egy mar letezo kulcs eseten felulirjuk a regi elemet
                this->values[insert_i]= value;
                return false;
            }
            //a beszurando kulcs egyedi
            //a megfelelo helyre beszurjuk az elemet
            //a tobbi elemet tovabb toljuk
            for(sort_i= this->n_key;sort_i> insert_i;--sort_i){
                this->keys[sort_i]= this->keys[sort_i-1];
                this->values[sort_i]= this->values[sort_i-1];
            }
            this->keys[insert_i]= key;
            this->values[insert_i]= value;
            //noveljuk a rekordok szamat
            inc_n_key();

            return true;
        }

        //
        // remove_element
        //
        void
        remove_element(
            const unsigned int index
        ){
/**
 * egy adott indexu elemet kivesz, az utana levoket balra tolja
 */
            unsigned int remove_i;

            assert(N_RECORD> index);
            assert(this->n_key> index);

            for(remove_i= index+1; remove_i!= this->n_key; ++remove_i){
                this->keys[remove_i-1]= this->keys[remove_i];
                this->values[remove_i-1]= this->values[remove_i];
            }

            dec_n_key();
        }

        inline
        const
        TKEY &
        get_key(
            const unsigned int index
        )const{
            assert(N_RECORD> index);

            return this->keys[index];
        }

        inline
        void
        set_key(
            const TKEY &key,
            const unsigned int index
        ){
            assert(N_RECORD> index);

            this->keys[index]= key;
        }

        const
        block_t &
        update_file_block(void){
            file_header->header.minimal_sized= this->minimal_sized;
            file_header->header.node_type= this->node_type;
            file_header->header.n_key= this->n_key;
            if(NULL!= this->parent){
                file_header->header.parent_block_id= this->parent->file_block.blockid;
            }
            memcpy(file_header->keys,this->keys,sizeof(TKEY)*this->n_key);
            memcpy(file_header->values,this->values,sizeof(TVALUE)*this->n_key);
            if(NULL!= prev_leaf_node){
                file_header->prev_leaf_node_block_id= prev_leaf_node->file_block.blockid;
            }
            if(NULL!= next_leaf_node){
                file_header->next_leaf_node_block_id= next_leaf_node->file_block.blockid;
            }

            return this->file_block;
        }
    };

    inline
    BP_NODE *
    get_root(void)const{
        return root;
    }

    const
    inline
    LEAF_NODE *
    get_first_leaf(void)const{
        return first_leaf;
    }

private:

    //=================================
    //lefoglalo/felszabadito fuggvenyek
    //=================================

    bool
    get_free_block(block_t *block){
        assert(NULL!= block);

        if(0== tree_file_header->first_free_block_id){
            //van a fileban elozoleg hasznalt, felszabaditott blokk
            if(!rm_success(bpti_file->readblock(tree_file_header->first_free_block_id,*block))){
                return false;
            }
            //kivesszuk a listabol
            tree_file_header->first_free_block_id= ((BP_TREE_FILE_FREE_BLOCK_ENTRY *)(block->buffer))->next;

            return true;
        }

        if(!rm_success(bpti_file->newblock(*block))){
            return false;;
        }

        return true;
    }

	inline
	LEAF_NODE* new_leaf_node(void){
        LEAF_NODE* leaf_node= leaf_pool.construct();

        if(!get_free_block(&(leaf_node->file_block))){
            leaf_pool.destroy(leaf_node);
            return NULL;
        }

		return leaf_node;
	}


	void delete_leaf_node(LEAF_NODE* leaf_node)const{
        assert(NULL!= leaf_node);
        assert(NODE_TYPE_LEAF== leaf_node->node_type);
		leaf_pool.destroy(leaf_node);
	}


	inline INNER_NODE* new_inner_node(void){
		INNER_NODE* inner_node= inner_pool.construct();

        if(!get_free_block(&(inner_node->file_block))){
            inner_pool.destroy(inner_node);
            return NULL;
        }

		return inner_node;
	}


	void delete_inner_node(INNER_NODE* inner_node)const{
		assert(NULL!= inner_node);
        assert(NODE_TYPE_INNER== inner_node->node_type);
		inner_pool.destroy(inner_node);
	}

	//=========================
	//beszuras belso fuggvenyei
	//=========================

    //
    // insert_into_leaf
    //
	bool
	insert_into_leaf(
        LEAF_NODE *node,
        const TKEY &key,
        const TVALUE &value,
        TKEY *main_key,
        BP_NODE **left,
        BP_NODE **right
    ){
/**
 * \return true ha kette volt valasztva a rekord
 * \param main_key az elso kulcs a record-bol, ez kerul a szulobe is
 */
        unsigned int insert_i= 0;
        LEAF_NODE *sibling= NULL;
        unsigned int move_i= 0;

        //ellenorzes
        assert(NULL!= node);
        assert(NODE_TYPE_LEAF== node->node_type);
        assert(NULL!= main_key);
        assert(NULL!= left);
        assert(NULL!= right);

        //pozicio megkeresese az adott levelben
        insert_i= node->get_insert_find_position(key);
        if(N_RECORD== node->n_key){
            //nincs ures hely
            //kette kell osztani a rekordot
            sibling= new_leaf_node();//letrehozzuk a testvert
            sibling->set_split_n_elem(true);
            //atmasoljuk az uj testver levelbe a rekordok masodik felet
            move_i= sibling->n_key;
            while(move_i-- != 0){
                sibling->keys[move_i]= node->keys[RECORD_TRESHOLD+move_i];
                sibling->values[move_i]= node->values[RECORD_TRESHOLD+move_i];
            }
            node->set_split_n_elem(false);
            //az uj ertek leendo csomopontja:
            if(insert_i< RECORD_TRESHOLD){
                //bal oldali level
                if(node->insert_nonfull(key,value,insert_i)){
                    ++n_elem;//noveljuk az ossz elemszamot
                }
            }else{
                //jobb oldali level
                if(sibling->insert_nonfull(key,value,insert_i-RECORD_TRESHOLD)){;//a jobb csomopont beszurasi indexe el van tolva
                    ++n_elem;//noveljuk az ossz elemszamot
                }
            }
            //azonos szulo beallitasa
            sibling->parent= node->parent;
            //lancolt lista beallitasa
            sibling->next_leaf_node= node->next_leaf_node;
            sibling->prev_leaf_node= node;
            node->next_leaf_node= sibling;
            if(NULL!= sibling->next_leaf_node){
                sibling->next_leaf_node->prev_leaf_node= sibling;
            }
            //beszuras utani visszateritesi ertekek beallitasa
            *main_key= sibling->keys[0];
            *left= node;
            *right= sibling;

            //kiiratas
            assert(rm_success(bpti_file->writeblock(node->update_file_block())));
            assert(rm_success(bpti_file->writeblock(sibling->update_file_block())));

            return true;
        }
        //van ures hely, beszuras a megfelelo pozcioba
        if(node->insert_nonfull(key,value,insert_i)){
            ++n_elem;//noveljuk az ossz elemszamot

            //kiiratas
            assert(rm_success(bpti_file->writeblock(node->update_file_block())));
        }

        return false;
    }


    //
    // insert_into_inner_non_full
    //
    void
    insert_into_inner_non_full(
        INNER_NODE *node,
        const unsigned int insert_depth,
        const TKEY &key,
        const TVALUE &value
    ){
/**
 * \return beszur egy elemet egy olyan agba, aminek a kiindulo csomopontja bistos nincs tele
 */
        unsigned int insert_i= 0;
        bool was_split= false;
        TKEY header_key;
        BP_NODE *left= NULL;
        BP_NODE *right= NULL;
        unsigned int move_i= 0;

        //ellenorzes
        assert(NULL!= node);
        assert(NODE_TYPE_INNER== node->node_type);
        assert(insert_depth<= depth);

        insert_i= node->get_insert_find_position(key);

        if(0== insert_depth-1){
            //az utodok level csomopontok
            was_split= insert_into_leaf(reinterpret_cast<LEAF_NODE *>(node->children[insert_i]),
                key,
                value,
                &header_key,
                &left,
                &right);
        }else{
            //az utodok belso csomopontok
            was_split= insert_into_inner(reinterpret_cast<INNER_NODE *>(node->children[insert_i]),
                insert_depth-1,
                key,
                value,
                &header_key,
                &left,
                &right);
        }

        if(was_split){
            //az utod csomopont kette volt osztva beszuras kozben
            //novelni kell a kulcsok/utodok szamat az aktualis belso csomopontban
            if(insert_i!= node->n_key){
                //a kulcs nem a legjobboldalibb helyre kerult
                //egy adott pillanatban MINDIG node->n_key+1 darab gyerek van
                node->children[node->n_key+1]= node->children[node->n_key];//a regi gyerek eggyel jobbra tolodik
                //a a ketteosztott csomopont indexe-tol eggyel balra toljuk a kucsokat/utodokat
                for(move_i= node->n_key;move_i!= insert_i;--move_i){
                    node->children[move_i]= node->children[move_i-1];
                    node->keys[move_i]= node->keys[move_i-1];
                }
                //a ketteosztott csomopontot ket felet betesszuk a regi helyre es tole eggyel jobbra
            }//kulonben a legjobboldalibb helyre (legnagyobb) kerult a kulcs
            node->keys[insert_i]= header_key;
            node->children[insert_i]= left;
            node->children[insert_i+1]= right;
            node->inc_n_key();
        }
    }


    //
    // insert_into_inner
    //
    bool
    insert_into_inner(
        INNER_NODE *node,
        const unsigned int insert_depth,
        const TKEY &key,
        const TVALUE &value,
        TKEY *main_key,
        BP_NODE **left,
        BP_NODE **right
    ){
/**
 * beszur egy elemet egy agba
 * \return true ha kette volt valasztva a rekord
 * \param main_key az elso kulcs a record-bol, ez kerul a szulobe is
 */
        INNER_NODE *sibling= NULL;
        unsigned int move_i= 0;

        //ellenorzes
        assert(NULL!= node);
        assert(NODE_TYPE_INNER== node->node_type);
        assert(insert_depth<= depth);
        assert(NULL!= main_key);
        assert(NULL!= left);
        assert(NULL!= right);

        //ellenorizzuk, hogy a belso csomopontba biztosan lehetseges-e ketteosztas nelkul beszurni
        if(BP_ORDER== node->n_key){
            //ketteosztjuk a csomopontot
            sibling= new_inner_node();
            sibling->set_split_n_elem(true);
            //atmasoljuk az elemeket az uj testverbe
            for(move_i= 0;move_i!= sibling->n_key;++move_i){
                sibling->keys[move_i]= node->keys[NODE_TRESHOLD+move_i];
                sibling->children[move_i]= node->children[NODE_TRESHOLD+move_i];
            }
            //azonos szulo beallitasa
            sibling->parent= node->parent;
            //mivel az utodok szama= kulcs+1, az utolso utodot kulon kell atmasolni
            sibling->children[sibling->n_key]= node->children[BP_ORDER];
            node->set_split_n_elem(false);
            //beallitjuk a visszateritesi ertekeket
            *main_key= node->keys[NODE_TRESHOLD-1];//belso csomopont eseten a legnagyobb lesz a fo kulcs
            *left= node;
            *right= sibling;
            //beszurjuk a megfelelo csomopontba
            if(key< *main_key){
                //bal oldaliba kerul
                insert_into_inner_non_full(node,insert_depth,key,value);
            }else{
                //jobb oldaliba kerul
                insert_into_inner_non_full(sibling,insert_depth,key,value);
            }

            return true;
        }
        //biztosan nem kell a belso csomopontot ketteosztani
        insert_into_inner_non_full(node,insert_depth,key,value);

        return false;
    }

    //========================
	//kereses belso fuggvenyei
	//========================

	//
	//find_element_and_node
    //
    const
    bool
    find_element_and_node(
        const TKEY &key,
        TVALUE *ret_value= NULL
    )const{
/**
 * megkeres egy adott elemet a faban, kulcs alapjan
 * \return true ha az adott kulcsu elem letezik a faban
 * \param record_leaf mutatot terit vissza arra a level csomopontra, amiben talahato az adott kulcsu elem
 * \param element_index az elem indexe a visszateritett level csomopontban
 */
        unsigned int search_depth= depth;
        unsigned int i_search= 0;
        BP_NODE *search_node= root;
        INNER_NODE *search_inner_node= NULL;
        LEAF_NODE *record_node= NULL;

        assert(NULL!= ret_value);

        //a belso csomopont megkereseke
        while(0!= search_depth--){
            assert(NODE_TYPE_INNER== search_node->node_type);
            search_inner_node= reinterpret_cast<INNER_NODE *>(search_node);
            i_search= search_inner_node->get_insert_find_position(key);
            search_node= search_inner_node->children[i_search];
        }

        record_node= reinterpret_cast<LEAF_NODE *>(search_node);
        assert(NODE_TYPE_LEAF== record_node->node_type);

        return record_node->find_key(key,ret_value);
    }

    //=======================
	//torles belso fuggvenyei
	//=======================

	void
	merge_nodes(
        const unsigned int merge_depth,
        BP_NODE *dst_left_node,
        BP_NODE *src_right_node,
        BP_NODE *anchor
    ){
        TKEY anchor_key;//a jobboldali csomopont fo (legelso kulcsa)
                        //az anchorba kerul
        unsigned int anchor_key_i;

        assert(merge_depth<= depth);
        assert(NULL!= dst_left_node);
        assert(NULL!= src_right_node);
        assert(NULL!= anchor);
        assert(0!= dst_left_node->n_key);
        assert(0!= src_right_node->n_key);
        assert(0!= anchor->n_key);

        if(merge_depth!= 0){
            assert(BP_ORDER>= (dst_left_node->n_key+src_right_node->n_key));
            //belso csomopont
            dst_left_node->inc_n_key(); //az elso elem a jobb csomopontbol
                                        //biztosan a balba kerul
            //az uj fo kulcs, mivel a legelso biztosan atkerul balra
            anchor_key= src_right_node->get_key(1);
            //megkeressuk, hogy hova kerulne az anchorba
            anchor_key_i= anchor->get_insert_find_position(anchor_key);
            //a jobb oldali csomopont kulcsa az anchorban
            anchor_key= anchor->get_key(anchor_key_i);
            //bemasoljuk a bal oldaliba az elso jobb oldali elemet
            ///(reinterpret_cast<INNER_NODE *>(dst_left_node))-> INNER_NODE->

            //frissitjuk az anchorban a jobb oldali csomopont kulcsat



            //a jobb oldalon levo csomopont legbalodalibb kulcsa atkerul a
            //bal oldali csompontba, a legjobboldalibb pozicioba
            dst_left_node->inc_n_key();
        }else{
            assert(N_RECORD>= (dst_left_node->n_key+src_right_node->n_key));
            //level csomopont
        }
    }

	//
	// find_remove_rebalance
    //
    const
    bool
    find_remove_rebalance(
        const TKEY &key,
        const unsigned int remove_depth,
        BP_NODE *node,
        BP_NODE *left_node,
        BP_NODE *right_node,
        INNER_NODE *left_anchor,
        INNER_NODE *right_anchor,
        TVALUE *ret_value= NULL
    )const{
/**
 * megkeres egy adott elemet a faban, kulcs alapjan
 * \return true ha az adott kulcsu elem letezik a faban TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * \param record_leaf mutatot terit vissza arra a level csomopontra, amiben talahato az adott kulcsu elem
 * \param element_index az elem indexe a visszateritett level csomopontban
 */
        ///BP_NODE *remove_node;
        BP_NODE *next_node;
        BP_NODE *next_left_node;
        BP_NODE *next_right_node;
        INNER_NODE *next_left_anchor;
        INNER_NODE *next_right_anchor;
        unsigned int remove_i;

        assert(remove_depth<= depth);
        assert(NULL!= node);
        /*assert(NULL!= left_node);
        assert(NULL!= right_node);
        assert(NULL!= left_anchor);
        assert(NULL!= right_anchor);*/

        //ellenorizzuk, hogy az adott csomopontot szukseges-e kiegyensulyozni
        if(!node->minimal_sized){
            balance_node= NULL;
        }else if(NULL== balance_node){
            balance_node= node;
            balance_node_type= (0!= remove_depth)?NODE_TYPE_INNER:NODE_TYPE_LEAF;
        }

        if(0!= remove_depth){
            //belso csomopont
            remove_i= node->get_insert_find_position(key);
            next_node= (reinterpret_cast<INNER_NODE *>(node))->get_child(remove_i);

            if(0== remove_i){
                //elso utod
                if(NULL!= left_node){
                    //van bal szomszed
                    next_left_node= (reinterpret_cast<INNER_NODE *>(left_node))->get_last_child();
                }else{
                    //nincs bal szomszed
                    next_left_node= NULL;
                }
                next_left_anchor= left_anchor;
            }else{
                //elso utan talalhato utod
                next_left_node= (reinterpret_cast<INNER_NODE *>(node))->get_child(remove_i-1);

                next_left_anchor= node;
            }
            if(node->n_key== remove_i){
                //utolso utod
                if(NULL!= right_node){
                    //van jobb szomszed
                    next_right_node= (reinterpret_cast<INNER_NODE *>(right_node))->get_first_child();
                }else{
                    //nincs jobb szomszed
                    next_right_node= NULL;
                }
                next_right_anchor= right_anchor;
            }else{
                //utolso elott talalhato utod
                next_right_node= (reinterpret_cast<INNER_NODE *>(node))->get_child(remove_i+1);

                next_right_anchor= node;
            }

            if(find_remove_rebalance(key,
                remove_depth-1,
                next_node,
                next_left_node,
                next_right_node,
                next_left_anchor,
                next_right_anchor,
                ret_value)){
                //az aktualis belso csomopontot is le kell torolni
                node->remove_element(remove_i);
                ///TODO
            }
        }else{
            //level csomopont
            if((reinterpret_cast<LEAF_NODE *>(node))->find_key(key,NULL,&remove_i)){

                ///node->
                //ha szukseges, visszateritjuk az erteket
                if(NULL!= ret_value){
                    ///*ret_value= node->get
                }
                //ha letezik a kulcs, kitoroljuk a rekordbol
                ///(reinterpret_cast<LEAF_NODE *>(node))->remove_element(remove_i);
                ///remove_node= node;
            }else{
                //nem letezik a kulcs
                ///remove_node= NULL;
                balance_node= NULL;

                return false;
            }
        }

        //kiegyensulyozas
        if(NULL!= balance_node){
            //1. eset: a gyokerben vagyunk, le kell szukiteni
            if( (0== depth) &&//ures-e a fa (a gyoker level tipusu)
                (depth== remove_depth) ){//ha a gyokerben vagyunk
                assert(NULL== left_node);
                assert(NULL== right_node);
                assert(NULL== left_anchor);
                assert(NULL== right_anchor);

                ///TODO
            }else if( ((NULL== left_node) || (left_node->minimal_sized)) &&
                ((NULL== right_node) || (right_node->minimal_sized)) ){
                //a ket csomopontot kotelezo modon egyesiteni kell
                ///MERGE
            }
        }
    }

    //=========
    //adattagok
    //=========

    //memoriakezelo
    //automatikusan lefoglal es felszabadit teruletet a node-okhoz
    boost::object_pool<INNER_NODE> inner_pool;
	boost::object_pool<LEAF_NODE>  leaf_pool;

	//gyoker
	//muszaly mutato legyen, mert lehet level vagy belso csomopont is
	BP_NODE *root;
	//a fa melysege
	unsigned int depth;
	//a kulcs-elem parok szama
	unsigned int n_elem;
	//mutato a legelso es legutolso rekordra
	LEAF_NODE *first_leaf;
	LEAF_NODE *last_leaf;

	//az index allomany
    std::string bpti_filepath;
    blockfile *bpti_file;

	//torlesnel lesz hasznalva
	BP_NODE *balance_node;
	NODE_TYPE balance_node_type;

public:
    //konstruktor
    BPlusTree(const char *indexfile_path= "index.dat"):
        root(leaf_pool.construct()),
        depth(0),
        n_elem(0),
        first_leaf(reinterpret_cast<LEAF_NODE *>(root)),
        last_leaf(reinterpret_cast<LEAF_NODE *>(root)),
        bpti_filepath(indexfile_path),
        bpti_file(new blockfile_ansi()),
        tree_file_header((BP_TREE_FILE_HEADER *)(header_block.buffer)){};
    //destruktor
    ~BPlusTree(void){
        close();
    }

    //elemek
    inline
    unsigned int
    size(void)const{
/**
 * \return az elemek szama a faban
 */
        return n_elem;
    }

    //beszuras
    void
    insert(
        const TKEY &key,
        const TVALUE &value
    ){
/**
 * beszur egy elemet a faba
 */
        bool was_split= false;
        TKEY header_key;
        BP_NODE *left= NULL;
        BP_NODE *right= NULL;
        INNER_NODE *root_inner= NULL;

#ifndef NDEBUG
        //std::cout<<"Inserting KEY=["<<key<<"] VALUE=["<<value<<"]"<<std::endl;
#endif // NDEBUG

        if(0== depth){
            //a gyoker level tipusu csomopont
            //direkt modon lehet a elemet beszurni
            assert(NODE_TYPE_LEAF== root->node_type);
            was_split= insert_into_leaf(reinterpret_cast<LEAF_NODE *>(root),
                key,
                value,
                &header_key,
                &left,
                &right);
        }else{
            //a gyoker egy belso csomopont
            //meg kell keresni a beszuras helyet
            assert(NODE_TYPE_INNER== root->node_type);
            was_split= insert_into_inner(reinterpret_cast<INNER_NODE *>(root),
                depth,
                key,
                value,
                &header_key,
                &left,
                &right);
        }

        if(was_split){
            //az eredeti gyoker ketfele valt
            //egy uj gyokeret kell letrehozni
            //a ket uj resz ra fog mutatni
            root= new_inner_node();//uj teruletet foglalunk le a gyokernek
            //az eddigi informacio nem vesz el: header_key tartalmazza az uj keresesi kulcsot
            //left es right a mutatokat a ketteosztott gyoker reszeire
            root_inner= reinterpret_cast<INNER_NODE *>(root);
            root_inner->children[0]= left;//elso utod (kulcstol balra) beallitasa
            root_inner->children[1]= right;//masodik utod (kulcstol jobbra) beallitasa
            root_inner->keys[0]= header_key;
            root_inner->inc_n_key();
            //szulo beallitasa
            left->parent= reinterpret_cast<INNER_NODE *>(root);
            right->parent= reinterpret_cast<INNER_NODE *>(root);
            //az elso es utolso level cimenek beallitasa a faban
            //mivel legfeljebb csak egy ketteosztas tortenhet, eleg csak 1el alrebb tolni
            if(NULL!= first_leaf->prev_leaf_node){
                first_leaf= first_leaf->prev_leaf_node;
            }
            if(NULL!= last_leaf->next_leaf_node){
                last_leaf= last_leaf->next_leaf_node;
            }

            //a fa melyebb lett
            ++depth;
        }

        return;
    }

    //find
    inline
    bool
    find(
        const TKEY &key,
        TVALUE *ret_value
    )const{
/**
 * megkeres egy adott elemet a faban, kulcs alapjan
 * \return true ha az adott kulcsu elem letezik a faban
 */
        assert(NULL!= ret_value);

        return find_element_and_node(key,ret_value);
    }

    //remove
    inline
    bool
    remove(
        const TKEY &key,
        TVALUE *ret_value= NULL
    ){
/**
 * megkeres egy adott elemet a faban, kulcs alapjan, es kitorli (ha letezik)
 * \return true ha az adott kulcsu elem letezett a faban torles elott
 */
        balance_node= NULL;
        //balance_node_type= NODE_TYPE_INNER;

        return false;
    }

    inline
    bool
    create(void){
        if(!rm_success(bpti_file->create(bpti_filepath))){
            return false;
        }

        //write the file header
        if(!rm_success(bpti_file->newblock(header_block))){
            return false;
        }

        //kezdoertekek
        tree_file_header->magic= BP_FILE_MAGIC;
        tree_file_header->tree_order= BP_ORDER;
        tree_file_header->record_order= N_RECORD;
        tree_file_header->first_free_block_id= 0;
        tree_file_header->depth= 0;
        tree_file_header->n_elem= 0;

        //gyoker lefoglalasa
        ///if(!rm_success(bpti_file->newblock(root_block))){
        if(!rm_success(bpti_file->newblock(root->file_block))){
            return false;
        }
        ///tree_file_header->root_block_id= root_block.blockid;
        ///root->file_block.blockid tree_file_header->root_block_id= root_block.blockid;
        ///(reinterpret_cast<LEAF_NODE *>(root))->file_header= (BP_TREE_FILE_LEAF_NODE *)root_block.buffer;
        (reinterpret_cast<LEAF_NODE *>(root))->file_header= (BP_TREE_FILE_LEAF_NODE *)root->file_block.buffer;

        //ures blokk lefoglalasa
        /*if(!rm_success(bpti_file->newblock(first_free_block))){
            return false;
        }
        tree_file_header->first_free_block_id= first_free_block.blockid;*/

        if(!rm_success(bpti_file->writeblock(header_block))){
            return false;
        }
        if(!rm_success(bpti_file->writeblock(root->file_block))){
            return false;
        }
        /*if(!rm_success(bpti_file->writeblock(first_free_block))){
            return false;
        }*/

        return true;
    }

    inline
    bool
    open(void){
        if(!rm_success(bpti_file->open(bpti_filepath))){
            return false;
        }

        //read header
        if(!rm_success(bpti_file->readblock(0,header_block))){
            return false;
        }

        return true;
    }

    inline
    bool
    close(void){
        if(!rm_success(bpti_file->close())){
            return false;
        }

        //verify header
        if(!verify_header()){
            return false;
        }

        return true;
    }

    //iterator
    /*class iterator: iterator_interface<TVALUE>{
    public:
        void
        iterator_interface::first(void){
        }

        void
        iterator_interface::next(void){
        }

        bool
        iterator_interface::is_done(void){
        }
    private:
    };*/
};

#endif // _BP_TREE_H_
