#ifndef _ITERATOR_INTERFACE_H_
#define _ITERATOR_INTERFACE_H_

//iterator interfesz
template<
class TITEM
>class iterator_interface{
public:
    virtual
    void
    first(void)= 0;

    virtual
    void
    next(void)= 0;

    virtual
    bool
    is_done(void)= 0;
protected:
    iterator_interface(void);
};

#endif // _ITERATOR_INTERFACE_H_
