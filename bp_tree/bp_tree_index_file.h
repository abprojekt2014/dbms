#include "bp_tree.h"
#include "../include/dbtypes.h"
#include <string>
#include "../recordmanager/ansi/blockfile_ansi.h"

#define BP_TREE_INDEX_FILE_BP_ORDER 4
#define BP_TREE_INDEX_FILE_N_RECORD 3

/*template<
typename TKEY
>class BPlusTreeIndexFile:
    public BPlusTree<
        TKEY,
        recordid_t,
        BP_TREE_INDEX_FILE_BP_ORDER,
        BP_TREE_INDEX_FILE_N_RECORD>
{
private:
    static const unsigned int magic= 'ITPB';
#pragma pack(push,1)
    struct BP_TREE_FILE_HEADER{
        unsigned int magic;

        blockid_t root_block;
    };
#pragma pop
    //az index allomany
    const std::string bpti_filepath;
    blockfile_ansi bpti_file;
protected:
public:
    //konstruktor
    BPlusTreeIndexFile(std::string indexfile_path):
        bpti_filepath(indexfile_path){}
    //destruktor
    ~BPlusTreeIndexFile(){
        close();
    }

    inline
    rm_error
    create(void){
        rm_error error= bpti_file.create(bpti_filepath);
        assert(rm_success(error));

        return error;
    }

    inline
    rm_error
    open(void){
        rm_error error= bpti_file.open(bpti_filepath);
        assert(rm_success(error));

        return error;
    }

    inline
    rm_error
    close(void){
        rm_error error= bpti_file.close();
        assert(rm_success(error));

        return error;
    }
};*/
