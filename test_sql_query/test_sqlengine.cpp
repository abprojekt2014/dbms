#include <CppUnitTest.h>  
#include <cassert>
#include <string>
#include "../sql_query/SqlEngine.h"
#include "../sql_query/CreateTableCommand.h"
#include "../sql_parser/SQLParser.h"
#include "../record_manager/table_manager.h"
#include "../index_manager/index_manager.h"

using namespace std;
using namespace hsql;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

TEST_CLASS(TestSqlEngine_Create)
{
public:
    TEST_CLASS_INITIALIZE(TestSetup)
    {
        remove("disciplines.tbl");
        remove("disciplines2.tbl");
        remove("system_columns.tbl");
        remove("system_tables.tbl");
        remove("system_indices.tbl");
        remove("idx_disciplines_DiscID.hdx");
    }

    TEST_CLASS_CLEANUP(TestCleanup)
    {
        remove("disciplines.tbl");
        remove("disciplines2.tbl");
        remove("system_columns.tbl");
        remove("system_tables.tbl");
        remove("system_indices.tbl");
        remove("idx_disciplines_DiscID.hdx");
    }

    TEST_METHOD(CreateTableSimpleQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5), DName varchar(30), CreditNr int); ");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->succesfull);
        Assert::IsTrue(result->message.empty());
        Assert::IsFalse(result->statements.empty());

        Assert::AreEqual<int>(result->statements[0]->type(), hsql::kStmtCreate);

        hsql::CreateStatement* stmt = static_cast<hsql::CreateStatement*>(result->statements[0]);
        Assert::AreEqual<string>(stmt->tableName, "disciplines");
        Assert::AreEqual<size_t>(stmt->columns->size(), 3);
        Assert::AreEqual<string>((*stmt->columns)[0]->name, "DiscID");
        Assert::IsFalse((*stmt->columns)[0]->keyconstraints->foreignkey);
        Assert::IsFalse((*stmt->columns)[0]->keyconstraints->primarykey);
        Assert::AreEqual<string>((*stmt->columns)[1]->name, "DName");
        Assert::AreEqual<string>((*stmt->columns)[2]->name, "CreditNr");

        Assert::IsNotNull(tablemanager::getinstance().gettable("disciplines"));
        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }

    TEST_METHOD(CreateTableFailingQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) UNKNOWN FIELD, DName varchar(30), CreditNr int); ");

        Assert::IsNotNull(result);
        Assert::IsFalse(result->succesfull);
        Assert::IsFalse(result->message.empty());
        Assert::IsTrue(result->statements.empty());
    }

    TEST_METHOD(CreateTableQueryTwice)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5), DName varchar(30), CreditNr int); ");
        delete result;
        result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5), DName varchar(30), CreditNr int); ");
        Assert::IsFalse(result->succesfull);
        delete result;
        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }

    TEST_METHOD(CreateTableWithDoubleColumDefinitionQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5), DName varchar(30), CreditNr int, DName varchar(30)); ");
        Assert::IsFalse(result->succesfull);
        delete result;
        Assert::IsNull(tablemanager::getinstance().gettable("disciplines"));
    }

    TEST_METHOD(CreateTableInvalidLengthQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(-1), DName varchar(30), CreditNr int); ");
        Assert::IsFalse(result->succesfull);
        delete result;
        Assert::IsNull(tablemanager::getinstance().gettable("disciplines"));
    }

    TEST_METHOD(CreateTableWithManyPrimaryKeysQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines (DiscID varchar(5) PRIMARY KEY, DName varchar(30) PRIMARY KEY, CreditNr int);");
        Assert::IsFalse(result->succesfull);
        delete result;
        Assert::IsNull(tablemanager::getinstance().gettable("disciplines"));
    }

    TEST_METHOD(CreateTableWithPrimaryKeyQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int); ");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->succesfull);
        Assert::IsTrue(result->message.empty());
        Assert::IsFalse(result->statements.empty());

        Assert::AreEqual<int>(result->statements[0]->type(), hsql::kStmtCreate);

        hsql::CreateStatement* stmt = static_cast<hsql::CreateStatement*>(result->statements[0]);
        Assert::AreEqual<string>(stmt->tableName, "disciplines");
        Assert::AreEqual<size_t>(stmt->columns->size(), 3);
        Assert::AreEqual<string>((*stmt->columns)[0]->name, "DiscID");
        Assert::IsTrue((*stmt->columns)[0]->keyconstraints->primarykey);
        Assert::AreEqual<string>((*stmt->columns)[1]->name, "DName");
        Assert::AreEqual<string>((*stmt->columns)[2]->name, "CreditNr");

        table_t* t;
        Assert::IsNotNull(t = tablemanager::getinstance().gettable("disciplines"));

        index_t* indexResolved = indexmanager::getinstance().openindex(index_t::generatename("disciplines", "DiscID"));
        Assert::IsNotNull(indexResolved);
        indexResolved->close();

        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }

    TEST_METHOD(CreateTableWithForeignKeyQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int); ");
        result = SqlEngine::query("CREATE TABLE disciplines2(DiscID varchar(5) FOREIGN KEY REFERENCES disciplines(DiscID), StudentNr int); ");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->succesfull);
        Assert::IsTrue(result->message.empty());
        Assert::IsFalse(result->statements.empty());

        Assert::AreEqual<int>(result->statements[0]->type(), hsql::kStmtCreate);

        hsql::CreateStatement* stmt = static_cast<hsql::CreateStatement*>(result->statements[0]);
        Assert::AreEqual<string>(stmt->tableName, "disciplines2");
        Assert::AreEqual<size_t>(stmt->columns->size(), 2);
        Assert::AreEqual<string>((*stmt->columns)[0]->name, "DiscID");
        Assert::IsTrue((*stmt->columns)[0]->keyconstraints->foreignkey);
        Assert::AreEqual<string>((*stmt->columns)[1]->name, "StudentNr");

        table_t* t;
        Assert::IsNotNull(t = tablemanager::getinstance().gettable("disciplines"));

        index_t* indexResolved = indexmanager::getinstance().openindex(index_t::generatename("disciplines", "DiscID"));
        Assert::IsNotNull(indexResolved);
        indexResolved->close();

        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines2"));
    }

    TEST_METHOD(CreateTableForeignKeyTypeMismatchQuery)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int); ");
        result = SqlEngine::query("CREATE TABLE disciplines2(DiscID int FOREIGN KEY REFERENCES disciplines(DiscID), StudentNr int); ");

        Assert::IsNotNull(result);
        Assert::IsFalse(result->succesfull);
        
        Assert::AreEqual<string>(result->message, "Column type mismatch for foreign key 'DiscID'");

        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }

    TEST_METHOD(CreateTableWithForeignKeyTypeMismatchStringLength)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int); ");
        result = SqlEngine::query("CREATE TABLE disciplines2(DiscID varchar(4) FOREIGN KEY REFERENCES disciplines(DiscID), StudentNr int); ");

        Assert::IsNotNull(result);
        Assert::IsFalse(result->succesfull);

        Assert::AreEqual<string>(result->message, "Column type mismatch for foreign key 'DiscID'");

        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }

    TEST_METHOD(CreateTableWithForeignKeyString)
    {
        SqlQueryResult* result = SqlEngine::query("CREATE TABLE disciplines(DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int); ");
        result = SqlEngine::query("CREATE TABLE disciplines2(DiscID varchar(5) FOREIGN KEY REFERENCES disciplines(DiscID), StudentNr int); ");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->succesfull);

        Assert::IsTrue(tablemanager::getinstance().deletetable("disciplines"));
    }
};

TEST_CLASS(TestSqlEngine_Insert)
{
public:
    TEST_CLASS_INITIALIZE(TestSetup)
    {
        remove("disciplines.tbl");
        remove("system_columns.tbl");
        remove("system_tables.tbl");
        remove("system_indices.tbl");
        remove("inserttest.tbl");
        remove("inserttestpk.tbl");
        remove("inserttestfk.tbl");
        remove("idx_inserttestpk_id.hdx");
        remove("idx_inserttestfk_id.hdx");

        SqlQueryResult* result;
        result = SqlEngine::query("Create table inserttest (id int, name varchar(50))");
        Assert::IsTrue(result->succesfull);

        result = SqlEngine::query("Create table inserttestpk (id int primary key, name varchar(50))");
        Assert::IsTrue(result->succesfull);

        result = SqlEngine::query("Create table inserttestfk (id int primary key, pkid int foreign key REFERENCES inserttestpk(id))");
        Assert::IsTrue(result->succesfull);
    }

    TEST_CLASS_CLEANUP(TestCleanup)
    {
        remove("disciplines.tbl");
        remove("system_columns.tbl");
        remove("system_tables.tbl");
        remove("system_indices.tbl");
        remove("inserttest.tbl");
        remove("inserttestpk.tbl");
        remove("inserttestfk.tbl");
        remove("idx_inserttestpk_id.hdx");
        remove("idx_inserttestfk_id.hdx");
    }

    void DeleteRows(table_t* table)
    {
        iterator_t* it = table->get_iterator();
        for (recordid_t rid = it->first();
             (rid != invalid_recordid) && !it->end();
             rid = it->next())
        {
            table->delete_row(rid);
        }
    }

    int GetNumberOfRows(table_t* table)
    {
        int numberOfRows = 0;
        iterator_t* it = table->get_iterator();

        for (recordid_t rid = it->first();
            (rid != invalid_recordid) && !it->end();
            rid = it->next())
        {
            numberOfRows++;
        }

        return numberOfRows;
    }

    TEST_METHOD(InsertIntoNonExistingTable)
    {
        table_t* table = tablemanager::getinstance().gettable("inserttest");
        Assert::IsNotNull(table);
        Assert::IsTrue(table->open());

        DeleteRows(table);
        Assert::AreEqual(GetNumberOfRows(table), 0);

        SqlQueryResult* result = SqlEngine::query("Insert into inserasdasdttest (id, name) values (1, 'InsertIntoExistingTable')");
        Assert::IsFalse(result->succesfull);
        Assert::AreEqual<string>(result->message, "Table 'inserasdasdttest' does not exist.");

        table->close();

        delete result;
    }

    TEST_METHOD(InsertIntoNonExistingColumn)
    {
        table_t* table = tablemanager::getinstance().gettable("inserttest");
        Assert::IsNotNull(table);
        Assert::IsTrue(table->open());

        DeleteRows(table);
        Assert::AreEqual(GetNumberOfRows(table), 0);

        SqlQueryResult* result = SqlEngine::query("Insert into inserttest (idfx, name) values (1, 'InsertIntoExistingTable')");
        Assert::IsFalse(result->succesfull);
        Assert::AreEqual<string>(result->message, "Column 'idfx' is not defined for table 'inserttest'");

        table->close();

        delete result;
    }

    TEST_METHOD(InsertIsCaseInsensitiveForColumns)
    {
        table_t* table = tablemanager::getinstance().gettable("inserttest");
        Assert::IsNotNull(table);
        Assert::IsTrue(table->open());

        DeleteRows(table);
        Assert::AreEqual(GetNumberOfRows(table), 0);

        SqlQueryResult* result = SqlEngine::query("Insert into inserttest (Id, nAmE) values (1, 'InsertIntoExistingTable')");
        Assert::IsTrue(result->succesfull);

        table->close();

        delete result;
    }

    TEST_METHOD(InsertWithDuplicateColumns)
    {
        table_t* table = tablemanager::getinstance().gettable("inserttest");
        Assert::IsNotNull(table);
        Assert::IsTrue(table->open());

        DeleteRows(table);
        Assert::AreEqual(GetNumberOfRows(table), 0);

        SqlQueryResult* result = SqlEngine::query("Insert into inserttest (id, id, name) values (1, 'InsertIntoExistingTable')");
        Assert::IsFalse(result->succesfull);
        Assert::AreEqual<string>(result->message, "Duplicate column entry found for column 'id'");

        table->close();

        delete result;
    }

    TEST_METHOD(InsertIntoExistingTable)
    {
        table_t* table = tablemanager::getinstance().gettable("inserttest");
        Assert::IsNotNull(table);
        Assert::IsTrue(table->open());

        DeleteRows(table);
        Assert::AreEqual(GetNumberOfRows(table), 0);

        SqlQueryResult* result = SqlEngine::query("Insert into inserttest (id, name) values (1, 'InsertIntoExistingTable')");
        Assert::IsNotNull(result);
        Assert::IsTrue(result->succesfull);
        Assert::AreEqual<int>(result->statements.size(), 1);
        Assert::AreEqual<int>(result->statements[0]->type(), kStmtInsert);

        Assert::AreEqual(GetNumberOfRows(table), 1);

        iterator_t* it(table->get_iterator());
        recordid_t rid;
        Assert::AreNotEqual<recordid_t>(rid = it->first(), invalid_recordid);

        BYTE binrow[500];   // the length should be got from the catalog

        Assert::IsTrue(table->read_row(rid, binrow));
        Assert::AreEqual<int>(*(int32_t*)(void*)binrow, 1);

        std::string name((char*)&binrow[4]);

        Assert::AreEqual<std::string>(name, "InsertIntoExistingTable");

        Assert::IsNotNull(table);
        table->close();

        delete result;
    }
};
