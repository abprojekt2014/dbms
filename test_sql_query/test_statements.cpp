#include <CppUnitTest.h>  
#include <string>
#include "..\sql_parser\SQLParser.h"
#include <cassert>

using namespace std;
using namespace hsql;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

TEST_CLASS(TestCreateStatement)
{
public:
    TEST_METHOD(CreateTableStatement)
    {
        SQLParserResult* result;
        SQLStatement* stmt;
        CreateStatement* cstmt;

        result = SQLParser::parseSQL("CREATE TABLE disciplines (DiscID varchar(5), DName varchar(30), CreditNr int);");

        assert(result->isValid);
        assert(result->size() == 1);

        stmt = result->getStatement(0);

        Assert::AreEqual((int)stmt->type(), (int)hsql::StatementType::kStmtCreate);

        cstmt = dynamic_cast<CreateStatement*>(stmt);

        Assert::AreEqual(cstmt->tableName.c_str(), "disciplines");
        Assert::IsTrue(cstmt->filePath.empty());
        Assert::AreEqual((int)cstmt->columns->size(), 3);

        Assert::AreEqual(cstmt->columns->at(0)->name.c_str(), "DiscID");
        Assert::AreEqual((int)cstmt->columns->at(0)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(0)->length, 5);
        Assert::IsFalse(cstmt->columns->at(0)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(0)->keyconstraints->foreignkey);

        Assert::AreEqual(cstmt->columns->at(1)->name.c_str(), "DName");
        Assert::AreEqual((int)cstmt->columns->at(1)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(1)->length, 30);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->foreignkey);

        Assert::AreEqual(cstmt->columns->at(2)->name.c_str(), "CreditNr");
        Assert::AreEqual((int)cstmt->columns->at(2)->type, (int)ColumnDefinition::DataType::INT);
        Assert::IsFalse(cstmt->columns->at(2)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(2)->keyconstraints->foreignkey);

        delete result;
    }


    TEST_METHOD(CreateTableWithPrimaryKeyStatement)
    {
        SQLParserResult* result;
        SQLStatement* stmt;
        CreateStatement* cstmt;

        result = SQLParser::parseSQL("CREATE TABLE disciplines (DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int);");

        Assert::IsTrue(result->isValid);
        Assert::AreEqual((int)result->size(), 1);

        stmt = result->getStatement(0);

        Assert::AreEqual((int)stmt->type(), (int)hsql::StatementType::kStmtCreate);

        cstmt = dynamic_cast<CreateStatement*>(stmt);

        Assert::AreEqual(cstmt->tableName.c_str(), "disciplines");
        Assert::IsTrue(cstmt->filePath.empty());
        Assert::AreEqual((int)cstmt->columns->size(), 3);

        Assert::AreEqual(cstmt->columns->at(0)->name.c_str(), "DiscID");
        Assert::AreEqual((int)cstmt->columns->at(0)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(0)->length, 5);
        Assert::IsTrue(cstmt->columns->at(0)->keyconstraints->primarykey);

        Assert::AreEqual(cstmt->columns->at(1)->name.c_str(), "DName");
        Assert::AreEqual((int)cstmt->columns->at(1)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(1)->length, 30);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->foreignkey);

        Assert::AreEqual(cstmt->columns->at(2)->name.c_str(), "CreditNr");
        Assert::AreEqual((int)cstmt->columns->at(2)->type, (int)ColumnDefinition::DataType::INT);
        Assert::IsFalse(cstmt->columns->at(2)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(2)->keyconstraints->foreignkey);

        delete result;
    }

    TEST_METHOD(CreateTableWithForeignKeyStatement)
    {
        SQLParserResult* result;
        SQLStatement* stmt;
        CreateStatement* cstmt;

        result = SQLParser::parseSQL("CREATE TABLE disciplines (DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int FOREIGN KEY REFERENCES Credits(CreditId));");

        Assert::IsTrue(result->isValid);
        Assert::AreEqual((int)result->size(), 1);

        stmt = result->getStatement(0);

        Assert::AreEqual((int)stmt->type(), (int)hsql::StatementType::kStmtCreate);

        cstmt = dynamic_cast<CreateStatement*>(stmt);

        Assert::AreEqual(cstmt->tableName.c_str(), "disciplines");
        Assert::IsTrue(cstmt->filePath.empty());
        Assert::AreEqual((int)cstmt->columns->size(), 3);

        Assert::AreEqual(cstmt->columns->at(0)->name.c_str(), "DiscID");
        Assert::AreEqual((int)cstmt->columns->at(0)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(0)->length, 5);
        Assert::IsTrue(cstmt->columns->at(0)->keyconstraints->primarykey);
        
        Assert::AreEqual(cstmt->columns->at(1)->name.c_str(), "DName");
        Assert::AreEqual((int)cstmt->columns->at(1)->type, (int)ColumnDefinition::DataType::VARCHAR);
        Assert::AreEqual((int)cstmt->columns->at(1)->length, 30);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->primarykey);
        Assert::IsFalse(cstmt->columns->at(1)->keyconstraints->foreignkey);
        
        Assert::AreEqual(cstmt->columns->at(2)->name.c_str(), "CreditNr");
        Assert::AreEqual((int)cstmt->columns->at(2)->type, (int)ColumnDefinition::DataType::INT);
        Assert::IsFalse(cstmt->columns->at(2)->keyconstraints->primarykey);
        Assert::IsTrue(cstmt->columns->at(2)->keyconstraints->foreignkey);
        Assert::AreEqual(cstmt->columns->at(2)->keyconstraints->table.c_str(), "Credits");
        Assert::AreEqual(cstmt->columns->at(2)->keyconstraints->column.c_str(), "CreditId");

        delete result;
    }
};

TEST_CLASS(TestInsertStatement)
{
public:
    TEST_METHOD(InsertValuesSimple)
    {
        SQLParserResult* result;
        SQLStatement* stmt;
        InsertStatement* istmt;

        result = SQLParser::parseSQL("Insert into disciplines (DiscID, DName, CreditNr) values (1, 'BMC', 7)");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->isValid);
        Assert::AreEqual((int)result->size(), 1);

        stmt = result->getStatement(0);

        Assert::AreEqual<int>(stmt->type(), hsql::StatementType::kStmtInsert);

        istmt = dynamic_cast<InsertStatement*>(stmt);
        Assert::AreEqual<int>(istmt->type, hsql::InsertStatement::InsertType::kInsertValues);
        Assert::AreEqual<string>(istmt->tableName, "disciplines");
        Assert::AreEqual<int>(istmt->columns->size(), 3);
        Assert::AreEqual<int>(istmt->values->size(), 3);
        Assert::IsNull(istmt->select);

        Assert::AreEqual<string>(*istmt->columns->at(0), "DiscID");
        Assert::AreEqual<string>(*istmt->columns->at(1), "DName");
        Assert::AreEqual<string>(*istmt->columns->at(2), "CreditNr");

        Assert::AreEqual<int>(istmt->values->at(0)->type, hsql::ExprType::kExprLiteralInt);
        Assert::AreEqual<int>(istmt->values->at(0)->ival, 1);
        
        Assert::AreEqual<int>(istmt->values->at(1)->type, hsql::ExprType::kExprLiteralString);
        Assert::AreEqual<string>(istmt->values->at(1)->name, "BMC");
        
        Assert::AreEqual<int>(istmt->values->at(2)->type, hsql::ExprType::kExprLiteralInt);
        Assert::AreEqual<int>(istmt->values->at(2)->ival, 7);
    }

    TEST_METHOD(InsertBySelectSimple)
    {
        SQLParserResult* result;
        SQLStatement* stmt;
        InsertStatement* istmt;

        result = SQLParser::parseSQL("Insert into disciplines select id, name, credit from cache where id = DiscId");

        Assert::IsNotNull(result);
        Assert::IsTrue(result->isValid);
        Assert::AreEqual((int)result->size(), 1);

        stmt = result->getStatement(0);

        Assert::AreEqual<int>(stmt->type(), hsql::StatementType::kStmtInsert);

        istmt = dynamic_cast<InsertStatement*>(stmt);
        Assert::AreEqual<int>(istmt->type, hsql::InsertStatement::InsertType::kInsertSelect);

        /// Will not support this type of query
    }
};
