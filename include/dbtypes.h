//
//  Author: Fulop Botond
//  Date: 2014. 02. 26
//
//  Description: Basic Windows type definitions

#ifndef _DBTYPES_H_
#define _DBTYPES_H_

// to have the ANSI C types
#include <stdint.h>

#if !(defined(_WIN32)) && !(defined(_WIN64))
//
//  Fixed length types
//

// unsigned types
typedef uint8_t  BYTE, *PBYTE;
typedef uint16_t WORD, *PWORD;
typedef uint32_t DWORD, *PDWORD;
typedef uint64_t QWORD, *PQWORD;

// signed types
typedef int8_t   CHAR, *PCHAR;
typedef int16_t  SHORT, *PSHORT;
typedef int32_t  INT, *PINT;
typedef int32_t  LONG, *PLONG;
typedef int64_t  LONGLONG, *PLONGLONG;

// pointer size types (32 bit length on x86 builds/64 on x64)
typedef void*    PVOID;

#ifdef AMD64
typedef QWORD       SIZE_T, *PSIZE_T;
typedef LONGLONG    SSIZE_T, *PSSIZE_T;
#else
typedef DWORD       SIZE_T, *PSIZE_T;
typedef LONG        SSIZE_T, *PSSIZE_T;
#endif

#else
// on windows we use the windows headers
#include <windows.h>
typedef uint64_t QWORD, *PQWORD;
#endif
//
// RecordManager basic types
//

// With DWORD we can address 4Gb in a file
typedef DWORD recordid_t;

//
// IndexManager basic types
//

//
// Statistics related types
//
struct statistics_t
{
    DWORD block_read_count;
    DWORD block_write_count;
    double time_in_ms;
};

typedef statistics_t* pstatistics_t;

#endif //_DBTYPES_H_