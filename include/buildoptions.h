//
//  Author: Fulop Botond
//  Date: 2014. 02. 26
//
//  Description: This file contains the compiletime configurations for the SquareDb project
//

#ifndef _BUILDOPTIONS_H_
#define _BUILDOPTIONS_H_

//
//  GLOBAL OPTIONS
//

//...

//
//  RECORD MANAGER defines
//

#define USE_4KB_BLOCK_SIZE
//#define USE_8KB_BLOCK_SIZE

//
//  INDEXING COMPONENT defines
//

//...

//
// Verifications and enabled/disabled macros
//

#if (defined(USE_8KB_BLOCK_SIZE) && defined(USE_4KB_BLOCK_SIZE))
#error ("Boti: Please use only one Record Manager block size!")
#endif

#if (!defined(USE_8KB_BLOCK_SIZE) && !defined(USE_4KB_BLOCK_SIZE))
#error ("Boti: Please define one of the following constants: USE_8KB_BLOCK_SIZE, USE_4KB_BLOCK_SIZE")
#endif

#endif //_BUILDOPTIONS_H_