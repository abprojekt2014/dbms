#include "test_catalog.h"
#include "test.h"
#include "../recordmanager/rm_types.h"
#include "../recordmanager/binary_catalog.h"
#include "../recordmanager/table_types.h"
#include <cstdio>
#include <string.h>

#define TST_1_COL_NR 5

void build_bin_column(bincolumn_t* col)
{
    static int _size = 4;
    static int _col_idx = 0;


    _size += 1;

    col->type = ctInteger;
    col->size = _size;
    strcpy((char*)col->name, "Column");
    col->name[6] = (BYTE)('0' + _col_idx);
    col->name[7] = 0;
    _col_idx++;

    col->attributes.auto_increment = false;
    col->attributes.primary_key = false;
    col->attributes.indexed = false;
    col->attributes.foreign_key = false;

    col->auto_increment_current = 0;
}

bool TestCatalogCreateFromBlock()
{
    bool res = true;
    block_t bl;
    pbincatalog_t bincat;
    std::vector<column_t*> cols;

    memset(&bl, 0, sizeof(bl));
    bincat = (pbincatalog_t)&bl.buffer[0];
    memcpy(bincat->magic, bincat_magic, sizeof(bincat_magic));

    bincat->column_nr = TST_1_COL_NR;

    for (int i=0; i<TST_1_COL_NR; i++)
    {
        build_bin_column(&bincat->columns[i]);
    }

    catalog c(bl);
    cols = c.columns();

    if (cols.size() != TST_1_COL_NR)
    {
        res = false;
    }

    for (column_t* col : cols)
    {
        static int i = 0;
        if (col->name[6] != '0' + i)
        {
            res = false;
            break;
        }
        i++;
    }

    return res;
}

bool TestCatalogCreate()
{
    bool final_res = true;
    gSubtestStarted++;

    printf("Catalog create:\n");

    printf("1. from block ");
    if (TestCatalogCreateFromBlock())
    {
        printf("OK\n");
    }
    else
    {
        final_res = false;
        printf("FAILED\n");
    }

    //printf("\t2. copy constructor\n");

    if (final_res)
    {
        gSubtestSucceeded++;
    }
    else
    {
        gSubtestFailed++;
    }

    return final_res;
}

bool TestCatalogOperations()
{
    bool final_res = true;
    bool res = true;
    catalog c;

    printf("Catalog operations:\n");
    printf("1. Add column ");
    c.addcolumn("TstCol", ctBoolean, 1);

    if (c.columns().size() != 1)
    {
        res = false;
    }
    else
    {
        column_t* col = c.columns()[0];

        if (col->name != std::string("TstCol"))
        {
            res = false;
        }
    }

    if (res)
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
        final_res = false;
    }

    res = true;
    printf("2. set autoincrement ");
    column_t* col = c.columns()[0];
    c.setautoincrement(col, 77);
    if (col->attributes.auto_increment != true || col->auto_increment_current != 77)
    {
        res = false;
    }

    if (res)
    {
        printf("OK\n");
    }
    else
    {
        final_res = false;
        printf("FAILED\n");
    }

    return final_res;
}

bool TestCatalog()
{
    bool final_res = true;
    gTestStarted++;

    printf("\n\nTESTING CATALOG\n");
    printf("===============\n");

    final_res &= TestCatalogCreate();
    final_res &= TestCatalogOperations();

    if (final_res)
    {
        gTestSucceeded++;
    }
    else
    {
        gTestFailed++;
    }

    return final_res;
}
