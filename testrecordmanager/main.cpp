#include <cstdio>
#include <string>

#include "../include/buildoptions.h"
#include "test_blockfile.h"
#include "test_table_manager.h"
#include "test_catalog.h"
#include "test_table.h"

int gTestStarted = 0;
int gTestSucceeded = 0;
int gTestFailed = 0;
int gSubtestStarted = 0;
int gSubtestSucceeded = 0;
int gSubtestFailed = 0;

int main(int argc, char **argv)
{
    TestBlockfile();
    TestTableManager();
    TestCatalog();
    TestTable();

    printf("\n\nSubtests started:   %d\n", gSubtestStarted);
    printf("Subtests failed:    %d\n", gSubtestFailed);
    printf("Subtests succeeded: %d\n\n", gSubtestSucceeded);

    printf("Tests started:      %d\n", gTestStarted);
    printf("Tests failed:       %d\n", gTestFailed);
    printf("Tests succeeded:    %d\n", gTestSucceeded);

	return 0;
}
