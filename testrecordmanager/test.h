#ifndef _TEST_H_
#define _TEST_H_

extern int gTestStarted;
extern int gTestSucceeded;
extern int gTestFailed;
extern int gSubtestStarted;
extern int gSubtestSucceeded;
extern int gSubtestFailed;

#endif // _TEST_H_