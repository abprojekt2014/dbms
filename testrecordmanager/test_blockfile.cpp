#include "test_blockfile.h"

#include <cstdio>
#include <string.h>

#include "../recordmanager/blockfile.h"
#include "../recordmanager/ansi/blockfile_ansi.h"
#include "test.h"

#if defined _WIN32 || defined _WIN64
#include "../recordmanager/windows/blockfile_win.h"
#endif

const std::string test_file = "test.bin";

unsigned int gTestBlockNr = 10000;

bool test_basic_block_file(std::string implementationname, blockfile* f)
{
    rm_error err;
    block_number_t bn = 0;
    block_t bl;
    bool failed = false;

    gSubtestStarted++;

    printf("TESTCASE FOR IMPLEMENTATION: %s\n", implementationname.c_str());
    printf("===========================================\n");
    printf("Setup\n");
    remove(test_file.c_str());

    printf("1. Creating new block file ");
    err = f->create(test_file.c_str());
    if (!rm_success(err))
    {
        printf("FAILED %d\n", err);
        failed = true;
    }
    else
    {
        f->close();

        FILE* t = fopen(test_file.c_str(), "rb");
        if (t)
        {
            printf("OK\n");
            fclose(t);
        }
        else
        {
            printf("FAILED\n");
            failed = true;
        }
    }

    printf("2. Opening block file ");
    err = f->open(test_file.c_str());
    if (!rm_success(err))
    {
        printf("FAILED %d\n", err);
        failed = true;
    }
    else
    {
        bn = f->numerofblocks();
        if (bn != 0)
        {
            printf("FAILED\n");
            failed = true;
        }
        else
        {
            printf("OK\n");
        }
    }

    printf("3. Creating new blocks ");
    for (unsigned int i=0; i<gTestBlockNr; i++)
    {
        err = f->newblock(bl);
        if (!rm_success(err))
        {
            printf("FAILED %d\n", err);
            failed = true;
            break;
        }
        else
        {
            if (bl.blockid != i)
            {
                printf("%dth FAILED\n", i);
                failed = true;
                err = errUnknownError;
                break;
            }
        }
    }

    if (rm_success(err))
    {
        printf("OK\n");
    }

    printf("4. Updating blocks ");
    for (unsigned int i=0; i<gTestBlockNr; i++)
    {
        bl.blockid = i;
        memset(bl.buffer, i%0xFF, sizeof(bl.buffer));

        err = f->writeblock(bl);
        if (!rm_success(err))
        {
            printf("FAILED %d\n", err);
            failed = true;
            break;
        }
    }

    if (rm_success(err))
    {
        printf("OK\n");
    }

    printf("4. Checking blocks ");
    for (unsigned int i=0; i<gTestBlockNr; i++)
    {
        err = f->readblock(i, bl);
        if (!rm_success(err))
        {
            printf("FAILED %d\n", err);
            failed = true;
            break;
        }

        if (bl.buffer[0] != i%0xFF)
        {
            printf("FAILED Wrong byte in record: %d\n", i);
            failed = true;
            break;
        }
    }

    if (rm_success(err))
    {
        printf("OK\n");
    }

    printf("\nCleanup\n\n");
    f->close();

    if (remove(test_file.c_str()) < 0)
    {
        printf("Cannot delete the file!\n");
        remove(test_file.c_str());
    }

    if (failed)
    {
        gSubtestFailed++;
    }
    else
    {
        gSubtestSucceeded++;
    }

    return !failed;
}

#if defined _WIN32 || defined _WIN64
bool TestBlockfileWindows()
{
    int res;
    blockfile* f = new blockfile_win();

    res = test_basic_block_file("Windows", f);
    delete f;

    return res;
}
#endif

bool TestBlockfileAnsi()
{
    bool res = false;
    blockfile* f = new blockfile_ansi();

    res = test_basic_block_file("Ansi", f);
    delete f;

    return res;
}

bool TestBlockfile()
{
    int failed = 0;

    gTestStarted++;

    printf("TESTING BLOCKFILE IMPLEMENTATIONS\n");
    printf("=================================\n\n");

#if defined _WIN32 || defined _WIN64
    if(!TestBlockfileWindows())
    {
       failed = true;
    }
#endif

    if(!TestBlockfileAnsi())
    {
       failed = true;
    }

    if (failed)
    {
        gTestFailed++;
    }
    else
    {
        gTestSucceeded++;
    }

    return !failed;
}
