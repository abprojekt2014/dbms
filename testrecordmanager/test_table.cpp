#include "test_table.h"
#include "test.h"
#include "../recordmanager/table_manager.h"
#include "../recordmanager/serializer.hpp"
#include <cstdio>

static serializer_t* serializer;
int rowsize = 0;

bool TestRowInserts(table* t, int testnr=1)
{
    DWORD row[2];

    printf("%d. insert row ", testnr);

    for (int i=0; i<100000; i++)
    {
        row[0] = i;
        row[1] = i+1;

        if (t->insert_row((PBYTE)row) == invalid_recordid)
        {
            goto failed;
        }
    }

    printf("OK\n");
    return true;
failed:
    printf("FAILED\n");
    return false;
}

bool TestRowIterator(table* t, int testnr=2)
{
    iterator_t* it = t->get_iterator();
    DWORD i = 0;
    BYTE binrow[rowsize];
    DWORD col[2];

    printf("%d. iterate rows ", testnr);

    if (nullptr == it)
    {
        goto failed;
    }

    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        assert(t->read_row(row, binrow));
        col[0] = (DWORD)serializer->deserialize_int(binrow, 0, 4);
        col[1] = (DWORD)serializer->deserialize_int(binrow, 4, 4);

        // iterating trough the rows
        //printf("row: id=%lu col1=%lu col2=%lu\n", row, col[0], col[1]);

        assert(i == col[0]);
        assert(i+1 == col[1]);

        i++;
    }

    printf("OK\n");
    return true;
failed:
    printf("FAILED\n");
    return false;
}

bool TestRowUpdate(table* t)
{
    iterator_t* it = t->get_iterator();
    DWORD i = 0;
    BYTE binrow[rowsize];
    DWORD col[2];

    printf("3. update rows ");

    if (nullptr == it)
    {
        goto failed;
    }

    i = 0;
    // updating all rows
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        serializer->serialize_int(binrow, 0, i, 4);
        serializer->serialize_int(binrow, 4, i, 4);

        assert(t->update_row(row, binrow));
        i++;
    }

    i = 0;
    // verifying the updates
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        assert(t->read_row(row, binrow));
        col[0] = (DWORD)serializer->deserialize_int(binrow, 0, 4);
        col[1] = (DWORD)serializer->deserialize_int(binrow, 4, 4);

        assert(i == col[0]);
        assert(i == col[1]);

        i++;
    }

    printf("OK\n");
    return true;
failed:
    printf("FAILED\n");
    return false;
}

bool TestRowDelete(table* t)
{
    iterator_t* it = t->get_iterator();

    printf("4. delete rows ");

    if (nullptr == it)
    {
        goto failed;
    }

    // deleting all rows
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        assert(t->delete_row(row));
    }

    // verifying the updates
    for (it->first(); !it->end(); it->next())
    {
        assert(false);  // all the rows were deleted
    }

    printf("OK\n");
    return true;
failed:
    printf("FAILED\n");
    return false;
}

void TestTable()
{
    bool succeded = false;
    table* t;
    catalog c;

    gTestStarted++;

    c.addcolumn("TstCol1", ctInteger, 4);
    c.addcolumn("TstCol2", ctInteger, 4);

    printf("\n\nTESTING TABLE\n");
    printf(    "=============\n");

    remove("test2.tbl");

    t = tablemanager::getinstance().createtable("test2", c);
    if (nullptr == t)
    {
        goto failed;
    }

    rowsize = t->getcatalog().rowlength();
    serializer = new serializer_t(rowsize);

    if (false == TestRowInserts(t))
    {
        goto failed;
    }

    if (false == TestRowIterator(t))
    {
        goto failed;
    }

    if (false == TestRowUpdate(t))
    {
        goto failed;
    }

    if (false == TestRowDelete(t))
    {
        goto failed;
    }

    if (false == TestRowInserts(t, 5))
    {
        goto failed;
    }

    if (false == TestRowIterator(t, 6))
    {
        goto failed;
    }

    succeded = true;

failed:
    if (nullptr != serializer)
    {
        delete serializer;
    }

    if (t != nullptr)
    {
        t->close();
    }

    tablemanager::getinstance().deletetable("test2");

    if (succeded)
    {
        gTestSucceeded++;
    }
    else
    {
        gTestFailed++;
    }
}
