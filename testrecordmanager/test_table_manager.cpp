#include "test_table_manager.h"
#include "../recordmanager/table_manager.h"
#include "../recordmanager/catalog.h"
#include "../recordmanager/table.h"
#include "test.h"
#include <string.h>
#include <cstdio>

bool TestTableManagerGetinstance()
{
    gSubtestStarted++;
    printf("1. tablemanager.getinstance() ");

    tablemanager::getinstance();
    /*if (nullptr == tablemanager.getinstance())
    {
        printf(" FAILED\n");
        gSubtestFailed++;
        return false;
    }*/
    printf("OK\n");

    gSubtestSucceeded++;
    return true;
}

bool TestTableManagerCreateTable()
{
    rm_error err = errNone;
    catalog c;

    gSubtestStarted++;
    printf("2. tablemanager.createtable() ");

    c.addcolumn("teszsor", ctBoolean, 1);

    remove("test1.tbl");    // making sure that the file is not existing

    std::string tablename("test1");
    table* t = tablemanager::getinstance().createtable(tablename, c);

    if (nullptr == t)
    {
        printf("FAILED\n");
        err = errUnknownError;
        goto test_end;
    }
    else
    {
        printf("OK\n");
    }

    t->getcatalog().addcolumn("name", ctBoolean, 2);
    /*printf("2.1 table.create() ");
    if(t->create())
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
        err = errUnknownError;
        goto test_end;
    }*/

    printf("2.2 table.writecatalog() ");
    if(t->writecatalog())
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
        err = errUnknownError;
    }

    printf("2.3 table.close() ");
    if(t->close())
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
        err = errUnknownError;
    }

    printf("2.4 table.open() ");
    if( t->open() &&        // first attempt must succeed
        false == t->open()) // the second must fail
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
        err = errUnknownError;
    }

    // closing the table to leave the table manager to open the file
    t->close();

    if (rm_success(err))
    {
        gSubtestSucceeded++;
    }
    else
    {
        gSubtestFailed++;
    }

test_end:
    return rm_success(err);
}

bool TestTableManagerOpenTable()
{
    /*rm_error err = errNone;
    std::string tablename = "test2";
    table* t = nullptr;

    gSubtestStarted++;
    printf("3. tablemanager.opentable() ");

    {
        block_t b;
        memset(b.buffer, 0, sizeof(b.buffer));
        memcpy(b.buffer, "DBCAT1.0", sizeof("DBCAT1.0")-1);

        FILE* f = fopen((tablename + ".tbl").c_str(), "w+");
        fwrite(b.buffer, sizeof(b.buffer), 1, f);
        fclose(f);
    }

    t = tablemanager::getinstance().opentable(tablename);
    if (nullptr == t)
    {
        printf("FAILED\n");
        err = errUnknownError;
        goto test_end;
    }
    printf("OK\n");

test_end:
    if (rm_success(err))
    {
        gSubtestSucceeded++;
    }
    else
    {
        gSubtestFailed++;
    }*/

    return true;
}

bool TestTableManagerDeleteTable()
{
    gSubtestStarted++;
    printf("4. tablemanager.deletetable() ");

    if (tablemanager::getinstance().deletetable("test1") &&
        !tablemanager::getinstance().deletetable("test2"))  // test2.tbl is not existing
    {
        printf("OK\n");
    }
    else
    {
        printf("FAILED\n");
    }

    gSubtestSucceeded++;
    return true;
}

bool TestTableManager()
{
    bool res = true;

    gTestStarted++;

    printf("TESTING TABLEMANAGER\n");
    printf("====================\n");

    remove("system_tables.tbl");

    res &= TestTableManagerGetinstance();
    res &= TestTableManagerCreateTable();
    res &= TestTableManagerOpenTable();
    res &= TestTableManagerDeleteTable();

    if (!res)
    {
        gTestFailed++;
    }
    else
    {
        gTestSucceeded++;
    }

    return res;
}
