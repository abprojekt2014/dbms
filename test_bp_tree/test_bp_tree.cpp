#include <iostream>
#include <stdio.h>
#include <vector>

#include <queue>

#include "../bp_tree/bp_tree.h"

#include <signal.h>

///TODO
#define BP_TREE_MIN_BP_ORDER 3
#define BP_TREE_MAX_BP_ORDER 20

#ifndef NDEBUG
#define DBG_MSG(_format,_msg) printf(_format,_msg)
#else
#define DBG_MSG(_format,_msg)
#endif // NDEBUG

template<
class TKEY,
class TVALUE,
unsigned int BP_ORDER= 4,
unsigned int N_RECORD= BP_ORDER-1>
class BPlusTree_DEBUG: public BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>{
private:
    unsigned int dist_from_root(typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::BP_NODE *node){
        unsigned int dist= 0;
        typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::BP_NODE *tmp_node= node;
        while(tmp_node!= this->get_root()){
            ++dist;
            tmp_node= tmp_node->parent;
        }
        return dist;
    }

public:
    void print_bptree_debug(const bool print_value= false){
        const typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::LEAF_NODE *print_node= this->get_first_leaf();
        unsigned int n= 0;

        printf("ELEMENTS:\n");
        printf("BP_ORDER: %d\n",BP_ORDER);
        printf("N_RECORD: %d\n",N_RECORD);
        printf("NODE_TRESHOLD: %d\n",this->NODE_TRESHOLD);
        printf("RECORD_TRESHOLD: %d\n",this->RECORD_TRESHOLD);

        while(NULL!= print_node){
            for(unsigned int i= 0;i< print_node->n_key;++i){
                if(!print_value){
                    std::cout<<print_node->keys[i]<<" ";
                }else{
                    std::cout<<print_node->values[i]<<" ";
                }
                ++n;
            }
            print_node= print_node->next_leaf_node;
        }
        std::cout<<std::endl;

        printf("size: %d <-> n: %d\n",this->size(),n);
        assert(this->size()== n);

        return;
    }

    void print_level_order(void){
        std::queue<typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::BP_NODE *> print_q;
        typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::BP_NODE *tmp_node= NULL;
        unsigned int node_rank = 0;
        unsigned int new_node_rank = 0;
        unsigned int i= 0;

        print_q.push(this->get_root());

        printf("================================================================================\n");
        printf("LEVEL ORDER:\n");
        printf("BP_ORDER: %d\n",BP_ORDER);
        printf("N_RECORD: %d\n",N_RECORD);
        printf("NODE_TRESHOLD: %d\n",this->NODE_TRESHOLD);
        printf("RECORD_TRESHOLD: %d\n",this->RECORD_TRESHOLD);

        while(!print_q.empty()){
            tmp_node= print_q.front();
            print_q.pop();

            if( (NULL!= tmp_node->parent) &&
                (tmp_node== tmp_node->parent->children[0]) &&
                (node_rank!= (new_node_rank= dist_from_root(tmp_node))) ){
                node_rank= new_node_rank;
                if(BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::NODE_TYPE_INNER== tmp_node->node_type){
                    std::cout<<std::endl<<"INNER: ";
                }else{
                    std::cout<<std::endl<<"LEAF : ";
                }
            }

            ///tmp_node->minimal_sized?std::cout<<"<MIN>":std::cout<<"<NO MIN>";
            if(BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::NODE_TYPE_INNER== tmp_node->node_type){
                //belso
                typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::INNER_NODE *tmp_inner= reinterpret_cast<typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::INNER_NODE *>(tmp_node);
                for(i= 0;i< tmp_inner->n_key;++i){
                    std::cout<<tmp_inner->keys[i]<<" ";
                    print_q.push(tmp_inner->children[i]);
                }
                print_q.push(tmp_inner->children[i]);
            }else{
                //level
                typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::LEAF_NODE *tmp_leaf= reinterpret_cast<typename BPlusTree<TKEY,TVALUE,BP_ORDER,N_RECORD>::LEAF_NODE *>(tmp_node);
                for(i= 0;i< tmp_leaf->n_key;++i){
                    std::cout<<tmp_leaf->keys[i]<<" ";
                }
            }

            std::cout<<"| ";
        }
        std::cout<<std::endl;
        printf("================================================================================\n");
    }
};

int main(void){
    /*unsigned int i= 0;
    unsigned int x_k= 0;
    float x_v= 0;
    float x_find;*/
    printf("test\n");

    srand(0);

    BPlusTree_DEBUG<int,recordid_t> test_tree;
    assert(test_tree.create());

    test_tree.insert(1,0xeeeeeeee);
    test_tree.print_level_order();

    //file bezarasa
    assert(test_tree.close());

    printf("ujra megnyitva\n");
    //file ujramegnyitasa
    assert(test_tree.open());

    /*std::vector<int> a;
    std::vector<int>::iterator it= a.begin();*/

    /*test_tree.insert(1,1.01);
    test_tree.print_level_order();
    test_tree.insert(2,1.02);
    test_tree.print_level_order();
    test_tree.insert(3,1.03);
    test_tree.print_level_order();
    test_tree.insert(4,1.04);
    test_tree.print_level_order();
    test_tree.insert(5,1.05);
    test_tree.print_level_order();
    test_tree.insert(6,1.06);
    test_tree.print_level_order();
    test_tree.insert(7,1.07);
    test_tree.print_level_order();
    test_tree.insert(8,1.08);
    test_tree.print_level_order();
    test_tree.insert(9,1.09);
    test_tree.print_level_order();
    test_tree.insert(10,1.10);
    test_tree.print_level_order();
    test_tree.insert(11,1.11);
    test_tree.print_level_order();
    test_tree.insert(12,1.12);
    test_tree.print_level_order();
    test_tree.insert(13,1.13);
    test_tree.print_level_order();
    test_tree.insert(14,1.14);
    test_tree.print_level_order();
    test_tree.insert(15,1.15);
    test_tree.print_level_order();*/

    ///3!!!
    /*for(i= 0;i< 50;++i){
        x_k= rand()%100;
        x_v= ((float)rand())/((float)rand());
        //printf("%d\n",rand()%100);
        printf("================================================================================\n");
        printf("%03d KEY: %d VALUE: %f\n",i,x_k,x_v);
        printf("================================================================================\n");
        if(23== x_k){
            //raise(SIGTRAP);
        }
        if(9== i){
            //raise(SIGTRAP);
        }
        if(3== i){
            //raise(SIGTRAP);
        }
        test_tree.insert(x_k,x_v);
        test_tree.print_level_order();
        test_tree.print_bptree_debug();
    }

    test_tree.print_bptree_debug();
    //test_tree.print_level_order();

    x_find= 1.666;
    test_tree.insert(28,x_find);
    std::cout<<"beszurt elem: "<<x_find<<std::endl;
    x_find= 0;

    printf("kereses\n");
    if(test_tree.find(28,&x_find)){
        std::cout<<"elem: "<<x_find<<std::endl;
    }

    printf("NINCS\n");
    if(test_tree.find(6666,&x_find)){
        std::cout<<"elem: "<<x_find<<std::endl;
    }*/

    return 0;
}
