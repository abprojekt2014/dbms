#
# If you want to see the BOOST error messages/variables
#
#set(CONFIG_MINGW_BOOST 1)
#set(CONFIG_MINGW_DEBUG 1)

#
# paths to msys and boost to configure properly the libraries
#

set(CFG_BOOST_ROOTS "d:/Egyetem/Mesteri2/boost_1_55_0" "d:/Enyim/Egyetem/Mesteri/boost_1_55_0" "d:/mesteri/2felev/ab/boost_1_55_0")
set(CFG_MSYS_ROOTS "c:/MinGw/msys/1.0" "d:/MinGw/msys/1.0" "d:/programok/Mingw-get/msys/1.0")
set(CFG_QT4_ROOTS "d:/Qt/4.8.6")

set(CFG_BOOT_ROOT_FOUND 0)
set(CFG_MSYS_ROOT_FOUND 0)


#
# IMPLEMENTATION
#

site_name(hostname)	#sets the hostname variable to the name of the computer
#message ("Computername : ${hostname}")

if (DEFINED CONFIG_MINGW_BOOST)
	set(Boost_DEBUG ON)
endif()

#
# Getting the right paths
#

if (DEFINED CONFIG_MINGW_DEBUG)
	message("-- Setting BOOST path:")
endif()

# setting the boost root
foreach (path ${CFG_BOOST_ROOTS})
	if (DEFINED CONFIG_MINGW_DEBUG)
		message("* verifying: ${path}")
	endif()
	if (EXISTS "${path}")
		if (DEFINED CONFIG_MINGW_DEBUG)
			message("* found boost path: ${path}")
		endif()
		set(CFG_BOOT_ROOT_FOUND 1)
		set(BOOST_ROOT "${path}")
	endif()
endforeach()

if (DEFINED CONFIG_MINGW_DEBUG)
	message("-- Setting FLEX and BISON paths:")
endif()
#setting flex and bison variables
foreach (path ${CFG_MSYS_ROOTS})
	if (DEFINED CONFIG_MINGW_DEBUG)
		message("* verifying: ${path}")
	endif()
	if (EXISTS "${path}")
		if (DEFINED CONFIG_MINGW_DEBUG)
			message("* found msys path: ${path}")
		endif()
		set(CFG_MSYS_ROOT_FOUND 1)
		set(FLEX_EXECUTABLE "${path}/bin/flex.exe")
		set(FL_LIBRARY "${path}/lib/libfl.a")
		set(BISON_EXECUTABLE "${path}/bin/bison.exe")
	endif()
endforeach()

if ("${CFG_BOOT_ROOT_FOUND}" EQUAL "0")
	message( FATAL_ERROR "BOOST library not found, please add the path into the CFG_BOOST_ROOTS array" )
endif()

if ("${CFG_MSYS_ROOTS}" EQUAL "0")
	message( FATAL_ERROR "FLEX and BISON libraries not found, please add the path into the CFG_MSYS_ROOTS array" )
endif()

# Finding the qt4 patha

foreach (path ${CFG_QT4_ROOTS})
	# The path where Qt is.
	if (DEFINED CONFIG_MINGW_DEBUG)
		message("* verifying: ${path}")
	endif()
	if (EXISTS "${path}")
		SET(QT_ROOT_DIR "${path}")
		if (DEFINED CONFIG_MINGW_DEBUG)
			message("* found qt path: ${path}")
		endif()
		set(QT_QMAKE_EXECUTABLE "${QT_ROOT_DIR}/bin/qmake.exe")

		# Loads CMake macros bound to Qt.
		find_package(Qt4 REQUIRED)
		# Includes used Qt headers.
		include(${QT_USE_FILE})
		#message("#### ${QT_INCLUDES}")
	endif()
endforeach()
