#include <cstdio>
#include <memory>
#include "../indexmanager/index_manager.h"
#include "../recordmanager/serializer.hpp"
#include "../recordmanager/table_manager.h"

using namespace std;

static const unsigned max_index_to_insert = 10000;
static const unsigned max_index_to_query = 100;

index_t* test_open_index()
{
    indexmanager& im = indexmanager::getinstance();
    index_t* idx;

    // printing the list of indices
    for (string s: im.getindexnames())
    {
        printf("idx: %s\n", s.c_str());
    }

    idx = im.createindex(idxHashTable, "abra", "cadabra", ctInteger, 4);
    if (nullptr == idx)
    {
        printf("Index not created\n");
        idx = im.openindex("idx_abra_cadabra.hdx");
        if (nullptr == idx)
        {
            printf("Index not opened\n");
            return nullptr;
        }
        printf("Index opened\n");
    }
    else
    {
        for (recordid_t i = 0; i < max_index_to_insert; i++)
        {
            DWORD value = i;

            assert(idx->insertrow(i, &value));
        }
    }

    return idx;
}

void test_index_searches(index_t* idx)
{

    printf("equals\n");

    /// EQUALS
    unsigned j = 0;
    for (DWORD i = 0; i < max_index_to_insert; i++)
    {
        std::unique_ptr<iterator_t> it(idx->getiterator_equal(&i));
        if (it == nullptr)
        {
            assert(false);
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (value == i);
            assert (rid == value);

            j++;
        }
    }

    assert (j == max_index_to_insert);

    printf("lesser\n");

    /// LESSER
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        std::unique_ptr<iterator_t> it(idx->getiterator_less(&i));
        if (it == nullptr)
        {
            assert(false);
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (rid < i);
            assert (value < i);
        }
    }

    printf("lesser or equal\n");

    /// LESSER EQUAL
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        std::unique_ptr<iterator_t> it(idx->getiterator_less_or_equal(&i));
        if (it == nullptr)
        {
            assert(false);
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (rid <= i);
            assert (value <= i);
        }
    }

    printf("greater\n");

    /// GREATER
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        std::unique_ptr<iterator_t> it(idx->getiterator_greater(&i));
        if (it == nullptr)
        {
            assert(false);
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (rid > i);
            assert (value > i);
        }
    }

    printf("greater or equal\n");

    /// GREATER OR EQUAL
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        std::unique_ptr<iterator_t> it(idx->getiterator_greater_or_equal(&i));
        if (it == nullptr)
        {
            assert(false);
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (rid >= i);
            assert (value >= i);
        }
    }

    printf("between\n");
    /// BETWEEN - a single testcase
    {
        DWORD _min = 1;
        DWORD _max = 100;

        std::unique_ptr<iterator_t> it(idx->getiterator(&_min, &_max));
        if (it == nullptr)
        {
            assert(false);
        }

        for (it->first(); !it->end(); it->next())
        {
            DWORD value;

            it->read_value(&value); // should be the same

            assert (value >= _min);
            assert (value <= _max);
        }
    }

    /// UPDATING all recordids in the table
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        DWORD new_value = i+1;
        assert(idx->update(i, &new_value));
    }

    /// DELETING all recordids from the table
    for (DWORD i = 0; i < max_index_to_query; i++)
    {
        assert(idx->deleterow(i));
    }
}

void test_delete_index(index_t* idx)
{
    // deleting the index
    string idxname = idx->getname();
    indexmanager::getinstance().deleteindex(idxname);

}

bool test_create_table_and_index_after()
{
    //
    // Creating a table
    //

    tablemanager& tm = tablemanager::getinstance();
    catalog c;

    c.addcolumn("ID", ctInteger, 4);
    c.addcolumn("Name", ctString, 10);
    c.addcolumn("IsNormal", ctBoolean, 1);

    table* t = tm.createtable("test_index", c);
    if (t == nullptr)
    {
        printf("The table already exists! skipping index creation on already existing tables!\n");
        return false;
    }

    assert(t != nullptr);

    //
    // inserting some rows into the table
    //

    BYTE binrow[15];
    std::string nameval;
    unique_ptr<serializer_t> s(new serializer_t(15));

    nameval = "Robert";

    s->serialize_int(binrow, 0, 0, 4);
    s->serialize_str(binrow, 4, nameval, 10);
    s->serialize_bool(binrow, 14, true);
    t->insert_row(binrow);

    nameval = "Jack";

    s->serialize_int(binrow, 0, 1, 4);
    s->serialize_str(binrow, 4, nameval, 10);
    s->serialize_bool(binrow, 14, true);
    t->insert_row(binrow);

    nameval = "Michael";

    s->serialize_int(binrow, 0, 2, 4);
    s->serialize_str(binrow, 4, nameval, 10);
    s->serialize_bool(binrow, 14, true);
    t->insert_row(binrow);

    //
    //  creating an index for the table
    //

    /// 1. we must open the table after it's name, so we are sure that the table is existing
    /// 2. the name of the column must be checked if it's existing!!!! - anyhow to get the column's size and type
    /// it's necessary to open the catalog and search the column after it's name
    index_t* idx = indexmanager::getinstance().createindex(idxHashTable, "test_index",  "Name", ctString, 10);

    /// 3. When the index is created after the table we must add all the rows into the index also
    unique_ptr<iterator_t> it(t->get_iterator());
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        BYTE binrow[15];

        // reading the row to the binrow
        t->read_row(row, binrow);

        // inserting the value into the index
        assert(idx->insertrow(row, &binrow[4]));
    }

    catalog& cat = t->getcatalog();

    bool found = false;
    // setting the column as indexed
    for (column_t* col: cat.columns())
    {
        if (col->name.compare("Name") == 0)
        {
            // we got the right column
            cat.setprimarykey(col, idx);
            found = true;
            break;
        }
    }
    assert(found);

    t->writecatalog();  // saving the catalog to the disk
    /// TODO the table/catalog automatically should close the idexes
    t->close();

    return true;
}

void test_table_open_with_index()
{
    tablemanager& tm = tablemanager::getinstance();

    table* t = tm.opentable("test_index");
    assert (t != nullptr);

    catalog& c = t->getcatalog();
    column_t* col = nullptr;

    for (column_t* _c: c.columns())
    {
        if (_c->name.compare("Name") == 0)
        {
            // we found the column
            col = _c;
            break;
        }
    }

    assert(col != nullptr);     // no index found for column named "Name"
    assert(col->attributes.indexed == true);
    assert(col->primary_key.primary_key_index != nullptr);

    if (col->attributes.indexed)
    {
        if (col->attributes.primary_key)
        {
            printf("The Name is indexed with a primary key; index file name: %s\n", col->primary_key.primary_key_index->getname().c_str());
        }
        else if (col->attributes.foreign_key)
        {
            printf("The Name is indexed with a foreign key; index file name: %s\n", col->foreign_key.foreign_key_index->getname().c_str());
        }
        else
        {
            printf("Unknown index type!!!\n");
            assert(false);
        }
    }
}

int main() {
    index_t* idx;

    idx = test_open_index();
    assert (idx != nullptr);

    test_index_searches(idx);
    test_delete_index(idx);
    idx = nullptr;
    if (!test_create_table_and_index_after())
    {
        test_table_open_with_index();
    }
}
