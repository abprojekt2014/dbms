#ifndef _HASH_INDEX_H_
#define _HASH_INDEX_H_

#include "boost/crc.hpp"
#include "../record_manager/ansi/blockfile_ansi.h"
#include "../record_manager/table_types.h"
#include <string>

class hash_index : public index_t
{
private:
    static const int bucket_number = rm_block_size/sizeof(blockid_t);

    blockfile*          _file;
    block_t             file_header;
    block_t             buckets_block;
    block_t             cache;
    blockid_t*          buckets;
    boost::crc_32_type  hasher;

    std::string         table_name;
    std::string         column_name;
    columntype          column_type;
    int                 column_size;
    std::string         index_name;

    blockid_t create_newblock();
    bool insert_newpair(recordid_t row, void* value, block_t &block);
    bool read_block_cached(block_t block);
    bool read_block_cached(blockid_t blockid);
    bool write_back_cache();
    bool update_buckets();
    blockid_t get_bucket(void* value);
    bool parse_file_header();

    friend class hash_iterator;

public:
    hash_index();
    virtual ~hash_index();
    virtual std::string getname();
    virtual bool create(std::string tablename, std::string columnname, columntype type, int column_size);
    virtual bool open(std::string tablename, std::string columnname);
    virtual bool close();
    virtual std::string gettablename();
    virtual std::string getcolumnname();
    virtual void setcolumnlength(int length);
    virtual int getcolumnlength();
    virtual iterator_t* getiterator();
    virtual iterator_t* getiterator(void* minimum, void* maximum);
    virtual iterator_t* getiterator_less(void* maximum);
    virtual iterator_t* getiterator_less_or_equal(void* maximum);
    virtual iterator_t* getiterator_greater(void* minimum);
    virtual iterator_t* getiterator_greater_or_equal(void* minimum);
    virtual iterator_t* getiterator_equal(void* value);
    virtual recordid_t getmin();
    virtual recordid_t getmax();
    virtual bool insertrow(recordid_t row, void* value);
    virtual bool deleterow(recordid_t row);
    virtual bool update(recordid_t row, void* new_value);
};

#endif
