#include "hash_index.h"
#include "boost/crc.hpp"
#include "../record_manager/table_iterator.h"
#include "../record_manager/ansi/blockfile_ansi.h"
#include "../record_manager/table_types.h"
#include "../record_manager/serializer.hpp"
#include <string>
#include <memory>

using namespace std;

struct block_header_t
{
    blockid_t next;
    DWORD pair_number;
};

struct pair_t
{
    recordid_t  key;
    BYTE        value[];
};

struct file_header_t
{
    char Magic[4];
    BYTE TableName[64];
    BYTE ColumnName[64];
    DWORD ColumnType;
    DWORD ColumnSize;
};

enum hash_iterator_type
{
    hiAll,

    hiLesser,
    hiLesserEqual,
    hiEqual,
    hiGreaterEqual,
    hiGreater,
    hiBetween,
};

class hash_iterator: public iterator_t
{
    hash_iterator_type hit;
    hash_index* idx;
    bool ended;
    bool _first;
    bool skip_going_to_next_value;

    blockid_t bucketid;   // current bucket id
    blockid_t blockid;  // current block id
    recordid_t offs;  // current record
    recordid_t end_offs;
    blockid_t ending_bucketid; // ending bucket id
    block_t cache;
    size_t record_size;
    void* pvalue;
    serializer_t* ser;
    PBYTE _value_to_search;
    PBYTE _value_to_search2;
    columntype coltype;
    int colsize;

public:
    hash_iterator(hash_index* hidx, hash_iterator_type type)
    {
        bucketid = 0;
        blockid = 0;
        offs = 0;
        end_offs = 0;
        ending_bucketid = hidx->bucket_number - 1;
        ended = false;
        _first = true;
        idx = hidx;
        hit = type;
        record_size = hidx->column_size + sizeof(pair_t);
        coltype = idx->column_type;
        colsize = idx->column_size;

        ser = new serializer_t(colsize);
        _value_to_search = new BYTE[colsize];
        _value_to_search2 = new BYTE[colsize];
    }

    ~hash_iterator()
    {
        if (nullptr != ser)
        {
            delete ser;
        }

        if (nullptr != _value_to_search)
        {
            delete _value_to_search;
        }
    }

    void set_value(void* value)
    {
        memcpy(_value_to_search, value, colsize);
    }

    void set_value_min_max(void* _min, void* _max)
    {
        memcpy(_value_to_search, _min, colsize);
        memcpy(_value_to_search2, _max, colsize);
    }

    void set_positions_by_value_for_eq(void* value)
    {
        bucketid = ending_bucketid = idx->get_bucket(value);
        set_value(value);
    }

    bool end()
    {
        return ended;
    }

    recordid_t first()
    {
        skip_going_to_next_value = true;
        goto_next_bucket();
        // no bucket was found with this value
        if (ended)
        {
            return invalid_recordid;
        }

        return next();
    }

    recordid_t next()
    {
        bool found = false;
        pair_t* p = nullptr;

        do
        {
            if (skip_going_to_next_value)
            {
                skip_going_to_next_value = false;
            }
            else
            {
                offs += record_size;
            }

            if (offs >= end_offs)
            {
                goto_next_block();
                if (ended)
                {
                    return invalid_recordid;
                }
            }

            p = (pair_t*)&cache.buffer[offs];
            pvalue = &p->value[0];

            switch(hit)
            {
            case hiEqual:
                if (!ser->values_equal(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiLesser:
                if (!ser->values_lesser_than(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiLesserEqual:
                if (!ser->values_lesser_or_equal(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiGreater:
                if (!ser->values_bigger_than(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiGreaterEqual:
                if (!ser->values_bigger_or_equal(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiBetween:
                // <= maximum
                // >= minimum
                if (!ser->values_lesser_or_equal(pvalue, _value_to_search2, coltype, colsize)) continue;
                if (!ser->values_bigger_or_equal(pvalue, _value_to_search, coltype, colsize)) continue;
                break;
            case hiAll:
                break;
            default:
                assert(false);
            }
            found = true;
        } while (!found);

        if (p)
        {
            return p->key;
        }

        return invalid_recordid;
    }

    void goto_next_bucket()
    {
        do
        {
            if (_first)
            {
                _first = false;

                idx->_file->readblock(1, idx->buckets_block);
            }
            else
            {
                bucketid += 1;
            }

            if (bucketid > ending_bucketid)
            {
                ended = true;
                continue;
            }

            blockid = idx->buckets[bucketid];
            if (blockid  == 0)
            {
                // goning to the nex block
                continue;
            }

            if (!rm_success(idx->_file->readblock(blockid, cache)))
            {
                assert(false);
                return;
            }

            set_new_offsets();
            break;
        } while (!ended);
    }

    void goto_next_block()
    {
        skip_going_to_next_value = true;

        block_header_t* bh = (block_header_t*)cache.buffer;
        if (bh->next == 0)
        {
            // no more blocks in this bucket
            goto_next_bucket();
            return;
        }

        blockid = bh->next;
        if (!rm_success(idx->_file->readblock(blockid, cache)))
        {
            assert(false);
            ended = true;
            return;
        }

        set_new_offsets();
    }

    void set_new_offsets()
    {
        block_header_t* bh = (block_header_t*)cache.buffer;

        offs = sizeof(block_header_t);
        end_offs = offs + (record_size * bh->pair_number);
        assert (bh->pair_number < 512);
    }

    bool read_value(void* value)
    {
        memcpy(value, pvalue, colsize);

        return true;
    }

    bool remove()
    {
        block_header_t* bh = (block_header_t*)cache.buffer;
        bool found = false;

        if (bh->pair_number != 0)
        {
            bh->pair_number -= 1;
            end_offs -= record_size;

            if (bh->pair_number != 0)
            {
                memcpy(&cache.buffer[offs], &cache.buffer[end_offs], record_size);
            }

            memset(&cache.buffer[end_offs], 0, record_size);

            found = true;
        }
        else
        {
            // going to the next block if somebody wants to delete without
            goto_next_block();
            // when we are in the next block we must delete the first element => recursion
            return remove();
        }

        // setting this variable, not to jump to the next value when using the next() function
        skip_going_to_next_value = true;

        if (!rm_success(idx->_file->writeblock(cache)))
        {
            return false;
        }

        return found;
    }

    bool update_value(void* value)
    {
        pair_t* p = (pair_t*)&cache.buffer[offs];

        // updating the value
        memcpy(&p->value[0], value, this->colsize);

        return rm_success(idx->_file->writeblock(cache));
    }
};

std::string hash_index::getname()
{
    return index_name;
}

hash_index::hash_index()
{
    _file = new blockfile_ansi();
    buckets = nullptr;
    column_size = 0;
    cache.blockid = invalid_blockid;
}

hash_index::~hash_index()
{
    if(_file != nullptr)
    {
        _file->close();
    }
}

bool hash_index::create(std::string tablename, std::string columnname, columntype type, int column_size)
{

    assert (tablename.length() < 64);
    assert (columnname.length() <64);

    index_name = generatename(tablename, columnname);
    if (rm_success(_file->open(index_name + ".hdx")))
    {
        // the index is already existing
        return false;
    }

    if (!rm_success(_file->create(index_name + ".hdx")))
    {
        index_name = "";;
        return false;
    }

    if (!rm_success(_file->newblock(file_header)))
    {
        return false;
    }

    if (!rm_success(_file->newblock(buckets_block)))
    {
        return false;
    }

    // file header initialization
    file_header_t* header = (file_header_t*)file_header.buffer;
    memcpy(header->Magic, "HIDX", 4);       // magic marker
    memcpy(header->TableName, tablename.c_str(), tablename.length());
    memcpy(header->ColumnName, columnname.c_str(), columnname.length());
    header->ColumnType = type;
    header->ColumnSize = column_size;

    // initializing the buckets
    buckets = (blockid_t*)buckets_block.buffer;
    table_name = tablename;
    column_name = columnname;
    column_type = type;
    this->column_size = column_size;

    _file->writeblock(file_header);

    return true;
}

bool hash_index::close()
{
    _file->close();
    return true;
}

bool hash_index::parse_file_header()
{
    file_header_t* header;
    char* buff[65];

    header = (file_header_t*)file_header.buffer;
    if (header->Magic[0] != 'H' ||
        header->Magic[1] != 'I' ||
        header->Magic[2] != 'D' ||
        header->Magic[3] != 'X' )
    {
        return false;
    }

    /// Table Name
    memcpy(buff, header->TableName, 64);
    buff[64] = 0;
    table_name = std::string((char*)buff);

    /// Column Name
    memcpy(buff, header->ColumnName, 64);
    buff[64] = 0;
    column_name = std::string((char*)buff);

    column_type = (columntype)header->ColumnType;
    column_size = header->ColumnSize;

    return true;
}

bool hash_index::open(std::string tablename, std::string columnname)
{
    index_name = generatename(tablename, columnname);

    // openinig the file
    if (!rm_success(_file->open(index_name + ".hdx")))
    {
        // the index is already existing
        return false;
    }

    if (!rm_success(_file->readblock(0, file_header)))
    {
        return false;
    }

    if (!rm_success(_file->readblock(1, buckets_block)))
    {
        return false;
    }

    if (!parse_file_header())
    {
        return false;
    }

    buckets = (blockid_t*)buckets_block.buffer;

    return true;
}

std::string hash_index::gettablename()
{
    return table_name;
}

std::string hash_index::getcolumnname()
{
    return column_name;
}

void hash_index::setcolumnlength(int length)
{
    assert(length!=0);
    assert(column_size==0);
    column_size = length;
}

int hash_index::getcolumnlength()
{
    return column_size;
}

iterator_t* hash_index::getiterator()
{
    hash_iterator* it = new hash_iterator(this, hiAll);

    // needs no more setup

    return it;
}

iterator_t* hash_index::getiterator(void* minimum, void* maximum)
{
    hash_iterator* it = new hash_iterator(this, hiBetween);
    it->set_value_min_max(minimum, maximum);

    return it;
}

iterator_t* hash_index::getiterator_less(void* maximum)
{
    hash_iterator* it = new hash_iterator(this, hiLesser);
    it->set_value(maximum);

    return it;
}

iterator_t* hash_index::getiterator_less_or_equal(void* maximum)
{
    hash_iterator* it = new hash_iterator(this, hiLesserEqual);
    it->set_value(maximum);

    return it;
}

iterator_t* hash_index::getiterator_greater(void* minimum)
{
    hash_iterator* it = new hash_iterator(this, hiGreater);
    it->set_value(minimum);

    return it;
}

iterator_t* hash_index::getiterator_greater_or_equal(void* minimum)
{
    hash_iterator* it = new hash_iterator(this, hiGreaterEqual);
    it->set_value(minimum);

    return it;
}

iterator_t* hash_index::getiterator_equal(void* value)
{
    hash_iterator* it = new hash_iterator(this, hiEqual);
    it->set_positions_by_value_for_eq(value);

    return it;
}

recordid_t hash_index::getmin()
{
    // not implemented; requres complete table iteration
    return invalid_recordid;
}

recordid_t hash_index::getmax()
{
    // not implemented; requres complete table iteration
    return invalid_recordid;
}

bool hash_index::insertrow(recordid_t row, void* value)
{
    DWORD bucket = get_bucket(value);
    blockid_t bucketid = buckets[bucket];

    if (bucketid == 0)
    {
        //
        //  No buckets were allocated
        //
        blockid_t bid = create_newblock();

        if(bid == invalid_blockid)
        {
            return false;
        }

        if (!read_block_cached(bid))
        {
            return false;
        }

        if(!insert_newpair(row, value, cache))
        {
            return false;
        }

        // minden ok, betesszuk a blockot a bucketbe
        buckets[bucket] = bid;

        // le kell menteni az uj blockot a lemezre
        write_back_cache();
        update_buckets();
    }
    else
    {
        //
        // We have a bucket, we have to verify if the bucket is full; if it's not then we can insert
        // the data in the block. Otherwise a new bucket must be allocated
        //
        block_t block;
        block_header_t* header;
        blockid_t current_block = bucketid;

        while(current_block != 0)
        {
            if (!rm_success(_file->readblock(current_block, block)))
            {
                assert(false);
                return false;
            }

            header = (block_header_t*)block.buffer;

            // we have no space in this block
            if(header->pair_number >= (rm_block_size - sizeof(block_header_t)) / (sizeof(recordid_t) + column_size))
            {
                // going to the next block
                current_block = header->next;
                continue;
            }

            // we insert the row into the block
            if(!insert_newpair(row, value, block))
            {
                // nem sikerult a beszuras
                return false;
            }

            //sikeresen beszurtuk
            return true;
        }

        //
        // new block must be allocated
        //
        blockid_t bid = create_newblock();
        block_t new_block;

        if (bid == invalid_blockid)
        {
            assert(false);
            return false;
        }

        if (!rm_success(_file->readblock(bid, new_block)))
        {
            assert(false);
            return false;
        }

        if (!insert_newpair(row, value, new_block))
        {
            assert(false);
            return false;
        }

        // a legutolso block next-jet updateljuk az uj block id-val
        ((block_header_t*)block.buffer)->next = bid;

        if (!rm_success(_file->writeblock(block)))
        {
            return false;
        }
}

    return true;
}

bool hash_index::deleterow(recordid_t row)
{
    unique_ptr<hash_iterator> it(new hash_iterator(this, hiAll));

    for (recordid_t rid = it->first(); !it->end(); rid = it->next())
    {
        if (rid == row)
        {
            // deleting the value
            assert(it->remove());
            return true;
        }
    }

    return false;
}

blockid_t hash_index::create_newblock()
{
    //uj block
    block_t block;

    if (!rm_success(_file->newblock(block)))
    {
        return invalid_blockid;
    }

    block_header_t* header = (block_header_t*)block.buffer;
    // new block initialization
    header->pair_number = 0;
    header->next = 0;

    if (!rm_success(_file->writeblock(block)))
    {
        return invalid_blockid;
    }

    return block.blockid;
}

bool hash_index::insert_newpair(recordid_t row, void* value, block_t &block)
{
    block_header_t* header = (block_header_t*)block.buffer;

    // lementjuk a kulcs/ertek part
    pair_t* new_pair = (pair_t*)&block.buffer[sizeof(block_header_t) + header->pair_number*(sizeof(recordid_t) + column_size)];

    header->pair_number += 1;    // egyetlen egy par/ertek par van benne
    new_pair->key = row;
    memcpy(new_pair->value, value, column_size);

    return rm_success(_file->writeblock(block));
}

bool hash_index::read_block_cached(block_t block)
{
    return read_block_cached(block.blockid);
}

bool hash_index::read_block_cached(blockid_t blockid)
{
    if (cache.blockid == blockid)
    {
        return true;
    }

    cache.blockid = blockid;
    return rm_success(_file->readblock(blockid, cache));
}

bool hash_index::write_back_cache()
{
    return rm_success(_file->writeblock(cache));
}

bool hash_index::update_buckets()
{
    return rm_success(_file->writeblock(buckets_block));
}

blockid_t hash_index::get_bucket(void* value)
{
    DWORD hashed_value = 0;
    assert(value!= nullptr);
    if(value == nullptr)
    {
        return false;
    }

    // a file meg kell legyen nyitva, egyebkent nem nullptr
    assert(buckets != nullptr);

    // lenullazzuk a hash-t
    hasher.reset();

    if (column_type == ctString)
    {
        int _size = strlen((char*)value);

        // kiszamoljuk a hash erteket
        hasher.process_bytes(value, _size);
        // megkapjuk a hash erteket
        hashed_value = hasher.checksum();
    }
    else
    {
        // kiszamoljuk a hash erteket
        hasher.process_bytes(value, column_size);
        // megkapjuk a hash erteket
        hashed_value = hasher.checksum();
    }

    //printf("Hashelt ertek: %lu\n", hash_value);

    return hashed_value%bucket_number;
}

bool hash_index::update(recordid_t row, void* new_value)
{
    assert(this->deleterow(row));
    assert(this->insertrow(row, new_value));

    return true;
}
