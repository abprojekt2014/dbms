#include "client.h"
#include <iostream>
#include "vector"
#include <sstream>
#include <stdint.h>
#include "../parser/adatsor.h"
#include "../TextTable/TextTable.h"

using namespace std;

client::client(boost::asio::io_service& io_service)
    : _socket(io_service)
{
	tcp::resolver resolver(io_service);
	char* serverName = "localhost";
	tcp::resolver::query query(serverName, "1666");
	_endpoint_iterator = resolver.resolve(query);
	_error = "";
}

bool client::connect()
{
	try
	{
		boost::asio::connect(_socket, _endpoint_iterator);
		return true;
	}
	catch (std::exception& e)
	{
		_error = e.what();
	}
	return false;
}

bool client::disconnect()
{
	try
	{
		_socket.close();
		return true;
	}
	catch (std::exception& e)
	{
		_error = e.what();
	}
	return false;
}

bool client::read()
{
	try{
        size_t header = 0;
        boost::system::error_code error;

        boost::asio::read(_socket, boost::asio::buffer(&header, sizeof(header)), boost::asio::transfer_exactly(sizeof(header)), error);
        if (error)
            throw boost::system::system_error(error);
        //std::cout << "Reading " << header << " bytes" << std::endl;

        size_t _rc = boost::asio::read(_socket, boost::asio::buffer(_data), boost::asio::transfer_exactly(header), error);
        if (error)
            throw boost::system::system_error(error);

        //_data[_rc - 1] = 0;
        adat = deserialize<AdatSor>(_data);

        return true;
    }
    catch(std::exception& e){
        _error = e.what();
    }
    return false;
}

bool client::write(string message)
{
	try{
        std::string serialized = serialize(message);

        const size_t header = serialized.size();
        //std::cout << "Sending " << header << " bytes" << std::endl;

        size_t rc = boost::asio::write(
                _socket,
                boost::asio::buffer(&header, sizeof(header))
                );
        rc = boost::asio::write(
                _socket,
                boost::asio::buffer(serialized)
                );

        //std::cout << "Sent " << rc << " bytes" << std::endl;
        return true;
    }
    catch(std::exception& e){
        _error = e.what();
    }
    return false;
}

bool client::finish_read()
{
	return _read_ok;
}

bool client::use_data()
{
    static TextTable* table = NULL;

    if (adat.adat[0].compare("OK") == 0)
    {
        return false;
    }

    if (adat.adat[0].compare("ERROR") == 0)
    {
        cout << "[ERROR] " << adat.adat[1] << endl;
        return false;
    }

    if (adat.adat[0].compare("TABLE::END") == 0)
    {
        cout << *table;

        return false;
    }

    if (adat.adat[0].compare("TABLE::NEW") == 0)
    {
        if (table)
        {
            delete table;
        }

        table = new TextTable();
    }

    if (adat.adat[0].compare("ROW") == 0)
    {
        // egy uj sor erkezett, jelenleg nincs mit tegyunk
    }

    assert(adat.adat.begin() != adat.adat.end());
    table->addRow(++adat.adat.begin(), adat.adat.end());
    
    return true;
}

std::vector<std::string> client::get_row()
{
	AdatSor deserialized = deserialize<AdatSor>(_data);

	if(deserialized.adat.at(0) == "OK")
		_read_ok = true;
	else
		_read_ok = false;

	if(deserialized.adat.at(0) == "ERROR")
	{
		_has_error = true;
		_read_ok = true;
	}
	else
	{
		_has_error = false;
		_read_ok = false;
	}

	return deserialized.adat;
}

std::vector<std::string> client::getTablesName()
{
	if(write("select TableName from system_tables;"))
		if(read())
		{
			AdatSor deserialized = deserialize<AdatSor>(_data);
			return deserialized.adat;
		}
	std::vector<std::string> vect;
	vect.push_back("error");
	return vect;
}

std::vector<std::string> client::getColumnsName(string table)
{
	if(write("select ColumnName from system_columns where TableName = '" + table + "';"))
		if(read())
		{
			AdatSor deserialized = deserialize<AdatSor>(_data);
			return deserialized.adat;
		}
	std::vector<std::string> vect;
	vect.push_back("error");

	return vect;
}

std::string client::get_error()
{
	return _error;
}

bool client::has_error()
{
	return _has_error;
}
