#ifndef CLIENT_H
#define CLIENT_H

#include "vector"
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "../parser/adatsor.h"

using boost::asio::ip::tcp;
using namespace std;


class client
{
private:
    enum { _max_length = 1024 };
    char _data[_max_length];

	AdatSor adat;

	bool _read_ok;
	std::string _error;
	bool _has_error;

	tcp::socket _socket;
	tcp::resolver::iterator _endpoint_iterator;


public:
	client(boost::asio::io_service& io_service);

	bool connect();
	bool disconnect();

	bool read();
	bool write(string message);
	bool use_data();


	bool finish_read();

	std::vector<std::string> get_row();
	std::vector<std::string> getTablesName();
	std::vector<std::string> getColumnsName(string table);
	std::string get_error();
	bool has_error();
};

#endif // CLIENT_H
