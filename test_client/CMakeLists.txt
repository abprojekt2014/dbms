cmake_minimum_required(VERSION 2.8)

project(testclient)

find_package(Boost COMPONENTS system serialization REQUIRED)

include_directories(${Boost_INCLUDE_DIRS}) 

set(HEADERS
)

set(SOURCES
    client.cpp
    client_main.cpp
)


set(WIN_LIBS
)

if (WIN32)
set(WIN_LIBS
    ${WIN_LIBS}
    wsock32
    ws2_32)
endif()


add_executable(
    testclient
    ${HEADERS}
    ${SOURCES}
)

target_link_libraries(
    testclient
    ${WIN_LIBS}
    ${Boost_LIBRARIES}
    ${Boost_SYSTEM_LIBRARY}
)

