#include "client.h"
#include <boost/asio.hpp>
#include <string>
#include <vector>

using namespace std;


int main(){
    try{
        boost::asio::io_service io_service;

        client c(io_service);
        vector<string> parancsok {"CREATE TABLE disciplines (DiscID varchar(5) PRIMARY KEY, DName varchar(30), CreditNr int);",
            "insert into disciplines (DiscID,DName,CreditNr) values ('DB1','Databases 1', 7);",
            "insert into disciplines (DiscID,DName,CreditNr) values ('DS','Data Structures',6);",
            "select DiscID, DName from disciplines where DiscID = 'DS';",
            "select DiscID, DName, CreditNr from disciplines;"
        };
        cout << c.connect() << endl;
        for (auto& s : parancsok){

            cout << "[EXECUTING]" << s << endl;

            if (!c.write(s))
            {
                break;
            }

            if (!c.read())
                break;

            while (c.use_data())
            {
                c.read();
            }
        }
    }
    catch (std::exception& e){
        std::cerr << e.what() << std::endl;
        int c;
        std::cin>>c;
    }

    return 0;
}
