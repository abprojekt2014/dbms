#include "database_gui.h"
#include <iostream>
#include <QtGui/QMessageBox>
#include <QtGui/QLayout>
#include <string>
#include <vector>

using namespace std;

database_gui::database_gui(QWidget *parent, Qt::WFlags flags): QMainWindow(parent, flags)
{
	ui.setupUi(this);

	_query_widget = new query_widget(this);
	_tools_widget = new tools_widget(this);

	connected = false;

	centralWidget()->setLayout(new QHBoxLayout());
	centralWidget()->layout()->addWidget(_tools_widget);
	centralWidget()->layout()->addWidget(_query_widget);
	_query_widget->hide();

	connect(_tools_widget->ui.pushButton_connect, SIGNAL(clicked()), _tools_widget->ui.widget_database_list, SLOT(clear()));
	connect(_tools_widget->ui.pushButton_connect, SIGNAL(clicked()), this, SLOT(connect_clicked()));

	connect(_tools_widget->ui.pushButton_disconnect, SIGNAL(clicked()), _tools_widget->ui.widget_database_list, SLOT(clear()));
	connect(_tools_widget->ui.pushButton_disconnect, SIGNAL(clicked()), this, SLOT(disconnect_clicked()));

	connect(_tools_widget->ui.pushButton_refreshlist, SIGNAL(clicked()), _tools_widget->ui.widget_database_list, SLOT(clear()));
	connect(_tools_widget->ui.pushButton_refreshlist, SIGNAL(clicked()), this, SLOT(refresh_clicked()));
	
	connect(_tools_widget->ui.pushButton_new_query, SIGNAL(clicked()), this, SLOT(new_query_clicked()));
	connect(_tools_widget->ui.pushButton_open_query, SIGNAL(clicked()), this, SLOT(open_query_clicked()));
	connect(_tools_widget->ui.pushButton_close_query, SIGNAL(clicked()), this, SLOT(close_query_clicked()));
	
	connect(_tools_widget->ui.pushButton_save_query, SIGNAL(clicked()), this, SLOT(save_query_clicked()));

	connect(_tools_widget->ui.pushButton_execute_query, SIGNAL(clicked()), this, SLOT(execute_query_clicked()));

}

database_gui::~database_gui()
{
}

void database_gui::connect_clicked()
{
	//cout<<"connect to server";
	if(connected == false)
	{
		if(_client_server->connect())
		{
			connected = true;
			refresh_list();
		}
		else
			error_message("Can not connect to the server!");
	}
}

void database_gui::disconnect_clicked()
{
	//cout<<"disconnect from server";
	if(connected == true)
	{
		if(_client_server->disconnect())
		{
			connected = false;
			_tools_widget->disconnect();
		}
		else
			error_message("Can not disconnect from the server!");
	}
}

void database_gui::refresh_clicked()
{
	//cout<<"refresh";
	if(connected == true)
		refresh_list();
	else
		error_message("First connect to the server!");
}

void database_gui::new_query_clicked()
{
	//cout<<"new query";
	if(!_query_widget->isVisible())
	{
		_query_widget->show();
		_tools_widget->show_buttons();
	}
	_query_widget->new_query();
	saved.push_back(false);
}

void database_gui::open_query_clicked()
{
	//cout<<"open query";
	QString file_name = _tools_widget->getOpenFileName();
	if(!_query_widget->isVisible())
	{
		_query_widget->show();
		_tools_widget->show_buttons();
	}
	_query_widget->read_from_file(file_name);
	saved.push_back(true);
}

void database_gui::close_query_clicked()
{
	//cout<<"close query";
	if(saved.size() > 0)
	{
		int i = _query_widget->current_tab_index();
		if(saved.at(i)==true)
		{
			_query_widget->close_query();
			saved.erase(saved.begin()+i);
			//test_file();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setText("The document has been modified.");
			msgBox.setInformativeText("Do you want to save your changes?");
			msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
			msgBox.setDefaultButton(QMessageBox::Save);
			int ret = msgBox.exec();

			switch (ret) {
				case QMessageBox::Save:
					save_query_clicked();
					break;
				case QMessageBox::Discard:
					_query_widget->close_query();
					saved.erase(saved.begin()+i);
					break;
				case QMessageBox::Cancel:
					break;
				default:
					break;
			}
		}
		if(saved.size() == 0)
		{
			_query_widget->hide();
			_tools_widget->hide_buttons();
		}
	}
}

void database_gui::save_query_clicked()
{
	//cout<<"save query";
	int i = _query_widget->current_tab_index();
	if(!saved.at(i))
	{
		QString file_name = _tools_widget->getSaveFileName();
		//ellenorizni:irt be file nevet vagy kilepett
		if(file_name != "")
		{
			_query_widget->write_to_file(file_name);
			_query_widget->set_current_tab_name(file_name);
			saved.at(i) = true;
		}
	}
}

void database_gui::execute_query_clicked()
{
	cout<<"execute query";
}

void database_gui::refresh_list()
{
	_tools_widget->refresh_list();

	//get the server name and a list of databases with their columns, keys
	vector<QString> databases = _client_server->getDatabasesName();
	for (long index=0;  index<(long)databases.size(); ++index)
	{
		try {
			//cout << "Element " << index << ": " << databases.at(index) << endl;
			vector<QString> tables = _client_server->getTablesName(databases.at(index).toStdString());
			vector<vector<QString>> columns;
			for (long index2=0;  index2<(long)tables.size(); ++index2)
			{
				try {
					vector<QString> col = _client_server->getColumnsName(databases.at(index).toStdString(), tables.at(index2).toStdString());
					columns.push_back(col);
				}
				catch (exception& e) {
					error_message("Index2 exceeds vector dimensions.");
				}
			}
			_tools_widget->add_to_list(databases.at(index), tables, columns);
		}
		catch (exception& e) {
			error_message("Index exceeds vector dimensions.");
		}
	}
}

void database_gui::error_message(QString message)
{
	QMessageBox messageBox;
	messageBox.critical(0,"Error", message);
	messageBox.setFixedSize(500,200);
}

void database_gui::test_file()
{
	QString message = "";
	QMessageBox messageBox;
	for (long index=0;  index<(long)saved.size(); ++index)
	{
		if(saved.at(index))
			message += "true";
		else
			message += "false";
	}
	messageBox.critical(0,"Error", message);
	messageBox.setFixedSize(500,200);
}

void database_gui::button_box()
{
	
}

void database_gui::messagebox_clicked()
{
}
