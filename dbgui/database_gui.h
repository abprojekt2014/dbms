#ifndef DATABASE_GUI_H
#define DATABASE_GUI_H

#include <QtGui/QMainWindow>
#include "ui_database_gui.h"
#include <QtGui/QMessageBox>

#include "query_widget.h"
#include "tools_widget.h"
#include "client_server.h"

class database_gui : public QMainWindow
{
private:
	Q_OBJECT

	query_widget *_query_widget;
	tools_widget *_tools_widget;

	client_server *_client_server;

	bool connected;
	vector<bool> saved;

	QMessageBox close_or_save;

public:
	database_gui(QWidget *parent = 0, Qt::WFlags flags = 0);
	~database_gui();

	/*signals:
		void index_changed(int index);*/

private:
	Ui::database_gui ui;

private slots:

	void connect_clicked();
	void disconnect_clicked();
	void refresh_clicked();

	void new_query_clicked();
	void open_query_clicked();
	void close_query_clicked();

	void save_query_clicked();
	void execute_query_clicked();

	void refresh_list();
	void error_message(QString message);

	void test_file();
	void button_box();

	void messagebox_clicked();
};

#endif // DATABASE_GUI_H
