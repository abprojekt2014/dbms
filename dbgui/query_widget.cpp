#include "query_widget.h"
#include <QtGui/QLayout>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

query_widget::query_widget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	ui.tabWidget_query->clear();
}

query_widget::~query_widget()
{

}

void query_widget::write_to_file(const QString& file_name)
{
	QTextEdit* tt = qobject_cast<QTextEdit*>(ui.tabWidget_query->currentWidget()->layout()->itemAt(0)->widget());
	QString s = tt->toPlainText();

	fstream f(file_name.toAscii().data(), ios_base::out);
	
	f << ".qr" << endl;
	f << s.toStdString() << endl;
	f.close();
}

void query_widget::read_from_file(const QString& file_name)
{
	fstream f(file_name.toAscii().data(), ios_base::in);

	string type;
	f >> type;

	if(type == ".qr")
	{
		string q = "";
		string line;
		while (getline(f, line))
		{
			q = q + line + "\n";
		}
		QString qstr = QString::fromStdString(q);

		//new tab
		QWidget* pg = new QWidget();
		QTextEdit * t = new QTextEdit();
		t->setText(qstr);
	
		pg->setLayout(new QHBoxLayout());
		pg->layout()->addWidget(t);

		ui.tabWidget_query->addTab(pg,file_name);
	}
}

void query_widget::new_query()
{
	//new tab
	QWidget* pg = new QWidget();
	QTextEdit * t = new QTextEdit();
	
	pg->setLayout(new QHBoxLayout());
	pg->layout()->addWidget(t);

	ui.tabWidget_query->addTab(pg,"new_query*");

	//ui.tabWidget_query->setTabsClosable(true);
}

void query_widget::close_query()
{
	//int ind = ui.tabWidget_query->currentIndex();
	ui.tabWidget_query->removeTab(current_tab_index());
}

void query_widget::set_current_tab_name(QString name)
{
	ui.tabWidget_query->setTabText(current_tab_index(), name);
}

int query_widget::current_tab_index()
{
	return ui.tabWidget_query->currentIndex();
}
