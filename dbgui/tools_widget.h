#ifndef TOOLS_WIDGET_H
#define TOOLS_WIDGET_H

#include <QtGui/QWidget>
#include "ui_tools_widget.h"
#include "vector"
using namespace std;

class tools_widget : public QWidget
{
	Q_OBJECT

public:
	Ui::tools_widget ui;

	tools_widget(QWidget *parent = 0);
	~tools_widget();

	void show_buttons();
	void hide_buttons();

	QString getSaveFileName();
	QString getOpenFileName();

	void refresh_list();
	void add_to_list(QString database, vector<QString> tables, vector<vector<QString>> columns);
	void disconnect();

	QTreeWidgetItem* addTreeRoot(QString name);
    QTreeWidgetItem* addTreeChild(QTreeWidgetItem *parent, QString name);
};

#endif // TOOLS_WIDGET_H