#include "client_server.h"
#include <iostream>
#include "vector"
using namespace std;

client_server::client_server()
{
}

client_server::~client_server()
{
}

bool client_server::isConnected()
{
	return true;
}

bool client_server::connect()
{
	return true;
}

bool client_server::disconnect()
{
	return true;
}

vector<QString>client_server::getDatabasesName()
{
	vector<QString> vect;
	vect.push_back("database1");
	vect.push_back("database2");
	return vect;
}

vector<QString> client_server::getTablesName(string database)
{
	vector<QString> vect;
	vect.push_back("table1");
	vect.push_back("table2");
	return vect;
}

vector<QString> client_server::getColumnsName(string database, string table)
{
	vector<QString> vect;
	vect.push_back("col1");
	vect.push_back("col2");
	return vect;
}

vector<QString> client_server::getKeysName(string database, string table)
{
	vector<QString> vect;
	return vect;
}

vector<string> client_server::executeQuery(string query)
{
	vector<string> vect;
	return vect;
}