#include "database_gui.h"
#include "query_widget.h"
#include <iostream>
#include <QtGui/QApplication>

using namespace std;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	database_gui d;
	d.show();

	return a.exec();
}
