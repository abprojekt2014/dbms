#ifndef CLIENT_SERVER_H
#define CLIENT_SERVER_H

#include <vector>
#include "qstring.h"
using namespace std;

class client_server
{
public:
	client_server();
	~client_server();

	bool isConnected();
	bool connect();
	bool disconnect();

	vector<QString> getDatabasesName();
	vector<QString> getTablesName(string database);
	vector<QString> getColumnsName(string database, string table);
	vector<QString> getKeysName(string database, string table);

	vector<string> executeQuery(string query);

};

#endif // CLIENT_SERVER_H
