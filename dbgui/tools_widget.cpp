#include "tools_widget.h"
#include <QFileDialog>
#include "vector"
using namespace std;

tools_widget::tools_widget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	hide_buttons();
}

tools_widget::~tools_widget()
{

}

void tools_widget::show_buttons()
{
	ui.pushButton_close_query->setVisible(true);
	ui.pushButton_execute_query->setVisible(true);
	ui.pushButton_save_query->setVisible(true);
}

void tools_widget::hide_buttons()
{
	ui.pushButton_close_query->hide();
	ui.pushButton_execute_query->hide();
	ui.pushButton_save_query->hide();
}

QString tools_widget::getSaveFileName()
{
	QString file_name = QFileDialog::getSaveFileName(this, "Save data...", "Query", "Query (*.qr)");
	return file_name;
}

QString tools_widget::getOpenFileName()
{
	QString file_name = QFileDialog::getOpenFileName(this, "Open file...", "Query", "Query (*.qr)");
	return file_name;
}

void tools_widget::refresh_list()
{
	ui.label_connection->setText("Connected to:");

	// Set the number of columns in the tree
    ui.widget_database_list->setColumnCount(1);    
}

void tools_widget::add_to_list(QString database, vector<QString> tables, vector<vector<QString>> columns)
{
	//databases
    QTreeWidgetItem* item_database_1 = addTreeRoot(database+"(Database)");

		QTreeWidgetItem* item_tables = addTreeChild(item_database_1, "Tables");

			for (long index=0;  index<(long)tables.size(); ++index)
			{
				try {
					QTreeWidgetItem* item_table = addTreeChild(item_tables, tables.at(index));
					QTreeWidgetItem* item_columns = addTreeChild(item_table, "Columns");
					
					for (long index2=0;  index2<columns.at(index).size(); ++index2)
					{
						try {
							QTreeWidgetItem* item_column = addTreeChild(item_columns, columns.at(index).at(index2));
						}
						catch (exception& e) {
							//index hiba
						}
					}
				}
				catch (exception& e) {
					//index hiba
				}
			}

			/*QTreeWidgetItem* item_table_1 = addTreeChild(item_tables, "table1");

				QTreeWidgetItem* item_columns = addTreeChild(item_table_1, "Columns");
					QTreeWidgetItem* item_column_1 = addTreeChild(item_columns, "column1");
					QTreeWidgetItem* item_column_2 = addTreeChild(item_columns, "column2");

				QTreeWidgetItem* item_keys = addTreeChild(item_table_1, "Keys");
					QTreeWidgetItem* item_key_1 = addTreeChild(item_keys, "key1");
					QTreeWidgetItem* item_key_2 = addTreeChild(item_keys, "key2");

			QTreeWidgetItem* item_table_2 = addTreeChild(item_tables, "table2");

	QTreeWidgetItem* item_database_2 = addTreeChild(item_databases, "database2");*/
}

void tools_widget::disconnect()
{
	ui.label_connection->setText("Disconnected from:");
}

QTreeWidgetItem* tools_widget::addTreeRoot(QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
	QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui.widget_database_list);

    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);

	return treeItem;
}

QTreeWidgetItem* tools_widget::addTreeChild(QTreeWidgetItem *parent, QString name)
{
    // QTreeWidgetItem(QTreeWidget * parent, int type = Type)
    QTreeWidgetItem *treeItem = new QTreeWidgetItem();

    // QTreeWidgetItem::setText(int column, const QString & text)
    treeItem->setText(0, name);

    // QTreeWidgetItem::addChild(QTreeWidgetItem * child)
    parent->addChild(treeItem);

	return treeItem;
}
