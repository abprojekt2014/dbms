#pragma once
#include <string>
#include <vector>
#include "../sql_parser/SQLParserResult.h"
#include "../sql_parser/sql/SQLStatement.h"

class SqlQueryResult
{
public:
    bool succesfull;
    std::string message;
    int line;
    int column;
    std::vector<hsql::SQLStatement*> statements;

    SqlQueryResult(hsql::SQLParserResult& result);
    ~SqlQueryResult();
private:
    void copyStatements(hsql::SQLParserResult& result);
    void cleanupStatements();
};

