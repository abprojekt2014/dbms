#include <cassert>
#include "SqlEngine.h"
#include "SqlQueryResult.h"
#include "../sql_parser/SQLParser.h"
#include "CreateTableCommand.h"
#include "InsertCommand.h"
#include "SqlExceptions.h"

SqlEngine::SqlEngine()
{
}

SqlEngine::~SqlEngine()
{
}

SqlQueryResult* SqlEngine::parseSqlStatement(std::string querystring)
{
    SqlQueryResult* result;
    hsql::SQLParserResult* parserResult = hsql::SQLParser::parseSQL(querystring);

    result = new SqlQueryResult(*parserResult);

    delete[] parserResult;
    return result;
}

SqlQueryResult * SqlEngine::query(std::string querystring)
{
    SqlQueryResult* result = parseSqlStatement(querystring);
    
    if (!result->succesfull)
        return result;

    try
    {
        executeStatements(result->statements.begin(), result->statements.end());
    }
    catch (ColumnsDefinedTwice* cdt)
    {
        result->succesfull = false;
        result->message = "Column with name '"+cdt->columnName+"' was defined twice.";
    }
    catch (InvalidColumnParameters)
    {
        result->succesfull = false;
        result->message = "One of the parameter's value is too long.";
    }
    catch (InvalidColumnType)
    {
        result->succesfull = false;
        result->message = "Invalid column type detected.";
    }
    catch (PrimaryKeyNotUnique* pknu)
    {
        result->succesfull = false;
        result->message = "Two or more primary keys were declared with name '"+pknu->publicKey+"'.";
    }
    catch (TableAlreadyExists* tae)
    {
        result->succesfull = false;
        result->message = "The table with name '"+tae->tableName+"' is already existing.";
    }
    catch (ColumnNameNotDefinedForTable* cndft)
    {
        result->succesfull = false;
        result->message = "Column '"+cndft->columnname+"' is not defined for table '"+cndft->tablename+"'";
    }
    catch (DuplicateColumnEntryFound* dc)
    {
        result->succesfull = false;
        result->message = "Duplicate column entry found for column '" + dc->col +"'";
    }
    catch (TableDoesNotExists* tdne)
    {
        result->succesfull = false;
        result->message = "Table '" + tdne->tableName + "' does not exist.";
    }
    catch (ForeignKeyTypeMismatch* fktm)
    {
        result->succesfull = false;
        result->message = "Column type mismatch for foreign key '" + fktm->columnname + "'";
    }
    return result;
}

void SqlEngine::executeStatements(std::vector<hsql::SQLStatement*>::const_iterator& begin, std::vector<hsql::SQLStatement*>::const_iterator& end)
{
    for (auto it = begin; it < end; it++)
    {
        executeStatement(*(*it));
    }
}

void SqlEngine::executeStatement(hsql::SQLStatement& statement)
{
    switch (statement.type())
    {
    case hsql::kStmtCreate:
        CreateTableCommand::execute(static_cast<hsql::CreateStatement&>(statement));
        break;
    case hsql::kStmtInsert:
        InsertCommand::execute(static_cast<hsql::InsertStatement&>(statement));
        break;
    default:
        assert("Not implemented" == nullptr);
    }
}
