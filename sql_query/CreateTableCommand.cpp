#include <set>
#include "CreateTableCommand.h"
#include "SqlEngine.h"
#include "../record_manager/table_manager.h"
#include "../index_manager/index_manager.h"
#include "../sql_query/SqlExceptions.h"

CreateTableCommand::CreateTableCommand()
{
}

CreateTableCommand::~CreateTableCommand()
{
}

bool CreateTableCommand::areColumnParametersGood(int coltype, int collen)
{
    switch (coltype)
    {
    case ctInteger:
        return true;
    case ctString:
        if (collen < 0 || collen > 4096)
        {
            return false;
        }
        return true;
    case ctBoolean:
        return true;
    }

    return false;
}

void CreateTableCommand::verifyColumns(std::vector<hsql::ColumnDefinition*>::const_iterator& begin, std::vector<hsql::ColumnDefinition*>::const_iterator& end)
{
    std::set<std::string> columns;

    for (auto col = begin; col < end; col++)
    {
        int coltype;
        int collen;

        getColumnParameters(*col, coltype, collen);
        if (!areColumnParametersGood(coltype, collen))
        {
            throw InvalidColumnParameters();
        }
        if (columns.find((*col)->name) != columns.end())
        {
            throw new ColumnsDefinedTwice((*col)->name);
        }
        columns.insert((*col)->name);
    }
}

int CreateTableCommand::fixColumnBinaryLength(int coltype, int collen)
{
    switch (coltype)
    {
    case ctInteger:
        return SqlEngine::IntLength;
    case ctBoolean:
        return SqlEngine::BoolLength;
    }

    return collen;
}

catalog CreateTableCommand::createCatalog(std::vector<hsql::ColumnDefinition*>::const_iterator& begin, std::vector<hsql::ColumnDefinition*>::const_iterator& end)
{
    catalog tablecatalog;

    for (auto col = begin; col < end; col++)
    {
        int coltype;
        int collen;

        getColumnParameters(*col, coltype, collen);
        collen = fixColumnBinaryLength(coltype, collen);
        column_t* mycol = tablecatalog.addcolumn((*col)->name, (BYTE)coltype, (DWORD)collen);
        if ((*col)->keyconstraints && (*col)->keyconstraints->primarykey)
        {
            if (primaryKey.found)
            {
                throw new PrimaryKeyNotUnique((*col)->name);
            }

            primaryKey.found = true;
            primaryKey.name = (*col)->name;
            primaryKey.length = collen;
            primaryKey.type = coltype;
        }
    }

    return tablecatalog;
}

void CreateTableCommand::createIndexForCreateCommand(hsql::CreateStatement& statement)
{
    if (!primaryKey.found)
        return;

    index_t* idx = indexmanager::getinstance().createindex(idxHashTable, statement.tableName, primaryKey.name, (columntype)primaryKey.type, primaryKey.length);
}

void CreateTableCommand::verifyForeignKeys(std::vector<hsql::ColumnDefinition *>* columns)
{
    for (auto c : *columns)
    {
        if (c->keyconstraints && c->keyconstraints->foreignkey)
        {
            verifyIfTableColumnExists(c->keyconstraints->table, c->keyconstraints->column);
            verifyForeignKeyType(c);
        }
    }
}

void CreateTableCommand::verifyIfTableColumnExists(std::string& table, std::string& column)
{
    table_t* t = tablemanager::getinstance().gettable(table);
    if (!t)
    {
        throw new TableDoesNotExists(table);
    }

    if (!t->column(column))
    {
        throw new ColumnNameNotDefinedForTable(table, column);
    }
}

void CreateTableCommand::verifyForeignKeyType(hsql::ColumnDefinition* c)
{
    table_t* t = tablemanager::getinstance().gettable(c->keyconstraints->table);
    hsql::ColumnDefinition::DataType cdt = c->type;
    columntype ct = ctUnknown;
    column_t* col = t->column(c->keyconstraints->column);

    if (cdt == hsql::ColumnDefinition::INT)
    {
        ct = ctInteger;
    }
    else if (cdt == hsql::ColumnDefinition::VARCHAR || cdt == hsql::ColumnDefinition::TEXT)
    {
        ct = ctString;
    }

    if (col->type != ct ||
        ct == ctString && col->length != c->length)
    {
        throw new ForeignKeyTypeMismatch(c->keyconstraints->column);
    }
}

void CreateTableCommand::createTableForCreateCommand(hsql::CreateStatement& statement)
{
    catalog tablecatalog;

    if (tablemanager::getinstance().gettable(statement.tableName))
    {
        throw new TableAlreadyExists(statement.tableName);
    }

    verifyColumns(statement.columns->begin(), statement.columns->end());
    verifyForeignKeys(statement.columns);
    tablecatalog = createCatalog(statement.columns->begin(), statement.columns->end());

    table_t* newtable = tablemanager::getinstance().createtable(statement.tableName, tablecatalog);

    newtable->close();
}

void CreateTableCommand::execute(hsql::CreateStatement& statement)
{
    CreateTableCommand command;

    command.primaryKey.found = false;
    command.createTableForCreateCommand(statement);
    command.createIndexForCreateCommand(statement);
}

void CreateTableCommand::getColumnParameters(hsql::ColumnDefinition* column, int& columntype, int& columnlength)
{
    switch (column->type)
    {
    case hsql::ColumnDefinition::INT:
        columntype = ctInteger;
        break;
    case hsql::ColumnDefinition::DOUBLE:
        assert("Not Implemented" == nullptr);
        columntype = ctInteger;
        break;
    case hsql::ColumnDefinition::VARCHAR:
    case hsql::ColumnDefinition::TEXT:
        columntype = ctString;
        break;
    }

    columnlength = column->length;
}