#include "DataRow.h"

DataRow::DataRow(catalog& cat):
    cat(cat),
    values_set_bitmap(0)
{
    buffer = new CHAR[cat.rowlength()];
    serializer = new serializer_t(cat.rowlength());
    values.resize(cat.columns().size());

    memset(buffer, 0, cat.rowlength());
}

DataRow::~DataRow()
{
    delete buffer;
    delete serializer;
}

void DataRow::Serialize(int index, int value)
{
    if (index > 63 || ((1 << index) & values_set_bitmap))
    {
        throw new ColumnAlreadySet(index);
    }

    column_t* col = cat.columns()[index];

    if (col->type != ctInteger)
    {
        throw WrongColumnType();
    }

    serializer->serialize_int(buffer, col->offset, value);
    values[index] = std::pair<int, void*>(0,&value);

    values_set_bitmap |= 1 << index;
}

void DataRow::Serialize(int index, std::string value)
{
    if (index > 63 || ((1 << index) & values_set_bitmap))
    {
        throw new ColumnAlreadySet(index);
    }

    column_t* col = cat.columns()[index];

    if (col->type != ctString)
    {
        throw WrongColumnType();
    }

    if (col->length < value.size())
    {
        throw DataTruncation();
    }

    serializer->serialize_str(buffer, col->offset, value.c_str(), value.size());
    values[index] = std::pair<int, void*>(0, &value);

    values_set_bitmap |= 1 << index;
}

void DataRow::Serialize(column_t& col, int value)
{
    int index = col.column_index;

    if (index > 63 || ((1 << index) & values_set_bitmap))
    {
        throw new ColumnAlreadySet(index);
    }

    if (col.type != ctInteger)
    {
        throw WrongColumnType();
    }

    serializer->serialize_int(buffer, col.offset, value);
    values[index] = std::pair<int, void*>(0, &value);

    values_set_bitmap |= 1 << index;
}

void DataRow::Serialize(column_t& col, std::string value)
{
    int index = col.column_index;

    if (index > 63 || ((1 << index) & values_set_bitmap))
    {
        throw new ColumnAlreadySet(index);
    }

    if (col.type != ctString)
    {
        throw WrongColumnType();
    }

    if (col.length < value.size())
    {
        throw DataTruncation();
    }

    serializer->serialize_str(buffer, col.offset, value.c_str(), value.size());
    values[index] = std::pair<int, void*>(0, &value);

    values_set_bitmap |= 1 << index;
}

std::pair<int, void*>& DataRow::GetValue(column_t& col)
{
    int index = col.column_index;

    return values[index];
}

void DataRow::ToBinary(char* buffer, SIZE_T size)
{
    if (size < cat.rowlength())
    {
        throw DataTruncation();
    }

    memcpy(buffer, this->buffer, cat.rowlength());
}

char* DataRow::GetSerializedRow()
{
    return buffer;
}
