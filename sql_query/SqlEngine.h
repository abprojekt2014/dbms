#pragma once
#include <string>
#include <vector>
#include "SqlQueryResult.h"
#include "../sql_parser/sql/SQLStatement.h"

class SqlEngine
{
public:
    static const int IntLength = 4;
    static const int BoolLength = 4;

    ~SqlEngine();
    static SqlQueryResult* query(std::string querystring);

private:
    SqlEngine();

    static SqlQueryResult* parseSqlStatement(std::string querystring);
    static void executeStatements(std::vector<hsql::SQLStatement*>::const_iterator& begin, std::vector<hsql::SQLStatement*>::const_iterator& end);
    static void executeStatement(hsql::SQLStatement& statement);

};

