#pragma once
#include "../sql_parser/sql/InsertStatement.h"
#include "../record_manager/table.h"
#include "../index_manager/index.h"
#include "DataRow.h"
#include <map>

class InsertCommand
{
public:
    InsertCommand() :col_map() {}
    ~InsertCommand();

    static void execute(hsql::InsertStatement& statement);

private:
    std::map<std::string, std::pair<column_t*, int>*> col_map;
    std::vector<column_t*> col_order;
    std::vector<std::pair<DataRow*, recordid_t>> insertedRows;

    typedef bool (*ColumnCompare)(InsertCommand* self, column_t* column, std::string& columnname, int colindex);
    static bool ValidateColumnName(InsertCommand* self, column_t* column, std::string& columnname, int colindex);
    static bool BindColumnByName(InsertCommand* self, column_t* column, std::string& columnname, int colindex);

    void InsertRowsInTable(hsql::InsertStatement& statement);
    void InsertValuesWithoutColumnNames(table_t* table, hsql::InsertStatement& statement);
    void VerifyIfStatementIsMatching(table_t* table, hsql::InsertStatement& statement);
    void RunForColumns(hsql::InsertStatement &statement, table_t* table, ColumnCompare cmp);
    void VerifyDataTypeForColumn(column_t* column, hsql::Expr* value);
    void InsertValuesWithColumnNames(hsql::InsertStatement& statement, table_t* table);
    void InsertValuesIntoIndices();
    void VerifyKeyConstraints(DataRow* row, char* binrow);
};

