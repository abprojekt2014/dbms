#include "InsertCommand.h"
#include "../record_manager/table_manager.h"
#include "../index_manager/index_manager.h"
#include <cstring>
#include <algorithm>
#include <memory>
#include "../sql_query/SqlExceptions.h"

InsertCommand::~InsertCommand()
{
    /*for (auto p : col_map)
    {
        auto _pair = p.second;

        delete _pair;
    }*/
}

void InsertCommand::execute(hsql::InsertStatement& statement)
{
    InsertCommand insert;

    insert.InsertRowsInTable(statement);
}


void InsertCommand::InsertRowsInTable(hsql::InsertStatement& statement)
{
    table_t* table = tablemanager::getinstance().gettable(statement.tableName);

    if (!table)
    {
        throw new TableDoesNotExists(statement.tableName);
    }

    VerifyIfStatementIsMatching(table, statement);
}

bool InsertCommand::ValidateColumnName(InsertCommand* self, column_t* column, std::string& columnname, int colindex)
{
    return 0 == ::stricmp(column->name.c_str(), columnname.c_str());
}

bool InsertCommand::BindColumnByName(InsertCommand* self, column_t* column, std::string& columnname, int colindex)
{
    if (0 != ::stricmp(column->name.c_str(), columnname.c_str()))
        return false;

    std::string _lower;
    _lower.resize(columnname.size());
    std::transform(columnname.begin(), columnname.end(), _lower.begin(), ::tolower);

    if (!self->col_map.insert(std::pair<std::string, std::pair<column_t*, int>*>(_lower, new std::pair<column_t*, int>(column, colindex))).second)
    {
        throw new DuplicateColumnEntryFound(columnname);
    }

    self->col_order[colindex] = column;

    // returns true if must break the iteration
    return true;
}

void InsertCommand::VerifyIfStatementIsMatching(table_t* table, hsql::InsertStatement& statement)
{
    if (statement.columns)
    {
        RunForColumns(statement, table, &this->ValidateColumnName);
        col_order.resize(statement.values->size());
        RunForColumns(statement, table, &this->BindColumnByName);
        InsertValuesWithColumnNames(statement, table);
        InsertValuesIntoIndices();
    }
    else
    {
        InsertValuesWithoutColumnNames(table, statement);
    }
}

void InsertCommand::RunForColumns(hsql::InsertStatement &statement, table_t* table, ColumnCompare cmp)
{
    catalog& cat = table->getcatalog();

    int colidx = 0;
    for (auto colname : *statement.columns)
    {
        bool f = false;
        for (auto column : cat.columns())
        {
            if (cmp(this, column, *colname, colidx))
            {
                f = true;
                break;
            }
        }

        colidx++;

        if (!f)
        {
            throw new ColumnNameNotDefinedForTable(table->name(), *colname);
        }
    }
}

void InsertCommand::VerifyDataTypeForColumn(column_t* column, hsql::Expr* value)
{
    throw std::logic_error("The method or operation is not implemented.");
}

void InsertCommand::InsertValuesWithColumnNames(hsql::InsertStatement& statement, table_t* table)
{
    DataRow* row = new DataRow(table->getcatalog());
    int rowlen = table->getcatalog().rowlength();
    std::vector<column_t*>::iterator column = col_order.begin();
    
    for (hsql::Expr* e : *statement.values)
    {
        switch (e->type)
        {
        case hsql::kExprLiteralInt:
            row->Serialize(**column, (int)e->ival);
            break;
        case hsql::kExprLiteralString:
            row->Serialize(**column, e->getName());
            break;
        default:
            assert(false);
            break;
        }
        column++;
    }

    char* binrow = new char[rowlen];
    row->ToBinary(binrow, rowlen);

    VerifyKeyConstraints(row, binrow);

    recordid_t rid = table->insert_row((PBYTE)binrow);
    if (invalid_recordid == rid)
    {
        throw new UnknownDatabaseError(rid);
    }

    insertedRows.push_back(std::pair<DataRow*, recordid_t>(row, rid));

    delete []binrow;
}

void InsertCommand::InsertValuesIntoIndices()
{
    for (auto record : insertedRows)
    {
        auto row = record.first;
        auto rid = record.second;

        for (auto col : col_order)
        {
            if (!col->attributes.indexed)
            {
                continue;
            }

            if (col->attributes.primary_key)
            {
                col->primary_key.primary_key_index->insertrow(rid, row->GetSerializedRow());
                continue;
            }

            if (col->attributes.foreign_key)
            {
                col->foreign_key.foreign_key_index->insertrow(rid, row->GetSerializedRow());
                continue;
            }
        }
    }
}

void InsertCommand::VerifyKeyConstraints(DataRow* row, char* binrow)
{
    for (auto col : col_order)
    {
        if (!col->attributes.indexed)
        {
            continue;
        }

        if (col->attributes.foreign_key)
        {
            table_t* t = tablemanager::getinstance().gettable(*col->foreign_key.ref_table_name);
            index_t* i = col->foreign_key.foreign_key_index;

            std::pair<int, void*> value = row->GetValue(*col);
            std::unique_ptr<iterator_t> it(i->getiterator_equal(value.second));

            if (it->first() == invalid_recordid)
            {
                throw UnexistingForeignKeyValue(col->name);
            }
            continue;
        }
        if (col->attributes.primary_key)
        {
            std::pair<int, void*> value = row->GetValue(*col);
            std::unique_ptr<iterator_t> it(col->primary_key.primary_key_index->getiterator_equal(value.second));

            if (it->first() != invalid_recordid)
            {
                throw ExistingPrimaryKeyValue();
            }
        }
    }
}

void InsertCommand::InsertValuesWithoutColumnNames(table_t* table, hsql::InsertStatement& statement)
{
    catalog& cat = table->getcatalog();
    const SIZE_T rowlength = cat.rowlength();

    std::vector<char*> binaryRows;

    if (statement.values->size() != cat.columns().size())
    {
        throw ColumNumberMistmatch();
    }

    std::vector<column_t*>::iterator colit;

    colit = cat.columns().begin();
    for (auto value : *statement.values)
    {
        VerifyDataTypeForColumn(*colit, value);
    }
}
