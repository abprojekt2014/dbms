#pragma once
#include <string>
#include "../include/dbtypes.h"

class InvalidColumnParameters {};
class ColumNumberMistmatch {};
class ExistingPrimaryKeyValue {};

class TableAlreadyExists 
{
public:
    std::string tableName;
    TableAlreadyExists(std::string tableName) :tableName(tableName) {}
};

class TableDoesNotExists
{
public:
    std::string tableName;
    TableDoesNotExists(std::string tableName) :tableName(tableName) {}
};

class InvalidColumnType
{
public:
    std::string columnType;
    InvalidColumnType(std::string columnType) :columnType(columnType) {}
};

class ColumnsDefinedTwice
{
public:
    std::string columnName;
    ColumnsDefinedTwice(std::string columnName) :columnName(columnName) {}
};

class PrimaryKeyNotUnique
{
public:
    std::string publicKey;
    PrimaryKeyNotUnique(std::string publicKey) :publicKey(publicKey) {}
};

class DuplicateColumnEntryFound {
public:
    std::string col;
    DuplicateColumnEntryFound(std::string& colname) :col(colname) {}
};

class UnknownDatabaseError {
public:
    recordid_t rid;
    UnknownDatabaseError(recordid_t rid) :rid(rid) {}
};

class UnexistingForeignKeyValue {
public:
    std::string col;
    UnexistingForeignKeyValue(std::string c) :col(c) {}
};

class ForeignKeyTypeMismatch {
public:
    std::string columnname;
    ForeignKeyTypeMismatch(std::string columnname) :columnname(columnname) {}
};

class ColumnNameNotDefinedForTable
{
public:
    std::string tablename;
    std::string columnname;
    ColumnNameNotDefinedForTable(std::string tableName, std::string columnName) :
        tablename(tableName),
        columnname(columnName)
    {}
};
