#pragma once
#include "../record_manager/catalog.h"
#include "../record_manager/serializer.hpp"
#include <string>
#include <vector>
#include <map>

class DataRow
{
public:
    class ColumnAlreadySet { public: int index; ColumnAlreadySet(int index) :index(index) {} };
    class WrongColumnType {};
    class DataTruncation {};

    DataRow(catalog& cat);
    ~DataRow();

    void Serialize(int index, int value);
    void Serialize(int index, std::string value);
    void Serialize(column_t& col, int value);
    void Serialize(column_t& col, std::string value);
    std::pair<int,void*>& GetValue(column_t& col);
    void ToBinary(char* buffer, SIZE_T size);
    char* GetSerializedRow();

protected:
    catalog cat;
    std::vector<column_t*> columns;
    std::vector<std::pair<int,void*>> values;
    unsigned long long values_set_bitmap;
    char* buffer;
    serializer_t* serializer;
};

