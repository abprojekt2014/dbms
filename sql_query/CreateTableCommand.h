#pragma once
#include "../sql_parser/sql/CreateStatement.h"

typedef struct catalog;

class CreateTableCommand
{
public:
    ~CreateTableCommand();
    
    static void execute(hsql::CreateStatement& statement);

private:
    struct
    {
        bool        found;
        std::string name;
        int         type;
        int         length;
    } primaryKey;

    CreateTableCommand();

    void getColumnParameters(hsql::ColumnDefinition* column, int& columntype, int& columnlength);
    bool areColumnParametersGood(int coltype, int collen);
    void verifyColumns(std::vector<hsql::ColumnDefinition*>::const_iterator& begin, std::vector<hsql::ColumnDefinition*>::const_iterator& end);
    catalog createCatalog(std::vector<hsql::ColumnDefinition*>::const_iterator& begin, std::vector<hsql::ColumnDefinition*>::const_iterator& end);
    int fixColumnBinaryLength(int coltype, int collen);
    void createTableForCreateCommand(hsql::CreateStatement& statement);
    void createIndexForCreateCommand(hsql::CreateStatement& statement);
    void verifyForeignKeys(std::vector<hsql::ColumnDefinition *>* columns);
    void verifyIfTableColumnExists(std::string& table, std::string& column);
    void verifyForeignKeyType(hsql::ColumnDefinition* c);
};

