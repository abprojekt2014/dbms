#include "SqlQueryResult.h"
#include <cassert>

#include "../sql_parser/SQLParser.h"

hsql::SQLStatement* copyStatement(hsql::SQLStatement* from);

// not in class to not affect the imports
std::string getErrorMessageFromParserResult(hsql::SQLParserResult& parserResult)
{
    return parserResult.errorMsg ? parserResult.errorMsg : "";
}

SqlQueryResult::SqlQueryResult(hsql::SQLParserResult& result)
{
    this->succesfull = result.isValid;
    this->message = getErrorMessageFromParserResult(result);
    this->line = result.errorLine;
    this->column = result.errorColumn;

    this->statements.resize(result.statements.size());

    copyStatements(result);
}

void SqlQueryResult::copyStatements(hsql::SQLParserResult& result)
{
    for (size_t i = 0; i < result.statements.size(); i++)
    {
        this->statements[i] = copyStatement(result.statements[i]);
    }
}

hsql::SQLStatement* copyStatement(hsql::SQLStatement* from)
{
    hsql::SQLStatement* res = nullptr;

    switch (from->type())
    {
    case hsql::kStmtCreate:
        res = new hsql::CreateStatement(*dynamic_cast<hsql::CreateStatement*>(from));
        break;
    case hsql::kStmtInsert:
        res = new hsql::InsertStatement(*dynamic_cast<hsql::InsertStatement*>(from));
        break;
    default:
        assert("Not implemented" == nullptr);
        res = new hsql::SQLStatement(*from);
    }

    return res;
}

SqlQueryResult::~SqlQueryResult()
{
    cleanupStatements();
}

void SqlQueryResult::cleanupStatements()
{
    for (auto i = statements.begin(); i < statements.end(); i++)
    {
        delete *i;
    }
}
