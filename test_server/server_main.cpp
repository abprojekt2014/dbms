#include "../server/server.h"
#include "../parser/parser.h"

using namespace std;

int main(){
    Server* s = nullptr;
    Parser* pp = nullptr;

    while (true)
    {
        //try
        {
            boost::asio::io_service io_service;
            s = new Server(io_service);
            pp = new Parser();

            pp->register_server(s);

            for (;;){
                if(!s->read())
                {
                    std::cerr << "[ERROR]: " << s->error() << std::endl;
                    break;
                }
                string query = s->get_data();

                cout << "[QUERY]" << query << endl;
                pp->interpret(query);
                cout << "[DONE]" << endl;

     //           if(!s->write_return_value)
       //             break;
            }
        }
        /*catch (std::exception& e){
            std::cerr << e.what() << std::endl;
            int c;
            std::cin>>c;
        }*/
        delete pp;
    }

    return 0;
}
