#include "where_items.h"
#include "table_functions.h"

using std::vector;
using std::map;
using std::cout;
using std::endl;
using std::shared_ptr;
using std::unique_ptr;
using std::string;
using std::ostream_iterator;

LogNode::LogNode(){}

LogNode::~LogNode(){}


LogEq::LogEq(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogEq::~LogEq(){}

vector<vector<row_id>> LogEq::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    if(child2.which() != 2){
        if(!local_table_pointers[name.table_name]->column(name.value)->attributes.indexed ||
           local_table_pointers[name.table_name]->column(name.value)->attributes.foreign_key){
            iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
            for (recordid_t row = it->first(); !it->end(); row = it->next()){
                log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
                if(result == child2){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
            delete it;
        }
        else{//van index
            index_t* idx = nullptr;
            column_t* col = local_table_pointers[name.table_name]->column(name.value);
            if(col->attributes.primary_key){
                idx = col->primary_key.primary_key_index;
            }
            else if (!col->attributes.foreign_key)
            {
                idx = col->index;
            }
            unique_ptr<iterator_t> it;

            if(child2.which() == 0){
                int i = boost::get<int>(child2);
                it.reset(idx->getiterator_equal(&i));
            }
            else{
                string s = boost::get<string>(child2);
                char* cstr = const_cast<char*>(s.c_str());
                it.reset(idx->getiterator_equal(cstr));
            }

            for (recordid_t rid = it->first(); !it->end(); rid = it->next()){
                temp_row[index_of_table] = rid;
                list.push_back(temp_row);
            }
        }
    }
    else{//joinrol van szo
        iterator_t* it = local_table_pointers[name.table_name]->get_iterator();

        Name child2_n = boost::get<Name>(child2);
        child2_n.resolve_table_name(local_table_pointers);
        //cout << name << " - " << child2_n << endl;
        //cout << local_table_pointers.size() << endl;
        size_t index_of_table2;
        for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
            if(it->first == child2_n.table_name){
                index_of_table2 = std::distance(local_table_pointers.begin(), it);
                break;
            }
        }
        //cout << index_of_table2 << endl;
        size_t column_index2 = get_column_index_in_table(local_table_pointers, child2_n.table_name, child2_n.value);
        iterator_t* it2 = local_table_pointers[child2_n.table_name]->get_iterator();
        for (recordid_t row = it->first(); !it->end(); row = it->next()){
            log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
            for (recordid_t row2 = it2->first(); !it2->end(); row2 = it2->next()){
                log_variant_t result2 = get_from_table(local_table_pointers, child2_n.table_name, row2, column_index2);
                if(result == result2){
                    temp_row[index_of_table] = row;
                    temp_row[index_of_table2] = row2;
                    list.push_back(temp_row);
                }
            }
        }

        delete it;
        delete it2;
    }
    //for(auto it=list.begin(); it!=list.end(); ++it){
    //  std::copy(it->begin(), it->end(), ostream_iterator<int>(cout, " "));cout << endl;
    //}

    return list;
}

void LogEq::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}

LogNotEq::LogNotEq(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogNotEq::~LogNotEq(){}

vector<vector<row_id>> LogNotEq::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    //nincs index
    iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
    for (recordid_t row = it->first(); !it->end(); row = it->next()){
        log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
        if(result.which()==0){
            if(boost::get<int>(result) != boost::get<int>(child2)){
                temp_row[index_of_table] = row;
                list.push_back(temp_row);
            }
        }
        else{
            if(boost::get<string>(result) != boost::get<string>(child2)){
                temp_row[index_of_table] = row;
                list.push_back(temp_row);
            }
        }
    }

    delete it;

    return list;
}

void LogNotEq::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}


LogGreater::LogGreater(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogGreater::~LogGreater(){}

vector<vector<row_id>> LogGreater::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    if(!local_table_pointers[name.table_name]->column(name.value)->attributes.indexed){
        iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
        for (recordid_t row = it->first(); !it->end(); row = it->next()){
            log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
            if(result.which()==0){
                if(boost::get<int>(result) > boost::get<int>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
            else{
                if(boost::get<string>(result) > boost::get<string>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
        }
        delete it;
    }
    else{
        index_t* idx = nullptr;
        column_t* col = local_table_pointers[name.table_name]->column(name.value);
        if(col->attributes.primary_key){
            idx = col->primary_key.primary_key_index;
        }
        else{
            idx = col->foreign_key.foreign_key_index;
        }
        unique_ptr<iterator_t> it;
        if(child2.which() == 0){
            int i = boost::get<int>(child2);
            it.reset(idx->getiterator_greater(&i));
        }
        else{
            string s = boost::get<string>(child2);
            it.reset(idx->getiterator_greater(&s));
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next()){
            temp_row[index_of_table] = rid;
            list.push_back(temp_row);
        }
    }

    return list;
}

void LogGreater::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}


LogGreaterEq::LogGreaterEq(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogGreaterEq::~LogGreaterEq(){}

vector<vector<row_id>> LogGreaterEq::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    if(!local_table_pointers[name.table_name]->column(name.value)->attributes.indexed){
        iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
        for (recordid_t row = it->first(); !it->end(); row = it->next()){
            log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
            if(result.which()==0){
                if(boost::get<int>(result) >= boost::get<int>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
            else{
                if(boost::get<string>(result) >= boost::get<string>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
        }
        delete it;
    }
    else{
        index_t* idx = nullptr;
        column_t* col = local_table_pointers[name.table_name]->column(name.value);
        if(col->attributes.primary_key){
            idx = col->primary_key.primary_key_index;
        }
        else{
            idx = col->foreign_key.foreign_key_index;
        }
        unique_ptr<iterator_t> it;
        if(child2.which() == 0){
            int i = boost::get<int>(child2);
            it.reset(idx->getiterator_greater_or_equal(&i));
        }
        else{
            string s = boost::get<string>(child2);
            it.reset(idx->getiterator_greater_or_equal(&s));
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next()){
            temp_row[index_of_table] = rid;
            list.push_back(temp_row);
        }
    }

    return list;
}

void LogGreaterEq::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}


LogLess::LogLess(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogLess::~LogLess(){}

vector<vector<row_id>> LogLess::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    if(!local_table_pointers[name.table_name]->column(name.value)->attributes.indexed){
        iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
        for (recordid_t row = it->first(); !it->end(); row = it->next()){
            log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
            if(result.which()==0){
                if(boost::get<int>(result) < boost::get<int>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
            else{
                if(boost::get<string>(result) < boost::get<string>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
        }
        delete it;
    }
    else{
        index_t* idx = nullptr;
        column_t* col = local_table_pointers[name.table_name]->column(name.value);
        if(col->attributes.primary_key){
            idx = col->primary_key.primary_key_index;
        }
        else{
            idx = col->foreign_key.foreign_key_index;
        }
        unique_ptr<iterator_t> it;
        if(child2.which() == 0){
            int i = boost::get<int>(child2);
            it.reset(idx->getiterator_less(&i));
        }
        else{
            string s = boost::get<string>(child2);
            it.reset(idx->getiterator_less(&s));
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next()){
            temp_row[index_of_table] = rid;
            list.push_back(temp_row);
        }
    }

    return list;
}

void LogLess::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}

LogLessEq::LogLessEq(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogLessEq::~LogLessEq(){}

vector<vector<row_id>> LogLessEq::execute(map<string, table_t*>& local_table_pointers){
    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    if(!local_table_pointers[name.table_name]->column(name.value)->attributes.indexed){
        iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
        for (recordid_t row = it->first(); !it->end(); row = it->next()){
            log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
            if(result.which()==0){
                if(boost::get<int>(result) <= boost::get<int>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
            else{
                if(boost::get<string>(result) <= boost::get<string>(child2)){
                    temp_row[index_of_table] = row;
                    list.push_back(temp_row);
                }
            }
        }
        delete it;
    }
    else{
        index_t* idx = nullptr;
        column_t* col = local_table_pointers[name.table_name]->column(name.value);
        if(col->attributes.primary_key){
            idx = col->primary_key.primary_key_index;
        }
        else{
            idx = col->foreign_key.foreign_key_index;
        }
        unique_ptr<iterator_t> it;
        if(child2.which() == 0){
            int i = boost::get<int>(child2);
            it.reset(idx->getiterator_less_or_equal(&i));
        }
        else{
            string s = boost::get<string>(child2);
            it.reset(idx->getiterator_less_or_equal(&s));
        }

        for (recordid_t rid = it->first(); !it->end(); rid = it->next()){
            temp_row[index_of_table] = rid;
            list.push_back(temp_row);
        }
    }

    return list;
}

void LogLessEq::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}


LogAnd::LogAnd(shared_ptr<LogNode>& _child1, shared_ptr<LogNode>& _child2) {
    child1 = _child1;
    child2 = _child2;
}

LogAnd::~LogAnd(){}

vector<vector<row_id>> LogAnd::execute(map<string, table_t*>& local_table_pointers){
    vector<vector<row_id>> list;
    vector<vector<row_id>> list1 = child1->execute(local_table_pointers);
    vector<vector<row_id>> list2 = child2->execute(local_table_pointers);

    /*for(auto it=list1.begin(); it!=list1.end(); ++it){
      std::copy(it->begin(), it->end(), ostream_iterator<int>(cout, " "));cout << endl;
      }
      cout << "====\n";
      for(auto it=list2.begin(); it!=list2.end(); ++it){
      std::copy(it->begin(), it->end(), ostream_iterator<int>(cout, " "));cout << endl;
      }*/

    for(size_t i = 0; i != list1.size(); ++i){
        for(size_t j = 0; j != list2.size(); ++j){
            vector<row_id> temp_row;
            bool flag = true;
            for(size_t k = 0; k != local_table_pointers.size(); ++k){
                if(list1[i][k] == list2[j][k])
                    temp_row.push_back(list1[i][k]);
                else if(list1[i][k] == -1)
                    temp_row.push_back(list2[j][k]);
                else if(list2[j][k] == -1)
                    temp_row.push_back(list1[i][k]);
                else {//nem kerul a sor a listahoz
                    flag = false;
                    break;
                }
            }
            if (flag)
                list.push_back(temp_row);
        }
    }

    /*    cout << "And result\n";
          for(size_t i = 0; i != list.size(); ++i){
          for(size_t j = 0; j != local_table_pointers.size(); ++j){
          cout << list[i][j] << " ";
          }
          cout << endl;
          }
          */
    return list;
}

void LogAnd::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    child1->resolve_table_name(table_pointers);
    child2->resolve_table_name(table_pointers);
}

LogOr::LogOr(shared_ptr<LogNode>& _child1, shared_ptr<LogNode>& _child2) {
    child1 = _child1;
    child2 = _child2;
}

LogOr::~LogOr(){}

vector<vector<row_id>> LogOr::execute(map<string, table_t*>& local_table_pointers){
    //!!!!!Csak join nelkul ( local_table_pointers.size()==1 ) mukodik helyesen, join eseteben meg kell oldani, hogy ne irja ki tobbszor ugyanazt

    vector<vector<row_id>> list;
    vector<vector<row_id>> list1 = child1->execute(local_table_pointers);
    vector<vector<row_id>> list2 = child2->execute(local_table_pointers);
    /*for(auto it=list1.begin(); it!=list1.end(); ++it){
      std::copy(it->begin(), it->end(), ostream_iterator<int>(cout, " "));cout << endl;
      }
      cout << "====\n";
      for(auto it=list2.begin(); it!=list2.end(); ++it){
      std::copy(it->begin(), it->end(), ostream_iterator<int>(cout, " "));cout << endl;
      }*/

    list.reserve(list1.size() + list2.size());
    list.insert(list.end(), list1.begin(), list1.end());
    list.insert(list.end(), list2.begin(), list2.end());

    if(local_table_pointers.size()==1){
        for(auto it=list.begin(); it!=list.end(); ++it){
            for(auto it2=it->begin(); it2!=it->end(); ++it2){
                if(*it2 == -1){
                    list.clear();
                    vector<row_id> temp_list {-1};
                    list.push_back(temp_list);
                    break;
                }
            }
        }
    }

    /*
       for(size_t i = 0; i != list.size(); ++i){
       for(size_t j = 0; j != local_table_pointers.size(); ++j){
       cout << list[i][j] << " ";
       }
       cout << endl;
       }
       */
    return list;
}

void LogOr::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    child1->resolve_table_name(table_pointers);
    child2->resolve_table_name(table_pointers);
}

LogLike::LogLike(Name n, log_variant_t ch2) : name(n), child2(ch2) {}

LogLike::~LogLike(){}

void replace(std::string& str, const std::string& from, const std::string& to){
    while(true){
        size_t start_pos = str.find(from);
        if(start_pos == std::string::npos)
            break;
        str.replace(start_pos, from.length(), to);
    }
}

vector<vector<row_id>> LogLike::execute(map<string, table_t*>& local_table_pointers){
    string pattern = boost::get<string>(child2);
    replace(pattern, "%", "(.*)");

    name.resolve_table_name(local_table_pointers);
    vector<vector<row_id>> list;
    vector<row_id> temp_row(local_table_pointers.size(), -1);
    size_t index_of_table;
    for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
        if(it->first == name.table_name){
            index_of_table = std::distance(local_table_pointers.begin(), it);
            break;
        }
    }

    size_t column_index = get_column_index_in_table(local_table_pointers, name.table_name, name.value);

    //nincs index
    iterator_t* it = local_table_pointers[name.table_name]->get_iterator();
    for (recordid_t row = it->first(); !it->end(); row = it->next()){
        log_variant_t result = get_from_table(local_table_pointers, name.table_name, row, column_index);
        if (std::regex_match (boost::get<string>(result), std::regex(pattern) )){
            temp_row[index_of_table] = row;
            list.push_back(temp_row);
        }
    }
    delete it;
    return list;
}

void LogLike::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    name.resolve_table_name(table_pointers);
}

