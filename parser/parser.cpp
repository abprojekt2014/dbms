#include "parser.h"

extern "C" {
    extern int parse_string(const char*);
    void* pv_exec;
}

using std::string;

Parser::Parser(){
    p_exec = new Executor();
    pv_exec = static_cast<void*>(p_exec);
}

Parser::~Parser(){
    delete p_exec;
}

void Parser::interpret(const string s){
    parse_string(s.c_str());
    //p_exec->send_error_msg("Synatx error.");
}
