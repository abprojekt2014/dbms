#ifndef DATABASE_MANAGER_H
#define DATABASE_MANAGER_H

#include "../record_manager/table.h"
#include "stack_items.h"
#include "stack_items_not_for_common.h"
#include <map>
#include <vector>
#include <string>
#include <memory>
#include <iostream>

class Reference {
    public:
        Reference(std::string table_name, std::vector<std::string> column_names, std::string what_table_name, std::vector<std::string> what_column_names);
        ~Reference();
        std::string table_name;
        std::vector<std::string> column_names;
        std::string what_table_name;
        std::vector<std::string> what_column_names;
};

class ColumnDescriptor{
    public:
        ColumnDescriptor(std::string& n, int t, int l);
        ~ColumnDescriptor();
        std::string name;
        int type;
        int length;
};

class TableDescriptor{
    public:
        TableDescriptor();
        ~TableDescriptor();

        std::vector<std::string> primary_key;
        std::vector<std::unique_ptr<ColumnDescriptor>> column_descriptor_pointers;
        std::vector<std::shared_ptr<Reference>> references_pointers;
        std::vector<std::shared_ptr<Reference>> referenced_pointers;
};

class DatabaseManager{
    public:
        DatabaseManager();
        ~DatabaseManager();
        FieldName resolve_name(Name n);
        void add_column(std::string table_name, std::string column_name, int t, int l);
        void add_primary_key(std::string table_name, std::vector<std::string> column_names);
        void add_references(std::string table_name, std::vector<std::string> column_names, std::string what_table_name, std::vector<std::string> what_column_names);
        bool check_delete(std::string table_name, row_id rid);
        bool check_insert(std::string table_name, std::vector<Name> columns, std::vector<variant_t> values);
        bool check_update(std::string table_name, row_id rid, Assignment assignment);

        std::map<std::string, table_t*> table_pointers;
        std::map<std::string, std::unique_ptr<TableDescriptor>> table_descriptor_pointers;
    
};

#endif
