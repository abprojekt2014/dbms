#ifndef PARSER_EXCEPTION_H
#define PARSER_EXCEPTION_H

#include <stdexcept>

class ParserException : public std::runtime_error {
    public:
        ParserException(std::string const& error): std::runtime_error(error){}
        // what() is defined in std::runtime_error to do what is correct
};

#endif
