%{
#include "executor_wrapper.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

extern void yy_scan_string ( const char *str );
int yylex();
void yyerror(char *s, ...);
void emit(char *s, ...);
extern void* pv_exec;
%}

%union {
        int intval;
        double floatval;
        char *strval;
        int subtok;
}

%token <strval> NAME
%token <strval> STRING
%token <intval> INTNUM
%token <floatval> APPROXNUM
%token <intval> BOOL

//%left <subtok> COMPARISON /* = <> < > <= >= <=> */
%left OR
%left XOR
%left ANDOP
%left EQUAL NOTEQUAL LESS GREATER LESSEQUAL GREATEREQUAL
%left '+' '-'
%left '*' '/'
%left NOT '!'
%nonassoc LIKE

//%token AND
%token ALL
%token AS
%token ASC
%token AVG
%token BIT
%token BY
%token CHAR
%token CREATE
%token DATABASE
%token DECIMAL
%token DEFAULT
%token DELETE
%token DESC
%token DISTINCT
%token DOUBLE
%token FCOUNT
%token FLOAT
%token FORCE
%token FROM
%token GROUP
%token IGNORE
%token INDEX
%token INSERT
%token INTEGER
%token INTO
%token JOIN
%token KEY
%token LIKE
%token MIN
%token MAX
%token ON
%token OR
%token ORDER
%token OUTER
%token PRIMARY
%token REFERENCES
%token REPLACE
%token SELECT
%token SET
%token STRAIGHT_JOIN
%token TABLE
%token USE
%token UPDATE
%token USING
%token VALUES
%token VARCHAR
%token WHERE
%token XOR

%type <intval> select_opts select_expr_list
%type <intval> table_references
%type <intval> groupby_list opt_asc_desc
%type <intval> column_list
%type <intval> index_list
/*%type <intval> index_hint*/
%type <intval> insert_vals insert_vals_list
%type <intval> expr
%type <intval> column_atts data_type create_col_list
%type <intval> opt_length
%type <intval> opt_references
%type <intval> update_asgn_list
/*%type <intval> opt_indexname*/
%start stmt_list

%%

stmt_list: stmt ';'
         | stmt_list stmt ';'
  ;
expr: NAME         { /*emit("NAME %s", $1);*/ executor_new_name(pv_exec, $1); free($1); }
        | NAME '.' NAME { /*emit("FIELDNAME %s.%s", $1, $3);*/ executor_new_fieldname(pv_exec, $1, $3); free($1); free($3); }
   | STRING        { /*emit("STRING %s", $1);*/ executor_new_string(pv_exec, $1); free($1); }
   | INTNUM        { /*emit("NUMBER %d", $1);*/ executor_new_int(pv_exec, $1);}
   | APPROXNUM     { emit("FLOAT %g", $1); executor_new_double(pv_exec, $1);}
   | BOOL          { emit("BOOL %d", $1); }
   | AVG '(' expr ')' { /*emit("AVG");*/ executor_avg(pv_exec); $$=$3; }
   | MIN '(' expr ')' { /*emit("MIN");*/ executor_min(pv_exec); $$=$3; }
   | MAX '(' expr ')' { /*emit("MAX");*/ executor_max(pv_exec); $$=$3; }
   ;

expr: expr '+' expr { emit("ADD"); }
           | expr '-' expr { emit("SUB"); }
   | expr '*' expr { emit("MUL"); }
   | expr '/' expr { emit("DIV"); }
   | expr ANDOP expr { /*emit("AND");*/ executor_new_and_node(pv_exec); }
   | expr OR expr { /*emit("OR");*/ executor_new_or_node(pv_exec); }
   | expr XOR expr { emit("XOR"); }
/*   | expr COMPARISON expr { emit("CMP %d", $2); ast_addExpression(past, 1, ms);}*/
   | expr EQUAL expr { /*emit("EQUAL");*/ executor_new_eq_node(pv_exec); }
   | expr NOTEQUAL expr { /*emit("NOTEQUAL");*/ executor_new_not_eq_node(pv_exec); }
   | expr LESS expr { /*emit("LESS");*/ executor_new_less_node(pv_exec); }
   | expr GREATER expr { /*emit("GREATER");*/ executor_new_greater_node(pv_exec); }
   | expr LESSEQUAL expr { /*emit("LESSEQUAL");*/ executor_new_less_or_eq_node(pv_exec); }
   | expr GREATEREQUAL expr { /*emit("GREATEREQUAL");*/ executor_new_greater_or_eq_node(pv_exec); }
   | expr LIKE expr {/*emit("LIKE");*/ executor_new_like_node(pv_exec); }
   | NOT expr { emit("NOT"); }
   | '!' expr { emit("NOT"); }
   | '(' expr ')' { $$=$2; }
    ;

expr: FCOUNT '(' '*' ')' { emit("COUNTALL"); }
   | FCOUNT '(' expr ')' { emit(" CALL 1 COUNT"); }
   ;

/*expr: expr LIKE expr { emit("LIKE"); }
   | expr NOT LIKE expr { emit("LIKE"); emit("NOT"); }
    ;*/

//SELECT
stmt: select_stmt { /*emit("STMT");*/ }
       ;

select_stmt: SELECT select_opts select_expr_list    { emit("SELECTNODATA %d %d", $2, $3); } 
    | SELECT select_opts select_expr_list FROM table_references opt_where opt_groupby opt_orderby { /*emit("SELECT %d %d %d", $2, $3, $5);*/ executor_select(pv_exec, $2, $3, $5); }
    ;

opt_where: /* nil */ 
            | WHERE expr { /*emit("WHERE");*/ executor_where(pv_exec); }

            ;

opt_groupby: /* nil */
    | GROUP BY groupby_list { /*emit("GROUPBYLIST %d", $3);*/ }
    ;

groupby_list: expr opt_asc_desc { /*emit("GROUPBY %d", $2);*/ executor_groupby(pv_exec); $$=1; }
    | groupby_list ',' expr opt_asc_desc { /*emit("GROUPBY %d", $4);*/ $$=$1+1; }
    ;

opt_asc_desc: /* nil */ { $$=0; }
    | ASC { $$=0; }
    | DESC { $$=1; }
    ;

opt_orderby: /* nil */
    | ORDER BY groupby_list { emit("ORDERBY %d", $3); } 
    ;

column_list: NAME { executor_new_name(pv_exec, $1); /*emit("COLUMN %s", $1);*/ free($1); $$ = 1; }
            /* | column_list ',' NAME  { executor_new_name(pv_exec, $3); emit("COLUMN %s", $3); free($3); $$ = $1 + 1; }*/
             | NAME',' column_list { executor_new_name(pv_exec, $1); /*emit("COLUMN %s", $1);*/ free($1); $$ = $3 + 1; }
  ;

select_opts:    { $$ = 0; }
            | select_opts ALL    { if($1 & 01) yyerror("duplicate ALL option"); $$ = $1 | 01; }
            | select_opts DISTINCT  { if($1 & 02) yyerror("duplicate DISTINCT option"); $$ = $1 | 02; }
            ;

select_expr_list: select_expr   { $$ = 1; }
                | select_expr_list ',' select_expr  {$$ = $1 + 1; }
                | '*'   { emit("SELECTALL"); $$ = 1; }
                ;

select_expr: expr opt_as_alias ;

opt_as_alias: AS NAME { /*emit ("ALIAS %s", $2);*/ executor_alias(pv_exec, $2); free($2); }
              | NAME              { /*emit ("ALIAS %s", $1);*/ free($1); }
  | /* nil */
  ;

table_references:    table_reference { $$ = 1; }
                | table_references ',' table_reference { $$ = $1 + 1; }
                ;

table_reference:  table_factor
                | join_table
                ;

table_factor:
            NAME opt_as_alias index_hint { /*emit("TABLE %s", $1);*/ executor_new_name(pv_exec, $1); free($1); }
            | NAME '.' NAME opt_as_alias index_hint { /*emit("TABLE %s.%s", $1, $3);*/ executor_new_fieldname(pv_exec, $1, $3); free($1); free($3); }
            | '(' table_references ')' { /*emit("TABLEREFERENCES %d", $2);*/ }
            ;

join_table:
          table_reference JOIN table_factor opt_join_condition { emit("JOIN %d", 100); }
    ;

opt_join_condition: /* nil */
                  | join_condition ;

join_condition:
              ON expr { emit("ONEXPR"); }
    | USING '(' column_list ')' { emit("USING %d", $3); }
    ;

index_hint:
          USE KEY '(' index_list ')'
                  { emit("INDEXHINT %d", $4); }
   | IGNORE KEY '(' index_list ')'
                  { emit("INDEXHINT %d", $4); }
   | FORCE KEY '(' index_list ')'
                  { emit("INDEXHINT %d", $4); }
   | /* nil */
   ;

index_list: NAME  { emit("INDEX %s", $1); free($1); $$ = 1; }
             | index_list ',' NAME { emit("INDEX %s", $3); free($3); $$ = $1 + 1; }
   ;


//INSERT
stmt: insert_stmt { /*emit("STMT");*/ }
    ;

insert_stmt: INSERT opt_into NAME opt_col_names VALUES insert_vals_list { executor_insert(pv_exec, $3); /*emit("INSERTVALS %d %s", $6, $3);*/ free($3); }
    ;

opt_into: INTO | /* nil */
    ;

opt_col_names: /* nil */
    | '(' column_list ')' { executor_get_columns(pv_exec, $2); /*emit("INSERTCOLS %d", $2);*/ }
    ;

insert_vals_list: '(' insert_vals ')' { executor_get_values(pv_exec, $2); /*emit("VALUES %d", $2);*/ $$=1; }
    | insert_vals_list ',' '(' insert_vals ')' { /*emit("VALUES %d", $4);*/ $$=$1+1; }
    ;

insert_vals:
    expr { $$=1; }
    | DEFAULT { emit("DEFAULT"); $$ = 1; }
    | insert_vals ',' expr { $$ = $1 + 1; }
    | insert_vals ',' DEFAULT { emit("DEFAULT"); $$ = $1 + 1; } 
    ;


//DELETE
stmt: delete_stmt { /*emit("STMT");*/ }
    ;

delete_stmt: DELETE FROM NAME opt_where { /*emit("DELETE: %s", $3 );*/ executor_deletefrom(pv_exec, $3); free($3); }
    ;


//CREATE DATABASE
stmt: create_database_stmt { /*emit("STMT");*/ }
    ;

create_database_stmt: CREATE DATABASE NAME { /*emit("CREATEDATABASE %s", $3);*/ free($3); }
;

//CREATE TABLE
stmt: create_table_stmt { /*emit("STMT");*/ }
    ;

create_table_stmt: CREATE TABLE NAME '(' create_col_list ')' { executor_create_table(pv_exec, $3); /*emit("CREATE %s", $3);*/ free($3); }
    ;
    
create_col_list: create_definition { $$ = 1; }
    | create_col_list ',' create_definition { $$ = $1 + 1; }
    ;

create_definition: PRIMARY KEY '(' column_list ')'    { executor_new_multiple_pr_key(pv_exec, $4); /*emit("PRIKEY %d", $4);*/ }
    ;

create_definition: { /*emit("STARTCOL");*/ } NAME data_type opt_references column_atts
    { executor_new_column_definition(pv_exec, $2, $3); /*emit("COLUMNDEF %d %s", $3, $2);*/ free($2); }
    ;

column_atts: /* nil */ { $$ = 0; }
    | column_atts PRIMARY KEY { executor_new_attr_pr_key(pv_exec); /*emit("ATTR PRIKEY");*/ $$ = $1 + 1; }
    ;

data_type:
    INTEGER opt_length{ $$ = parser_int_code + $2; }
    | VARCHAR '(' INTNUM ')' { $$ = parser_string_code + $3; }
    ;

opt_length: /* nil */ { $$ = 0; }
    | '(' INTNUM ')' { $$ = $2; }
    ;

opt_references: /* nil */ { $$ = 0; }
    | REFERENCES NAME '(' column_list ')' { executor_new_reference(pv_exec, $2, $4); /*emit("REFERENCES %s %d", $2, $4);*/ free($2); }
    ;

/** update **/
stmt: update_stmt { /*emit("STMT");*/ }
       ;

update_stmt: UPDATE table_references SET update_asgn_list opt_where { /*emit("UPDATE %d %d %d", $2, $4);*/ executor_update(pv_exec); }
;

update_asgn_list: NAME EQUAL expr { executor_create_assignment(pv_exec, $1); /*emit("ASSIGN %s", $1);*/ free($1); $$ = 1; }
   /*| NAME '.' NAME EQUAL expr { emit("ASSIGN %s.%s", $1, $3); free($1); free($3); $$ = 1; }*/
   | update_asgn_list ',' NAME EQUAL expr { executor_create_assignment(pv_exec, $3); /*emit("ASSIGN %s.%s", $3);*/ free($3); $$ = $1 + 1; }
   /*| update_asgn_list ',' NAME '.' NAME EQUAL expr { emit("ASSIGN %s.%s", $3, $5); free($3); free($5); $$ = 1; }*/
   ;

stmt: use_stmt { emit("STMT"); }
    ;

use_stmt: USE NAME { emit("USE %s", $2); free($2); }
        ;

stmt: create_index_stmt { /*emit("STMT");*/ }
    ;

create_index_stmt: CREATE INDEX opt_indexname ON table_name '(' column_list ')' { executor_create_index(pv_exec,$7); /*emit("CREATEINDEX %d", $7);*/ }
                 ;

table_name: NAME { /*emit("TABLE %s", $1);*/ executor_new_name(pv_exec, $1); free($1); }
    ;

opt_indexname: /* nil */
    | NAME { /*emit("INDEXNAME %s", $1);*/ executor_set_index_name(pv_exec, $1); free($1); }
    ;

%%

void emit(char *s, ...) {
    extern int yylineno;
    va_list ap;
    va_start(ap, s);
    printf("rpn: ");
    vfprintf(stdout, s, ap);
    printf("\n");
}

void yyerror(char *s, ...) {
    extern int yylineno;
    va_list ap;
    va_start(ap, s);

    fprintf(stderr, "%d: error: ", yylineno);
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");

}

int parse_string(const char* s){
    yy_scan_string(s);
    return yyparse();
}

void doparse(int ac, char **av){
    extern FILE *yyin;

    if(ac > 1 && !strcmp(av[1], "-d")) {
        /*yydebug = 1; */ac--; av++;
    }

    if(ac > 1 && (yyin = fopen(av[1], "r")) == NULL) {
        perror(av[1]);
        exit(1);
    }

    if(!yyparse())
        printf("SQL parse worked\n");
    else
        printf("SQL parse failed\n");
}
