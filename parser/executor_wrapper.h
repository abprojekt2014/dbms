#ifndef EXECUTORWRAPPER_H
#define EXECUTORWRAPPER_H

#include <stdio.h>

#define parser_int_code     40000
#define parser_string_code  130000

#ifdef __cplusplus
extern "C"
{
#endif

    void executor_new_name(void* v, const char* s);
    void executor_new_fieldname(void* v, const char* tn, const char* n);
    void executor_new_int(void* v, int i);
    void executor_new_double(void* v, double d);
    void executor_new_string(void* v, const char* s);
    void executor_create_database(void* v, const char* s);
    void executor_get_columns(void* v, int i);
    void executor_get_values(void* v, int i);
    void executor_insert(void* v, const char* s);
    void executor_create_assignment(void* v, const char* s);
    void executor_new_attr_pr_key(void* v);
    void executor_new_multiple_pr_key(void* v, int i);
    void executor_create_table(void* v, const char* s);
    void executor_new_column_definition(void* v, const char* s, int type);
    void executor_new_reference(void* v, const char* tn, int i);
    void executor_update(void* v);
    void executor_deletefrom(void* v, const char* s);
    void executor_create_index(void* v, int column_nr);

    void executor_new_and_node(void* v);
    void executor_new_or_node(void* v);
    void executor_new_eq_node(void* v);
    void executor_new_not_eq_node(void* v);
    void executor_new_greater_node(void* v);
    void executor_new_less_node(void* v);
    void executor_new_greater_or_eq_node(void* v);
    void executor_new_less_or_eq_node(void* v);
    void executor_new_like_node(void* v);

    void executor_where(void* v);
    void executor_select(void* v, int select_opt, int column_nr, int table_nr);

    void executor_avg(void* v);
    void executor_min(void* v);
    void executor_max(void* v);
    void executor_alias(void* v, const char* s);
    void executor_groupby(void* v);
    void executor_set_index_name(void* v, const char* s);

#ifdef __cplusplus
}
#endif

#endif
