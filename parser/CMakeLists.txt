cmake_minimum_required(VERSION 2.8)

project(parser)

find_package(Boost COMPONENTS serialization REQUIRED) 
find_package(BISON REQUIRED) 
find_package(FLEX REQUIRED) 

include_directories(${Boost_INCLUDE_DIRS})

BISON_TARGET(query_parser
    ${CMAKE_CURRENT_SOURCE_DIR}/pmysql.y 
    ${CMAKE_CURRENT_BINARY_DIR}/y.tab.c
)
FLEX_TARGET(query_scanner
    ${CMAKE_CURRENT_SOURCE_DIR}/pmysql.l
    ${CMAKE_CURRENT_BINARY_DIR}/lex.yy.c
)
ADD_FLEX_BISON_DEPENDENCY(query_scanner query_parser)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
)

#set(HEADERS
#    common.h
#    executor.h
#   executor_wrapper.h
#    stack_items.h
#    where_items.h
#)

set(SOURCES
    database_manager.cpp
    executor.cpp
    executor_wrapper.cpp
    parser.cpp
    select_items.cpp
    stack_items.cpp
    where_items.cpp
)

#add_executable(
add_library(
    parser
    ${HEADERS}
    ${SOURCES}
    ${BISON_query_parser_OUTPUTS}
    ${FLEX_query_scanner_OUTPUTS}
)

target_link_libraries(
    parser 
    ${FLEX_LIBRARIES}
    ${BISON_LIBRARIES}
    ${Boost_LIBRARIES}
)

