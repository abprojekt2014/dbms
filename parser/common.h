#ifndef COMMON_H
#define COMMON_H

#include "stack_items.h"
//#include "where_items.h"
#include <boost/variant.hpp>
#include <string>
#include <memory>

class Name;
class FieldName;
class AttributePrimaryKey;
class ColumnDefinition;
class References;
class LogNode;
class Selection;

typedef boost::variant<Name, FieldName, int, double, std::string, AttributePrimaryKey, ColumnDefinition, References, std::shared_ptr<LogNode>, std::shared_ptr<Selection> > variant_t;
typedef boost::variant<int, std::string> simple_variant_t; //groupby tipus, variant_t eseten a map kerne a < operatort mindenfele sajat stack_itemre ami a variant_t-ben van
typedef boost::variant<int, std::string, Name> log_variant_t;


typedef long row_id;

#endif
