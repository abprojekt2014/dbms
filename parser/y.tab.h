/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NAME = 258,
    STRING = 259,
    INTNUM = 260,
    APPROXNUM = 261,
    BOOL = 262,
    OR = 263,
    XOR = 264,
    ANDOP = 265,
    EQUAL = 266,
    NOTEQUAL = 267,
    LESS = 268,
    GREATER = 269,
    LESSEQUAL = 270,
    GREATEREQUAL = 271,
    NOT = 272,
    LIKE = 273,
    ALL = 274,
    AS = 275,
    ASC = 276,
    AVG = 277,
    BIT = 278,
    BY = 279,
    CHAR = 280,
    CREATE = 281,
    DATABASE = 282,
    DECIMAL = 283,
    DEFAULT = 284,
    DELETE = 285,
    DESC = 286,
    DISTINCT = 287,
    DOUBLE = 288,
    FCOUNT = 289,
    FLOAT = 290,
    FORCE = 291,
    FROM = 292,
    GROUP = 293,
    IGNORE = 294,
    INDEX = 295,
    INSERT = 296,
    INTEGER = 297,
    INTO = 298,
    JOIN = 299,
    KEY = 300,
    MIN = 301,
    MAX = 302,
    ON = 303,
    ORDER = 304,
    OUTER = 305,
    PRIMARY = 306,
    REFERENCES = 307,
    REPLACE = 308,
    SELECT = 309,
    SET = 310,
    STRAIGHT_JOIN = 311,
    TABLE = 312,
    USE = 313,
    UPDATE = 314,
    USING = 315,
    VALUES = 316,
    VARCHAR = 317,
    WHERE = 318
  };
#endif
/* Tokens.  */
#define NAME 258
#define STRING 259
#define INTNUM 260
#define APPROXNUM 261
#define BOOL 262
#define OR 263
#define XOR 264
#define ANDOP 265
#define EQUAL 266
#define NOTEQUAL 267
#define LESS 268
#define GREATER 269
#define LESSEQUAL 270
#define GREATEREQUAL 271
#define NOT 272
#define LIKE 273
#define ALL 274
#define AS 275
#define ASC 276
#define AVG 277
#define BIT 278
#define BY 279
#define CHAR 280
#define CREATE 281
#define DATABASE 282
#define DECIMAL 283
#define DEFAULT 284
#define DELETE 285
#define DESC 286
#define DISTINCT 287
#define DOUBLE 288
#define FCOUNT 289
#define FLOAT 290
#define FORCE 291
#define FROM 292
#define GROUP 293
#define IGNORE 294
#define INDEX 295
#define INSERT 296
#define INTEGER 297
#define INTO 298
#define JOIN 299
#define KEY 300
#define MIN 301
#define MAX 302
#define ON 303
#define ORDER 304
#define OUTER 305
#define PRIMARY 306
#define REFERENCES 307
#define REPLACE 308
#define SELECT 309
#define SET 310
#define STRAIGHT_JOIN 311
#define TABLE 312
#define USE 313
#define UPDATE 314
#define USING 315
#define VALUES 316
#define VARCHAR 317
#define WHERE 318

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 15 "pmysql.y" /* yacc.c:1909  */

        int intval;
        double floatval;
        char *strval;
        int subtok;

#line 187 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
