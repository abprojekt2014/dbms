%option noyywrap yylineno case-insensitive
%{
#include "y.tab.h"
#include <stdarg.h>
#include <string.h>

void yyerror(char *s, ...);

int oldstate;

%}

%x COMMENT
%s BTWMODE

%%

ALL     { return ALL; }
AND     { return ANDOP; }
AS      { return AS; }
ASC     { return ASC; }
AVG     { return AVG; }
BIT     { return BIT; }
BY      { return BY; }
CHAR(ACTER)?    { return CHAR; }
CREATE  { return CREATE; }
DATABASE    { return DATABASE; }
NUMERIC|DEC|DECIMAL { return DECIMAL; }
DEFAULT { return DEFAULT; }
DELETE  { return DELETE; }
DESC    { return DESC; }
DISTINCT    { return DISTINCT; }
FLOAT4? { return FLOAT; }
FLOAT8|DOUBLE   { return DOUBLE; }
FORCE   { return FORCE; }
FROM    { return FROM; }
GROUP   { return GROUP; }
IGNORE  { return IGNORE; }
INDEX   { return INDEX; }
INSERT  { return INSERT; }
INT4?|INTEGER   { return INTEGER; }
INTO    { return INTO; }
JOIN    { return JOIN; }
INDEX|KEY     { return KEY; }
LIKE    { return LIKE; }
MIN     { return MIN; }
MAX     { return MAX; }
ON      { return ON; }
OR      { return OR; }
ORDER   { return ORDER; }
PRIMARY { return PRIMARY; }
REFERENCES { return REFERENCES; }
REPLACE { return REPLACE; }
SELECT  { return SELECT; }
SET     { return SET; }
TABLE   { return TABLE; }
UPDATE  { return UPDATE; }
USE     { return USE; }
USING   { return USING; }
VALUES  { return VALUES; }
VARCHAR(ACTER)? { return VARCHAR; }
WHERE   { return WHERE; }
XOR     { return XOR; }

-?[0-9]+    { yylval.intval = atoi(yytext); return INTNUM; }

-?[0-9]+"."[0-9]* |
-?"."[0-9]+ { yylval.floatval = atof(yytext); return APPROXNUM; }

TRUE    { yylval.intval = 1; return BOOL; }
UNKNOWN { yylval.intval = -1; return BOOL; }
FALSE   { yylval.intval = 0; return BOOL; }

'(\\.|''|[^'\n])*' |
\"(\\.|\"\"|[^"\n])*\"  { yylval.strval = strdup(yytext); return STRING; }

[-+&~|^/%*(),.;!]   { return yytext[0]; }
[ \t\n]

"&&"    { return ANDOP; }
"||"    { return OR; }

"="     { /*yylval.subtok = 4; return COMPARISON;*/ return EQUAL; }
"<=>"   { /*yylval.subtok = 12; return COMPARISON;*/}
">="    { /*yylval.subtok = 6; return COMPARISON;*/ return GREATEREQUAL; }
">"     { /*yylval.subtok = 2; return COMPARISON;*/ return GREATER; }
"<="    { /*yylval.subtok = 5; return COMPARISON;*/ return LESSEQUAL; }
"<"     { /*yylval.subtok = 1; return COMPARISON;*/ return LESS;}
"!="    |
"<>"    { /*yylval.subtok = 3; return COMPARISON;*/ return NOTEQUAL; }

COUNT    { int c = input(); unput(c);
           if(c == '(') return FCOUNT;
                      yylval.strval = strdup(yytext);
                                 return NAME; }

[A-Za-z][A-Za-z0-9_]*   { yylval.strval = strdup(yytext); return NAME; }

`[^`/\\.\n]+`           { yylval.strval = strdup(yytext+1); yylval.strval[yyleng-2] = 0; return NAME; }

%%
