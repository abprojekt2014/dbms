#include "database_manager.h"

using std::vector;
using std::map;
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::ostream_iterator;

DatabaseManager::DatabaseManager(){}

DatabaseManager::~DatabaseManager(){}

bool DatabaseManager::check_delete(std::string table_name, row_id rid){
    for(auto it = table_descriptor_pointers[table_name]->referenced_pointers.begin(); it != table_descriptor_pointers[table_name]->referenced_pointers.end(); ++it){
        cout << "CHECK IF IN TABLE " << (*it)->table_name << ", IS ";
        std::copy((*it)->column_names.begin(), (*it)->column_names.end(), ostream_iterator<string>(cout, ", "));
        cout << "EQUAL TO ";
        std::copy((*it)->what_column_names.begin(), (*it)->what_column_names.end(), ostream_iterator<string>(cout, ", "));
        cout << "FROM " << (*it)->what_table_name << endl;
        //if(true)
        //    return true;
    }
    return false;
}

bool DatabaseManager::check_insert(std::string table_name, std::vector<Name> columns, std::vector<variant_t> values){
    cout << "CHECK IF PRIMARY KEY ";
    std::copy(table_descriptor_pointers[table_name]->primary_key.begin(), table_descriptor_pointers[table_name]->primary_key.end(), ostream_iterator<string>(cout, ", "));
    cout << "DOES NOT EXIST\n";

    for(auto it = table_descriptor_pointers[table_name]->references_pointers.begin(); it != table_descriptor_pointers[table_name]->references_pointers.end(); ++it){
        cout << "CHECK IF IN TABLE " << (*it)->table_name << ", COLUMNS ";
        std::copy((*it)->column_names.begin(), (*it)->column_names.end(), ostream_iterator<string>(cout, ", "));
        cout << " ARE REFERENCING VALID VALUES IN TABLE " << (*it)->what_table_name << "( ";
        std::copy((*it)->what_column_names.begin(), (*it)->what_column_names.end(), ostream_iterator<string>(cout, ", "));
        cout << ")\n";
    }
    return false;
}

bool DatabaseManager::check_update(std::string table_name, row_id rid, Assignment assignment){
    //composite key eseten trukkos

    //t)->what_column_names.end()ellenorzom h primary key-e
    if(std::find(table_descriptor_pointers[table_name]->primary_key.begin(), table_descriptor_pointers[table_name]->primary_key.end(), assignment.name.value) != table_descriptor_pointers[table_name]->primary_key.end()){

    }

    //ellenorzom vki hivatkozik-e ra?
    for(auto it = table_descriptor_pointers[table_name]->referenced_pointers.begin(); it != table_descriptor_pointers[table_name]->referenced_pointers.end(); ++it){
        if(std::find((*it)->what_column_names.begin(), (*it)->what_column_names.end(), assignment.name.value) != (*it)->what_column_names.end()){
            //ellenorizni h (*it)->what_table_name tartalmazza-e az assignment.value erteket
        }
    }

    //ellenorzom h ha az uj ertek foreign key, akkor ervenyes elemre hivatkozik-e
    for(auto it = table_descriptor_pointers[table_name]->references_pointers.begin(); it != table_descriptor_pointers[table_name]->references_pointers.end(); ++it){
        if(std::find((*it)->column_names.begin(), (*it)->column_names.end(), assignment.name.value) != (*it)->column_names.end()){
            //ellenorizni h (*it)->what_table_name tartalmazza-e az assignment.value erteket
        }
    }
    
    return false;
}

void DatabaseManager::add_primary_key(string table_name, vector<string> column_names){
    table_descriptor_pointers[table_name]->primary_key = column_names;
}

void DatabaseManager::add_references(string table_name, vector<string> column_names, string what_table_name, vector<string> what_column_names){
    std::shared_ptr<Reference> ref(new Reference(table_name, column_names, what_table_name, what_column_names));
    table_descriptor_pointers[table_name]->references_pointers.push_back(ref);
    table_descriptor_pointers[what_table_name]->referenced_pointers.push_back(ref);
}

void DatabaseManager::add_column(string table_name, string column_name, int t, int l){
    if(table_descriptor_pointers.count(table_name) == 0)
        table_descriptor_pointers[table_name] = std::unique_ptr<TableDescriptor>(new TableDescriptor());
    table_descriptor_pointers[table_name]->column_descriptor_pointers.push_back(std::unique_ptr<ColumnDescriptor>(new ColumnDescriptor(column_name, t, l)));
}

FieldName DatabaseManager::resolve_name(Name n){
    for(auto it = table_descriptor_pointers.begin(); it != table_descriptor_pointers.end(); ++it){
        for(auto it2 = it->second->column_descriptor_pointers.begin(); it2 != it->second->column_descriptor_pointers.end(); ++it2){
            if((*it2)->name == n.value){
                return FieldName(it->first, n.value);
            }
        }
    }
    cerr << "Column " << n.value << "not found in database.\n";
    return FieldName();
}

TableDescriptor::TableDescriptor(){}

TableDescriptor::~TableDescriptor(){}

ColumnDescriptor::ColumnDescriptor(std::string& n, int t, int l) : name(n), type(t), length(l) {}

ColumnDescriptor::~ColumnDescriptor(){}

Reference::Reference(string table_name, vector<string> column_names, string what_table_name, vector<string> what_column_names):
    table_name(table_name), column_names(column_names), what_table_name(what_table_name), what_column_names(what_column_names)
{}

Reference::~Reference(){}
