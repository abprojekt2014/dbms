#include "select_items.h"
#include <boost/format.hpp>
#include <list>
#include "../TextTable/TextTable.h"

using std::vector;
using std::map;
using std::cout;
using std::endl;
using std::string;
using std::shared_ptr;
using std::unique_ptr;
using std::ostream_iterator;
using std::list;

bool ReturnRowBuilderNoGroupBy::is_result_empty(){
    auto it = return_row.begin();
    variant_t res = (*it)->get_single_item(0);
    if(res.which() == 4)
        if(boost::get<string>(res) == "NULL")
            return true;
    return false;
}

bool ReturnRowBuilderWithGroupBy::is_result_empty(){
    auto it1 = return_row.begin();
    auto it = it1->second.begin();
    variant_t res((*it)->get_single_item(0));
    if(res.which() == 4)
        if(boost::get<string>(res) == "NULL")
            return true;
    return false;
}

void ReturnRowBuilderNoGroupBy::get_result(int select_opt){
    out_rows_hash.clear();
    size_t result_hash_value = 0;
    AdatSor adat_kuldeni;
    bool are_more_rows = is_result_empty();
    TextTable table;

    adat_kuldeni.adat.push_back("TABLE::NEW");
    std::copy(alias_list.begin(), alias_list.end(), std::back_inserter(adat_kuldeni.adat));
    
    if(server_p)
        server_p->set_write_return_value(server_p->write(adat_kuldeni));

    table.addRow(alias_list.begin(), alias_list.end());

    if(are_more_rows){
        adat_kuldeni.adat.clear();
        adat_kuldeni.adat.push_back("TABLE::END");
        if(server_p)
            server_p->set_write_return_value(server_p->write(adat_kuldeni));

        return;
    }

    size_t length = 1;
    for (auto it = return_row.begin(); it != return_row.end(); ++it){
        if ((*it)->get_size() > 1){
            length = (*it)->get_size();
            break;
        }
    }
    for(size_t i = 0; i < length; ++i){
        result_hash_value = 0;
        adat_kuldeni.adat.clear();
        adat_kuldeni.adat.push_back("ROW");
        list<string> line;

        for(size_t j = 0; j != return_row.size(); ++j)
        {
            variant_t return_item(return_row[j]->get_single_item(i));
            switch (return_item.which())
            {
            case 2:  //int
            {
                int plain_result = boost::get<int>(return_item);
                std::string txt = boost::str(boost::format("%d") % plain_result);

                adat_kuldeni.adat.push_back(txt);
                table.add(txt);

                boost::hash_combine(result_hash_value, plain_result);
                break;
            }
            case 4: //string
            {
                string plain_result(boost::get<string>(return_item));

                adat_kuldeni.adat.push_back(plain_result);
                table.add(plain_result);

                boost::hash_combine(result_hash_value, plain_result);
                break;
            }
            case 3: //double
            {
                double plain_result = boost::get<double>(return_item);
                cout << plain_result << " ";
                adat_kuldeni.adat.push_back(boost::str(boost::format("%f") % plain_result));
                boost::hash_combine(result_hash_value, plain_result);
            }
            break;
            default:
                assert(FALSE);
            }
        }

        /// WTF is this select_opt ??
        if(select_opt == 2){
            size_t out_rows_hash_prev_size = out_rows_hash.size();

            out_rows_hash.insert(result_hash_value);
            
            if(out_rows_hash_prev_size < out_rows_hash.size())//kiir
            {
                /// WTF? line will always be empty; commented the table.addRow because of this

                //std::copy(line.begin(), line.end(), ostream_iterator<string>(cout, " | "));
                //table.addRow(line.begin(), line.end());
                
                if(server_p)
                    server_p->set_write_return_value(server_p->write(adat_kuldeni));
            }
            continue;
        }

        table.endOfRow();
        if(server_p)
            server_p->set_write_return_value(server_p->write(adat_kuldeni));
    }

    cout << table;

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("TABLE::END");
        server_p->set_write_return_value(server_p->write(as));
    }
}

void ReturnRowBuilderWithGroupBy::get_result(int select_opt){
    out_rows_hash.clear();
    size_t result_hash_value = 0;
    AdatSor adat_kuldeni;
    bool are_more_rows = is_result_empty();
    TextTable table;

    adat_kuldeni.adat.push_back("TABLE::NEW");
    std::copy(alias_list.begin(), alias_list.end(), std::back_inserter(adat_kuldeni.adat));

    if (server_p)
    {
        server_p->set_write_return_value(server_p->write(adat_kuldeni));
    }

    table.addRow(alias_list.begin(), alias_list.end());
    
    if(are_more_rows){
        adat_kuldeni.adat.clear();
        adat_kuldeni.adat.push_back("TABLE::END");
        if(server_p)
            server_p->set_write_return_value(server_p->write(adat_kuldeni));

        return;
    }

    for(auto it1 = return_row.begin(); it1 != return_row.end(); ++it1){
        adat_kuldeni.adat.clear();
        adat_kuldeni.adat.push_back("ROW");

        result_hash_value = 0;
        ++it1;
        are_more_rows = (it1 != return_row.end()) ? 1 : 0;
        --it1;
        /*for (auto it = it1->second.begin(); it != it1->second.end(); ++it){
            variant_t return_item((*it)->get_single_item(0));
            //serializer es server
            if(return_item.which() == 2)//int
                cout << boost::get<int>(return_item) << " ";
            if(return_item.which() == 4)//string
                cout << boost::get<string>(return_item) << " ";
            if(return_item.which() == 3)//double
                cout << boost::get<double>(return_item) << " ";
        }*/
        for(size_t j = 0; j != it1->second.size(); ++j){
            variant_t return_item(it1->second[j]->get_single_item(0));
            
            switch (return_item.which())
            {
            case 2: //int
            {
                int plain_result = boost::get<int>(return_item);
                std::string txt = boost::str(boost::format("%d") % plain_result);
                
                table.add(txt);
                adat_kuldeni.adat.push_back(txt);

                boost::hash_combine(result_hash_value, plain_result);
                break;
            }
            case 4: //string
            {
                string plain_result(boost::get<string>(return_item));
                
                table.add(plain_result);
                adat_kuldeni.adat.push_back(plain_result);

                boost::hash_combine(result_hash_value, plain_result);
                break;
            }
            case 3: //double
            {
                double plain_result = boost::get<double>(return_item);
                std::string txt = boost::str(boost::format("%f") % plain_result);

                table.add(txt);
                adat_kuldeni.adat.push_back(txt);

                boost::hash_combine(result_hash_value, plain_result);
                break;
            }
            default:
                assert(FALSE);
            }
        }
        if(select_opt == 2){
            size_t out_rows_hash_prev_size = out_rows_hash.size();
            
            out_rows_hash.insert(result_hash_value);
            
            if (out_rows_hash_prev_size < out_rows_hash.size())//kiir
            {
                if (server_p)
                {
                    server_p->set_write_return_value(server_p->write(adat_kuldeni));
                }
            }

            table.endOfRow();
            continue;
        }
        
        table.endOfRow();
        if(server_p)
            server_p->set_write_return_value(server_p->write(adat_kuldeni));
    }

    cout << table;

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("TABLE::END");
        server_p->set_write_return_value(server_p->write(as));
    }
}

ReturnRowBuilder::ReturnRowBuilder(vector<Name>& select_list,
        vector<string>& alias_list,
        vector<string>& aggregate_list,
        map<string, table_t*>& local_table_pointers,
        Server* _server_p):
    select_list(select_list),
    alias_list(alias_list),
    aggregate_list(aggregate_list),
    local_table_pointers(local_table_pointers),
    server_p(_server_p){
            for(auto it = local_table_pointers.begin(); it != local_table_pointers.end(); ++it){
                table_order_index[it->first] = std::distance(local_table_pointers.begin(), it);
            }

            for(size_t i = 0; i != select_list.size(); ++i){
                select_list[i].resolve_table_name(local_table_pointers);
                vector<column_t*> table_columns(local_table_pointers[select_list[i].table_name]->getcatalog().columns());
                return_row_rowsize.push_back(local_table_pointers[select_list[i].table_name]->getcatalog().rowlength());
                for(size_t j = 0; j != table_columns.size(); ++j){
                    if(table_columns[j]->name == select_list[i].value){//megtalaltuk az oszlop tulajdonsagait
                        return_row_table_index.push_back(j);
                        break;
                    }
                }
            }
    }

ReturnRowBuilder::~ReturnRowBuilder(){}

ReturnRowBuilderNoGroupBy::ReturnRowBuilderNoGroupBy(vector<Name>& select_list,
        vector<string>& alias_list,
        vector<string>& aggregate_list,
        map<string, table_t*>& local_table_pointers,
        Server* _server_p):
    ReturnRowBuilder(select_list, alias_list, aggregate_list, local_table_pointers, _server_p){

            for(size_t i = 0; i != aggregate_list.size(); ++i){
                if(aggregate_list[i] == "all")
                    return_row.push_back(shared_ptr<ReturnElem>(new ALL));
                else if(aggregate_list[i] == "avg")
                    return_row.push_back(shared_ptr<ReturnElem>(new AVG));
                else if(aggregate_list[i] == "min")
                    return_row.push_back(shared_ptr<ReturnElem>(new MIN));
                else //aggregate_list[i] == "max"
                    return_row.push_back(shared_ptr<ReturnElem>(new MAX));
            }
        }

ReturnRowBuilderNoGroupBy::~ReturnRowBuilderNoGroupBy(){}

void ReturnRowBuilderNoGroupBy::process_row(vector<row_id>& rid){
    //for i select_list
    //get item
    //return_row[i]->add(item);

    for(size_t i = 0; i != select_list.size(); ++i){
        vector<column_t*> table_columns(local_table_pointers[select_list[i].table_name]->getcatalog().columns());
        unique_ptr<serializer_t> pserializer(new serializer_t(return_row_rowsize[i]));
        recordid_t row = rid[table_order_index[select_list[i].table_name]];
        BYTE* binrow = new BYTE[return_row_rowsize[i]];

        local_table_pointers[select_list[i].table_name]->read_row(row, binrow);

        if(table_columns[return_row_table_index[i]]->type == 0){//int
            int r = (int)pserializer->deserialize_int(binrow, table_columns[return_row_table_index[i]]->offset, table_columns[return_row_table_index[i]]->length);
            return_row[i]->add(variant_t(r));
        }
        else if(table_columns[return_row_table_index[i]]->type == 2){//string
            string r = (string)pserializer->deserialize_str(binrow, table_columns[return_row_table_index[i]]->offset, table_columns[return_row_table_index[i]]->length);
            return_row[i]->add(variant_t(r));
        }

        delete[] binrow;
    }

/*    for(size_t i = 0; i != select_list.size(); ++i){
        local_table_pointers[select_list[i].table_name];//ebbol a tablazotbol
        rid[table_order_index[select_list[i].table_name]];//ebbol a sorbol
        select_list[i].value;//ez az oszlopertek kell
        //return_row[i]->add(variant_t(amit kaptam a tablazatbol));

        vector<column_t*> table_columns(local_table_pointers[select_list[i].table_name]->getcatalog().columns());
        int rowsize = local_table_pointers[select_list[i].table_name]->getcatalog().rowlength();
        BYTE binrow[rowsize];
        unique_ptr<serializer_t> pserializer(new serializer_t(rowsize));
        recordid_t row = rid[table_order_index[select_list[i].table_name]];
        local_table_pointers[select_list[i].table_name]->read_row(row, binrow);
        for(size_t j = 0; j != table_columns.size(); ++j){
            //cout << select_list[i].value << endl;
            if(table_columns[j]->name == select_list[i].value){//megtalaltuk az oszlop tulajdonsagait
                //cout << "Type: " << table_columns[j]->type << endl;
                //cout << "Az oszlop neve " << table_columns[j]->name << endl;
                if(table_columns[j]->type == 0){//int
                    int r = (int)pserializer->deserialize_int(binrow, table_columns[j]->offset, table_columns[j]->length);
                    return_row[i]->add(variant_t(r));
                    //cout << "added to " << i << endl;
                }
                if(table_columns[j]->type == 2){//string
                    string r = (string)pserializer->deserialize_str(binrow, table_columns[j]->offset, table_columns[j]->length);
                    return_row[i]->add(variant_t(r));
                    //cout << "added to " << i << endl;
                }
                break;
            }
        }
    }
*/
}

ReturnRowBuilderWithGroupBy::ReturnRowBuilderWithGroupBy(vector<Name>& select_list,
        vector<string>& alias_list,
        vector<string>& aggregate_list,
        map<string, table_t*>& local_table_pointers,
        Name _group_by,
        Server* _server_p):
    ReturnRowBuilder(select_list, alias_list, aggregate_list, local_table_pointers, _server_p),
    group_by(_group_by),
    rf(std::unique_ptr<RowFactory>(new RowFactory(aggregate_list))) {
        group_by.resolve_table_name(local_table_pointers);
        vector<column_t*> table_columns(local_table_pointers[group_by.table_name]->getcatalog().columns());
        group_by_rowsize = local_table_pointers[group_by.table_name]->getcatalog().rowlength();
        for(size_t j = 0; j != table_columns.size(); ++j){
            if(table_columns[j]->name == group_by.value){//megtalaltuk az oszlop tulajdonsagait
                group_by_table_index = j;
                break;
            }
        }
    }

ReturnRowBuilderWithGroupBy::~ReturnRowBuilderWithGroupBy(){}

void ReturnRowBuilderWithGroupBy::process_row(vector<row_id>& rid){
    /*get group_by_value as variant_t
      if (return_row.find(group_by_value) == return_row.end()){
      std::vector<std::shared_ptr<ReturnElem>> new_row = ;
      return_row.insert(std::make_pair(group_by_value, new_row));
      }

    //for i select_list
    //get item
    //return_row[group_by_value][i]->add(item);
     */
    //simple_variant_t group_by_value;
    //local_table_pointers[group_by.table_name];//ebbol a tablazotbol
    //rid[table_order_index[group_by.table_name]];//ebbol a sorbol
    //group_by.value;//ez az oszlopertek kell es megvan a group_by erteke, amit simple_variant_t-re alakitok (group_by_value)

    simple_variant_t group_by_value;
    vector<column_t*> table_columns(local_table_pointers[group_by.table_name]->getcatalog().columns());
    unique_ptr<serializer_t> pserializer(new serializer_t(group_by_rowsize));
    recordid_t row = rid[table_order_index[group_by.table_name]];
    BYTE* binrow = new BYTE[group_by_rowsize];

    local_table_pointers[group_by.table_name]->read_row(row, binrow);

    if(table_columns[group_by_table_index]->type == 0){//int
        int r = (int)pserializer->deserialize_int(binrow, table_columns[group_by_table_index]->offset, table_columns[group_by_table_index]->length);
        //cout << r << endl;
        group_by_value = simple_variant_t(r);
    }
    else if(table_columns[group_by_table_index]->type == 2){//string
        string r = (string)pserializer->deserialize_str(binrow, table_columns[group_by_table_index]->offset, table_columns[group_by_table_index]->length);
        group_by_value = simple_variant_t(r);
    }

    delete[] binrow;

    if (return_row.find(group_by_value) == return_row.end()){
        std::vector<std::shared_ptr<ReturnElem>> new_row = rf->get_new_row();
        return_row.insert(std::make_pair(group_by_value, new_row));
    }

    for(size_t i = 0; i != select_list.size(); ++i){
        //local_table_pointers[select_list[i].table_name];//ebbol a tablazotbol
        //rid[table_order_index[select_list[i].table_name]];//ebbol a sorbol
        //select_list[i].value;//ez az oszlopertek kell
        //return_row[group_by_value][i]->add(variant_t(amit kaptam a tablazatbol));

        vector<column_t*> table_columns(local_table_pointers[select_list[i].table_name]->getcatalog().columns());
        unique_ptr<serializer_t> pserializer(new serializer_t(return_row_rowsize[i]));
        recordid_t row = rid[table_order_index[select_list[i].table_name]];
        local_table_pointers[select_list[i].table_name]->read_row(row, binrow);
        BYTE* binrow = new BYTE[return_row_rowsize[i]];

        if(table_columns[return_row_table_index[i]]->type == 0){//int
            int r = (int)pserializer->deserialize_int(binrow, table_columns[return_row_table_index[i]]->offset, table_columns[return_row_table_index[i]]->length);
            return_row[group_by_value][i]->add(variant_t(r));
        }
        else if(table_columns[return_row_table_index[i]]->type == 2){//string
            string r = (string)pserializer->deserialize_str(binrow, table_columns[return_row_table_index[i]]->offset, table_columns[return_row_table_index[i]]->length);
            return_row[group_by_value][i]->add(variant_t(r));
        }

        delete[] binrow;
    }
}

ReturnElem::ReturnElem(){}
ReturnElem::~ReturnElem(){}

ALL::ALL(){}
ALL::~ALL(){}

AVG::AVG() : avg(0), itemnr(0) {}
AVG::~AVG(){}

MIN::MIN() : val(std::numeric_limits<int>::max()){}
MIN::~MIN(){}

MAX::MAX() : val(std::numeric_limits<int>::min()){}
MAX::~MAX(){}

void AVG::add(variant_t item){
    avg += boost::get<int>(item);
    ++itemnr;
}

std::vector<variant_t> AVG::get(){
    vector<variant_t> tempvector {variant_t(static_cast<double>(avg)/itemnr)};
    return tempvector;
}

variant_t ALL::get_single_item(size_t i) {
    if(items.size() > i)
        return variant_t(items[i]);
    else
        return variant_t("NULL");
}

variant_t AVG::get_single_item(size_t i) { return variant_t(static_cast<double>(avg)/itemnr); }

void MIN::add(variant_t item){
    int i = boost::get<int>(item);
    if(i < val)
        val = i;
}

std::vector<variant_t> MIN::get(){
    vector<variant_t> tempvector {variant_t(val)};
    return tempvector;
}

variant_t MIN::get_single_item(size_t i) { return variant_t(val); }

void MAX::add(variant_t item){
    int i = boost::get<int>(item);
    if(i > val)
        val = i;
}

variant_t MAX::get_single_item(size_t i) { return variant_t(val); }

std::vector<variant_t> MAX::get(){
    vector<variant_t> tempvector {variant_t(val)};
    return tempvector;
}

ReturnRowBuilderWithGroupBy::Maker::Maker(){}
ReturnRowBuilderWithGroupBy::Maker::~Maker(){}

ReturnRowBuilderWithGroupBy::ALLMaker::ALLMaker(){}
ReturnRowBuilderWithGroupBy::ALLMaker::~ALLMaker(){}

ReturnRowBuilderWithGroupBy::AVGMaker::AVGMaker(){}
ReturnRowBuilderWithGroupBy::AVGMaker::~AVGMaker(){}

ReturnRowBuilderWithGroupBy::MINMaker::MINMaker(){}
ReturnRowBuilderWithGroupBy::MINMaker::~MINMaker(){}

ReturnRowBuilderWithGroupBy::MAXMaker::MAXMaker(){}
ReturnRowBuilderWithGroupBy::MAXMaker::~MAXMaker(){}

ReturnRowBuilderWithGroupBy::RowFactory::RowFactory(std::vector<std::string> aggregate_list){
    for(size_t i = 0; i != aggregate_list.size(); ++i){
        if(aggregate_list[i] == "all")
            row_template.push_back(std::move(unique_ptr<ReturnRowBuilderWithGroupBy::Maker>(new ReturnRowBuilderWithGroupBy::ALLMaker)));
        else if(aggregate_list[i] == "avg")
            row_template.push_back(std::move(unique_ptr<ReturnRowBuilderWithGroupBy::Maker>(new ReturnRowBuilderWithGroupBy::AVGMaker)));
        else if(aggregate_list[i] == "min")
            row_template.push_back(std::move(unique_ptr<ReturnRowBuilderWithGroupBy::Maker>(new ReturnRowBuilderWithGroupBy::MINMaker)));
        else //aggregate_list[i] == "max"
            row_template.push_back(std::move(unique_ptr<ReturnRowBuilderWithGroupBy::Maker>(new ReturnRowBuilderWithGroupBy::MAXMaker)));
    }
}

ReturnRowBuilderWithGroupBy::RowFactory::~RowFactory(){}

std::vector<std::shared_ptr<ReturnElem>> ReturnRowBuilderWithGroupBy::RowFactory::get_new_row(){
    std::vector<std::shared_ptr<ReturnElem>> tempvector;
    for(auto& it : row_template)
        tempvector.push_back(it->get_new_elem());
    return tempvector;
}
