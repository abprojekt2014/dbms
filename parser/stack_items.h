#ifndef STACKITEMS_H
#define STACKITEMS_H

#include "../record_manager/table_manager.h"
#include "../record_manager/catalog.h"
#include "../record_manager/table.h"
#include "../include/dbtypes.h"
#include "common.h"
#include "parser_exception.h"
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <exception>

class ColumnDefinition{
    public:
        std::string name;
        int type;
        bool primary_key;
        bool references;
        std::string reference_table_name;
        std::vector<std::string> reference_column_names;
        ColumnDefinition(std::string name="", int type=0);
        ~ColumnDefinition();
};

class MultiPrimaryKey{
    public:
        std::vector<std::string> columns;
        MultiPrimaryKey(){};
        ~MultiPrimaryKey(){};
};

class References{
    public:
        References(const char* table_name, std::vector<std::string> column_names);
        ~References();
        std::string table_name;
        std::vector<std::string> column_names;
};

class AttributePrimaryKey{
    public:
        AttributePrimaryKey(){};
        ~AttributePrimaryKey(){};
};

class Name{
    public:
        std::string value;
        std::string table_name;
        Name(std::string v="");
        ~Name();
        void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
        void check_table_name(std::map<std::string, table_t*>& table_pointers);
        friend std::ostream& operator<<(std::ostream& os, const Name n);
        friend bool operator==(const Name n1, const Name n2);
};

class FieldName{
    public:
        std::string tablename;
        std::string name;
        FieldName(std::string tablename="", std::string name="");
        ~FieldName();
        friend std::ostream& operator<<(std::ostream& os, const FieldName n);
};

class Selection{
    public:
        Name name;
        std::string alias;
        std::string aggregate;
        Selection(Name n);
        ~Selection();
};


#endif
