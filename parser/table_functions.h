#include "common.h"
#include <iostream>
#include <map>
#include <vector>
#include <memory>
#include <string>

using std::vector;
using std::map;
using std::cout;
using std::endl;
using std::string;
using std::shared_ptr;
using std::unique_ptr;

size_t get_column_index_in_table(map<string, table_t*>& local_table_pointers, string& table_name, string& column_name){
    //cout << table_name << endl;
    vector<column_t*> table_columns(local_table_pointers[table_name]->getcatalog().columns());
    for(size_t j = 0; j != table_columns.size(); ++j){
        if(table_columns[j]->name == column_name){//megtalaltuk az oszlop tulajdonsagait
            return j;
        }   
    }   
    return 0;
}

simple_variant_t get_from_table(map<string, table_t*>& local_table_pointers, string& table_name, recordid_t row, size_t column_index)
{
    int rowsize = local_table_pointers[table_name]->getcatalog().rowlength();
    vector<column_t*> table_columns(local_table_pointers[table_name]->getcatalog().columns());
    unique_ptr<serializer_t> pserializer(new serializer_t(rowsize));
    BYTE* binrow = new BYTE[rowsize];

    local_table_pointers[table_name]->read_row(row, binrow);
    if(table_columns[column_index]->type == 0){//int
        int r = (int)pserializer->deserialize_int(binrow, table_columns[column_index]->offset, table_columns[column_index]->length);
        delete [] binrow;
        return simple_variant_t(r);
    }   
    else /*if(table_columns[j]->type == 2)*/{//string
        string r = (string)pserializer->deserialize_str(binrow, table_columns[column_index]->offset, table_columns[column_index]->length);
        delete [] binrow;
        return simple_variant_t(r);
    }
}

