#ifndef SELECTITEMS_H
#define SELECTITEMS_H

#include <boost\asio.hpp>
#include "../record_manager/table_manager.h"
#include "../record_manager/catalog.h"
#include "../record_manager/table.h"
#include "../record_manager/serializer.hpp"
#include "../include/dbtypes.h"
#include "../server/server.h"
#include "adatsor.h"
#include "common.h"
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>
#include <limits>
#include <memory>
#include <string>
#include <boost/functional/hash.hpp>

class ReturnElem {
    public:
        ReturnElem();
        virtual ~ReturnElem();
        virtual void add(variant_t item) = 0;
        virtual std::vector<variant_t> get() = 0;
        virtual size_t get_size() = 0;
        virtual variant_t get_single_item(size_t i) = 0;
};

class ALL : public ReturnElem {
    std::vector<variant_t> items;
    public:
    ALL();
    ~ALL();
    virtual void add(variant_t item) { items.push_back(item); }
    virtual std::vector<variant_t> get() { return items; }
    virtual size_t get_size() { return items.size(); } 
    virtual variant_t get_single_item(size_t i);
}; 

class AVG : public ReturnElem {
    long avg;
    long itemnr;
    public:
        AVG();
        ~AVG();
        virtual void add(variant_t item);
        virtual std::vector<variant_t> get();
        virtual size_t get_size() { return 1; }
        virtual variant_t get_single_item(size_t i);
};

class MIN : public ReturnElem {
    int val;
    public:
        MIN();
        ~MIN();
        virtual void add(variant_t item);
        virtual std::vector<variant_t> get();
        virtual size_t get_size() { return 1; }
        virtual variant_t get_single_item(size_t i);
};

class MAX : public ReturnElem {
    int val;
    public:
        MAX();
        ~MAX();
        virtual void add(variant_t item);
        virtual std::vector<variant_t> get();
        virtual size_t get_size() { return 1; }
        virtual variant_t get_single_item(size_t i);
};

class ReturnRowBuilder {
    protected:
        std::vector<Name>& select_list;
        std::vector<std::string>& alias_list;
        std::vector<std::string>& aggregate_list;
        std::map<std::string, table_t*>& local_table_pointers;
        std::map<std::string, int> table_order_index;
        std::vector<size_t> return_row_table_index;
        std::vector<size_t> return_row_rowsize;
        std::unordered_set<size_t> out_rows_hash;
        Server* server_p;
    public:
        ReturnRowBuilder(std::vector<Name>& select_list,
                std::vector<std::string>& alias_list,
                         std::vector<std::string>& aggregate_list,
                         std::map<std::string, table_t*>& local_table_pointers,
                         Server* _server_p);
        ~ReturnRowBuilder();
        virtual void process_row(std::vector<row_id>& rid) = 0;
        virtual void get_result(int select_opt) = 0;
        virtual bool is_result_empty() = 0;
};

class ReturnRowBuilderNoGroupBy : public ReturnRowBuilder {
    std::vector<std::shared_ptr<ReturnElem>> return_row;
    public:
    ReturnRowBuilderNoGroupBy(std::vector<Name>& select_list,
                         std::vector<std::string>& alias_list,
                         std::vector<std::string>& aggregate_list,
                         std::map<std::string, table_t*>& local_table_pointers,
                         Server* _server_p);
    ~ReturnRowBuilderNoGroupBy();
    virtual void process_row(std::vector<row_id>& rid);
    virtual void get_result(int select_opt);
    virtual bool is_result_empty();
};

class ReturnRowBuilderWithGroupBy : public ReturnRowBuilder {
    Name group_by;
    std::map< simple_variant_t, std::vector<std::shared_ptr<ReturnElem>> > return_row;
    size_t group_by_table_index;
    size_t group_by_rowsize;
    public:
    ReturnRowBuilderWithGroupBy(std::vector<Name>& select_list,
                         std::vector<std::string>& alias_list,
                         std::vector<std::string>& aggregate_list,
                         std::map<std::string, table_t*>& local_table_pointers,
                         Name group_by,
                         Server* _server_p);
    ~ReturnRowBuilderWithGroupBy();
    virtual void process_row(std::vector<row_id>& rid);
    virtual void get_result(int select_opt);
    virtual bool is_result_empty();

    class RowFactory;
    std::unique_ptr<RowFactory> rf;

    class Maker{
        public:
        Maker();
        virtual ~Maker();
        virtual std::shared_ptr<ReturnElem> get_new_elem() = 0;
    };

    class ALLMaker : public Maker{
        public:
        ALLMaker();
        virtual ~ALLMaker();
        virtual std::shared_ptr<ReturnElem> get_new_elem() { return std::shared_ptr<ReturnElem>(new ALL); }
    };

    class AVGMaker : public Maker{
        public:
        AVGMaker();
        virtual ~AVGMaker();
        virtual std::shared_ptr<ReturnElem> get_new_elem() { return std::shared_ptr<ReturnElem>(new AVG); }

    };

    class MINMaker : public Maker{
        public:
        MINMaker();
        virtual ~MINMaker();
        virtual std::shared_ptr<ReturnElem> get_new_elem() { return std::shared_ptr<ReturnElem>(new MIN); }

    };

    class MAXMaker : public Maker{
        public:
        MAXMaker();
        virtual ~MAXMaker();
        virtual std::shared_ptr<ReturnElem> get_new_elem() { return std::shared_ptr<ReturnElem>(new MAX); }

    };

    class RowFactory{
        std::vector<std::unique_ptr<Maker>> row_template;
        public:
        RowFactory(std::vector<std::string> aggregate_list);
        ~RowFactory();
        std::vector<std::shared_ptr<ReturnElem>> get_new_row();
    };
};

#endif
