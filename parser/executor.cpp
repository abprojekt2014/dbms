#include "executor.h"
#include "executor_wrapper.h"
#include "adatsor.h"

using std::cout;
using std::endl;
using std::string;
using std::ostream_iterator;
using std::vector;
using std::map;
using std::shared_ptr;
using std::unique_ptr;
using std::exception;

Executor::Executor() : is_groupby(false), is_index_name(false), dbm(std::unique_ptr<DatabaseManager>(new DatabaseManager)), server_p(nullptr) {}

Executor::~Executor(){}

//method to extract all row_id combinations from where_result
void get(size_t index, vector<row_id> res, vector<row_id>& min_id, vector<row_id>& max_id, unique_ptr<ReturnRowBuilder>& prrb, map<string, table_t*>& local_table_pointers){
    if(min_id[index] != -1){
        for(row_id i = min_id[index]; i <= max_id[index]; ++i){
            vector<row_id> localres = res;
            localres.push_back(i);
            if(index < min_id.size() - 1)
                get(index+1, localres, min_id, max_id, prrb, local_table_pointers);
            else{
                prrb->process_row(localres);
                //std::copy(localres.begin(), localres.end(), ostream_iterator<int>(cout, " "));cout << endl;
            }
        }
    }
    else {
        auto ltb_it = local_table_pointers.begin();
        std::advance(ltb_it, index);
        iterator_t* t_it = ltb_it->second->get_iterator();
        for (recordid_t row = t_it->first(); !t_it->end(); row = t_it->next()){
            vector<row_id> localres = res;
            localres.push_back(row);
            if(index < min_id.size() - 1)
                get(index+1, localres, min_id, max_id, prrb, local_table_pointers);
            else{
                prrb->process_row(localres);
                //std::copy(localres.begin(), localres.end(), ostream_iterator<int>(cout, " "));cout << endl;
            }
        }
        delete t_it;
    }
}

void Executor::select(int select_opt, int column_nr, int table_nr){
    try{
        map<string, table_t*> primary_table_names;
        tablemanager& tm = tablemanager::getinstance();

        for(int i = 0; i != table_nr; ++i){
            string tn = boost::get<Name>(command_stack.top()).value;
            primary_table_names[tn] = tm.opentable(tn);
            if(!primary_table_names[tn])
                throw ParserException("Error: there is no table with name \"" + tn + "\".");
            //assert(primary_table_names[tn]);
            command_stack.pop();
        }

        vector<Name> select_list;
        vector<string> alias_list;
        vector<string> aggregate_list;
        for(int i = 0; i != column_nr; ++i){
            variant_t item = command_stack.top();
            command_stack.pop();
            if (item.which() == 0){//Name
                Name temp_name = boost::get<Name>(item);
                temp_name.resolve_table_name(primary_table_names);
                if(temp_name.table_name == ""){
                    //ha tovabbra sincs tablanev a table_pointers-t hasznaljuk
                    temp_name.resolve_table_name(table_pointers);
                }
                select_list.push_back(temp_name);
                alias_list.push_back(temp_name.value);
                aggregate_list.push_back("all");
            }
            else {//shared_ptr<Selection>
                shared_ptr<Selection> sps = boost::get<shared_ptr<Selection>>(item);
                sps->name.resolve_table_name(primary_table_names);
                if(sps->name.table_name == ""){
                    //ha tovabbra sincs tablanev a table_pointers-t hasznaljuk
                    sps->name.resolve_table_name(table_pointers);
                }
                select_list.push_back(sps->name);
                alias_list.push_back(sps->alias);
                aggregate_list.push_back(sps->aggregate);
            }
        }
        std::reverse(select_list.begin(), select_list.end());
        std::reverse(aggregate_list.begin(), aggregate_list.end());
        std::reverse(alias_list.begin(), alias_list.end());

        //std::copy(select_list.begin(), select_list.end(), ostream_iterator<Name>(cout, ", "));
        //cout << endl;
        //std::copy(aggregate_list.begin(), aggregate_list.end(), ostream_iterator<string>(cout, ", "));
        //cout << endl;
        //std::copy(alias_list.begin(), alias_list.end(), ostream_iterator<string>(cout, ", "));
        //cout << endl;

        map<string, table_t*> local_table_pointers(primary_table_names);

        //for(auto it = select_list.begin(); it != select_list.end(); ++it)
        //    local_table_pointers[it->table_name] = table_pointers[it->table_name];
        size_t ltb_size = local_table_pointers.size();
        vector<vector<row_id>> where_result;
        if(where_tree){
            where_result = where_tree->execute(local_table_pointers);
        }
        else{//ha nincs feltetel minden kell
            vector<row_id> temp_row;
            for(size_t i = 0; i != ltb_size; ++i)
                temp_row.push_back(-1);
            where_result.push_back(temp_row);
        }
        unique_ptr<ReturnRowBuilder> prrb;
        if(is_groupby)
            prrb.reset(new ReturnRowBuilderWithGroupBy(select_list, alias_list, aggregate_list, local_table_pointers, groupby, server_p));
        else
            prrb.reset(new ReturnRowBuilderNoGroupBy(select_list, alias_list, aggregate_list, local_table_pointers, server_p));

        vector<row_id> min_id(ltb_size, 0);
        vector<row_id> max_id(ltb_size, 0);

        //cout << where_result.size() << endl;
        //std::copy(where_result[0].begin(), where_result[0].end(), ostream_iterator<int>(cout, " "));cout << endl;

        for(size_t i = 0; i != where_result.size(); ++i){
            for(size_t j = 0; j != ltb_size; ++j){
                if(where_result[i][j] == -1){
                    min_id[j] = -1;
                    max_id[j] = -1;
                }
                else{
                    min_id[j] = where_result[i][j];
                    max_id[j] = where_result[i][j];
                }
            }
            vector<row_id> res;
            //recursively get all row_id combinations and process them
            get(0, res, min_id, max_id, prrb, local_table_pointers);
        }

        prrb->get_result(select_opt);

        //reset s_groupby variable
        is_groupby = false;
        where_tree.reset();
    }
    catch(exception& e){
        cout << e.what() << endl << endl;
        send_error_msg(e.what());
        is_groupby = false;
        where_tree.reset();
        return;
    }
}

void Executor::create_index(int column_nr){
    try{
        vector<string> index_columns;
        for(int i = 0; i != column_nr; ++i){
            index_columns.push_back(boost::get<Name>(command_stack.top()).value);
            command_stack.pop();
        }
        string index_table_name = boost::get<Name>(command_stack.top()).value;
        command_stack.pop();

        cout << "Creating index in table: " << index_table_name << ", using: ";
        //tablemanager& tm = tablemanager::getinstance();
        //table_t* t = tm.opentable(index_table_name);
        table_t* t = tablemanager::getinstance().opentable(index_table_name);
        if(!t)
            throw ParserException("Error: there is no table with name \"" + index_table_name + "\".");

        //assert(t);
        catalog& cat = t->getcatalog();

        for(string& s : index_columns){
            bool found = false;
            // setting the column as indexed
            for (column_t* col: cat.columns())
            {
                if (col->name.compare(s) == 0)
                {
                    // we got the right column
                    found = true;
                    break;
                }
            }
            cout << "Column " << s << " not found in table " << index_table_name << endl;
            if(!found)
                throw ParserException("Error: there is no column \"" + s + " in table \"" + index_table_name + "\".");
            //assert(found);
        }
        vector<int> offsets;
        vector<index_t*> indices;
        for(auto it = index_columns.begin(); it != index_columns.end(); ++it){
            cout << *it << " ";

            for (column_t* col: cat.columns())
            {
                if (col->name.compare(*it) == 0)
                {
                    if(col->attributes.indexed)
                    {
                        cout << "Index already existing; skip" << endl;
                        continue;
                    }
                    // we got the right column
                    index_t* idx = indexmanager::getinstance().createindex(idxHashTable, index_table_name,  *it, col->type, col->length);
                    cat.setindex(col, idx);
                    offsets.push_back(col->offset);
                    indices.push_back(idx);
                    break;
                }
            }
        }
        cout << endl;

        int rowsize = cat.rowlength();
        unique_ptr<iterator_t> it(t->get_iterator());
        for(recordid_t row = it->first(); !it->end(); row = it->next()){
            BYTE* binrow = new BYTE[rowsize];

            t->read_row(row, binrow);
            for(size_t i = 0; i < offsets.size(); i++){
                indices[i]->insertrow(row, &binrow[offsets[i]]);
            }

            delete[] binrow;
        }

        t->writecatalog();

        if(is_index_name)
            cout << "Index name is: " << index_name << endl;

        is_index_name = false;

        if (server_p)
        {
            AdatSor as;
            as.adat.push_back("OK");
            server_p->set_write_return_value(server_p->write(as));
        }
    }
    catch(exception& e){
        cout << e.what() << endl << endl;
        send_error_msg(e.what());
        is_index_name = false;
        return;
    }
}

void Executor::deletefrom(const char* s){
    string table_name(s);
    //cout << "Deleting from table " << table_name << "\n";

    try{
        table_pointers[table_name] = tablemanager::getinstance().opentable(table_name);
        if(!table_pointers[table_name])
            throw ParserException("Error: there is no table with name \"" + table_name + "\".");

        map<string, table_t*> local_table_pointers { std::make_pair(table_name, table_pointers[table_name])};
        vector<vector<row_id>> where_result;//ez csak szimpla vektor delete es update eseteben
        if(where_tree){
            where_result = where_tree->execute(local_table_pointers);
        }
        else{//ha nincs feltetel mindent torolni kell
            vector<row_id> temp_row {-1};
            where_result.push_back(temp_row);
        }

        if(where_result.size() != 0){
            table_t* t = table_pointers[table_name];
            BYTE* binrow = new BYTE[t->getcatalog().rowlength()];

            if(where_result[0].size() > 0){ //kell torolni
                if(where_result[0][0] == -1){ //mindent sort torolni kell

                    int primary_offs = 0;
                    int primary_col_size = 0;

                    // ellenorizzuk a primary key-eket
                    index_t* primary_key = nullptr;
                    for (column_t* c: t->getcatalog().columns())
                    {
                        if (c->attributes.primary_key)
                        {
                            primary_key = c->primary_key.primary_key_index;
                            primary_offs = c->offset;
                            primary_col_size = (int)c->length;
                            break;
                        }
                    }

                    // ossze kell gyujteni az osszes olyan tablat, amiben foreign key van a mostani tablara
                    vector<table_t*>      ref_tables;
                    vector<int>         ref_offs;
                    vector<columntype>  ref_coltype;
                    vector<int>         ref_row_len;

                    if (primary_key)
                    {
                        for (string tbl_name : tablemanager::getinstance().gettablenames())
                        {
                            //cout << "Table " << tbl_name << endl;
                            table_t* tbl = tablemanager::getinstance().gettable(tbl_name);

                            assert(tbl);
                            for (column_t* c: tbl->getcatalog().columns())
                            {
                                if (c->attributes.foreign_key)
                                {
                                    //cout << "Column " << c->foreign_key.foreign_key_index->gettablename() << "." << c->foreign_key.foreign_key_index->getcolumnname() << endl;
                                    if ( 0 == c->foreign_key.ref_table_name->compare(table_name))
                                    {
                                        // ez a mi tablazatunk neve
                                        //cout << "Table with reference "<< tbl_name << endl;
                                        ref_tables.push_back(tbl);
                                        ref_offs.push_back(c->offset);
                                        ref_coltype.push_back(c->type);
                                        ref_row_len.push_back(tbl->getcatalog().rowlength());
                                    }
                                }
                            }
                        }
                    }

                    iterator_t* it = t->get_iterator();
                    for (recordid_t row = it->first(); !it->end(); row = it->next())
                    {
                        bool reference_found = false;
                        if (primary_key && (ref_tables.size() > 0))
                        {
                            // ha van primary_key

                            // kiolvassuk a sor tartalmat
                            t->read_row(row, binrow);

                            // meg kell vizsgalni, hogy ez az ertek benne van-e meg valami tablaban
                            for (size_t i = 0; i< ref_tables.size(); i++)
                            {
                                BYTE* b = new BYTE[ref_row_len[i]];
                                table_t* tbl = ref_tables[i];
                                serializer_t s(1000);   // dummy value, to not assert
                                unique_ptr<iterator_t> it(tbl->get_iterator());

                                for (recordid_t r = it->first(); !it->end(); r = it->next())
                                {
                                    tbl->read_row(r, b);
                                    if (s.values_equal(&binrow[primary_offs], &b[ref_offs[i]], ref_coltype[i], primary_col_size))
                                    {
                                        cout << "Referenced column found, skipping delete!" << endl;
                                        reference_found = true;
                                        break;
                                    }
                                }

                                delete[] b;
                            }
                        }

                        if (reference_found)
                        {
                            // nem toroljuk a sort
                            continue;
                        }

                        t->delete_row(row);

                        for (column_t* col: t->columns())
                        {
                            index_t* idx = nullptr;
                            if (col->attributes.primary_key)
                            {
                                idx = col->primary_key.primary_key_index;
                            }
                            else if (!col->attributes.foreign_key)
                            {
                                idx = col->index;
                            }

                            if (idx != nullptr)
                            {
                                unique_ptr<iterator_t> it(idx->getiterator());

                                for (recordid_t row = it->first(); !it->end(); row = it->next())
                                {
                                    idx->deleterow(row);
                                }
                            }
                        }
                    }
                    delete it;
                }
                else{
                    int primary_offs = 0;
                    int primary_col_size = 0;

                    // ellenorizzuk a primary key-eket
                    index_t* primary_key = nullptr;
                    for (column_t* c: t->getcatalog().columns())
                    {
                        if (c->attributes.primary_key)
                        {
                            primary_key = c->primary_key.primary_key_index;
                            primary_offs = c->offset;
                            primary_col_size = (int)c->length;
                            break;
                        }
                    }

                    //
                    // ossze kell gyujteni az osszes olyan tablat, amiben foreign key van a mostani tablara
                    //
                    vector<table_t*>      ref_tables;
                    vector<int>         ref_offs;
                    vector<columntype>  ref_coltype;
                    vector<int>         ref_row_len;

                    if (primary_key)
                    {
                        for (string tbl_name : tablemanager::getinstance().gettablenames())
                        {
                            //cout << "Table " << tbl_name << endl;
                            table_t* tbl = tablemanager::getinstance().gettable(tbl_name);

                            assert(tbl);
                            for (column_t* c: tbl->getcatalog().columns())
                            {
                                if (c->attributes.foreign_key)
                                {
                                    //cout << "Column " << c->foreign_key.foreign_key_index->gettablename() << "." << c->foreign_key.foreign_key_index->getcolumnname() << endl;
                                    if ( 0 == c->foreign_key.ref_table_name->compare(table_name))
                                    {
                                        // ez a mi tablazatunk neve
                                        //cout << "Table with reference "<< tbl_name << endl;
                                        ref_tables.push_back(tbl);
                                        ref_offs.push_back(c->offset);
                                        ref_coltype.push_back(c->type);
                                        ref_row_len.push_back(tbl->getcatalog().rowlength());
                                    }
                                }
                            }
                        }
                    }

                    for(size_t i = 0; i != where_result[0].size(); ++i){
                        recordid_t row = where_result[0][i];

                        bool reference_found = false;
                        if (primary_key && (ref_tables.size() > 0))
                        {
                            // ha van primary_key

                            // kiolvassuk a sor tartalmat
                            t->read_row(row, binrow);

                            // meg kell vizsgalni, hogy ez az ertek benne van-e meg valami tablaban
                            for (size_t i = 0; i< ref_tables.size(); i++)
                            {
                                BYTE* b = new BYTE[ref_row_len[i]];
                                table_t* tbl = ref_tables[i];
                                serializer_t s(1000);   // dummy value, to not assert
                                unique_ptr<iterator_t> it(tbl->get_iterator());

                                for (recordid_t r = it->first(); !it->end(); r = it->next())
                                {
                                    tbl->read_row(r, b);
                                    if (s.values_equal(&binrow[primary_offs], &b[ref_offs[i]], ref_coltype[i], primary_col_size))
                                    {
                                        cout << "Referenced column found, skipping delete!" << endl;
                                        reference_found = true;
                                        break;
                                    }
                                }

                                delete[] b;
                            }
                        }

                        if (reference_found)
                        {
                            // nem toroljuk a sort
                            continue;
                        }

                        // toroljuk a sort
                        table_pointers[table_name]->delete_row(row);


                        for (column_t* col: table_pointers[table_name]->columns())
                        {
                            if (col->attributes.indexed)
                            {
                                index_t* idx = nullptr;
                                if (col->attributes.primary_key)
                                {
                                    idx = col->primary_key.primary_key_index;
                                }
                                else if (!col->attributes.foreign_key)
                                {
                                    idx = col->index;
                                }

                                if (idx != nullptr)
                                {
                                    idx->deleterow(row);
                                }
                            }
                        }
                    }
                }
            }

            delete[] binrow;
        }
    }
    catch(exception& e){
        cout << e.what() << endl << endl;
        where_tree.reset();
        send_error_msg(e.what());
        return;
    }

    where_tree.reset();

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("OK");
        server_p->set_write_return_value(server_p->write(as));
    }
}

void Executor::update(){
    try{
        string table_name;
        if(command_stack.top().which() == 0){//Name
            table_name = boost::get<Name>(command_stack.top()).value;
        }
        /*else if(command_stack.top().which() == 1){//FieldName
          string table_name = boost::get<FieldName>(command_stack.top());
          }*/
        command_stack.pop();

        /*cout << "Updating table " << table_name << " with:\n";
          for(auto it = assignment_list.begin(); it != assignment_list.end(); ++it){
          cout << "Name = " << it->name.value << ", value = ";
          if(it->value.which() == 2)//int
          cout << boost::get<int>(it->value) << " ";
          else if(it->value.which() == 4)//string
          cout << boost::get<string>(it->value) << " ";
          cout << endl;
          }*/

        table_t* t = tablemanager::getinstance().opentable(table_name);
        if(!t)
            throw ParserException("Error: there is no table with name \"" + table_name + "\".");
        map<string, table_t*> local_table_pointers { std::make_pair(table_name, t)};
        vector<vector<row_id>> where_result;//ez csak szimpla vektor delete es update eseteben
        if(where_tree){
            where_result = where_tree->execute(local_table_pointers);
        }
        else{//ha nincs feltetel mindenhol updatelni kell
            vector<row_id> temp_row {-1};
            where_result.push_back(temp_row);
        }

        if(where_result.size() != 0){

            vector<size_t> row_table_index;
            assignment_list[0].name.resolve_table_name(local_table_pointers);
            vector<column_t*> table_columns(local_table_pointers[assignment_list[0].name.table_name]->getcatalog().columns());
            size_t row_rowsize = local_table_pointers[assignment_list[0].name.table_name]->getcatalog().rowlength();
            for(size_t i = 0; i != assignment_list.size(); ++i){
                assignment_list[i].name.resolve_table_name(local_table_pointers);
                for(size_t j = 0; j != table_columns.size(); ++j){
                    if(table_columns[j]->name == assignment_list[i].name.value){
                        row_table_index.push_back(j);
                        break;
                    }
                }
            }
            unique_ptr<serializer_t> pserializer(new serializer_t(row_rowsize));
            BYTE* binrow = new BYTE[row_rowsize];

            if(where_result[0].size() > 0){ //kell updatelni
                if(where_result[0][0] == -1){ //mindent sort updatelni kell
                    iterator_t* it = table_pointers[table_name]->get_iterator();
                    for (recordid_t row = it->first(); !it->end(); row = it->next()){
                        local_table_pointers[assignment_list[0].name.table_name]->read_row(row, binrow);
                        for(size_t ii = 0; ii != assignment_list.size(); ++ii){
                            if(table_columns[row_table_index[ii]]->type == 0){
                                pserializer->serialize_int(binrow, table_columns[row_table_index[ii]]->offset, boost::get<int>(assignment_list[ii].value), table_columns[row_table_index[ii]]->length);

                                column_t* col = table_columns[row_table_index[ii]];
                                index_t* idx = nullptr;

                                if (col->attributes.indexed)
                                {
                                    if (col->attributes.primary_key)
                                    {
                                        idx = col->primary_key.primary_key_index;
                                    }
                                    else if (!col->attributes.foreign_key)
                                    {
                                        idx = col->index;
                                    }

                                    if (idx != nullptr)
                                    {
                                        idx->update(row, &binrow[col->offset]);
                                    }
                                }
                            }
                            else{//string
                                pserializer->serialize_str(binrow, table_columns[row_table_index[ii]]->offset, boost::get<string>(assignment_list[ii].value), table_columns[row_table_index[ii]]->length);

                                column_t* col = table_columns[row_table_index[ii]];
                                index_t* idx = nullptr;

                                if (col->attributes.indexed)
                                {
                                    if (col->attributes.primary_key)
                                    {
                                        idx = col->primary_key.primary_key_index;
                                    }
                                    else if (!col->attributes.foreign_key)
                                    {
                                        idx = col->index;
                                    }

                                    if (idx != nullptr)
                                    {
                                        idx->update(row, &binrow[col->offset]);
                                    }
                                }
                            }
                        }
                        table_pointers[table_name]->update_row(row, binrow);
                    }
                    delete it;
                }
                else{
                    for(size_t i = 0; i != where_result[0].size(); ++i){
                        local_table_pointers[assignment_list[0].name.table_name]->read_row(where_result[0][i], binrow);
                        for(size_t ii = 0; ii != assignment_list.size(); ++ii){
                            if(table_columns[row_table_index[ii]]->type == 0){
                                pserializer->serialize_int(binrow, table_columns[row_table_index[ii]]->offset, boost::get<int>(assignment_list[ii].value), table_columns[row_table_index[ii]]->length);
                            }
                            else{//string
                                pserializer->serialize_str(binrow, table_columns[row_table_index[ii]]->offset, boost::get<string>(assignment_list[ii].value), table_columns[row_table_index[ii]]->length);
                            }
                        }
                        table_pointers[table_name]->update_row(where_result[0][i], binrow);
                    }
                }
            }

            delete[] binrow;
        }
    }
    catch(exception& e){
        assignment_list.clear();
        where_tree.reset();
        cout << e.what() << endl << endl;
        send_error_msg(e.what());
        return;
    }

    assignment_list.clear();
    where_tree.reset();

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("OK");
        server_p->set_write_return_value(server_p->write(as));
    }
}

void Executor::add_columndef(const char* s, int type){
    column_defs.push_back(ColumnDefinition(s, type));
    if (!command_stack.empty()){
        if(command_stack.top().which() == 5){//AttributePrimaryKey
            command_stack.pop();
            column_defs.back().primary_key = true;
        }
    }
    if (!command_stack.empty()){
        if(command_stack.top().which() == 7){//References
            column_defs.back().references = true;
            References temp_ref = boost::get<References>(command_stack.top());
            column_defs.back().reference_table_name = temp_ref.table_name;
            column_defs.back().reference_column_names = temp_ref.column_names;
            command_stack.pop();
        }
    }
}

void Executor::create_table(const char* s){
    catalog c;

    int default_length = 4;
    for(vector<ColumnDefinition>::const_iterator it = column_defs.begin(); it != column_defs.end(); ++it){
        if(it->type >= parser_string_code){ //string
            int length = it->type - parser_string_code;
            if(length == 0)
                length = default_length;
            c.addcolumn(it->name, ctString, length);
            //cout << length << endl;
            //dbm->add_column(string(s), it->name, 0, length);
        }
        else if(it->type >= parser_int_code){ //integer
            int length = it->type - parser_int_code;
            if(length == 0)
                length = default_length;
            c.addcolumn(it->name, ctInteger, length);
            //cout << length << endl;
            //dbm->add_column(string(s), it->name, 0, length);
        }
        //cout << "Added " << it->name << " type: " << it->type << endl;

        //cout << endl;
    }

    table_t* t = tablemanager::getinstance().createtable(s, c);

    if (nullptr != t){
        //cout << "Created table " << s << endl;
        table_pointers[string(s)] = t;
    }
    else
    {
        if (server_p)
        {
            send_error_msg(string("Table ") + string(s) + " is already an existing table");
        }

        cout << "Table not created\n";
        return;
    }

    for(vector<ColumnDefinition>::const_iterator it = column_defs.begin(); it != column_defs.end(); ++it){
        if(it->primary_key){
            //cout << "primary key attr";
            vector<string> temp_vector {it->name};
            //dbm->add_primary_key(string(s), temp_vector);
            column_t* col = t->column(it->name);
            assert(col);
            index_t* idx = indexmanager::getinstance().createindex(idxHashTable, s, it->name, col->type, col->length);
            t->getcatalog().setprimarykey(col, idx);
        }
        if(it->references){
            //cout << ", references ";
            //for(size_t i = 0; i != it->reference_column_names.size(); ++i)
            //    cout << it->reference_column_names[i] << " ";
            //cout << " in " << it->reference_table_name;
            vector<string> temp_vector {it->name};
            //dbm->add_references(string(s), temp_vector, it->reference_table_name, it->reference_column_names);

            table_t* t2 = tablemanager::getinstance().opentable(it->reference_table_name);
            column_t* col = t2->column(it->reference_column_names[0]);
            assert(col->attributes.primary_key);
            t->getcatalog().setforeignkey(t->column(it->name), col->primary_key.primary_key_index->gettablename() , col->primary_key.primary_key_index);
        }
    }

    t->writecatalog();

    if(multi_primary_key.columns.size() != 0){
        //cout << "Multiple primary key: ";
        //for (unsigned i=0; i!=multi_primary_key.columns.size(); ++i){
        //    cout << multi_primary_key.columns[i] << " ";
        //}
        //cout << endl;
        //dbm->add_primary_key(string(s), multi_primary_key.columns);
        ;
    }

    //cleaning
    column_defs.clear();
    multi_primary_key.columns.clear();
    //cout << command_stack.size() << endl;

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("OK");
        server_p->set_write_return_value(server_p->write(as));
    }
}

void Executor::get_primary_key_clumns(int ii){
    for(int i=0; i<ii; ++i){
        Name temp_name = boost::get<Name>(command_stack.top());
        multi_primary_key.columns.push_back(temp_name.value);
        command_stack.pop();
    }
}

void Executor::get_reference(const char* tn, int ii){
    vector<string> pk;
    for(int i=0; i<ii; ++i){
        Name temp_name = boost::get<Name>(command_stack.top());
        pk.push_back(temp_name.value);
        command_stack.pop();
    }
    command_stack.push( variant_t(References(tn, pk)) );
}

void Executor::create_assignment(const char* s){
    Assignment assignment((Name(s)));
    //assignment.name = Name(s);
    if(command_stack.top().which() == 2)//int
        assignment.value = boost::get<int>(command_stack.top());
    if(command_stack.top().which() == 4)//string
        assignment.value = boost::get<string>(command_stack.top());
    command_stack.pop();
    assignment_list.push_back(assignment);
}

void Executor::insert(const char* s){
    try{
        table_t* t = table_pointers[s] = tablemanager::getinstance().opentable(s);

        if(!t)
            throw ParserException("Error: there is no table with name \"" + string(s) + "\".");

        int rowsize = t->getcatalog().rowlength();
        //cout << "rowsize: " << rowsize << endl;
        unique_ptr<serializer_t> pserializer(new serializer_t(rowsize));
        BYTE* binrow = new BYTE[rowsize];
        vector<column_t*> table_columns(table_pointers[s]->getcatalog().columns());

        //feltetelezzuk, hogy a tablaban levo sorrendben vannak az adatok

        /*cout << "Inserting into ";
          copy(columns.begin(), columns.end(), ostream_iterator<Name>(cout, " "));
          cout << " in " << s << " values ";
          for(unsigned i=0; i!=values.size(); ++i){
          if(values[i].which() == 2)//int
          cout << boost::get<int>(values[i]) << " ";
          if(values[i].which() == 4)//string
          cout << boost::get<string>(values[i]) << " ";
          }
          cout << endl;
         */

        for(size_t i = 0; i != table_columns.size(); ++i){
            index_t* idx = nullptr;
            column_t* col = table_columns[i];
            bool primary = false;

            if (col->attributes.indexed)
            {
                if (col->attributes.primary_key)
                {
                    idx = col->primary_key.primary_key_index;
                    primary = true;
                }
                else if (col->attributes.foreign_key)
                {
                    //idx = col->foreign_key.foreign_key_index;
                    //foreign = true;
                }
                else
                {
                    idx = col->index;
                }
            }
            if(table_columns[i]->type == 0){//int
                //cout << "serialized to int for insert\n";
                int value = boost::get<int>(values[i]);
                if (idx != nullptr)
                {
                    if (primary)
                    {
                        // le kell ellenorizni, hogy mar letezik-e az adott ertek
                        unique_ptr<iterator_t> it(idx->getiterator_equal(&value));

                        if (it->first() != invalid_recordid)
                        {
                            // a sor mar letezik
                            send_error_msg(string("The row with value is existing"));
                            cout << "The row with value " << value << " is existing" << endl;
                            return;
                        }
                    }
                }

                pserializer->serialize_int(binrow, table_columns[i]->offset, value, table_columns[i]->length);
            }
            if(table_columns[i]->type == 2){//string
                //cout << "serialized to string for insert\n";
                //cout << "offset " << table_columns[i]->offset << " value " << boost::get<string>(values[i]) << " length " << table_columns[i]->length << endl;
                int offset = table_columns[i]->offset;
                std::string value = boost::get<string>(values[i]);
                int collength = table_columns[i]->length;
                if (idx != nullptr)
                {
                    char* cstr = const_cast<char*>(value.c_str());
                    if (primary)
                    {
                        // le kell ellenorizni, hogy mar letezik-e az adott ertek
                        unique_ptr<iterator_t> it(idx->getiterator_equal(cstr));

                        if (it->first() != invalid_recordid)
                        {
                            // a sor mar letezik

                            send_error_msg("The row with value " + value + " is existing");
                            cout << "The row with value " + value + " is existing" << endl;
                            return;
                        }
                    }
                }

                pserializer->serialize_str(binrow, offset, value, collength);
            }
        }

        recordid_t rowid;

        rowid = table_pointers[s]->insert_row(binrow);
        if(rowid == invalid_recordid)
            cout << "Not inserted\n";

        for(size_t i = 0; i != table_columns.size(); ++i){
            index_t* idx = nullptr;
            column_t* col = table_columns[i];

            if (!col->attributes.indexed)
            {
                continue;
            }

            if (col->attributes.primary_key)
            {
                idx = col->primary_key.primary_key_index;
            }
            else if (col->attributes.foreign_key)
            {
                //idx = col->foreign_key.foreign_key_index;
            }
            else
            {
                idx = col->index;
            }

            if (idx != nullptr)
            {
                idx->insertrow(rowid, &binrow[col->offset]);
            }
        }

        delete[] binrow;
    }
    catch(exception& e){
        columns.clear();
        values.clear();
        cout << e.what() << endl << endl;
        send_error_msg(e.what());
        return;
    }

    columns.clear();
    values.clear();
    //cout << command_stack.size() << endl;

    if (server_p)
    {
        AdatSor as;
        as.adat.push_back("OK");
        server_p->set_write_return_value(server_p->write(as));
    }
}

void Executor::get_columns(int ii){
    for(int i=0; i<ii; ++i){
        columns.push_back(boost::get<Name>(command_stack.top()));
        command_stack.pop();
    }
}

void Executor::get_values(int ii){
    for(int i=0; i<ii; ++i){
        values.push_back(command_stack.top());
        command_stack.pop();
    }
    std::reverse(values.begin(), values.end());
}

bool Executor::create_database(const char* s, catalog& c){

    //to delete if implemented
    return true;
}

void Executor::new_name(const char* s){
    command_stack.push(variant_t(Name(s)));
}

void Executor::send_error_msg(const string& s){
    AdatSor adat_kuldeni;
    adat_kuldeni.adat.push_back("ERROR");
    adat_kuldeni.adat.push_back(s);
    if(server_p)
        server_p->set_write_return_value(server_p->write(adat_kuldeni));

}
