#include "executor.h"
#include <iostream>

using std::cout;
using std::endl;

void* pv_exec;

#ifdef __cplusplus
extern "C" {
#endif
    extern int doparse(int ac, char **av);
#ifdef __cplusplus
}
#endif

int main(int ac, char **av){
    Executor* p_exec = new Executor();
    pv_exec = static_cast<void*>(p_exec);
    doparse(ac, av);
    delete p_exec;
}
