//#include "common.h"
#include "executor.h"
#include "executor_wrapper.h"
#include "where_items.h"
#include <memory>
#include <iostream>
#include <string>
#include <boost/variant.hpp>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::string;

std::pair<Name, log_variant_t> get_variant_t_childs(Executor* p);
std::pair<shared_ptr<LogNode>, shared_ptr<LogNode>> get_LogNode_childs(Executor* p);

void executor_create_index(void* v, int column_nr){
    Executor* p = static_cast<Executor*>(v);
    p->create_index(column_nr);
}

void executor_select(void* v, int select_opt, int column_nr, int table_nr){
    Executor* p = static_cast<Executor*>(v);
    p->select(select_opt, column_nr, table_nr);
    //p->where_tree->execute(3);
}

void executor_where(void* v){
    Executor* p = static_cast<Executor*>(v);
    p->where_tree = boost::get<shared_ptr<LogNode>>(p->command_stack.top());
    p->command_stack.pop();
}

void executor_new_and_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<shared_ptr<LogNode>, shared_ptr<LogNode>> childs = get_LogNode_childs(p);
    p->command_stack.push( variant_t ( shared_ptr<LogNode>( new LogAnd(childs.first, childs.second) ) ) );
}

void executor_new_or_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<shared_ptr<LogNode>, shared_ptr<LogNode>> childs = get_LogNode_childs(p);
    p->command_stack.push( variant_t ( shared_ptr<LogNode>( new LogOr(childs.first, childs.second) ) ) );
}


void executor_new_eq_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogEq(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );
}

void executor_new_not_eq_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogNotEq(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );
}

void executor_new_greater_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogGreater(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );

}

void executor_new_less_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogLessEq(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );

}

void executor_new_greater_or_eq_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogGreaterEq(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );
}

void executor_new_less_or_eq_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogLessEq(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );
}

void executor_new_like_node(void* v){
    Executor* p = static_cast<Executor*>(v);
    std::pair<Name, log_variant_t> childs = get_variant_t_childs(p);
    shared_ptr<LogNode> ple(new LogLike(childs.first, childs.second));
    p->command_stack.push( variant_t(ple) );
}

void executor_update(void* v){
    Executor* p = static_cast<Executor*>(v);
    p->update();
}

void executor_deletefrom(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->deletefrom(s);
}

void executor_new_reference(void* v, const char* tn, int i){
    Executor* p = static_cast<Executor*>(v);
    p->get_reference(tn, i);
}

void executor_new_column_definition(void* v, const char* s, int type){
    Executor* p = static_cast<Executor*>(v);
    p->add_columndef(s, type);
}

void executor_create_table(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->create_table(s);
}

void executor_new_multiple_pr_key(void* v, int i){
    Executor* p = static_cast<Executor*>(v);
    p->get_primary_key_clumns(i);
}

void executor_new_attr_pr_key(void* v){
    Executor* p = static_cast<Executor*>(v);
    p->command_stack.push( variant_t(AttributePrimaryKey()) );
}

void executor_create_assignment(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->create_assignment(s); 
}

void executor_insert(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->insert(s); 
}

void executor_get_columns(void* v, int i){
    Executor* p = static_cast<Executor*>(v);
    p->get_columns(i); 
}

void executor_get_values(void* v, int i){
    Executor* p = static_cast<Executor*>(v);
    p->get_values(i); 
}

void executor_create_database(void* v, const char* s){
    /// TODO: parse the columns, the types to create a catalog from them what shall be passed to the create_database
    //Executor* p = static_cast<Executor*>(v);
    ///p->create_database(s);   // commeted out until the catalog is built
}

void executor_new_name(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->new_name(s);
    //p->command_stack.push( variant_t(Name(s)) );
}

void executor_new_fieldname(void* v, const char* tn, const char* n){
    Executor* p = static_cast<Executor*>(v);
    //p->command_stack.push( variant_t(FieldName(tn, n)) );
    string s(string(n) + "." + string(tn));
    p->command_stack.push( variant_t(Name(s)) );
    //std::cout << p->command_stack.size() << " " <<  p->command_stack.top().which() << std::endl;
}

void executor_new_int(void* v, int i){
    Executor* p = static_cast<Executor*>(v);
    p->command_stack.push( variant_t(i) );
    //std::cout << p->command_stack.size() << " " <<  p->command_stack.top().which() << std::endl;
}

void executor_new_double(void* v, double d){
    Executor* p = static_cast<Executor*>(v);
    p->command_stack.push( variant_t(d) );
    //std::cout << p->command_stack.size() << " " <<  p->command_stack.top().which() << std::endl;
}

void executor_new_string(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    std::string temp_string(s);
    //remove ''
    temp_string.erase(0,1);
    temp_string.erase(temp_string.size()-1,1);
    p->command_stack.push( variant_t(temp_string) );
    //std::cout << p->command_stack.size() << " " <<  p->command_stack.top().which() << std::endl;
}

inline std::pair<Name, log_variant_t> get_variant_t_childs(Executor* p){
    variant_t child = p->command_stack.top();
    p->command_stack.pop();
    Name child1 = boost::get<Name>(p->command_stack.top());
    p->command_stack.pop();
    if(child.which()==2){//int
        int i = boost::get<int>(child);
        return std::make_pair(child1, log_variant_t(i));
    }

    else if(child.which()==0){//Name
        Name n = boost::get<Name>(child);
        return std::make_pair(child1, log_variant_t(n));
    }

    else {//string
        string s = boost::get<string>(child);
        return std::make_pair(child1, log_variant_t(s));
    }
}

inline std::pair<shared_ptr<LogNode>, shared_ptr<LogNode>> get_LogNode_childs(Executor* p){
    shared_ptr<LogNode> child2 = boost::get<shared_ptr<LogNode>>(p->command_stack.top());
    p->command_stack.pop();
    shared_ptr<LogNode> child1 = boost::get<shared_ptr<LogNode>>(p->command_stack.top());
    p->command_stack.pop();
    return std::make_pair(child1, child2);
}

void executor_avg(void* v){
    Executor* p = static_cast<Executor*>(v);
    Name n = boost::get<Name>(p->command_stack.top());
    p->command_stack.pop();
    std::shared_ptr<Selection> temp_sp(new Selection(n));    
    temp_sp->aggregate = "avg";
    p->command_stack.push( variant_t(temp_sp) );
}

void executor_min(void* v){
    Executor* p = static_cast<Executor*>(v);
    Name n = boost::get<Name>(p->command_stack.top());
    p->command_stack.pop();
    std::shared_ptr<Selection> temp_sp(new Selection(n));    
    temp_sp->aggregate = "min";
    p->command_stack.push( variant_t(temp_sp) );
} 

void executor_max(void* v){
    Executor* p = static_cast<Executor*>(v);
    Name n = boost::get<Name>(p->command_stack.top());
    p->command_stack.pop();
    std::shared_ptr<Selection> temp_sp(new Selection(n));    
    temp_sp->aggregate = "max";
    p->command_stack.push( variant_t(temp_sp) );
}

void executor_alias(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    variant_t tempv = p->command_stack.top();
    p->command_stack.pop();
    if(tempv.which() == 0){//Name
        Name n = boost::get<Name>(tempv);
        std::shared_ptr<Selection> temp_sp(new Selection(n));    
        temp_sp->alias = s;
        p->command_stack.push( variant_t(temp_sp) );
    }
    else {
        std::shared_ptr<Selection> temp_sp = boost::get<std::shared_ptr<Selection>>(tempv);
        temp_sp->alias = s;
        p->command_stack.push( variant_t(temp_sp) );
    }
}

void executor_groupby(void* v){
    Executor* p = static_cast<Executor*>(v);
    Name name = boost::get<Name>(p->command_stack.top());
    p->command_stack.pop();
    p->groupby = name;
    p->is_groupby = true;
}

void executor_set_index_name(void* v, const char* s){
    Executor* p = static_cast<Executor*>(v);
    p->index_name = s;
    p->is_index_name = true;
    //cout << p->command_stack.size() << endl;
}
