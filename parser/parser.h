#ifndef PARSER_H
#define PARSER_H

#include "executor.h"
#include <memory>
#include <string>

class Server;

class Parser{
    private:
        Executor* p_exec;
    public:
        Parser();
        ~Parser();
        void interpret(const std::string s);
        void register_server(Server* _server_p){
            p_exec->register_server(_server_p);
        }
};

#endif
