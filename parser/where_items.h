#ifndef WHERE_ITEMS_H
#define WHERE_ITEMS_H

#include "../record_manager/table_manager.h"
#include "../record_manager/catalog.h"
#include "../record_manager/table.h"
#include "../record_manager/serializer.hpp"
#include "../include/dbtypes.h"
#include "../index_manager/index_manager.h"
#include "common.h"
#include <vector>
#include <map>
#include <iostream>
#include <memory>
#include <string>
#include <regex>

class LogNode {
    public:
        LogNode();
        virtual ~LogNode();
        virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers) = 0;
        virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers) = 0;
};

class LogAnd : public LogNode {
    std::shared_ptr<LogNode> child1;
    std::shared_ptr<LogNode> child2;

    public:
    LogAnd(std::shared_ptr<LogNode>& child1, std::shared_ptr<LogNode>& child2);
    virtual ~LogAnd();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogOr : public LogNode {
    std::shared_ptr<LogNode> child1;
    std::shared_ptr<LogNode> child2;

    public:
    LogOr(std::shared_ptr<LogNode>& child1, std::shared_ptr<LogNode>& child2);
    virtual ~LogOr();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogEq : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogEq(Name n, log_variant_t child2);
    virtual ~LogEq();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogNotEq : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogNotEq(Name n, log_variant_t child2);
    virtual ~LogNotEq();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogGreater : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogGreater(Name n, log_variant_t child2);
    virtual ~LogGreater();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogGreaterEq : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogGreaterEq(Name n, log_variant_t child2);
    virtual ~LogGreaterEq();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogLess : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogLess(Name n, log_variant_t child2);
    virtual ~LogLess();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogLessEq : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogLessEq(Name n, log_variant_t child2);
    virtual ~LogLessEq();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

class LogLike : public LogNode {
    Name name;
    log_variant_t child2;

    public:
    LogLike(Name n, log_variant_t child2);
    virtual ~LogLike();
    virtual std::vector<std::vector<row_id>> execute(std::map<std::string, table_t*>& local_table_pointers);
    virtual void resolve_table_name(std::map<std::string, table_t*>& table_pointers);
};

#endif
