#ifndef EXECUTOR_H
#define EXECUTOR_H

#include "select_items.h"
#include "../record_manager/table_manager.h"
#include "../record_manager/catalog.h"
#include "../record_manager/table.h"
#include "../record_manager/serializer.hpp"
#include "database_manager.h"
#include "stack_items.h"
#include "stack_items_not_for_common.h"
#include "where_items.h"
#include "common.h"
#include "parser_exception.h"
#include <stack>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <memory>
#include <exception>
//#include <boost/variant.hpp>

//typedef boost::variant<Name, FieldName, int, double, std::string> variant_t;

class Server;

class Executor{
    public:
        std::stack<variant_t> command_stack;
        std::vector<Name> columns;
        std::vector<variant_t> values;
        std::vector<ColumnDefinition> column_defs;
        std::vector<Assignment> assignment_list;
        MultiPrimaryKey multi_primary_key;
        std::shared_ptr<LogNode> where_tree;
        Name groupby;
        bool is_groupby;
        std::string index_name;
        bool is_index_name; 
        std::unique_ptr<DatabaseManager> dbm;
        std::map<std::string, table_t*> table_pointers;

        Executor();
        ~Executor();
        void get_reference(const char*, int ii);
        void get_columns(int ii);
        void get_values(int ii);
        void insert(const char* s);
        void create_assignment(const char* s);
        void get_primary_key_clumns(int i);
        void create_table(const char* s);
        void add_columndef(const char* s, int type);
        bool create_database(const char* s, catalog& c);
        void update();
        void deletefrom(const char* s);
        void select(int select_opt, int column_nr, int table_nr);
        void create_index(int column_nr);
        void new_name(const char* s);
    
        Server* server_p;
        void register_server(Server* _server_p){ server_p = _server_p; }
        void send_error_msg(const std::string& s);
};

#endif
