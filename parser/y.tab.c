/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "pmysql.y" /* yacc.c:339  */

#include "executor_wrapper.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

extern void yy_scan_string ( const char *str );
int yylex();
void yyerror(char *s, ...);
void emit(char *s, ...);
extern void* pv_exec;

#line 80 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NAME = 258,
    STRING = 259,
    INTNUM = 260,
    APPROXNUM = 261,
    BOOL = 262,
    OR = 263,
    XOR = 264,
    ANDOP = 265,
    EQUAL = 266,
    NOTEQUAL = 267,
    LESS = 268,
    GREATER = 269,
    LESSEQUAL = 270,
    GREATEREQUAL = 271,
    NOT = 272,
    LIKE = 273,
    ALL = 274,
    AS = 275,
    ASC = 276,
    AVG = 277,
    BIT = 278,
    BY = 279,
    CHAR = 280,
    CREATE = 281,
    DATABASE = 282,
    DECIMAL = 283,
    DEFAULT = 284,
    DELETE = 285,
    DESC = 286,
    DISTINCT = 287,
    DOUBLE = 288,
    FCOUNT = 289,
    FLOAT = 290,
    FORCE = 291,
    FROM = 292,
    GROUP = 293,
    IGNORE = 294,
    INDEX = 295,
    INSERT = 296,
    INTEGER = 297,
    INTO = 298,
    JOIN = 299,
    KEY = 300,
    MIN = 301,
    MAX = 302,
    ON = 303,
    ORDER = 304,
    OUTER = 305,
    PRIMARY = 306,
    REFERENCES = 307,
    REPLACE = 308,
    SELECT = 309,
    SET = 310,
    STRAIGHT_JOIN = 311,
    TABLE = 312,
    USE = 313,
    UPDATE = 314,
    USING = 315,
    VALUES = 316,
    VARCHAR = 317,
    WHERE = 318
  };
#endif
/* Tokens.  */
#define NAME 258
#define STRING 259
#define INTNUM 260
#define APPROXNUM 261
#define BOOL 262
#define OR 263
#define XOR 264
#define ANDOP 265
#define EQUAL 266
#define NOTEQUAL 267
#define LESS 268
#define GREATER 269
#define LESSEQUAL 270
#define GREATEREQUAL 271
#define NOT 272
#define LIKE 273
#define ALL 274
#define AS 275
#define ASC 276
#define AVG 277
#define BIT 278
#define BY 279
#define CHAR 280
#define CREATE 281
#define DATABASE 282
#define DECIMAL 283
#define DEFAULT 284
#define DELETE 285
#define DESC 286
#define DISTINCT 287
#define DOUBLE 288
#define FCOUNT 289
#define FLOAT 290
#define FORCE 291
#define FROM 292
#define GROUP 293
#define IGNORE 294
#define INDEX 295
#define INSERT 296
#define INTEGER 297
#define INTO 298
#define JOIN 299
#define KEY 300
#define MIN 301
#define MAX 302
#define ON 303
#define ORDER 304
#define OUTER 305
#define PRIMARY 306
#define REFERENCES 307
#define REPLACE 308
#define SELECT 309
#define SET 310
#define STRAIGHT_JOIN 311
#define TABLE 312
#define USE 313
#define UPDATE 314
#define USING 315
#define VALUES 316
#define VARCHAR 317
#define WHERE 318

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 15 "pmysql.y" /* yacc.c:355  */

        int intval;
        double floatval;
        char *strval;
        int subtok;

#line 253 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 270 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  31
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   413

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  74
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  44
/* YYNRULES -- Number of rules.  */
#define YYNRULES  117
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  241

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   318

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    22,     2,     2,     2,     2,     2,     2,
      71,    72,    19,    17,    73,    18,    70,    20,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    69,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    21,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   105,   105,   106,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   119,   120,   121,   122,   123,   124,   125,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     139,   140,   148,   151,   152,   155,   156,   160,   161,   164,
     165,   168,   169,   170,   173,   174,   177,   179,   182,   183,
     184,   187,   188,   189,   192,   194,   195,   196,   199,   200,
     203,   204,   208,   209,   210,   214,   217,   218,   221,   222,
     226,   228,   230,   232,   235,   236,   241,   244,   247,   247,
     250,   251,   254,   255,   259,   260,   261,   262,   267,   270,
     275,   278,   282,   285,   288,   289,   292,   295,   295,   299,
     300,   304,   305,   308,   309,   312,   313,   317,   320,   323,
     325,   329,   332,   335,   338,   341,   344,   345
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "STRING", "INTNUM", "APPROXNUM",
  "BOOL", "OR", "XOR", "ANDOP", "EQUAL", "NOTEQUAL", "LESS", "GREATER",
  "LESSEQUAL", "GREATEREQUAL", "'+'", "'-'", "'*'", "'/'", "NOT", "'!'",
  "LIKE", "ALL", "AS", "ASC", "AVG", "BIT", "BY", "CHAR", "CREATE",
  "DATABASE", "DECIMAL", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DOUBLE",
  "FCOUNT", "FLOAT", "FORCE", "FROM", "GROUP", "IGNORE", "INDEX", "INSERT",
  "INTEGER", "INTO", "JOIN", "KEY", "MIN", "MAX", "ON", "ORDER", "OUTER",
  "PRIMARY", "REFERENCES", "REPLACE", "SELECT", "SET", "STRAIGHT_JOIN",
  "TABLE", "USE", "UPDATE", "USING", "VALUES", "VARCHAR", "WHERE", "';'",
  "'.'", "'('", "')'", "','", "$accept", "stmt_list", "expr", "stmt",
  "select_stmt", "opt_where", "opt_groupby", "groupby_list",
  "opt_asc_desc", "opt_orderby", "column_list", "select_opts",
  "select_expr_list", "select_expr", "opt_as_alias", "table_references",
  "table_reference", "table_factor", "join_table", "opt_join_condition",
  "join_condition", "index_hint", "index_list", "insert_stmt", "opt_into",
  "opt_col_names", "insert_vals_list", "insert_vals", "delete_stmt",
  "create_database_stmt", "create_table_stmt", "create_col_list",
  "create_definition", "$@1", "column_atts", "data_type", "opt_length",
  "opt_references", "update_stmt", "update_asgn_list", "use_stmt",
  "create_index_stmt", "table_name", "opt_indexname", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,    43,    45,    42,
      47,   272,    33,   273,   274,   275,   276,   277,   278,   279,
     280,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   293,   294,   295,   296,   297,   298,   299,
     300,   301,   302,   303,   304,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,   316,   317,   318,    59,
      46,    40,    41,    44
};
# endif

#define YYPACT_NINF -152

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-152)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-1)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      80,    25,   -32,    10,  -152,    13,     1,    82,    -9,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,  -152,    64,    75,    78,
      90,  -152,    99,    47,  -152,    24,     1,   -54,    34,  -152,
    -152,  -152,    36,  -152,  -152,  -152,    55,    45,    59,    67,
      60,  -152,  -152,  -152,  -152,  -152,    58,    58,  -152,    69,
    -152,    71,    77,    92,    58,   330,   -14,  -152,  -152,   167,
     178,   -29,   -52,   179,     1,     1,  -152,   180,   129,    58,
    -152,   181,   120,   184,   169,   169,    58,    85,    58,    58,
     213,    58,    58,    58,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,  -152,     1,    58,  -152,    98,
     143,   144,   146,  -152,  -152,   186,   -59,    34,   -36,  -152,
     127,   150,     3,  -152,   188,   348,   130,   132,   134,  -152,
     232,   136,   248,   264,   280,  -152,   363,   377,   390,   102,
     102,   102,   102,   102,   102,   141,   141,   169,   169,   183,
     -55,  -152,   -29,   138,   139,   140,    58,   198,  -152,    58,
     142,  -152,  -152,   181,   145,  -152,   129,   -42,   181,  -152,
     128,   161,  -152,  -152,  -152,  -152,  -152,   171,  -152,   209,
     209,   209,   348,   204,   348,   181,   163,   181,  -152,   147,
     166,   160,  -152,  -152,   348,    23,   182,   225,   215,  -152,
      79,    81,    93,    58,   214,  -152,   229,   233,  -152,   265,
     299,  -152,  -152,   168,   128,    58,   276,  -152,  -152,   316,
    -152,  -152,   348,  -152,  -152,   250,   251,   254,   270,  -152,
     348,    96,   298,   255,    58,  -152,  -152,  -152,   181,   277,
    -152,  -152,  -152,  -152,    58,   255,   257,  -152,   298,  -152,
    -152
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,    79,    48,     0,     0,     0,     0,    32,
      76,    88,    90,    92,   107,   111,   113,     0,   116,     0,
       0,    78,     0,     0,   112,    57,     0,     0,    58,    60,
      61,     1,     0,     2,    91,   117,     0,     0,    35,    80,
       4,     6,     7,     8,     9,    53,     0,     0,    49,     0,
      50,     0,     0,     0,     0,    57,    33,    51,    56,     0,
       0,    73,     0,     0,     0,     0,     3,     0,    97,     0,
      89,     0,     0,     0,    27,    28,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    54,     0,     0,    55,    57,
       0,     0,     0,    62,    64,     0,    35,    59,    66,   115,
       0,     0,     0,    94,     0,    36,    46,     0,     0,     5,
       0,     0,     0,     0,     0,    29,    18,    19,    17,    20,
      21,    22,    23,    24,    25,    13,    14,    15,    16,    26,
      35,    52,    73,     0,     0,     0,     0,     0,   108,     0,
       0,    65,    67,     0,     0,    93,    97,     0,     0,    81,
       0,    77,    10,    30,    31,    11,    12,    37,    63,     0,
       0,     0,   109,     0,    68,     0,     0,     0,    95,   103,
       0,   105,    47,    85,    84,     0,     0,     0,    44,    74,
       0,     0,     0,     0,     0,   114,     0,     0,   101,     0,
       0,    99,    82,     0,     0,     0,     0,    34,    72,     0,
      71,    70,   110,    69,    96,     0,     0,     0,    98,    87,
      86,     0,    41,    38,     0,    75,   104,   102,     0,     0,
      83,    42,    43,    39,     0,    45,     0,   100,    41,   106,
      40
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -152,  -152,   -46,   323,  -152,   -84,  -152,   107,    94,  -152,
    -151,  -152,  -152,   238,   -44,   -23,   273,   286,  -152,  -152,
    -152,   212,     6,  -152,  -152,  -152,  -152,   165,  -152,  -152,
    -152,  -152,   228,  -152,  -152,  -152,  -152,  -152,  -152,  -152,
    -152,  -152,  -152,  -152
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     7,    55,     8,     9,    70,   188,   223,   233,   207,
     117,    23,    56,    57,    61,    27,    28,    29,    30,   151,
     152,   103,   190,    10,    22,    72,   161,   185,    11,    12,
      13,   112,   113,   114,   218,   181,   198,   201,    14,   106,
      15,    16,   110,    36
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      74,    75,   176,    62,    25,   179,    63,   182,    80,    69,
      20,    95,   100,    69,   147,   101,    24,   149,    64,    64,
     104,    64,   148,   115,   194,   180,   196,    58,    96,   150,
     120,   122,   123,   124,   102,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,    59,
      40,    41,    42,    43,    44,   142,   167,    17,    21,    97,
      33,    40,    41,    42,    43,    44,    45,    34,    46,    47,
      18,    48,    26,   140,    49,   155,   156,   236,    35,    46,
      47,    37,    31,    65,    50,    49,    51,    19,    40,    41,
      42,    43,    44,    38,    60,   202,   203,    51,    52,    53,
     172,    58,    39,   174,   121,    66,    46,    47,    67,    52,
      53,     1,    49,     1,   184,     2,    68,     2,    54,    90,
      91,    92,    93,    59,    51,    94,     3,    69,     3,    54,
      73,    40,    41,    42,    43,    44,    52,    53,    71,     4,
      76,     4,    77,     5,     6,     5,     6,   212,    78,    46,
      47,   208,   209,   210,   209,    49,    54,   220,   184,   222,
      92,    93,   183,    79,    94,   211,   209,    51,   230,   203,
      98,    40,    41,    42,    43,    44,   191,   192,   222,    52,
      53,    99,   105,   109,   116,   111,   118,   119,   238,    46,
      47,   157,    94,   143,   144,    49,   145,   146,   153,    54,
     154,   173,   219,   158,   159,   160,    -1,    51,   163,   169,
     170,   171,   189,   175,   187,   193,   177,   200,   197,    52,
      53,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,   186,   195,    94,   199,   215,    54,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,   204,   205,    94,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,   206,
     216,    94,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,   125,   213,    94,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,   214,   217,    94,   162,   224,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,   225,
     164,    94,   226,   227,   231,   228,   229,   237,   234,   239,
      32,   235,   240,    58,   232,   141,   165,   107,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,   108,   166,    94,   168,    59,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,   221,
       0,    94,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,   178,     0,    94,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,     0,
      94,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,     0,    94
};

static const yytype_int16 yycheck[] =
{
      46,    47,   153,    26,     3,    47,    60,   158,    54,    68,
      42,    55,    41,    68,    73,    44,     3,    53,    73,    73,
      72,    73,   106,    69,   175,    67,   177,     3,    42,    65,
      76,    77,    78,    79,    63,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    25,
       3,     4,     5,     6,     7,    99,   140,    32,    48,    73,
      69,     3,     4,     5,     6,     7,    19,     3,    21,    22,
      45,    24,    71,    96,    27,    72,    73,   228,     3,    21,
      22,     3,     0,    49,    37,    27,    39,    62,     3,     4,
       5,     6,     7,     3,    70,    72,    73,    39,    51,    52,
     146,     3,     3,   149,    19,    69,    21,    22,    53,    51,
      52,    31,    27,    31,   160,    35,    71,    35,    71,    17,
      18,    19,    20,    25,    39,    23,    46,    68,    46,    71,
      70,     3,     4,     5,     6,     7,    51,    52,    71,    59,
      71,    59,    71,    63,    64,    63,    64,   193,    71,    21,
      22,    72,    73,    72,    73,    27,    71,   203,   204,   205,
      19,    20,    34,    71,    23,    72,    73,    39,    72,    73,
       3,     3,     4,     5,     6,     7,   170,   171,   224,    51,
      52,     3,     3,     3,     3,    56,    66,     3,   234,    21,
      22,     3,    23,    50,    50,    27,    50,    11,    71,    71,
      50,     3,    34,    73,    72,    71,    23,    39,    72,    71,
      71,    71,     3,    71,    43,    11,    71,    57,    71,    51,
      52,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    73,    72,    23,    71,     5,    71,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    71,    29,    23,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    54,
       5,    23,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    72,    72,    23,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    72,     3,    23,    72,    29,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,     3,
      72,    23,    72,    72,    26,    71,    56,    50,    73,    72,
       7,   224,   238,     3,    36,    97,    72,    64,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    65,    72,    23,   142,    25,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,   204,
      -1,    23,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,   156,    -1,    23,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    -1,    -1,
      23,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    -1,    -1,    23
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    31,    35,    46,    59,    63,    64,    75,    77,    78,
      97,   102,   103,   104,   112,   114,   115,    32,    45,    62,
      42,    48,    98,    85,     3,     3,    71,    89,    90,    91,
      92,     0,    77,    69,     3,     3,   117,     3,     3,     3,
       3,     4,     5,     6,     7,    19,    21,    22,    24,    27,
      37,    39,    51,    52,    71,    76,    86,    87,     3,    25,
      70,    88,    89,    60,    73,    49,    69,    53,    71,    68,
      79,    71,    99,    70,    76,    76,    71,    71,    71,    71,
      76,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    23,    88,    42,    73,     3,     3,
      41,    44,    63,    95,    72,     3,   113,    90,    91,     3,
     116,    56,   105,   106,   107,    76,     3,    84,    66,     3,
      76,    19,    76,    76,    76,    72,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      89,    87,    88,    50,    50,    50,    11,    73,    79,    53,
      65,    93,    94,    71,    50,    72,    73,     3,    73,    72,
      71,   100,    72,    72,    72,    72,    72,    79,    95,    71,
      71,    71,    76,     3,    76,    71,    84,    71,   106,    47,
      67,   109,    84,    34,    76,   101,    73,    43,    80,     3,
      96,    96,    96,    11,    84,    72,    84,    71,   110,    71,
      57,   111,    72,    73,    71,    29,    54,    83,    72,    73,
      72,    72,    76,    72,    72,     5,     5,     3,   108,    34,
      76,   101,    76,    81,    29,     3,    72,    72,    71,    56,
      72,    26,    36,    82,    73,    81,    84,    50,    76,    72,
      82
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    74,    75,    75,    76,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      76,    76,    77,    78,    78,    79,    79,    80,    80,    81,
      81,    82,    82,    82,    83,    83,    84,    84,    85,    85,
      85,    86,    86,    86,    87,    88,    88,    88,    89,    89,
      90,    90,    91,    91,    91,    92,    93,    93,    94,    94,
      95,    95,    95,    95,    96,    96,    77,    97,    98,    98,
      99,    99,   100,   100,   101,   101,   101,   101,    77,   102,
      77,   103,    77,   104,   105,   105,   106,   107,   106,   108,
     108,   109,   109,   110,   110,   111,   111,    77,   112,   113,
     113,    77,   114,    77,   115,   116,   117,   117
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     3,     1,     3,     1,     1,     1,     1,
       4,     4,     4,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     2,     2,     3,
       4,     4,     1,     3,     8,     0,     2,     0,     3,     2,
       4,     0,     1,     1,     0,     3,     1,     3,     0,     2,
       2,     1,     3,     1,     2,     2,     1,     0,     1,     3,
       1,     1,     3,     5,     3,     4,     0,     1,     2,     4,
       5,     5,     5,     0,     1,     3,     1,     6,     1,     0,
       0,     3,     3,     5,     1,     1,     3,     3,     1,     4,
       1,     3,     1,     6,     1,     3,     5,     0,     5,     0,
       3,     2,     4,     0,     3,     0,     5,     1,     5,     3,
       5,     1,     2,     1,     8,     1,     0,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:
#line 108 "pmysql.y" /* yacc.c:1646  */
    { /*emit("NAME %s", $1);*/ executor_new_name(pv_exec, (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 1561 "y.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 109 "pmysql.y" /* yacc.c:1646  */
    { /*emit("FIELDNAME %s.%s", $1, $3);*/ executor_new_fieldname(pv_exec, (yyvsp[-2].strval), (yyvsp[0].strval)); free((yyvsp[-2].strval)); free((yyvsp[0].strval)); }
#line 1567 "y.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 110 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STRING %s", $1);*/ executor_new_string(pv_exec, (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 1573 "y.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 111 "pmysql.y" /* yacc.c:1646  */
    { /*emit("NUMBER %d", $1);*/ executor_new_int(pv_exec, (yyvsp[0].intval));}
#line 1579 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 112 "pmysql.y" /* yacc.c:1646  */
    { emit("FLOAT %g", (yyvsp[0].floatval)); executor_new_double(pv_exec, (yyvsp[0].floatval));}
#line 1585 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 113 "pmysql.y" /* yacc.c:1646  */
    { emit("BOOL %d", (yyvsp[0].intval)); }
#line 1591 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 114 "pmysql.y" /* yacc.c:1646  */
    { /*emit("AVG");*/ executor_avg(pv_exec); (yyval.intval)=(yyvsp[-1].intval); }
#line 1597 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 115 "pmysql.y" /* yacc.c:1646  */
    { /*emit("MIN");*/ executor_min(pv_exec); (yyval.intval)=(yyvsp[-1].intval); }
#line 1603 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 116 "pmysql.y" /* yacc.c:1646  */
    { /*emit("MAX");*/ executor_max(pv_exec); (yyval.intval)=(yyvsp[-1].intval); }
#line 1609 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 119 "pmysql.y" /* yacc.c:1646  */
    { emit("ADD"); }
#line 1615 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 120 "pmysql.y" /* yacc.c:1646  */
    { emit("SUB"); }
#line 1621 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 121 "pmysql.y" /* yacc.c:1646  */
    { emit("MUL"); }
#line 1627 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 122 "pmysql.y" /* yacc.c:1646  */
    { emit("DIV"); }
#line 1633 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 123 "pmysql.y" /* yacc.c:1646  */
    { /*emit("AND");*/ executor_new_and_node(pv_exec); }
#line 1639 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 124 "pmysql.y" /* yacc.c:1646  */
    { /*emit("OR");*/ executor_new_or_node(pv_exec); }
#line 1645 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 125 "pmysql.y" /* yacc.c:1646  */
    { emit("XOR"); }
#line 1651 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 127 "pmysql.y" /* yacc.c:1646  */
    { /*emit("EQUAL");*/ executor_new_eq_node(pv_exec); }
#line 1657 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 128 "pmysql.y" /* yacc.c:1646  */
    { /*emit("NOTEQUAL");*/ executor_new_not_eq_node(pv_exec); }
#line 1663 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 129 "pmysql.y" /* yacc.c:1646  */
    { /*emit("LESS");*/ executor_new_less_node(pv_exec); }
#line 1669 "y.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 130 "pmysql.y" /* yacc.c:1646  */
    { /*emit("GREATER");*/ executor_new_greater_node(pv_exec); }
#line 1675 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 131 "pmysql.y" /* yacc.c:1646  */
    { /*emit("LESSEQUAL");*/ executor_new_less_or_eq_node(pv_exec); }
#line 1681 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 132 "pmysql.y" /* yacc.c:1646  */
    { /*emit("GREATEREQUAL");*/ executor_new_greater_or_eq_node(pv_exec); }
#line 1687 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 133 "pmysql.y" /* yacc.c:1646  */
    {/*emit("LIKE");*/ executor_new_like_node(pv_exec); }
#line 1693 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 134 "pmysql.y" /* yacc.c:1646  */
    { emit("NOT"); }
#line 1699 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 135 "pmysql.y" /* yacc.c:1646  */
    { emit("NOT"); }
#line 1705 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 136 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval)=(yyvsp[-1].intval); }
#line 1711 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 139 "pmysql.y" /* yacc.c:1646  */
    { emit("COUNTALL"); }
#line 1717 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 140 "pmysql.y" /* yacc.c:1646  */
    { emit(" CALL 1 COUNT"); }
#line 1723 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 148 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 1729 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 151 "pmysql.y" /* yacc.c:1646  */
    { emit("SELECTNODATA %d %d", (yyvsp[-1].intval), (yyvsp[0].intval)); }
#line 1735 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 152 "pmysql.y" /* yacc.c:1646  */
    { /*emit("SELECT %d %d %d", $2, $3, $5);*/ executor_select(pv_exec, (yyvsp[-6].intval), (yyvsp[-5].intval), (yyvsp[-3].intval)); }
#line 1741 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 156 "pmysql.y" /* yacc.c:1646  */
    { /*emit("WHERE");*/ executor_where(pv_exec); }
#line 1747 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 161 "pmysql.y" /* yacc.c:1646  */
    { /*emit("GROUPBYLIST %d", $3);*/ }
#line 1753 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 164 "pmysql.y" /* yacc.c:1646  */
    { /*emit("GROUPBY %d", $2);*/ executor_groupby(pv_exec); (yyval.intval)=1; }
#line 1759 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 165 "pmysql.y" /* yacc.c:1646  */
    { /*emit("GROUPBY %d", $4);*/ (yyval.intval)=(yyvsp[-3].intval)+1; }
#line 1765 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 168 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval)=0; }
#line 1771 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 169 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval)=0; }
#line 1777 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 170 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval)=1; }
#line 1783 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 174 "pmysql.y" /* yacc.c:1646  */
    { emit("ORDERBY %d", (yyvsp[0].intval)); }
#line 1789 "y.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 177 "pmysql.y" /* yacc.c:1646  */
    { executor_new_name(pv_exec, (yyvsp[0].strval)); /*emit("COLUMN %s", $1);*/ free((yyvsp[0].strval)); (yyval.intval) = 1; }
#line 1795 "y.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 179 "pmysql.y" /* yacc.c:1646  */
    { executor_new_name(pv_exec, (yyvsp[-2].strval)); /*emit("COLUMN %s", $1);*/ free((yyvsp[-2].strval)); (yyval.intval) = (yyvsp[0].intval) + 1; }
#line 1801 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 182 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 0; }
#line 1807 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 183 "pmysql.y" /* yacc.c:1646  */
    { if((yyvsp[-1].intval) & 01) yyerror("duplicate ALL option"); (yyval.intval) = (yyvsp[-1].intval) | 01; }
#line 1813 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 184 "pmysql.y" /* yacc.c:1646  */
    { if((yyvsp[-1].intval) & 02) yyerror("duplicate DISTINCT option"); (yyval.intval) = (yyvsp[-1].intval) | 02; }
#line 1819 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 187 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 1; }
#line 1825 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 188 "pmysql.y" /* yacc.c:1646  */
    {(yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 1831 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 189 "pmysql.y" /* yacc.c:1646  */
    { emit("SELECTALL"); (yyval.intval) = 1; }
#line 1837 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 194 "pmysql.y" /* yacc.c:1646  */
    { /*emit ("ALIAS %s", $2);*/ executor_alias(pv_exec, (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 1843 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 195 "pmysql.y" /* yacc.c:1646  */
    { /*emit ("ALIAS %s", $1);*/ free((yyvsp[0].strval)); }
#line 1849 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 199 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 1; }
#line 1855 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 200 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 1861 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 208 "pmysql.y" /* yacc.c:1646  */
    { /*emit("TABLE %s", $1);*/ executor_new_name(pv_exec, (yyvsp[-2].strval)); free((yyvsp[-2].strval)); }
#line 1867 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 209 "pmysql.y" /* yacc.c:1646  */
    { /*emit("TABLE %s.%s", $1, $3);*/ executor_new_fieldname(pv_exec, (yyvsp[-4].strval), (yyvsp[-2].strval)); free((yyvsp[-4].strval)); free((yyvsp[-2].strval)); }
#line 1873 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 210 "pmysql.y" /* yacc.c:1646  */
    { /*emit("TABLEREFERENCES %d", $2);*/ }
#line 1879 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 214 "pmysql.y" /* yacc.c:1646  */
    { emit("JOIN %d", 100); }
#line 1885 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 221 "pmysql.y" /* yacc.c:1646  */
    { emit("ONEXPR"); }
#line 1891 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 222 "pmysql.y" /* yacc.c:1646  */
    { emit("USING %d", (yyvsp[-1].intval)); }
#line 1897 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 227 "pmysql.y" /* yacc.c:1646  */
    { emit("INDEXHINT %d", (yyvsp[-1].intval)); }
#line 1903 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 229 "pmysql.y" /* yacc.c:1646  */
    { emit("INDEXHINT %d", (yyvsp[-1].intval)); }
#line 1909 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 231 "pmysql.y" /* yacc.c:1646  */
    { emit("INDEXHINT %d", (yyvsp[-1].intval)); }
#line 1915 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 235 "pmysql.y" /* yacc.c:1646  */
    { emit("INDEX %s", (yyvsp[0].strval)); free((yyvsp[0].strval)); (yyval.intval) = 1; }
#line 1921 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 236 "pmysql.y" /* yacc.c:1646  */
    { emit("INDEX %s", (yyvsp[0].strval)); free((yyvsp[0].strval)); (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 1927 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 241 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 1933 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 244 "pmysql.y" /* yacc.c:1646  */
    { executor_insert(pv_exec, (yyvsp[-3].strval)); /*emit("INSERTVALS %d %s", $6, $3);*/ free((yyvsp[-3].strval)); }
#line 1939 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 251 "pmysql.y" /* yacc.c:1646  */
    { executor_get_columns(pv_exec, (yyvsp[-1].intval)); /*emit("INSERTCOLS %d", $2);*/ }
#line 1945 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 254 "pmysql.y" /* yacc.c:1646  */
    { executor_get_values(pv_exec, (yyvsp[-1].intval)); /*emit("VALUES %d", $2);*/ (yyval.intval)=1; }
#line 1951 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 255 "pmysql.y" /* yacc.c:1646  */
    { /*emit("VALUES %d", $4);*/ (yyval.intval)=(yyvsp[-4].intval)+1; }
#line 1957 "y.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 259 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval)=1; }
#line 1963 "y.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 260 "pmysql.y" /* yacc.c:1646  */
    { emit("DEFAULT"); (yyval.intval) = 1; }
#line 1969 "y.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 261 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 1975 "y.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 262 "pmysql.y" /* yacc.c:1646  */
    { emit("DEFAULT"); (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 1981 "y.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 267 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 1987 "y.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 270 "pmysql.y" /* yacc.c:1646  */
    { /*emit("DELETE: %s", $3 );*/ executor_deletefrom(pv_exec, (yyvsp[-1].strval)); free((yyvsp[-1].strval)); }
#line 1993 "y.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 275 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 1999 "y.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 278 "pmysql.y" /* yacc.c:1646  */
    { /*emit("CREATEDATABASE %s", $3);*/ free((yyvsp[0].strval)); }
#line 2005 "y.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 282 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 2011 "y.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 285 "pmysql.y" /* yacc.c:1646  */
    { executor_create_table(pv_exec, (yyvsp[-3].strval)); /*emit("CREATE %s", $3);*/ free((yyvsp[-3].strval)); }
#line 2017 "y.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 288 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 1; }
#line 2023 "y.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 289 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 2029 "y.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 292 "pmysql.y" /* yacc.c:1646  */
    { executor_new_multiple_pr_key(pv_exec, (yyvsp[-1].intval)); /*emit("PRIKEY %d", $4);*/ }
#line 2035 "y.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 295 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STARTCOL");*/ }
#line 2041 "y.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 296 "pmysql.y" /* yacc.c:1646  */
    { executor_new_column_definition(pv_exec, (yyvsp[-3].strval), (yyvsp[-2].intval)); /*emit("COLUMNDEF %d %s", $3, $2);*/ free((yyvsp[-3].strval)); }
#line 2047 "y.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 299 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 0; }
#line 2053 "y.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 300 "pmysql.y" /* yacc.c:1646  */
    { executor_new_attr_pr_key(pv_exec); /*emit("ATTR PRIKEY");*/ (yyval.intval) = (yyvsp[-2].intval) + 1; }
#line 2059 "y.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 304 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = parser_int_code + (yyvsp[0].intval); }
#line 2065 "y.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 305 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = parser_string_code + (yyvsp[-1].intval); }
#line 2071 "y.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 308 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 0; }
#line 2077 "y.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 309 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = (yyvsp[-1].intval); }
#line 2083 "y.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 312 "pmysql.y" /* yacc.c:1646  */
    { (yyval.intval) = 0; }
#line 2089 "y.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 313 "pmysql.y" /* yacc.c:1646  */
    { executor_new_reference(pv_exec, (yyvsp[-3].strval), (yyvsp[-1].intval)); /*emit("REFERENCES %s %d", $2, $4);*/ free((yyvsp[-3].strval)); }
#line 2095 "y.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 317 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 2101 "y.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 320 "pmysql.y" /* yacc.c:1646  */
    { /*emit("UPDATE %d %d %d", $2, $4);*/ executor_update(pv_exec); }
#line 2107 "y.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 323 "pmysql.y" /* yacc.c:1646  */
    { executor_create_assignment(pv_exec, (yyvsp[-2].strval)); /*emit("ASSIGN %s", $1);*/ free((yyvsp[-2].strval)); (yyval.intval) = 1; }
#line 2113 "y.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 325 "pmysql.y" /* yacc.c:1646  */
    { executor_create_assignment(pv_exec, (yyvsp[-2].strval)); /*emit("ASSIGN %s.%s", $3);*/ free((yyvsp[-2].strval)); (yyval.intval) = (yyvsp[-4].intval) + 1; }
#line 2119 "y.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 329 "pmysql.y" /* yacc.c:1646  */
    { emit("STMT"); }
#line 2125 "y.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 332 "pmysql.y" /* yacc.c:1646  */
    { emit("USE %s", (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 2131 "y.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 335 "pmysql.y" /* yacc.c:1646  */
    { /*emit("STMT");*/ }
#line 2137 "y.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 338 "pmysql.y" /* yacc.c:1646  */
    { executor_create_index(pv_exec,(yyvsp[-1].intval)); /*emit("CREATEINDEX %d", $7);*/ }
#line 2143 "y.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 341 "pmysql.y" /* yacc.c:1646  */
    { /*emit("TABLE %s", $1);*/ executor_new_name(pv_exec, (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 2149 "y.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 345 "pmysql.y" /* yacc.c:1646  */
    { /*emit("INDEXNAME %s", $1);*/ executor_set_index_name(pv_exec, (yyvsp[0].strval)); free((yyvsp[0].strval)); }
#line 2155 "y.tab.c" /* yacc.c:1646  */
    break;


#line 2159 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 348 "pmysql.y" /* yacc.c:1906  */


void emit(char *s, ...) {
    extern int yylineno;
    va_list ap;
    va_start(ap, s);
    printf("rpn: ");
    vfprintf(stdout, s, ap);
    printf("\n");
}

void yyerror(char *s, ...) {
    extern int yylineno;
    va_list ap;
    va_start(ap, s);

    fprintf(stderr, "%d: error: ", yylineno);
    vfprintf(stderr, s, ap);
    fprintf(stderr, "\n");

}

int parse_string(const char* s){
    yy_scan_string(s);
    return yyparse();
}

void doparse(int ac, char **av){
    extern FILE *yyin;

    if(ac > 1 && !strcmp(av[1], "-d")) {
        /*yydebug = 1; */ac--; av++;
    }

    if(ac > 1 && (yyin = fopen(av[1], "r")) == NULL) {
        perror(av[1]);
        exit(1);
    }

    if(!yyparse())
        printf("SQL parse worked\n");
    else
        printf("SQL parse failed\n");
}
