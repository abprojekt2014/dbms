#include "stack_items.h"
#include "stack_items_not_for_common.h"

using std::ostream;
using std::string;
using std::vector;
using std::map;
using std::cout;
using std::endl;
using std::exception;

ColumnDefinition::ColumnDefinition(string n, int t):name(n), type(t), primary_key(false), references(false), reference_table_name(""){}

ColumnDefinition::~ColumnDefinition(){}

References::References(const char* tn,  vector<string> cn):table_name(tn), column_names(cn){}

References::~References(){}

Name::Name(string v):value(v){
    std::size_t found = v.find(".");
    if (found!=std::string::npos){
        table_name = v.substr(found+1, v.size());
        value = v.substr(0,found);
        //cout << table_name << " " << value << endl;
    }
    else
        ;//resolve table_name using catalog
}

Name::~Name(){}

void Name::check_table_name(std::map<std::string, table_t*>& table_pointers){
    for(auto it = table_pointers.begin(); it != table_pointers.end(); ++it){
        if(it->first == table_name){
            vector<column_t*> columns(it->second->getcatalog().columns());
            for(auto it2 = columns.begin(); it2 != columns.end(); ++it2){
                if((*it2)->name == this->value) {
                    return;
                }
            }
            break;
        }
    }
    throw ParserException("Error: there is no table \"" + table_name + "\" or column \"" + value + "\" in it.");
}

void Name::resolve_table_name(std::map<std::string, table_t*>& table_pointers){
    if(table_name != ""){
        try{
            check_table_name(table_pointers);
            return;
        }
        catch(exception& e){
            throw;
        }
        return;
    }
    for(auto it = table_pointers.begin(); it != table_pointers.end(); ++it){
        vector<column_t*> columns(it->second->getcatalog().columns());
        for(auto it2 = columns.begin(); it2 != columns.end(); ++it2){
            if((*it2)->name == this->value) {
                this->table_name = it->first;
                //cout << "resolved " << it->first << endl;
                return;
            }
        }
    }
    throw ParserException("Error: there is no table with column \"" + value + "\".");
}

/*ostream& operator<<(ostream& os, const Name n){
  os << n.value;
  return os;
  }*/

bool operator==(const Name n1, const Name n2){
    if( (n1.value == n2.value) && (n1.table_name == n2.table_name) )
        return true;
    else
        return false;
}

ostream& operator<<(ostream& os, const Name n){
    os << n.table_name << "." << n.value;
    return os;
}

ostream& operator<<(ostream& os, const FieldName n){
    os << n.tablename << "." << n.name;
    return os;
}

FieldName::FieldName(string tn, string n):tablename(tn), name(n){}

FieldName::~FieldName(){}

Assignment::Assignment(Name n, variant_t v):name(n), value(v){}

Assignment::~Assignment(){}

Selection::Selection(Name n):name(n){}

Selection::~Selection(){}
