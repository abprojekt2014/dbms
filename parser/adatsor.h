#ifndef _ADATSOR_H_
#define _ADATSOR_H_

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <sstream>
#include <iostream>

template<typename T>
std::string serialize(const T & value){
    std::ostringstream oss;
    boost::archive::text_oarchive oa(oss);
    oa << value;
    return oss.str();
}

template<typename T>
T deserialize(const std::string & buffer){
    std::istringstream iss(buffer);
    boost::archive::text_iarchive ia(iss);
    T ret;
    ia >> ret;
    return ret;
}

class AdatSor{
    public:
        AdatSor(){}
        ~AdatSor(){}

        template<class Archive>
            void serialize(Archive & ar, const unsigned int){
                ar & adat;
            }
        std::vector<std::string> adat;
};

#endif // _ADATSOR_H_
