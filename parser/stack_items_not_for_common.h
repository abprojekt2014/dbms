#ifndef STACKITEMS_NOTFORCOMMON_H
#define STACKITEMS_NOTFORCOMMON_H

#include "common.h"
#include <string>
#include <vector>
#include <iostream>

class Assignment{
    public:
        Name name;
        variant_t value;
        Assignment(Name n, variant_t v=0);
        ~Assignment();
};

#endif
