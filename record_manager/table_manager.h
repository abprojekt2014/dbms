#ifndef TABLE_MANAGER_H
#define TABLE_MANAGER_H

#include <list>
#include <string>
#include "table.h"

// Singleton pattern
class tablemanager
{
private:
    // table list
    std::list<table_t*> tables;
    std::list<std::string> tablenames;

    // constructor
    tablemanager();

protected:
    DWORD tableid_curr;

    table_t* system_tables;    // table for tables
    table_t* system_columns;   // table for columns

    bool addtable(table_t* new_table);
    void initialize_system_tables();

public:
    const char* system_tables_name  = "system_tables";
    const char* system_columns_name = "system_columns";

    // destructor
    ~tablemanager();
    // getting the instance
    static tablemanager& getinstance();

    // returns all the existing table's name
    std::list<std::string> gettablenames();

    // creates a table with the given name
    table_t* createtable(const std::string tablename, catalog& c);
    // returns a pointer to the table if it's already registered
    table_t*  gettable(const std::string tablename);
    // opens an existing table if it's not registered into the tablemanager
    table_t* opentable(const std::string tablename);
    // deletes a table from the manager and from the disk also
    bool deletetable(const std::string tablename);
};

#endif // TABLEMANAGER_H
