//
//  Author: Fulop Botond
//  Date: 2014. 02. 26
//

#ifndef _RM_OPTIONS_8K_H_
#define _RM_OPTIONS_8K_H_

#include "dbtypes.h"

// Block size in bytes
const SIZE_T rm_block_size = 8192;
const DWORD rm_block_offset_mask = 0x1FFF;
const DWORD rm_block_offset_bit_nr = 13;
const DWORD rm_block_number_mask = 0xFFFFE000;
const DWORD rm_block_number_max = rm_block_number_mask >> rm_block_offset_bit_nr;

#endif //_RM_OPTIONS_8K_H_
