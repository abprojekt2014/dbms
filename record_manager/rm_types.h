//
//  Author: Fulop Botond
//  Date: 2014. 02. 26
//

#ifndef _RM_OPTIONS_H_
#define _RM_OPTIONS_H_

#include "../include/buildoptions.h"
#include "../include/dbtypes.h"
#include "rm_errors.h"

#ifdef USE_4KB_BLOCK_SIZE
#include "rm_options_4k.h"
#elif defined (USE_8KB_BLOCK_SIZE)
#include "rm_options_8k.h"
#endif

typedef recordid_t block_number_t;
typedef recordid_t block_offset_t;
typedef recordid_t blockid_t;
typedef recordid_t file_offset_t;

const recordid_t invalid_recordid = 0;
const blockid_t invalid_blockid = rm_block_number_max + 1;

struct block_t
{
    blockid_t blockid;
    BYTE buffer[rm_block_size];
};

inline blockid_t rm_getblockid (const recordid_t recordid)
{
    return (recordid >> rm_block_offset_bit_nr);
}

inline block_offset_t rm_getblockoffset(const recordid_t recordid)
{
    return (recordid & rm_block_offset_mask);
}

inline file_offset_t rm_blockoffset(const recordid_t recordid)
{
    return recordid&rm_block_offset_mask;
}

inline file_offset_t rm_recordoffset(const recordid_t recordid)
{
    return recordid;
}

inline recordid_t rm_blockid_to_recordid(const blockid_t blockid)
{
    return blockid << rm_block_offset_bit_nr;
}

#endif //_RM_OPTIONS_H_
