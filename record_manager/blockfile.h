#ifndef _BLOCKFILE_H_
#define _BLOCKFILE_H_

#include <string>
#include "rm_types.h"

//
// It's an abstract class
//
class blockfile
{
public:
    virtual ~blockfile() {};

    // creates a new block file
    virtual rm_error create(const std::string filepath) = 0;
    // opens the file in the filepath
    virtual rm_error open(const std::string filepath) = 0;
    // closes the file
    virtual rm_error close() = 0;
    
    // this function reads the block with the given blockid
    virtual rm_error readblock(const blockid_t blockid, block_t &block) = 0;
    // this function reads the block what contains the record with the recordid
    virtual rm_error readblockofrecord(const recordid_t recordid, block_t &block) = 0;
    
    // writes the given block to the file
    virtual rm_error writeblock(const block_t &block) = 0;
    // creates a new block in the file (appends a new block to the file)
    virtual rm_error newblock(block_t &block) = 0;
    
    // returns the number of blocks in the file
    virtual block_number_t numerofblocks() = 0;
};

#endif //_BLOCKFILE_H_