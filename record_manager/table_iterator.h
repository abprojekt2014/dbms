#ifndef TABLE_ITERATOR_H
#define TABLE_ITERATOR_H

#include "table.h"
#include "table_types.h"
#include "../index_manager/iterator.h"
#include "rm_types.h"
#include "table_internal.hpp"

class table_t;
struct block_t;

class table_iterator: iterator_t
{
protected:
    blockid_t current_block_index;
    row_iterator it;
    block_t block;
    table_t* t;
    DWORD blockcount;
    bool ended;

    friend class table_t;

    void skip_empty_rows();
public:
    table_iterator(table_t* const t);
    virtual ~table_iterator();

    recordid_t first();
    recordid_t next();
    bool end();
    bool read_value(void* value) { return false;}
};

#endif // TABLE_ITERATOR_H
