#ifndef CATALOG_H
#define CATALOG_H

#include <vector>
#include <string>
#include "table.h"
#include "rm_types.h"
#include "binary_catalog.h"
#include "../index_manager/index.h"

class index_t;
struct column_t;
//struct block_t;

class catalog_exception
{
private:
    const std::string _message = {};
public:
    catalog_exception(const std::string message): _message(message) {}
    std::string getmessage() { return _message; }
};

class catalog
{
protected:
    block_t block;          // the binary block where the block is stored
    pbincatalog_t bincat;   // the pointer in the block structure pointing to the start of the file
    std::vector<column_t*> _columns; // a vector with the columns

    // converting the inmemory representation to binary (filling the bincat and block)
    bool tobinary();
    // converting the bincat(block) to inmemory representation of the catalog
    bool readfrombinary();
    // sets an index
    bool setindex_internal(column_t* column, index_t* idx);

    friend class table_t;     // table is a friend, to beeing able to read the block of the catalog
public:
    catalog();                  // default constructor
    catalog(block_t &block);    // constructor, it needs only a block, where the catalog file is stored; this way the catalog is finalized
    catalog(const catalog& cat); // copy constructor
    ~catalog();                 // destructor

    // adds a new column into the catalogfile
    // parameters:
    //   colname - name of the column
    //   coltype - type of the column
    //   length - optional - the length of the column (only for the strings)
    // returns a copy of the generated column if the result is not nullptr
    column_t* addcolumn(std::string colname,
                        BYTE coltype,
                        DWORD binlength);

    bool setprimarykey(column_t* column, index_t* idx);
    bool setforeignkey(column_t* column, std::string ref_table_name, index_t* idx);
    bool setindex(column_t* column, index_t* idx);
    void setautoincrement(column_t* col, QWORD maximum);

    // returns a vector with the contained columns
    std::vector<column_t*>& columns();

    // returns the length of the row
    const SIZE_T rowlength();
};

#endif // CATALOG_H
