#include "table_manager.h"
#include <stdio.h>
#include "serializer.hpp"

using namespace std;

tablemanager::tablemanager(): tableid_curr(0)
{
    initialize_system_tables();
}

tablemanager::~tablemanager()
{
    while (!tables.empty())
    {
        table_t* t = tables.front();
        delete t;
        tables.pop_front();
    }

    system_tables->close();
    delete system_tables;
}

tablemanager& tablemanager::getinstance()
{
    static tablemanager instance;

    return instance;
}

bool tablemanager::addtable(table_t* new_table)
{
    if (nullptr == new_table)
    {
        return false;
    }

    tables.push_back(new_table);

    return true;
}

table_t* tablemanager::gettable(std::string tablename)
{
    for (table_t* t: tables)
    {
        if ( t->name().compare(tablename) == 0 )
        {
            return t;
        }
    }

    return nullptr;
}

table_t* tablemanager::createtable(const std::string tablename, catalog& c)
{
    if (nullptr != gettable(tablename))
    {
        // the table already exists
        return nullptr;
    }

    table_t* tbl = new table_t(tablename, c);
    if (false == addtable(tbl))
    {
        return nullptr;
    }

    // adding a row to the system_tables with the new table's name
    BYTE* binrow = new BYTE[system_tables->size_of_row];
    serializer_t s(system_tables->size_of_row-1);

    s.serialize_int(binrow, 0, tableid_curr++, 4);
    s.serialize_str(binrow, 4, tablename, table_t::max_table_name_length);

    assert(system_tables->insert_row(binrow) != invalid_recordid);

    if (!tbl->create())
    {
        delete[] binrow;
        return nullptr;
    }

    if (!tbl->writecatalog())
    {
        delete[] binrow;
        return nullptr;
    }

    BYTE binrow2[128+8];
    serializer_t s2(128+8);

    for (column_t* c : tbl->columns())
    {
        //system_columns->getcatalog().addcolumn("TableName", ctString, 64);
        //system_columns->getcatalog().addcolumn("ColumnName", ctString, 64);
        //system_columns->getcatalog().addcolumn("ColumnType", ctInteger, 4);
        //system_columns->getcatalog().addcolumn("ColumnLength", ctInteger, 4);

        s2.serialize_str(binrow2, 0, tablename, 64);
        s2.serialize_str(binrow2, 64, c->name, 64);
        s2.serialize_int(binrow2, 128, (int)c->type, 4);
        s2.serialize_int(binrow2, 132, c->length, 4);

        system_columns->insert_row(binrow2);
    }

    tablenames.push_back(tablename);

    delete[] binrow;
    return tbl;
}

table_t* tablemanager::opentable(const std::string tablename)
{
    table_t* t = gettable(tablename);

    if (nullptr != t)
    {
        t->open();
    }

    return t;
}

bool tablemanager::deletetable(const std::string tablename)
{
    std::string tn = tablename + ".tbl";
    table_t* t = gettable(tablename);

    if (nullptr == t)
    {
        // the table does not exists
        return false;
    }

    BYTE* binrow = new BYTE[system_tables->size_of_row];
    serializer_t s(system_tables->size_of_row-1);

    // we delete the table's name from the tablenames list
    this->tablenames.remove(tablename);
    this->tables.remove(t);

    // searching the table name in the system table
    iterator_t* it = system_tables->get_iterator();
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        std::string tname;

        assert(system_tables->read_row(row, binrow));
        tname = s.deserialize_str(binrow, 4, table_t::max_table_name_length);

        if (tname.compare(tablename) == 0)
        {
            // tablename found
            system_tables->delete_row(row);
        }
    }
    delete it;

    delete[] binrow;

    // we delete the given table
    if (remove(tn.c_str()) < 0)
    {
        return false;
    }

    return true;
}

void tablemanager::initialize_system_tables()
{
    // ready to open the table
    system_tables = new table_t(system_tables_name);
    system_columns = new table_t(system_columns_name);

    if(!system_tables->open())
    {
        // if no system_tables table was found we delete the system_colums table also
        remove( (std::string(system_columns_name) + ".tbl").c_str());

        // we will create a new table
        system_tables->create();

        // creating the table rows
        system_tables->getcatalog().addcolumn("TableId", ctInteger, 4);
        system_tables->getcatalog().addcolumn("TableName", ctString, table_t::max_table_name_length);
        system_tables->writecatalog();

        // Adding the system_tables into the system_tables table
        BYTE* binrow = new BYTE[system_tables->size_of_row];
        serializer_t s(system_tables->size_of_row-1);

        s.serialize_int(binrow, 0, tableid_curr++, 4);
        s.serialize_str(binrow, 4, "system_tables", table_t::max_table_name_length);

        assert(system_tables->insert_row(binrow) != invalid_recordid);

        s.serialize_int(binrow, 0, tableid_curr++, 4);
        s.serialize_str(binrow, 4, "system_columns", table_t::max_table_name_length);

        assert(system_tables->insert_row(binrow) != invalid_recordid);

        system_columns->create();
        system_columns->getcatalog().addcolumn("TableName", ctString, 64);
        system_columns->getcatalog().addcolumn("ColumnName", ctString, 64);
        system_columns->getcatalog().addcolumn("ColumnType", ctInteger, 4);
        system_columns->getcatalog().addcolumn("ColumnLength", ctInteger, 4);
        system_columns->writecatalog();

        delete[] binrow;
    }
    else
    {
        assert(system_columns->open());
    }

    BYTE* binrow = new BYTE[system_tables->size_of_row];
    serializer_t s(system_tables->size_of_row-1);

    // the system_tables is opened
    iterator_t* it = system_tables->get_iterator();
    for (recordid_t row = it->first(); !it->end(); row = it->next())
    {
        DWORD tid;      // TableId
        string tname;   // TableName

        assert(system_tables->read_row(row, binrow));
        tid = s.deserialize_int(binrow, 0, 4);
        tname = s.deserialize_str(binrow, 4, table_t::max_table_name_length);

        if (tid >= tableid_curr)
        {
            tableid_curr = tid + 1; // incrementing the TableId current value
        }

        // importing tables from system_tables
        table_t* nt = new table_t(tname);
        tablenames.push_back(tname);
        tables.push_back(nt);
    }
    delete it;
    delete[] binrow;
}

list<string> tablemanager::gettablenames()
{
    return tablenames;
}
