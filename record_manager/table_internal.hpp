#ifndef _TABLE_INTERNAL_HPP_
#define _TABLE_INTERNAL_HPP_

#include "rm_types.h"
#include <cassert>
#include <string.h>

class row_iterator
{
    SIZE_T row_size = 0;
    SIZE_T curr = (SIZE_T)-1LL;
    SIZE_T row_count = 0;
    PBYTE RowBuffer = nullptr;

public:
    void initialize(PBYTE buff, SIZE_T rsize, SIZE_T rowcount)
    {
        assert (rsize >= 1);
        assert (buff != nullptr);
        assert (rowcount != 0);

        row_size = rsize;
        RowBuffer = buff;
        this->row_count = rowcount;
        curr = 0;
    }

    void Delete()
    {
        this->RowBuffer[0] = 0;
    }

    bool IsEmpty()
    {
        unsigned char c = this->RowBuffer[0];
        return (c == '\0');
    }

    void Update(const PBYTE Buffer)
    {
        assert (Buffer[0] == 1);

        this->RowBuffer[0] = 1;
        memcpy(RowBuffer+1, Buffer, row_size-1);    // we do not want to overwrite the first byte what was currently set; the length of Buffer is only table.rowlength() long without the +1 byte
    }

    bool GotoNext()
    {
        assert(row_count != 0);
        if (curr >= row_count)
        {
            return false;
        }

        if (curr != 0)
        {
            this->RowBuffer += row_size;
        }

        curr ++;

        return true;
    }

    int Index()
    {
        return curr-1;
    }

    BYTE operator[](const SIZE_T idx)
    {
        assert (idx < row_size);
        return RowBuffer[idx+1];
    }
};

#endif // _TABLE_INTERNAL_HPP_
