//
//  Author: Fulop Botond
//  Date: 2014. 02. 26
//

#ifndef _RM_OPTIONS_4K_H_
#define _RM_OPTIONS_4K_H_

#include "../include/dbtypes.h"

// Block size in bytes
const SIZE_T rm_block_size = 4096;
const DWORD rm_block_offset_mask = 0xFFF;
const DWORD rm_block_offset_bit_nr = 12;
const DWORD rm_block_number_mask = 0xFFFFF000;
const DWORD rm_block_number_max = rm_block_number_mask >> rm_block_offset_bit_nr;

///TEST MODE
/*const SIZE_T rm_block_size = 0x100;
const DWORD rm_block_offset_mask = 0xFF;
const DWORD rm_block_offset_bit_nr = 8;
const DWORD rm_block_number_mask = 0xFFFFFF00;
const DWORD rm_block_number_max = rm_block_number_mask >> rm_block_offset_bit_nr;*/

#endif //_RM_OPTIONS_4K_H_
