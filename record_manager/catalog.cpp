#include "catalog.h"
#include "../index_manager/index_manager.h"
#include <string.h>
#include <cassert>

catalog::catalog()
{
    memset(&this->block, 0, sizeof(block_t));
    _columns.clear();
}

catalog::catalog(block_t &block)
{
    this->block = block;
    readfrombinary();
}

catalog::catalog(const catalog& cat)
{
    this->block = cat.block;
    this->bincat = cat.bincat;

    for (column_t* c: _columns)
    {
        if (c->attributes.indexed)
        {
            if (c->attributes.primary_key)
            {
                assert(c->primary_key.primary_key_index != nullptr);
                c->primary_key.primary_key_index->close();
                c->primary_key.primary_key_index = nullptr;
            }
            else if (c->attributes.foreign_key)
            {
                assert(c->foreign_key.foreign_key_index != nullptr);
                c->foreign_key.foreign_key_index->close();
                c->foreign_key.foreign_key_index = nullptr;
            }
        }
    }

    _columns.clear();
    this->_columns = cat._columns;
}

catalog::~catalog()
{
    _columns.clear();
}

bool catalog::readfrombinary()
{
    DWORD offset_so_far = 0;

    // mapping the block to the bincat variable
    bincat = (pbincatalog_t)&block.buffer[0];

    if (0 != memcmp(bincat->magic, bincat_magic, sizeof(bincat_magic)))
    {
        // wrong magic
        goto failed;
    }

    // iterating the columns
    for (int i=0; i<bincat->column_nr; i++)
    {
        bincolumn_t* bc;
        column_t* c = new column_t();

        bc = &bincat->columns[i];

        c->column_index = i;
        c->length = bc->size;
        c->offset = offset_so_far;
        c->type = (columntype)bc->type;

        c->name = std::string((const char*)(bc->name));

        // attributes
        c->attributes.auto_increment = bc->attributes.auto_increment;
        c->attributes.primary_key = bc->attributes.primary_key;
        c->attributes.indexed = bc->attributes.indexed;
        c->attributes.foreign_key = bc->attributes.foreign_key;

        if (c->attributes.indexed)
        {
            if (c->attributes.primary_key)
            {
                c->primary_key.primary_key_index = indexmanager::getinstance().openindex((const char*)bc->primary_key.file_name);

                if (nullptr == c->primary_key.primary_key_index)
                {
                    throw new catalog_exception("No index found: "+std::string((const char*)bc->primary_key.file_name));
                }
            }
            else if (c->attributes.foreign_key)
            {
                c->foreign_key.foreign_key_index = indexmanager::getinstance().openindex((const char*)bc->foreign_key.file_name);

                if (nullptr == c->foreign_key.foreign_key_index)
                {
                    throw new catalog_exception("No index found: "+std::string((const char*)bc->foreign_key.file_name));
                }

                c->foreign_key.ref_table_name = new std::string((const char*)bc->foreign_key.ref_table_name);
            }
            else
            {
                c->index = indexmanager::getinstance().openindex((const char*)bc->index.file_name);

                if (nullptr == c->index)
                {
                    throw new catalog_exception("No index found: "+std::string((const char*)bc->index.file_name));
                }
            }

        }

        offset_so_far += c->length;
        _columns.push_back(c);
    }

    return true;
failed:
    bincat = nullptr;
    _columns.clear();
    return false;
}

bool catalog::tobinary()
{
    PBYTE currpos;

    memcpy(block.buffer, bincat_magic, sizeof(bincat_magic));
    bincat = (pbincatalog_t)block.buffer;

    currpos = block.buffer + sizeof(bincatalog_t);

    bincat->column_nr = _columns.size();

    // writing the columns into the file
    for(column_t* c: _columns)
    {
        bincolumn_t bc;

        memset(&bc, 0, sizeof(bc));

        memcpy(bc.name, c->name.c_str(), c->name.length());
        bc.type = (BYTE)c->type;
        bc.size = c->length;

        // attributes
        bc.attributes.auto_increment = c->attributes.auto_increment;
        bc.attributes.primary_key = c->attributes.primary_key;
        bc.attributes.indexed = c->attributes.indexed;
        bc.attributes.foreign_key = c->attributes.foreign_key;

        bc.auto_increment_current = c->auto_increment_current;

        if (bc.attributes.indexed)
        {
            if (bc.attributes.foreign_key)
            {
                std::string idx_name = c->primary_key.primary_key_index->getname();
                memcpy(bc.primary_key.file_name, idx_name.c_str(), idx_name.length());
            }
            else if (bc.attributes.foreign_key) {
                std::string idx_name = c->foreign_key.foreign_key_index->getname();
                std::string* ref_tbl = c->foreign_key.ref_table_name;
                memcpy(bc.foreign_key.file_name, idx_name.c_str(), idx_name.length());
                memcpy(bc.foreign_key.ref_table_name, ref_tbl->c_str(), ref_tbl->length());
            }
            else
            {
                std::string idx_name = c->index->getname();
                memcpy(bc.index.file_name, idx_name.c_str(), idx_name.length());
            }
        }

        memcpy(currpos, &bc, sizeof(bc));
        currpos += sizeof(bincolumn_t);
    }

    // we have the final size :) we should update the catalog with it

    return true;
}

const SIZE_T catalog::rowlength()
{
    SIZE_T len = 0;

    for (column_t* c : this->_columns)
    {
        len += c->length;
    }

    return len;
}

column_t* catalog::addcolumn(std::string colname, BYTE coltype, DWORD binlength)
{
    column_t* res = nullptr;

    // the column name is too big
    assert(colname.length() < 32);

    // column name too long
    if (colname.length() >= 32)
    {
        return nullptr;
    }

    res = new column_t;

    res->column_index = _columns.size();
    res->name = colname;
    res->length = binlength;
    res->offset = rowlength();
    res->type = (columntype)coltype;
    res->auto_increment_current = 0;

    res->attributes.auto_increment = false;
    res->attributes.primary_key = false;
    res->attributes.indexed = false;
    res->attributes.foreign_key = false;

    res->primary_key.primary_key_index = nullptr;
    res->foreign_key.foreign_key_index = nullptr;

    _columns.push_back(res);

    return res;
}

bool catalog::setindex_internal(column_t* column, index_t* idx)
{
    // the column is null
    assert(column != nullptr);
    assert(idx != nullptr);

    if (column->attributes.indexed)
    {
        // already indexed (foreign key or primary key)
        return false;
    }

    column->attributes.indexed = true;

    return true;
}

bool catalog::setprimarykey(column_t* column, index_t* idx)
{
    // the column is null
    assert(column != nullptr);
    assert(idx != nullptr);

    if (!setindex_internal(column, idx))
    {
        return false;
    }

    column->primary_key.primary_key_index = idx;
    column->attributes.primary_key = true;
    column->attributes.foreign_key = false;
    return true;
}

bool catalog::setforeignkey(column_t* column, std::string ref_table_name, index_t* idx)
{
    // the column is null
    assert(column != nullptr);
    assert(idx != nullptr);

    if (!setindex_internal(column, idx))
    {
        return false;
    }

    column->foreign_key.foreign_key_index = idx;
    column->foreign_key.ref_table_name = new std::string(ref_table_name);
    column->attributes.foreign_key = true;
    column->attributes.primary_key = false;
    return true;
}

bool catalog::setindex(column_t* column, index_t* idx)
{
    // the column is null
    assert(column != nullptr);
    assert(idx != nullptr);

    if (!setindex_internal(column, idx))
    {
        return false;
    }

    column->index = idx;
    column->attributes.foreign_key = false;
    column->attributes.primary_key = false;
    return true;
}


void catalog::setautoincrement(column_t* col, QWORD maximum)
{
    // the column is null
    assert(col != nullptr);

    if (col->attributes.auto_increment)
    {
        return;
    }

    col->attributes.auto_increment = true;
    col->auto_increment_current = maximum;
}

std::vector<column_t*>& catalog::columns()
{
    return _columns;
}
