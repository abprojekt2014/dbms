#ifndef _BINARY_CATALOG_H_
#define _BINARY_CATALOG_H_

#include "../include/dbtypes.h"

const BYTE bincat_magic[8] = {0x44, 0x42, 0x43, 0x41, 0x54, 0x31, 0x2E, 0x30}; //'DBCAT1.0'

// the binary representation of a column entry in the catalog file
struct bincolumn_t
{
    BYTE name[33]; // name of the column
    BYTE type;  // type of the column represented as a byte
    BYTE size;  // column size in bytes

    struct
    {
        bool auto_increment:1;
        bool primary_key:1;
        bool indexed:1;
        bool foreign_key:1;
    } attributes;

    QWORD auto_increment_current;

    union
    {
        struct
        {
            BYTE file_name[65];    // index file's name
        } primary_key;

        struct
        {
            BYTE  file_name[65];   // index file's name
            BYTE  ref_table_name[65];   // referenced table's name
        } foreign_key;

        struct
        {
            BYTE  file_name[65];   // index file's name
        } index;
    };
};

typedef bincolumn_t* pbincolumn_t;

// the header structure of the catalog
struct bincatalog_t
{
    BYTE magic[8]; // it must be the value of bincat_magic

    // number of the columns in the catalog
    BYTE  column_nr;
    // the offset from there the column entries are starting
    bincolumn_t columns[];
};

typedef bincatalog_t* pbincatalog_t;

#endif //_BINARY_CATALOG_H_
