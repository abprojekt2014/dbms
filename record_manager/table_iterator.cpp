#include "table_iterator.h"
#include <cmath>

table_iterator::table_iterator(table_t* table_for_iterator)
{
    this->t = table_for_iterator;
}

void table_iterator::skip_empty_rows()
{
    bool found = false;

next:
    while (it.GotoNext())
    {
        if (!it.IsEmpty())
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        // we have a non empty block
        return;
    }

    //
    // reading the next block
    //

    current_block_index += 1;
    if (current_block_index > blockcount)
    {
        // we are at the end of the table
        ended = true;
        return;
    }

    if (!rm_success(t->file->readblock(current_block_index, block)))
    {
        // failed to read the block
        ended = true;
        return;
    }

    it.initialize(block.buffer, t->size_of_row, t->max_row_count_in_a_block);
    goto next;
}

table_iterator::~table_iterator()
{
}

recordid_t table_iterator::first()
{
    current_block_index = t->free_space_block_number + 1;
    blockcount = t->file->numerofblocks();
    ended = false;

    t->file->readblock(current_block_index, block);
    it.initialize(block.buffer, t->size_of_row, t->max_row_count_in_a_block);

    return next();
}

recordid_t table_iterator::next()
{
    skip_empty_rows();

    if (ended)
    {
        return invalid_recordid;
    }

    return rm_blockid_to_recordid(block.blockid) + it.Index()*t->size_of_row;
}

bool table_iterator::end()
{
    return ended;
}
