#ifndef _RM_ERRORS_H_
#define _RM_ERRORS_H_

typedef enum
{
    // succes values
    errNone=0,
    
    // warning values
    errUnknownWarning=1000,
    
    // error values
    errUnknownError     =2000,
    errFileNotFound     =2001,
    errPathNotFound     =2002,
    errAccessDenied     =2003,
    errInvalidHandle    =2004,
    errNotEnoughMemory  =2005,
    errReadFault        =2006,
    errWriteFault       =2007,
    errSharingViolation =2008,
    errEof              =2009,
    errDiskFull         =2010,
    errNotSupported     =2011,
    errOpenFailed       =2012,
    errCreateFailed     =2013,
    errInvalidName      =2014,
    errNegativeSeek     =2015,
    errSeekOnDevice     =2016,
    errAlreadyExists    =2017,
    errFileTooLong      =2018,
    errFileNotOpened    =2019,
    errInvalidSeek      =2020,
    errNotBlockFile     =2021,        // the length of the file is not rounded to block size
    errBlockNumberTooBig=2022,        // the maximum block number was already reached
    errBlockNotInFile   =2023,        // the block is not in the file
}rm_error;

inline bool rm_success(rm_error error)
{
    return error < errUnknownError;
}

#endif