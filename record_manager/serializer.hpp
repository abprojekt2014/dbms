#ifndef _SERIALIZER_HPP_
#define _SERIALIZER_HPP_

#include <string>
#include <cassert>
#include <algorithm>
#include <stdint.h>
#include <cstdlib>
#include "columntypes.h"

#ifdef min
#undef min      // comes from minwindef.h
#endif

class serializer_t
{
protected:
    int rowsize;

public:
    static const int default_bool_size = 1;
    static const int default_int_size = 4;

    serializer_t(int rowsize): rowsize(rowsize)
    {
        assert(rowsize > 0);
    }

    //
    // Deserialization
    //
    bool deserialize_bool       (void* row, int offset, int size_of_column=default_bool_size)
    {
        char* pchar = (char*)row;

        assert(row != nullptr);
        assert(offset >= 0);
        assert(size_of_column > 0);
        assert(rowsize >= offset + size_of_column);

        for (int i = 0; i < size_of_column; i++)
        {
            if (pchar[i + offset] != 0)
            {
                return true;
            }
        }

        return false;
    }

    int64_t deserialize_int        (void* row, int offset, int size_of_column=default_int_size)
    {
        char* pchar = (char*)row;

        assert(row != nullptr);
        assert(offset >= 0);
        assert(size_of_column > 0);
        assert(rowsize >= offset + size_of_column);

        switch (size_of_column)
        {
        case 1:
            // a single byte
            return (int64_t) pchar[offset];
        case 2:
            return (int64_t)*(int16_t*)(pchar + offset);
        case 4:
            return (int64_t)*(int32_t*)(pchar + offset);
        case 8:
            return (int64_t)*(int64_t*)(pchar + offset);
        }

        assert(false);  // size_of_column must be one of the following values: 1, 2 4
        return 0;
    }

    std::string deserialize_str (void* row, int offset, int size_of_column)
    {
        char* pchar = (char*)row;
        char* cstr = new char[size_of_column + 1];
        std::string res;

        if (nullptr == cstr)
            goto failed;

        assert(row != nullptr);
        assert(offset >= 0);
        //assert(offset < size_of_column);
        assert(size_of_column > 0);
        assert(rowsize >= offset + size_of_column);

        memcpy(cstr, pchar + offset, size_of_column); // copying the c string over into the temporary buffer
        cstr[size_of_column] = '\0';  // ending the string with 0 for the security

        res = std::string(cstr);        // converting the c string to a c++ string

        delete []cstr;                  // deleting the allocated buffer

        return res;
    failed:
        return "";
    }

    //
    // Serialization
    //
    void serialize_bool         (void* row, int offset, bool value, int size_of_column=default_bool_size)
    {
        char* pchar = (char*)row;

        assert(row != nullptr);
        assert(offset >= 0);
        assert(size_of_column > 0);
        assert(rowsize >= offset + size_of_column);

        pchar[offset] = value?1:0;
    }

    void serialize_int          (void* row, int offset, long value, int size_of_column=default_int_size)
    {
        char* pchar = (char*)row;

        assert(row != nullptr);
        assert(offset >= 0);
        assert(size_of_column > 0);
        assert(rowsize >= offset + size_of_column);

        switch (size_of_column)
        {
        case 1:
            // a single byte
            assert((unsigned long)value <= 0xFF);       // too big for a 1 byte int
            pchar[offset] = (char)value;
            return;
        case 2:
            assert((unsigned long)value <= 0xFFFF);     // too big for a 2 byte int
            *(int16_t*)(pchar + offset) = (int16_t)value;
            return;
        case 4:
            assert((unsigned long)value <= 0xFFFFFFFF); // too long for a 4 byte int
            *(int32_t*)(pchar + offset) = (int32_t)value;
            return;
        case 8:
            assert((unsigned long)value <= 0xFFFFFFFF); // too long for a 4 byte int
            *(int64_t*)(pchar + offset) = (int64_t)value;
            return;
        }

        assert(false);  // size_of_column must be one of the following values: 1, 2 4
    }

    void serialize_str(void* row, int offset, std::string value, int size_of_column)
    {
        char* pchar = (char*)row;
        const int strsize = std::min(size_of_column, (int)value.length()) + 1;
        char* cstr = (char*)malloc(size_of_column);

        if (nullptr == cstr)
            return;

        assert(row != nullptr);
        assert(offset >= 0);
        //assert(offset < size_of_column);
        assert(size_of_column > 1);     // the string must be at least 1 character long
        assert(rowsize >= offset + size_of_column);

        memcpy(pchar + offset, value.c_str(), strsize); // copying the c string over into the temporary buffer
        cstr[size_of_column-1] = '\0';  // ending the string with 0 for the security

        free(cstr);                  // deleting the allocated buffer
    }

    bool values_lesser_than(void* value1, void* value2, columntype type, int size_of_columns)
    {
        switch (type)
        {
            case ctInteger:
                {
                    int64_t a = deserialize_int(value1, 0, size_of_columns);
                    int64_t b = deserialize_int(value2, 0, size_of_columns);

                    return a < b;
                }
            case ctBoolean:
                {
                    bool a = deserialize_bool(value1, 0, size_of_columns);
                    bool b = deserialize_bool(value2, 0, size_of_columns);

                    return a != b;
                }
            case ctString:
                {
                    std::string a = deserialize_str(value1, 0, size_of_columns);
                    std::string b = deserialize_str(value2, 0, size_of_columns);

                    return a.compare(b) < 0;
                }
            default:
                assert(false);
        }

        return false;
    }

    bool values_lesser_or_equal(void* value1, void* value2, columntype type, int size_of_columns)
    {
        switch (type)
        {
            case ctInteger:
                {
                    int64_t a = deserialize_int(value1, 0, size_of_columns);
                    int64_t b = deserialize_int(value2, 0, size_of_columns);

                    return a <= b;
                }
            case ctBoolean:
                {
                    bool a = deserialize_bool(value1, 0, size_of_columns);
                    bool b = deserialize_bool(value2, 0, size_of_columns);

                    return a == b;
                }
            case ctString:
                {
                    std::string a = deserialize_str(value1, 0, size_of_columns);
                    std::string b = deserialize_str(value2, 0, size_of_columns);

                    return a.compare(b) <= 0;
                }
            default:
                assert(false);
        }

        return false;
    }

    bool values_equal(void* value1, void* value2, columntype type, int size_of_columns)
    {
        switch (type)
        {
            case ctInteger:
                {
                    int64_t a = deserialize_int(value1, 0, size_of_columns);
                    int64_t b = deserialize_int(value2, 0, size_of_columns);

                    return a == b;
                }
            case ctBoolean:
                {
                    bool a = deserialize_bool(value1, 0, size_of_columns);
                    bool b = deserialize_bool(value2, 0, size_of_columns);

                    return a == b;
                }
            case ctString:
                {
                    std::string a = deserialize_str(value1, 0, size_of_columns);
                    std::string b = deserialize_str(value2, 0, size_of_columns);

                    return a.compare(b) == 0;
                }
            default:
                assert(false);
        }

        return false;
    }

    bool values_bigger_than(void* value1, void* value2, columntype type, int size_of_columns)
    {
        return !values_lesser_or_equal(value1, value2, type, size_of_columns);
    }

    bool values_bigger_or_equal(void* value1, void* value2, columntype type, int size_of_columns)
    {
        return !values_lesser_than(value1, value2, type, size_of_columns);
    }
};

#endif//_SERIALIZER_HPP_
