#ifndef _TABLE_H_
#define _TABLE_H_

#include <string>
#include <vector>
#include "catalog.h"
#include "blockfile.h"
#include "table_types.h"
#include "../include/dbtypes.h"
#include "table_iterator.h"
#include "../index_manager/iterator.h"

class catalog;

class table_exception
{
private:
    const std::string _message = {};
public:
    table_exception(const std::string message):_message(message) {}
    std::string getmessage() {return _message;}
};

class table_t
{
protected:
    std::string tablename;
    catalog* cat;
    blockfile* file;
    bool opened;
    block_t cache;  // holds the latest read block

    static const int free_space_block_number = 4;

    block_t free_space[free_space_block_number];  // for every block we have a bit
    DWORD max_row_count_in_a_block = 0;
    DWORD size_of_row = 0;

    void set_block_as_full(blockid_t id);
    void set_block_as_not_full(blockid_t id);

    friend class tablemanager;
    friend class table_iterator;

	recordid_t firstempty();
public:
    const static unsigned max_table_name_length = 64;

    table_t(const std::string &tablename);
    // this table will be initialized
    table_t(const std::string &tablename, catalog& cat);
    // copy constructor of a table
    table_t(const table_t& copy_from);
    // destructor
    ~table_t();

    // creates a new table file; if already exists -> returns false
    bool create();
    // opens an existing database file
    bool open();
    // closes an opened database
    bool close();

    // returns the catalog of the table
    catalog& getcatalog();
    // writes the catalog into the dtabase file
    bool writecatalog();

    // returns the name of the table
    std::string name() const;
    // returns the columns of the table
    std::vector<column_t*>& columns();

    // returns the table's column
    column_t* operator[](std::string columnname);

    column_t* column(std::string columnname);

    // returns the first row's id
    recordid_t first_row_offs();

    // inserts a row into the table
    recordid_t insert_row(PBYTE row);
    // deletes the given row
    bool delete_row(recordid_t row);
    // updates the given row
    bool update_row(recordid_t recordid, PBYTE row);
    // reads the specified row into the givven row buffer
    bool read_row(recordid_t recordid, PBYTE row);

    iterator_t* get_iterator();
};

#endif // _TABLE_H_
