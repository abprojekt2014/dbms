#ifndef _COLUMN_TYPES_H_
#define _COLUMN_TYPES_H_

// represents the type of the column in a table
enum columntype
{
    ctInteger,          // INTEGER
    //ctUnsignedInteger,  // UNSIGNED INTEGER
    ctBoolean,          // BOOLEAN
    ctString,           // STRING
    ctUnknown,
};


#endif //_COLUMN_TYPES_H_