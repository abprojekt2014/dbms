#include "../include/dbtypes.h"
#include "../index_manager/index.h"
#include "columntypes.h"

#ifndef _TABLE_TYPES_H_
#define _TABLE_TYPES_H_

class index_t;

// the in memory structure of a column in a table
struct column_t
{
    int             column_index;   // index of the column
    std::string     name;           // name of column
    columntype      type;           // the type of the column
    DWORD           length;         // length of the record
    DWORD           offset;         // offset where the column's value is starting

    struct
    {
        bool        auto_increment:1;
        bool        primary_key:1;
        bool        indexed:1;
        bool        foreign_key:1;
    } attributes;

    QWORD           auto_increment_current; // the current position of the column if it's auto incremented

    union
    {
        struct
        {
            index_t* primary_key_index;
        } primary_key;

        struct
        {
            index_t* foreign_key_index; // the foreign key is another column what is primary key in another table
            std::string* ref_table_name;
        } foreign_key;

        index_t* index;
    };

    column_t():
        column_index(0),
        name(),
        type(columntype::ctUnknown),
        length(0),
        offset(0),
        auto_increment_current(0),
        index(nullptr)
    {}
};

typedef column_t* pcolumn_t;

#endif //_TABLE_TYPES_H_
