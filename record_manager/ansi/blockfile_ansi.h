#ifndef _BLOCKFILE_ANSI_H_
#define _BLOCKFILE_ANSI_H_

#include <cstdio>
#include "../blockfile.h"

class blockfile_ansi: public blockfile
{
protected:
    FILE* file;
    DWORD fileSize;
    DWORD dwRecordCount;
//    std::recursive_mutex lock;
public:
    blockfile_ansi();
    blockfile_ansi(const blockfile_ansi& other);
    ~blockfile_ansi();

    // creating a block file
    rm_error create(const std::string filepath);
    // opening a block file
    rm_error open(const std::string filepath);
    // closing the block file
    rm_error close();

    rm_error readblock(const blockid_t blockid, block_t &block);
    rm_error readblockofrecord(const recordid_t recordid, block_t &block);
    rm_error writeblock(const block_t &block);
    rm_error newblock(block_t &block);

    block_number_t numerofblocks();
};

#endif //_BLOCKFILE_ANSI_H_
