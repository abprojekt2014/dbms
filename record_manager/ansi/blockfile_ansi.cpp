#include <cassert>
#include <cstdio>
#include "blockfile_ansi.h"
#include <string.h>

#ifdef __APPLE__
#include <sys/errno.h>
#endif

rm_error getansierror(int err)
{
    switch (err)
    {
        case EACCES: return errAccessDenied;
        case ENOENT: return errFileNotFound;
        /// TODO: more ansi errors
    };

    return errUnknownError;
}

blockfile_ansi::blockfile_ansi()
    :file(nullptr),
    fileSize(0),
    dwRecordCount(0)
{
}

blockfile_ansi::~blockfile_ansi()
{
    if (nullptr != file)
    {
        fclose(file);
        file = nullptr;
        fileSize = 0;
    }
}

blockfile_ansi::blockfile_ansi(const blockfile_ansi& other)
{
    this->file = other.file;
    this->fileSize = other.fileSize;
}

// creating a block file
rm_error blockfile_ansi::create(const std::string filepath)
{
    FILE* pFile = nullptr;
    rm_error err = errNone;

    err = close();
    if (!rm_success(err))
    {
        return err;
    }

    pFile = fopen(filepath.c_str(), "w+b");
    if (pFile == nullptr)
    {
        /// TODO: get error
        return errCreateFailed;
    }
    file = pFile;
    fileSize = 0;

    return errNone;
}

// opening a block file
rm_error blockfile_ansi::open(const std::string filepath)
{
    int res;
    FILE* pFile = nullptr;
    rm_error err = errNone;

    err = close();
    if (!rm_success(err))
    {
        return err;
    }

    pFile = fopen(filepath.c_str(), "r+b");
    if (pFile == nullptr)
    {
        return errOpenFailed;
    }
    file = pFile;

    res = fseek(file, 0, SEEK_END);
    if (0 != res)
    {
        close();
        return getansierror(res);
    }
    fileSize = ftell(file);

    return errNone;
}

// closing the block file
rm_error blockfile_ansi::close()
{
    if (nullptr != file)
    {
        fclose(file);
        file = nullptr;
    }

    return errNone;
}

rm_error blockfile_ansi::readblock(const blockid_t blockid, block_t &block)
{
    int res;
    file_offset_t offs = 0;

    // the file was not opened
    assert(file != nullptr);

    offs = (blockid << rm_block_offset_bit_nr);
    res = fseek(file, offs, SEEK_SET);
    if (0 != res)
    {
        return getansierror(res);
    }

    res = fread(
            &block.buffer[0],
            sizeof(block.buffer),
            1,
            file);
    if (1 != res)
    {
        return errReadFault;
    }

    block.blockid = blockid;

    return errNone;
}

rm_error blockfile_ansi::readblockofrecord(const recordid_t recordid, block_t &block)
{
    // the file was not opened
    assert(file != nullptr);

    return readblock(rm_getblockid(recordid), block);
}

rm_error blockfile_ansi::writeblock(const block_t &block)
{
    int res;
    file_offset_t offs = 0;

    // the file was not opened
    assert(file != nullptr);

    offs = (block.blockid << rm_block_offset_bit_nr);

    res = fseek(file, offs, SEEK_SET);
    if (0 != res)
    {
        return getansierror(res);
    }

    res = fwrite(
        block.buffer,
        sizeof(block.buffer),
        1,
        file);
    if (1 != res)
    {
        return errWriteFault;
    }

    return errNone;
}

rm_error blockfile_ansi::newblock(block_t &block)
{
    int res;
    file_offset_t offs = 0;

    // the file was not opened
    assert(file != nullptr);

    offs = this->numerofblocks();
    offs <<= rm_block_offset_bit_nr;

    res = fseek(file, offs, SEEK_SET);
    if (0 != res)
    {
        return getansierror(res);
    }

    // zeroing the new block's buffer
    memset(block.buffer, 0, rm_block_size);

    res = fwrite(
        &block.buffer[0],
        sizeof(block.buffer),
        1,
        file);
    if (1 != res)
    {
        return errWriteFault;
    }
    block.blockid = (fileSize >> rm_block_offset_bit_nr);

    fileSize += rm_block_size;

    return errNone;
}

block_number_t blockfile_ansi::numerofblocks()
{
    DWORD res = fileSize / rm_block_size;

    return res;
}
