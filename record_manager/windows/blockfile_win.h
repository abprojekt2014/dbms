#ifndef _BLOCKFILE_WIN_H_
#define _BLOCKFILE_WIN_H_

#if defined _WIN32 || defined _WIN64

// needs minimum windows XP
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0501

#include <windows.h>
#include "../blockfile.h"
//#include <mutex>

class blockfile_win: public blockfile
{
protected:
    HANDLE hFile;
    LARGE_INTEGER lFileSize;
    DWORD  dwRecordCount;
//    std::recursive_mutex lock;
public:
    blockfile_win();
    virtual ~blockfile_win();
    
    rm_error create(const std::string filepath);
    // opening a block file
    rm_error open(const std::string filepath);
    // closing the block file
    rm_error close();
    
    rm_error readblock(const blockid_t blockid, block_t &block);
    rm_error readblockofrecord(const recordid_t recordid, block_t &block);
    
    rm_error writeblock(const block_t &block);
    rm_error newblock(block_t &block);
    
    block_number_t numerofblocks();
};

#endif

#endif //_BLOCKFILE_WIN_H_