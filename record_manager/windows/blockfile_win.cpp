#if defined _WIN32 || defined _WIN64
#include "blockfile_win.h"
#include <WinBase.h>
#include <cassert>

rm_error getwinerror()
{
    DWORD error = GetLastError();
    
    switch (error)
    {
        case ERROR_SUCCESS:             return errNone;
        case ERROR_FILE_NOT_FOUND:      return errFileNotFound;
        case ERROR_PATH_NOT_FOUND:      return errPathNotFound;
        case ERROR_ACCESS_DENIED:       return errAccessDenied;
        case ERROR_INVALID_HANDLE:      return errInvalidHandle;
        case ERROR_NOT_ENOUGH_MEMORY:   return errNotEnoughMemory;
        case ERROR_READ_FAULT:          return errReadFault;
        case ERROR_SHARING_VIOLATION:   return errSharingViolation;
        case ERROR_HANDLE_EOF:          return errEof;
        case ERROR_HANDLE_DISK_FULL:    return errDiskFull;
        case ERROR_NOT_SUPPORTED:       return errNotSupported;
        case ERROR_DISK_FULL:           return errDiskFull;
        case ERROR_OPEN_FAILED:         return errOpenFailed;
        case ERROR_INVALID_NAME:        return errInvalidName;
        case ERROR_NEGATIVE_SEEK:       return errNegativeSeek;
        case ERROR_SEEK_ON_DEVICE:      return errSeekOnDevice;
        case ERROR_ALREADY_EXISTS:      return errAlreadyExists;
        
        
        default:                        return errUnknownError;
    }
}

blockfile_win::blockfile_win()
{
    hFile = INVALID_HANDLE_VALUE;
    lFileSize.QuadPart = -1;
}

blockfile_win::~blockfile_win()
{
    close();
}

rm_error blockfile_win::create(const std::string filepath)
{
    HANDLE file;
    rm_error err = errNone;
    
    err = close();
    if (!rm_success(err))
    {
        return err;
    }
    
//    std::lock_guard<std::mutex> lk(lock);
    lFileSize.QuadPart = 0;
    
    // opening the file
    file = CreateFileA(filepath.c_str(),
                GENERIC_READ|GENERIC_WRITE,        // we need all access to the file
                FILE_SHARE_READ,    // we give only access for reading
                0,
                CREATE_ALWAYS,
                // we dont want to use the Cache Manager, so we disable its features
                FILE_ATTRIBUTE_NORMAL,// | FILE_FLAG_NO_BUFFERING,
                0);
    // error during file open
    if (INVALID_HANDLE_VALUE == file)
    {
        hFile = INVALID_HANDLE_VALUE;
        
        return getwinerror();
    }
    // file opened succesfully
    hFile = file;
    lFileSize.QuadPart = 0;
    
    return errNone;
}

rm_error blockfile_win::open(const std::string filepath)
{
    HANDLE file;
    rm_error err = errNone;
    
    err = close();
    if (!rm_success(err))
    {
        return err;
    }
    
//    std::lock_guard<std::mutex> lk(lock);
    lFileSize.QuadPart = 0;
    
    // opening the file
    file = CreateFileA(filepath.c_str(),
                GENERIC_READ|GENERIC_WRITE,        // we need all access to the file
                FILE_SHARE_READ,    // we give only access for reading
                0,
                OPEN_ALWAYS,
                // we dont want to use the Cache Manager, so we disable its features
                FILE_ATTRIBUTE_NORMAL,// | FILE_FLAG_NO_BUFFERING,
                0);
    // error during file open
    if (INVALID_HANDLE_VALUE == file)
    {
        hFile = INVALID_HANDLE_VALUE;
        
        return getwinerror();
    }
    // file opened succesfully
    hFile = file;
    
    // reading the file length
    if (!GetFileSizeEx(hFile, &lFileSize))
    {
        return getwinerror();
    }
    
    // verifying if it's a block file
    if (lFileSize.QuadPart % rm_block_size != 0)
    {
        lFileSize.QuadPart = 0;
        return errNotBlockFile;
    }
    
    return errNone;
}

rm_error blockfile_win::close()
{
    if (INVALID_HANDLE_VALUE != hFile)
    {
//        std::lock_guard<std::mutex> lk(lock);
        if (!CloseHandle(hFile))
        {
            return getwinerror();
        }
        
        hFile = INVALID_HANDLE_VALUE;
    }
    
    return errNone;
}

rm_error blockfile_win::readblock(const blockid_t blockid, block_t &block)
{
    DWORD bytesread = 0;
    DWORD res = 0;
    BOOLEAN bres = FALSE;
    file_offset_t offset = 0;
    
    if (blockid > rm_block_number_max)
    {
        // block id overflow detected
        return errBlockNumberTooBig;
    }
    
//    std::lock_guard<std::mutex> lk(lock);
    if (INVALID_HANDLE_VALUE == hFile)
    {
        // the file was not opened
        return errFileNotOpened;
    }
    
    offset = blockid << rm_block_offset_bit_nr;
    
    if (lFileSize.QuadPart < offset + rm_block_size)
    {
        // the block is not in the file
        return errBlockNotInFile;
    }
    
    // seeking to the block from the start of file
    res = SetFilePointer(this->hFile,
        offset,
        0,
        FILE_BEGIN);
    if (INVALID_SET_FILE_POINTER == res)
    {
        return errInvalidSeek;
    }
    
    SetLastError(0);
    
    // reading it's content
    bres = ReadFile(this->hFile,
        &block.buffer[0],       // lpBuffer
        sizeof(block.buffer),   // nNumberOfBytesToRead
        &bytesread,             // lpNumberOfBytesRead
        0);                     // lpOverlapped
    if (!bres)
    {
        return getwinerror();
    }
    
    block.blockid = blockid;
    
    if (bytesread != rm_block_size)
    {
        return errReadFault;
    }
    
    return errNone;
}

rm_error blockfile_win::readblockofrecord(const recordid_t recordid, block_t &block)
{
    // converting the recordid to blockid then using the readblock function
    return readblock(rm_getblockid(recordid), block);
}

rm_error blockfile_win::writeblock(const block_t &block)
{
    DWORD byteswritten = 0;
    DWORD res = 0;
    BOOLEAN bres = FALSE;
    file_offset_t offset = 0;
    
    if (block.blockid > rm_block_number_max)
    {
        // block id overflow detected
        return errBlockNumberTooBig;
    }
    
//    std::lock_guard<std::mutex> lk(lock);
    if (INVALID_HANDLE_VALUE == hFile)
    {
        // the file was not opened
//        LeaveCriticalSection(&lock);
        return errFileNotOpened;
    }
    
    offset = block.blockid << rm_block_offset_bit_nr;
    
    if ((lFileSize.QuadPart - rm_block_size) < offset)
    {
        // the block is not in the file
        return errBlockNotInFile;
    }
    
    // seeking to the block from the start of file
    res = SetFilePointer(hFile,
        offset,
        0,
        FILE_BEGIN);
    if (INVALID_SET_FILE_POINTER == res)
    {
        return errInvalidSeek;
    }
    
    // writing the block to the disk
    bres = WriteFile(hFile,
        block.buffer,           // lpBuffer
        sizeof(block.buffer),   // nNumberOfBytesToWrite
        &byteswritten,          // lpNumberOfBytesWrite
        0);                     // lpOverlapped
    if (!bres)
    {
        return getwinerror();
    }
    
    if (rm_block_size != byteswritten)
    {
        return errWriteFault;
    }
    
    return errNone;
}

rm_error blockfile_win::newblock(block_t &block)
{
//    LARGE_INTEGER new_size;
    LARGE_INTEGER tmp;
    DWORD res;
    DWORD byteswritten;
    
//    new_size.QuadPart = 0;
    tmp.QuadPart = 0;
    res = 0;
    byteswritten = 0;
    
//    std::lock_guard<std::mutex> lk(lock);
    
    if (INVALID_HANDLE_VALUE == hFile)
    {
        // the file was not opened
        return errFileNotOpened;
    }
    
    if (lFileSize.QuadPart + rm_block_size > rm_block_number_mask)
    {
        return errFileTooLong;
    }
    
    // zeroing the block's content
    memset(&block.buffer[0], 0, sizeof(block.buffer));
    
    // seeking to the end of file
    res = SetFilePointerEx(hFile,
        tmp,
        0,
        FILE_END);
    if (INVALID_SET_FILE_POINTER == res)
    {
        return errInvalidSeek;
    }
    
    // writing the block to the disk
    res = WriteFile(hFile,
        &block.buffer[0],       // lpBuffer
        sizeof(block.buffer),   // nNumberOfBytesToWrite
        &byteswritten,          // lpNumberOfBytesWrite
        0);                     // lpOverlapped
    if (!res)
    {
        return getwinerror();
    }
    
    if (rm_block_size != byteswritten)
    {
        return errWriteFault;
    }
    
    block.blockid = lFileSize.LowPart >> rm_block_offset_bit_nr;
    lFileSize.QuadPart += rm_block_size;
    
    return errNone;
}

block_number_t blockfile_win::numerofblocks()
{
    return (DWORD)(lFileSize.QuadPart / rm_block_size);
}

#endif