#include "table.h"
#include "table_types.h"
#include "ansi/blockfile_ansi.h"
#include "table_internal.hpp"
#include <cstdlib>
#include <cstdio>
#include "rm_types.h"
#include <string.h>
#include <assert.h>
#include <boost/algorithm/string.hpp>

using std::vector;
using std::string;

table_t::table_t(const string &tablename, catalog& cat) : size_of_row(cat.rowlength() + sizeof(BYTE))
{
    this->tablename = tablename;
    this->cat = new catalog(cat);
    this->file = new blockfile_ansi();
    this->opened = false;
    this->cache.blockid = invalid_blockid;  // pre set to an invalid blockid

    if (tablename.length() > max_table_name_length)
    {
        throw new table_exception(std::string("Table name too long: ") + tablename);
    }

    assert(cat.rowlength() != 0);

    // calculating the maximum row count in a block
    this->max_row_count_in_a_block =  rm_block_size / size_of_row;
}

table_t::table_t(const std::string &tablename)
{
    this->tablename = tablename;
    this->cat = new catalog();
    this->file = new blockfile_ansi();
    this->opened = false;
    this->cache.blockid = invalid_blockid;

    if (tablename.length() > max_table_name_length)
    {
        throw new table_exception(std::string("Table name too long: ") + tablename);
    }

    size_of_row = 0;
    this->max_row_count_in_a_block = 0;
}

table_t::table_t(const table_t& copy_from) : size_of_row(copy_from.size_of_row)
{
    this->tablename = copy_from.tablename;
    this->cat = copy_from.cat;
    this->file = copy_from.file;
    this->opened = copy_from.opened;

    //this->size_of_row = this->cat->rowlength();
    this->max_row_count_in_a_block = rm_block_size / size_of_row;
}

table_t::~table_t()
{
    delete file;
    delete cat;
}

bool table_t::create()
{
    std::string tn = tablename + ".tbl";

    if (opened)
    {
        assert(false);
        return false;
    }

    opened = false;
    if (rm_success(this->file->open(tn)))
    {
        this->file->close();
        assert(false);
        return false;
    }

    if (!rm_success(this->file->create(tn)))
    {
        assert(false);
        return false;
    }

    block_t b;
    file->newblock(b);
    this->file->writeblock(b);

    for (int i=0; i<4; i++)
    {
        file->newblock(this->free_space[i]);
		if (0 == i)
		{
			free_space[i].buffer[0] = 0xF8;	// marking the first 5 pages as occupied
		}
        //this->file->writeblock(this->free_space[i]);
    }

    this->file->writeblock(this->free_space[0]);
    this->file->newblock(b); // creating the first empty data block

    opened = true;

    return true;
}

bool table_t::open()
{
    rm_error err = errNone;
    std::string tn = tablename + ".tbl";

    if (opened)
    {
        return false;
    }

    if (!rm_success(this->file->open(tn)))
    {
        return false;
    }

    // reading the catalog from the file
    err = file->readblock(0 /*the first block is for the catalog*/,
                          cat->block);
    if (!rm_success(err))
    {
        return false;
    }

    // converting the binary representation of the catalog into in memory representation
    if (!cat->readfrombinary())
    {
        // invalid catalog
        return false;
    }

    size_of_row = cat->rowlength() + 1;
    this->max_row_count_in_a_block = rm_block_size / size_of_row;

    for (int i=0; i<4; i++)
    {
        this->file->readblock(i+1, this->free_space[i]);
    }

    opened = true;

    return true;
}

bool table_t::close()
{
    if (!opened)
    {
        return false;
    }

    // befor closing the database we must save the free_space table
    for (int i=0; i<free_space_block_number; i++)
    {
        file->writeblock(free_space[i]);
    }

    if (!rm_success(file->close()))
    {
        return false;
    }

    opened = false;
    return true;
}

catalog& table_t::getcatalog()
{
    return *this->cat;
}

bool table_t::writecatalog()
{
    this->cat->tobinary();
    this->size_of_row = this->cat->rowlength() + 1;
    this->max_row_count_in_a_block = rm_block_size / size_of_row;
    return rm_success(this->file->writeblock(this->cat->block));
}

string table_t::name() const
{
    return this->tablename;
}

vector<pcolumn_t>& table_t::columns()
{
    return this->cat->_columns;
}

recordid_t table_t::first_row_offs()
{
    // returns the fifth block's starting address
    return sizeof(rm_block_size)*free_space_block_number;
}

recordid_t table_t::firstempty()
{
//	recordid_t currpos = invalid_recordid;
	int bidx = 0;	// bit index
	block_number_t block_index = 0;
	bool found = false;
	row_iterator rowit;
	DWORD nr_blocks = 0;

	// verifying the pages where is an empty slot
	for (int free_space_idx=0; (free_space_idx < free_space_block_number) && (!found); free_space_idx++)
	{
		for (recordid_t pb=0; pb < rm_block_size; pb++)
		{
			BYTE v = free_space[free_space_idx].buffer[pb];

			// verifying if all the bits are set in the bitmap
			if (v != 0xFF)
			{
				// we can have a free space in these pages

				// verifying the high part of the byte
				if (0 == ((v >> 4) & 8))
				{
					found = true;
					bidx = 0;
				}
				else if (0 == ((v >> 4) & 4))
				{
					found = true;
					bidx = 1;
				}
				else if (0 == ((v >> 4) & 2))
				{
					found = true;
					bidx = 2;
				}
				else if (0 == ((v >> 4) & 1))
				{
					found = true;
					bidx = 3;
				}

				// the low part of the byte
				else if (0 == ((v & 0xF) & 8))
				{
					found = true;
					bidx = 4;
				}
				else if (0 == ((v  & 0xF) & 4))
				{
					found = true;
					bidx = 5;
				}
				else if (0 == ((v  & 0xF) & 2))
				{
					found = true;
					bidx = 6;
				}
				else if (0 == ((v  & 0xF) & 1))
				{
					found = true;
					bidx = 7;
				}
			}

			if (found)
			{
				block_index = (free_space_idx * rm_block_size) + (pb * 8) + bidx;
				break;
			}
		}
	}

    nr_blocks = file->numerofblocks();

	if (found && (block_index >= nr_blocks))
	{
		block_t b;
		rm_error err;

		// there was no free space in the file
		assert(block_index == nr_blocks);

		err = file->newblock(b);
		if (!rm_success(err))
		{
			assert(false);
			return invalid_recordid;
		}

		return rm_blockid_to_recordid(b.blockid);
	}

	// the block what contains a free space was found; we should search the
	// free space in this block

	block_t b;

	// getting the content of the block
	if ( !rm_success(file->readblock(block_index, b)) )
    {
        // the block can not be read
        assert(false);
        return invalid_recordid;
    }

    rowit.initialize(b.buffer, size_of_row, max_row_count_in_a_block);

    while(rowit.GotoNext())
    {
        if (rowit.IsEmpty())
        {
            return rm_blockid_to_recordid(b.blockid) + size_of_row*rowit.Index();
        }
    }

	return invalid_recordid;
}

recordid_t table_t::insert_row(PBYTE row)
{
	recordid_t r = firstempty();
	recordid_t rid_of_block = invalid_recordid;
	recordid_t offset = 0;
	block_t block;
	row_iterator it;
	bool has_empty = false;

	assert(row != nullptr);

	if (invalid_recordid == r)
	{
	    assert(false);
		// No free space found
		return invalid_recordid;
	}

	if (!rm_success(file->readblockofrecord(r, block)))
	{
		assert(false);
		return invalid_recordid;
	}

	rid_of_block = rm_blockid_to_recordid(block.blockid);
	assert(rid_of_block <= r);
	assert(r - rid_of_block < rm_block_size);

	offset = r - rid_of_block;

	if (block.blockid == cache.blockid)
    {
        cache.blockid = invalid_blockid;    // invalidating the block id, because the block was modified
    }

    // marking as occupied
    block.buffer[offset] = 1;
	memcpy(&block.buffer[offset+1], row, size_of_row-1);
	if (!rm_success(file->writeblock(block)))
	{
		assert(false);
		return invalid_recordid;
	}

    it.initialize(block.buffer, size_of_row, max_row_count_in_a_block);

    while (it.GotoNext())
    {
        if (it.IsEmpty())
        {
            has_empty = true;
            break;
        }
    }

    /*if (!rm_success(file->writeblock(block)))
    {
        assert(false);
        return invalid_recordid;
    }*/

	if (!has_empty)
    {
        // this block is full, we have to mark it
        set_block_as_full(block.blockid);
    }

	return r;
}

bool table_t::update_row(recordid_t recordid, PBYTE row)
{
	block_t block;
	recordid_t rid;

	assert (row != nullptr);

	if (!rm_success(file->readblockofrecord(recordid,block)))
	{
		assert(false);
		return false;
	}

	rid = rm_blockid_to_recordid(block.blockid);

	if (block.blockid == cache.blockid)
    {
        cache.blockid = invalid_blockid;
    }

	assert(rid <= recordid);
	assert(recordid - rid < rm_block_size);

    // updating the row in the block
	memcpy(&block.buffer[recordid - rid + 1], row, size_of_row-1);

	//
	if (!rm_success(file->writeblock(block)))
	{
		assert(false);
		return false;
	}

	return true;
}

bool table_t::delete_row(recordid_t row)
{
    block_t block;
    recordid_t offs;

    // reading the block of the row
    if (!rm_success(file->readblockofrecord(row, block)))
    {
        assert(false);
        return false;
    }

    offs = row - rm_blockid_to_recordid(block.blockid);
    assert(block.buffer[offs] == 1); // the block was already deleted

    // marking row as deleted
    block.buffer[offs] = 0;

    // saving the block to the disk
    if (!rm_success(file->writeblock(block)))
    {
        assert(false);
        return false;
    }

    // marking the block as not full
	this->set_block_as_not_full(block.blockid);

	return true;
}

void table_t::set_block_as_full(blockid_t id)
{
    recordid_t rid = (id/8)%rm_block_size;
    blockid_t bid = rid/rm_block_size;
    BYTE mask = 1<<(7-(id%8));

    //applying the bit
    free_space[bid].buffer[rid] |= mask;
    file->writeblock(free_space[bid]);
}

void table_t::set_block_as_not_full(blockid_t id)
{
    recordid_t rid = (id/8)%rm_block_size;
    blockid_t bid = rid/rm_block_size;
    BYTE mask = 1<<(7-(id%8));

    // removing the bit
    free_space[bid].buffer[rid] &= ~mask;
    file->writeblock(free_space[bid]);
}

iterator_t* table_t::get_iterator()
{
    return new table_iterator(this);
}

bool table_t::read_row(recordid_t recordid, PBYTE row)
{
    blockid_t bid = rm_getblockid(recordid);
    recordid_t offs;

    assert(file != nullptr);
    assert(row != nullptr);

    if (bid != cache.blockid)
    {
        // reading the block of the recordid into the cache
        if (!rm_success(file->readblockofrecord(recordid, cache)))
        {
                return false;
        }
    }

    offs = rm_blockoffset(recordid);

    memcpy(row, &cache.buffer[offs + 1], this->size_of_row - 1);

    return true;
}

column_t* table_t::operator[](std::string columnname)
{
    for (column_t* c: cat->columns())
    {
        if (boost::iequals(c->name, columnname))
        {
            return c;
        }
    }

    return nullptr;
}

column_t* table_t::column(std::string columnname)
{
    return this->operator[](columnname);
}
