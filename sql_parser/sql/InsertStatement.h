#ifndef __INSERT_STATEMENT_H__
#define __INSERT_STATEMENT_H__

#include "SQLStatement.h"
#include "SelectStatement.h"
#include "CreateStatement.h"

namespace hsql {
    /**
     * Represents SQL Insert statements.
     * Example: "INSERT INTO students VALUES ('Max', 1112233, 'Musterhausen', 2.3)"
     */
    struct InsertStatement : SQLStatement {
        enum InsertType {
            kInsertValues,
            kInsertSelect
        };

        InsertStatement(InsertType type) :
            SQLStatement(kStmtInsert),
            type(type),
            tableName(""),
            columns(),
            values(),
            select() {}

        InsertStatement(InsertStatement& from):
            SQLStatement(kStmtInsert),
            type(from.type),
            tableName(from.tableName),
            columns(nullptr),
            values(nullptr),
            select(from.select)
        {
            if (from.columns)
            {
                // should create a separate copy columns function
                this->columns = new std::vector<std::string*>();

                for (auto i : *from.columns)
                {
                    this->columns->push_back(new std::string(*i));
                }
            }

            if (from.values)
            {
                this->values = new std::vector<hsql::Expr*>();

                for (auto i : *from.values)
                {
                    this->values->push_back(new hsql::Expr(*i));
                }
            }
        }

        virtual ~InsertStatement() {
            if (columns)
            {
                for (auto col : *columns)
                {
                    delete col;
                }
                delete columns;
            }

            delete values;
            if (select)
            {
                delete select;
            }
        }

        InsertType type;
        std::string tableName;
        std::vector<std::string*>* columns;
        std::vector<Expr*>* values;
        SelectStatement* select;
    };

} // namsepace hsql
#endif