#ifndef __CREATE_STATEMENT_H__
#define __CREATE_STATEMENT_H__

#include "SQLStatement.h"

namespace hsql {
    struct KeyConstraints {
        bool foreignkey;
        bool primarykey;
        std::string table;
        std::string column;

        KeyConstraints() :
            foreignkey(false),
            primarykey(false),
            table(""),
            column("") {}

        KeyConstraints(KeyConstraints& from) :
            foreignkey(from.foreignkey),
            primarykey(from.primarykey),
            table(from.table),
            column(from.column) {}
    };

    /**
     * Represents definition of a table column
     */
    struct ColumnDefinition {
        enum DataType {
            TEXT,
            INT,
            DOUBLE,
            VARCHAR
        };

        std::string   name;
        DataType      type;
        size_t        length;
        KeyConstraints* keyconstraints;

        ColumnDefinition(ColumnDefinition& from) :
            name(from.name),
            type(from.type),
            length(from.length),
            keyconstraints(new KeyConstraints(*from.keyconstraints)) {
        }

        ColumnDefinition(char* name = nullptr, DataType type = INT, size_t length = 0, KeyConstraints* constraints = nullptr) :
            name(name),
            type(type),
            length(length),
            keyconstraints(constraints) {}

        virtual ~ColumnDefinition() {
            if (keyconstraints)
                delete keyconstraints;
        }
    };

    /**
     * Represents SQL Create statements.
     * Example: "CREATE TABLE students (name TEXT, student_number INTEGER, city TEXT, grade DOUBLE)"
     */
    struct CreateStatement : SQLStatement {
        enum CreateType {
            kTable,
            kTableFromTbl // Hyrise file format
        };

        CreateStatement(CreateType type) :
            SQLStatement(kStmtCreate),
            type(type),
            ifNotExists(false),
            filePath(""),
            tableName(""),
            columns(NULL) {};

        CreateStatement(CreateStatement& stmt) :
            SQLStatement(kStmtCreate),
            type(stmt.type),
            ifNotExists(stmt.ifNotExists),
            filePath(stmt.filePath),
            tableName(stmt.tableName)
            {
                if (stmt.columns)
                {
                    // should create a separate copy columns function
                    this->columns = new std::vector<ColumnDefinition*>();
                    
                    for (auto i = stmt.columns->begin(); i < stmt.columns->end(); i++)
                    {
                        this->columns->push_back(new ColumnDefinition(**i));
                    }
                }
                else
                {
                    this->columns = nullptr;
                }
                
            };

        virtual ~CreateStatement() {
            if (columns)
            {
                for (auto i = columns->begin(); i < columns->end(); i++)
                {
                    delete *i;
                }

                delete columns;
            }
        }

        CreateType type;

        bool ifNotExists;
        std::string filePath;
        std::string tableName;
        std::vector<ColumnDefinition*>* columns;
    };

} // namespace hsql
#endif